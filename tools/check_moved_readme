Here is a script that I found useful when importing patchsets extracted
from a CVS archive with cvsps into a tree that uses explicit tags.

It first obtains the list of added and deleted files from 'tla
tree-lint' and then for each deleted file checks if the copy in the
pristine tree matches up with any of the added files. If more than 60%
of the lines match, the added file is considered a rename candidate. If
several files match, the closest match is used.

It then uses tla move-tag/add-tag/delete-tag' to make sure that the
explicit tags are correct for the new tree. It needs to be run with
'-f' as argument before it modifies anything though.

My usage is something like,

    patch -p1 < ../patch-xxx
    check_moved.py -f
    find . -empty
    tla commit -L 'applied patch-xxx'

Some things I don't like all that much, but happen to work right most of
the time,

- I have to somehow find the current pristine tree, and for this I'm
  using "tla find-pristine `tla ls-pristines`". This breaks as soon as
  we have multiple pristines, or when the tree doesn't match our actual
  revision. There is a 'tree-version' that comes close, but I really was
  looking for 'tla tree-revision', or have 'tla find-pristine' default
  to returning the path to the pristine tree that matches our current
  working directory, or an error when it doesn't exist.

- Getting the list of added/deleted files out of the tree-lint output.
  First of all I'm assuming that all filenames always start with './'
  and that any name that matches '**/.arch-ids/*.id' corresponds to a
  deleted file.

(btw. I think my python version is 2.1 or 2.2, but except for possibly
the availability of difflib it should work with pretty much any version).

Jan

