#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# arch-tag: simple merge test with precommit hook that fails
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

srcdir=$(cd ${srcdir} && pwd)

. ${srcdir}/test-framework

PQM_CONFIG_FILE="${srcdir}/pqm-tests-3.conf"
initial_setup_with_simple_submission
run_queue_xfail
verbose 'checking test failure'
grep 'Status: failure' ${workdir}/pqm.output 1>/dev/null

endverbose

clean_workdir
