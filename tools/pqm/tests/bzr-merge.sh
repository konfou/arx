#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Copyright © 2005 Canonical Limited
#   Authors: Robert Collins <robert@canonical.com>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

srcdir=$(cd ${srcdir} && pwd)

. ${srcdir}/test-framework

PQM_CONFIG_FILE="${srcdir}/pqm-tests-2.conf"
initial_bzr_setup_with_simple_submission
run_queue
bzr_archive_has_revision_with_summary "hello-world/mainline/1.0" 2 'minor rename'

clean_workdir
