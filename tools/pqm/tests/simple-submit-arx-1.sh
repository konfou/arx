#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# Copyright � 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

export TLA=arx
srcdir=$(cd ${srcdir} && pwd)
. ${srcdir}/test-framework
initial_setup_with_simple_submission
run_queue
archive_has_revision_with_summary support@example.com--2003 hello-world--mainline--1.0,1 'minor rename'

clean_workdir
