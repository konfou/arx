#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# arch-tag: signed simple commit test
# Copyright © 2003 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

srcdir=$(cd ${srcdir} && pwd)

. ${srcdir}/test-framework

initial_setup

identity_user1
cd ${WORKDIR}/hello-world
$TLA mv hello-world.c hello_world.c
$TLA commit -s 'rename hello-world => hello_world.c'
echo 'star-merge user1@example.com--2003/hello-world--mainline support@example.com--2003/hello-world--mainline--1.0' | submit_merge 'minor rename' "verify"

clean_workdir
