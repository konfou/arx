#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# arch-tag: signed simple patch submission test
# Copyright © 2004 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

srcdir=$(cd ${srcdir} && pwd)

. ${srcdir}/test-framework

initial_setup
submit_simple_patch
clean_workdir
