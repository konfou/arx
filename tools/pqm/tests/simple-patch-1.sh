#!/bin/sh
# -*- mode: sh; coding: utf-8 -*-
# arch-tag: simple merge test with patch
# Copyright © 2004 Colin Walters <walters@verbum.org>
#
# See the file "COPYING" for further information about
# the copyright and warranty status of this work.

set -e

srcdir=$(cd ${srcdir} && pwd)

. ${srcdir}/test-framework

PQM_CONFIG_FILE="${srcdir}/pqm-tests.conf"
initial_setup_with_patch_submission
run_queue
archive_has_revision_with_summary support@example.com--2003 hello-world--mainline--1.0--patch-1 'fix punctuation'

endverbose

clean_workdir
