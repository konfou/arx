
from twisted.trial import unittest
import os
import shutil
import logging
from pqm.tests.test_pqm import QueueSetup

class TestTwistedUI(unittest.TestCase):

    def testImports(self):
        import pqm.ui.twistd

    def testQueue(self):
        import pqm
        queueSetup = QueueSetup()
        queueSetup.setUp()
        try:
            configp = pqm.ConfigParser()
            configp.read([queueSetup.configFileName])
            queuedir = pqm.get_queuedir(configp, logging, [])
            patches =  pqm.find_patches(queuedir, logging, None)
            self.assertEqual(1, len(patches))
            self.assertEqual(patches[0].filename, queueSetup.messageFileName)
        finally:
            queueSetup.tearDown()
