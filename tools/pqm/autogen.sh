#!/bin/sh
# arch-tag: script to rerun autotools
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="arch-pqm"

(test -f $srcdir/configure.ac \
  && test -f $srcdir/sample-arch-pqm.conf \
  && test -f $srcdir/bin/arch-pqm) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

DIE=0

have_autoconf=false
if autoconf --version < /dev/null > /dev/null 2>&1 ; then
	autoconf_version=`autoconf --version | sed 's/^[^0-9]*\([0-9.][0-9.]*\).*/\1/'`
	case $autoconf_version in
	    2.5*)
		have_autoconf=true
		;;
	esac
fi
if $have_autoconf ; then : ; else
	echo
	echo "You must have autoconf installed to compile $PROJECT."
	echo "libtool the appropriate package for your distribution,"
	echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
	DIE=1
fi

if automake-1.7 --version < /dev/null > /dev/null 2>&1; then
  AUTOMAKE=automake-1.7
  ACLOCAL=aclocal-1.7
elif automake-1.8 --version < /dev/null > /dev/null 2>&1; then
  AUTOMAKE=automake-1.8
  ACLOCAL=aclocal-1.8
else
	echo
	echo "You must have automake >= 1.7 installed to compile $PROJECT."
	echo "Get ftp://ftp.gnu.org/pub/gnu/automake/automake-1.7.2.tar.gz"
	echo "(or a newer version if it is available)"
	DIE=1
fi

if test "$DIE" -eq 1; then
	exit 1
fi

$ACLOCAL
$AUTOMAKE -a --foreign
autoconf

if test -z "$AUTOGEN_SUBDIR_MODE"; then
        $srcdir/configure --enable-maintainer-mode --enable-debug "$@"
        echo 
        echo "Now type 'make' to compile $PKG_NAME."
fi
