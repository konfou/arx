;; Find out what patches are missing from the current tree relative to
;; another revision.

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California
;; Copyright (C) 2004 Walter Landry

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-missing-error
     'error-conditions
     '(error arx-error arx-missing-error))

(put 'arx-missing-error 'error-message "arx-missing failed")

(defun arx-missing (branch directory)
  "Find out what patches are missing from the current tree relative to
another revision."
  (interactive 
   (list (completing-read "Branch: " 'arx-complete-branches)
	 (read-file-name "Directory: "
				     default-directory default-directory)))
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer)
    (display-buffer "*arx*" t)
    (if (not (= 0 (call-process "arx" nil "*arx*" t "missing"
				"-d" (expand-file-name directory)
				branch)))
	(signal 'arx-missing-error (list branch directory)))))







