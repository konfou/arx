;; A collection of functions to make using arx easier from Emacs.

;; Copyright (C) 2002, 2003 Walter Landry, the Regents of the University
;; of California
;; Copyright (C) 2002 Mark Flacy
;; Copyright (C) 2003 Tim Barbour
;; Copyright (C) 2005 Walter Landry

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

;; Load code that is common to Emacs and XEmacs

(load-library "arx-common")

(define-minor-mode arx-mode
  "Mode to enable bindings of various arx commands to convenient
keys.  Note that it inhibits automatic backups.
\\[arx-add] Adds a file or directory to the list of files
        explicitly managed by arch
\\[arx-rm] Deletes a file or directory from the list of files
            explicitly managed by arch.
\\[arx-diff] Create a buffer with a summary of what has changed since the
        last revision.  It also creates a directory, ',,diff...',
        which has more comprehensive info.
\\[arx-commit] Commit the current changes.
\\[arx-get] get a particular revision and put it in the directory.
\\[arx-log] print a summary of the log files for a particular branch.
\\[arx-update] update a revision from another revision (not implemented)."
  nil
  " arx"
  '(("\C-xvi" . (lambda () (interactive) (arx-add (buffer-file-name))))
    ("\C-xvs" . arx-commit)
    ("\C-xv=" . arx-diff)
    ("\C-xv~" . arx-file-diff)
    ("\C-xvr" . arx-get)
    ("\C-xvl" . arx-log)
    ("\C-x4a" . arx-changelog)
    ("\C-xvt" . arx-tree-lint)
    ("\C-xva" . undefined)
    ("\C-xvb" . undefined)
    ("\C-xvc" . undefined)
    ("\C-xvd" . undefined)
    ("\C-xvg" . undefined)
    ("\C-x\C-q" . arx-edit)
    ("\C-xvm" . arx-update)
    ("\C-xvv" . undefined)
    ("\C-xvu" . arx-file-undo))
  (set (make-local-variable 'backup-inhibited) t)
  )

(defun arx-hook-find-file-hook ()
  (let ((filename (buffer-file-name)))
    (if filename
	(progn
	  (if (= 0 (call-process "arx" nil nil t "tree-root"))
	      (arx-mode 1))))))
(add-hook 'find-file-hooks 'arx-hook-find-file-hook)

(defun dired-arx-check ()
  "Check if directory being edited is under control of arx, and if so, invoke arx-mode."
  (progn
    (if (= 0 (call-process "arx" nil "*arx*" t "tree-root"))
        (arx-mode 1))))

(add-hook 'dired-mode-hook 'dired-arx-check t)
(require 'arx-menu)
