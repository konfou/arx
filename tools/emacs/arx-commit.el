;; Commit the changes to a directory.

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California
;; Copyright (C) 2004 Walter Landry

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

;; A function to actually do the commit.

(put 'arx-commit-error
     'error-conditions
     '(error arx-error arx-commit-error))

(put 'arx-commit-error 'error-message "arx-commit failed")

(defun arx-commit (message)
  "Commit the current changes."
  (interactive "MCommit message: ")
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (if (= 0 (call-process "arx" nil "*arx*" t "commit" "-s" message))
      (arx-refresh-buffers)
    (progn (display-buffer "*arx*" t)
	   (signal 'arx-commit-error nil))))


(defun arx-refresh-buffers ()
  "Refresh all of the buffers that may have been marked read-only by commit"
  (interactive)
  (setq buflist (buffer-list))
  (while buflist
    (setq buf (car buflist))
    (setq buflist (cdr buflist))
    (setq buffile (buffer-file-name buf))
    ;; Make sure the buffer is under arx-mode control and it is not modified
    (save-excursion
      (set-buffer buf)
      (if (and arx-mode
               buffile
               (not (buffer-modified-p buf))
               (file-readable-p buffile)
               (not (file-writable-p buffile)))
          (revert-buffer t t)))))

