;; Returns the completion for a revision name

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(defun arx-complete-revisions (partial-name predicate flag)
  "Returns the completion for a revision name.  If the
{category,branch,version} name is not complete or unique, it only
looks up {category,branch,version} names.  If the version name is
complete or unique, it looks up the revision names."
  (arx-do-completion
   partial-name
   (let* ((name-list (arx-split-names partial-name))
	  (category (car name-list))
	  (branch (concat category "--" (nth 1 name-list)))
	  (version (concat branch "--" (nth 2 name-list)))
	  (revision (concat version "--" (nth 3 name-list))))
     ;; If revision exists
     (if (= 4 (length name-list))
	 (arx-get-revisions version)
       ;; If versions exists
       (if (= 3 (length name-list))
	   ;; Check for the case where the branch and category name is
	   ;; identical
	   (if (and (arx-complete-branches category predicate 'lambda)
		    (arx-complete-versions branch predicate 'lambda))
	       (arx-get-revisions version)
	     ;; Otherwise, append the completions of only or unique
	     ;; completions of version to all possible versions
	     (append
	      (let ((version-list (arx-complete-versions version
							   predicate t)))
		(if (= 1 (length version-list))
		    (arx-get-revisions (car version-list))
		  (if (arx-complete-versions version predicate 'lambda)
		      (arx-get-revisions version))))
	      (let ((versions (arx-get-versions branch)))
		(if (not (and (= 1 (length versions))
			      (string= partial-name version)))
		    (remove partial-name versions)))))
	 ;; if branch exists
	 (if (= 2 (length name-list))
	     ;; Check for identical branch and category names
	     (append
	      (if (arx-complete-branches category predicate 'lambda)
		  (let ((version-list (arx-complete-versions branch
							       predicate t)))
		    (if version-list
			(append
			 (if (= 1 (length version-list))
			     (arx-get-revisions (car version-list))
			   (if (arx-complete-versions branch
							predicate 'lambda)
			       (arx-get-revisions branch)))
			 (let ((versions (arx-get-versions category)))
			   (if (not (and (= 1 (length versions))
					 (string= partial-name version)))
			       (remove partial-name versions)))))))
	      ;; Code for when branch and category are not identical
	      (let ((branch-list (arx-complete-branches branch predicate t)))
		(append
		 ;; Get all possible versions of the branch, and, if
		 ;; only one version, all of it's revisions.
		 (let ((version-list 
			(if (= 1 (length branch-list))
			    (arx-get-versions (car branch-list))
			  (if (arx-complete-branches branch
						       predicate 'lambda)
			      (arx-get-versions branch)))))
		   (if (= 1 (length version-list))
		       (arx-get-revisions (car version-list))
		     version-list))
		 (let ((versions (arx-complete-versions branch predicate t)))
		   (if (not (and (= 1 (length versions))
				 (string= partial-name branch)))
		       (remove partial-name versions))))))
	   ;; Only have a category
	   (let ((category-list (arx-complete-categories category
							   predicate t)))
	     (append
	      (let ((branch-list
		     (if (= 1 (length category-list))
			 (arx-get-branches (car category-list))
		       (if (arx-complete-categories category
						      predicate 'lambda)
			   (arx-get-branches category)))))
		(if (= 1 (length branch-list))
		    (let ((version-list
			   (arx-get-versions (car branch-list))))
		      (if (= 1 (length version-list))
			  (arx-get-revisions (car version-list))
			version-list))
		  branch-list))
	      (if (not (and (= 1 (length category-list))
			    (string= partial-name category)))
		  (remove partial-name category-list))))))))
   predicate flag))

		    
	

