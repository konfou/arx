;; Undo the edits on a tree and saves those differences in an undo
;; directory.

;; Copyright (C) 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-undo-error
     'error-conditions
     '(error arx-error arx-undo-error))

(put 'arx-undo-error 'error-message "arx-undo failed")

(defun arx-undo (directory)
  "Undo the edits on a tree and saves those differences in an undo directory."
  (interactive "DDirectory to undo: ")
  (cd directory)
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (if (= 0 (call-process "arx" nil "*arx*" t "undo"))
      (message "Tree reverted")
    (progn (display-buffer "*arx*" t)
	   (signal 'arx-undo-error (list filename revision)))))
