;; Create a new buffer with the branch's readme

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-branch-readme-error
     'error-conditions
     '(error arx-error arx-branch-readme-error))

(put 'arx-branch-readme-error 'error-message "arx-branch-readme failed")


(defun arx-branch-readme (branch)
  "Create a new buffer with the branch's readme"
  (interactive
   (list (completing-read "Branch name: "
			  'arx-complete-branches)))
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (if (= 0 (call-process "arx" nil "*arx*" t "browse" "--level" "0"
                         "--readmes" branch))
      (display-buffer "*arx*" t)
    (progn (display-buffer "*arx*" t)
	   (signal 'arx-branch-readme-error '(branch)))))



