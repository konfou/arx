;; Diff current file with the version in revision

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California
;; Copyright (C) 2004 Walter Landry

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-file-diff-error
     'error-conditions
     '(error arx-error arx-file-diff-error))

(put 'arx-file-diff-error 'error-message "arx-file-diff failed")

(defun arx-file-diff (filename revision)
  "Diff current file with the version in revision"
  (interactive
   (list (read-file-name "File to diff: " default-directory)
	 (completing-read "Revision to diff against: "
			  'arx-complete-revisions)))
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (let ((return-value
         (call-process "arx" nil "*arx*" t "file-diff"
                       (expand-file-name filename) revision)))
    (if (= 0 return-value)
        (message "No changes to that file")
      (if (= 1 return-value)
          (display-buffer "*arx*" t)
        (progn (display-buffer "*arx*" t)
               (signal 'arx-file-diff-error (list filename revision)))))))
