;; Code common to Emacs and XEmacs.  Loads all of the necessary
;; libraries and sets variables to defaults.

;; Copyright (C) 2002, 2003 Walter Landry, the Regents of the University
;; of California
;; Copyright (C) 2002 Mark Flacy
;; Copyright (C) 2003 Tim Barbour

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

;; User Commands
(load-library "arx-my-id")
(load-library "arx-my-default-archive")
(load-library "arx-register-archive")
(load-library "arx-unregister-archive")
(load-library "arx-whereis-archive")
(load-library "arx-archives")

;; Project Tree Commands
(load-library "arx-init")
(load-library "arx-tree-branch")

;; Project Tree Inventory Commands
(load-library "arx-inventory")

(load-library "arx-tagging-method")
(load-library "arx-tree-lint")

(load-library "arx-add")
(load-library "arx-edit")
(load-library "arx-rm")
(load-library "arx-mv")

;; Archive Commands
(load-library "arx-make-archive")

(load-library "arx-browse")

(load-library "arx-cat-archive-log")

(load-library "arx-archive-cache-revision")
(load-library "arx-archive-cached-revisions")
(load-library "arx-archive-uncache-revision")

(load-library "arx-branch-readme")

;; Patch Log Commands
(load-library "arx-commit")
(load-library "arx-log")
(load-library "arx-changelog")
(load-library "arx-history")

;; Archive Transaction Commands
(load-library "arx-get")
(load-library "arx-mirror")

;; Commands for Branching and Merging
(load-library "arx-fork")
(load-library "arx-missing")
(load-library "arx-tag")

;; Local Cache Commands
(load-library "arx-diff")
(load-library "arx-file-diff")
(load-library "arx-file-undo")
(load-library "arx-undo")
(load-library "arx-redo")

;; Web Related Commands
(load-library "arx-update-distributions")

;; Commands specific to arx-mode and not visible to the user
(load-library "arx-get-tagging-method")
(load-library "arx-get-tree-branch")
(load-library "arx-get-archives")
(load-library "arx-get-branches")
(load-library "arx-get-revisions")

(load-library "arx-complete-archives")
(load-library "arx-complete-branches")
(load-library "arx-complete-revisions")
(load-library "arx-do-completion")
(load-library "arx-strip-trailing-dash")
(load-library "arx-split-names")

;; (load-library "arx-get-all-branches")

;; Global variables for caching
(setq arx-archive-list nil)
(setq arx-branch-alist nil)
(setq arx-revision-alist nil)
