;; Set or display the default id

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-my-id-error
     'error-conditions
     '(error arx-error arx-my-id-error))

(put 'arx-my-id-error 'error-message
     "arx-my-id failed")

(defun arx-my-id (id)
  "Set or display the default id"
  (interactive "sId name: ")
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (let ((output
         (if (string= "" id)
             (if (= 0 (call-process "arx" nil "*arx*" t "param" "id"))
                 (car (save-current-buffer
                        (set-buffer "*arx*")
                        (split-string (buffer-string) "\n")))
               (progn (display-buffer "*arx*" t)
                      (signal 'arx-my-id (list id))))
           (if (= 0 (call-process "arx" nil "*arx*" t "param" "id" id))
               (car (save-current-buffer
                      (set-buffer "*arx*")
                      (split-string (buffer-string) "\n")))
             (progn (display-buffer "*arx*" t)
                    (signal 'arx-my-id (list id)))))))
    (if (interactive-p)
	(message output)
      output)))


    
