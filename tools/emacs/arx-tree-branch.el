;; Prints out the current branch of the project tree.

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-tree-branch-error
     'error-conditions
     '(error arx-error arx-tree-branch-error))

(put 'arx-tree-branch-error 'error-message
     "arx-tree-branch failed")

(defun arx-tree-branch ()
  "Prints out the current branch of the project tree."
  (interactive)
  (message (condition-case nil 
	       (arx-get-tree-branch)
	     (arx-get-tree-branch-error (signal 'arx-tree-branch-error
                                                nil)))))

