;; Create a tar file of revision with the name dist-name

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-update-distributions-error
     'error-conditions
     '(error arx-error arx-update-distributions-error))

(put 'arx-update-distributions-error 'error-message
     "arx-update-distributions failed")


(defun arx-update-distributions (revision dist-name ftp-dir)
  "Create a tar file of revision with the name dist-name"
  (interactive (list (completing-read "Revision to create: "
				      'arx-complete-revisions)
		     (read-string "Name of distribution: ")
		     (read-file-name "Directory for tar file: "
				     default-directory default-directory)))
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (if (not (= 0 (call-process "arx" nil "*arx*" t "update-distributions"
			      revision dist-name (expand-file-name ftp-dir))))
      (progn (display-buffer "*arx*" t)
	     (signal 'arx-update-distributions-error (list revision)))
    (message "Distribution %s created" dist-name)))

