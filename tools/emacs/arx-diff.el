;; Find out what has changed from the previous revision.

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-diff-error
     'error-conditions
     '(error arx-error arx-diff-error))

(put 'arx-diff-error 'error-message "arx-diff failed")

(defun arx-diff (old_revision)
  "Find out what has changed from the previous revision."
  (interactive (list (completing-read
		      "Revision name: "
		      'arx-complete-revisions)))
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (let ((return-value
	 (call-process "arx" nil "*arx*" t "diff"
                       "--uri" "--revision" old_revision)))
    (if (= 0 return-value)
	  ;; If nothing has changed
	  ;; Just print out no changes
	  (message "No changes")
	;; Otherwise, display the *arx* buffer and go to dired mode
	;; on the ,,diff directory
      (if (= 1 return-value)
	  (progn
	    (dired (save-current-buffer
		     ;; Change directory to the tree-root
		     (set-buffer "*arx*")
                     (goto-char (point-min))
                     ;; Get rid of the file:// part
                     (kill-region (progn (beginning-of-line) (point))
				  (progn (beginning-of-line)
                                         (forward-char 7)
                                         (point)))
		     (kill-region (progn (beginning-of-line) (point))
				  (progn (end-of-line) (point)))
                     (insert-file-contents (car kill-ring-yank-pointer))
                     (file-name-directory (car kill-ring-yank-pointer))))
	    (revert-buffer)
	    (arx-diff-mode)
	    (arx-mode)
	    (display-buffer "*arx*" t)
	    ;; I have to rebuild the subdirectory alist because it gets
	    ;; confused after getting into arx-diff-mode.
	    (dired-build-subdir-alist)
)
	(progn (display-buffer "*arx*" t)
	       (signal 'arx-diff-error nil))))))


(define-derived-mode arx-diff-mode  dired-mode "arx-diff"
  "Major Mode for browsing diff directories
\\[arx-diff-delete-dired] quit the mode and delete the ,,diff directory.")

(define-key arx-diff-mode-map "q" 'arx-diff-delete-dired)

(defun arx-diff-delete-dired ()
  "Deletes the ,,diff.* directories and kills the buffer"
  (interactive)
  (call-process "rm" nil "*arx*" t "-rf" (expand-file-name dired-directory))
  (kill-buffer (current-buffer))
)


