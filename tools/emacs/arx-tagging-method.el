;; Set or get the tagging mathod for this tree.

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-tagging-method-error
     'error-conditions
     '(error arx-error arx-tagging-method-error))

(put 'arx-tagging-method-error 'error-message
     "arx-tagging-method failed")

(defun arx-tagging-method (tag-method directory)
  "Set the tagging mathod for this tree.  If there is no argument, it
returns what the current tagging method is."
  (interactive "sTagging method: \nDDirectory: ")
  (if (string= "" tag-method)
      (let ((tag-method
	     (condition-case nil (arx-get-tagging-method directory)
	       (arx-get-tagging-method-error
		(signal 'arx-tagging-method-error (list directory))))))
	(if (interactive-p)
	    (message tag-method)
	  tag-method))
    (save-current-buffer
      (set-buffer (get-buffer-create "*arx*"))
      (erase-buffer)
      (cd directory)
      (if (not (= 0 (call-process "arx" nil "*arx*" t "tagging-method"
				  tag-method)))
	  (progn (display-buffer "*arx*" t)
		 (signal 'arx-tagging-method-error
			 (list tag-method directory)))))))


