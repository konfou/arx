;; Make a file editable

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California
;; Copyright (C) 2004 Walter Landry

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-edit-error
     'error-conditions
     '(error arx-error arx-edit-error))

(put 'arx-edit-error 'error-message "arx-edit failed")

(defun arx-edit (filename)
  "Make a file editable"
  (interactive "fEdit file: ")
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (if (not (= 0 (call-process "arx" nil "*arx*" t "edit"
			      (expand-file-name filename))))
      (progn (display-buffer "*arx*" t)
	     (signal 'arx-edit-error (list filename)))
    (if (equal (buffer-file-name) (expand-file-name filename))
        (revert-buffer t t))))

