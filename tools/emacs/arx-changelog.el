;; Create a new buffer with the contents of the changelog for the
;; indicated revision

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California
;; Copyright (C) 2005 Walter Landry

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-changelog-error
     'error-conditions
     '(error arx-error arx-changelog-error))

(put 'arx-changelog-error 'error-message "arx-changelog failed")


(defun arx-changelog (directory)
  "Create a new buffer with the output 'log --formatted (branch)'"
  (interactive "DDirectory: ")
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (if (= 0 (call-process "arx" nil "*arx*" t "log" "--formatted"
                         "--dir" (expand-file-name directory)))
      (progn
        (pop-to-buffer "*arx*" t)
        (goto-line 1)
        (change-log-mode))
    (progn (display-buffer "*arx*" t)
	   (signal 'arx-changelog-error (list branch)))))



