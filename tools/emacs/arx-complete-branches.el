;; Returns the completion for a branch name

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(defun arx-complete-branches (partial-name predicate flag)
  "Returns the completion for a branch name.  It looks up category
names.  It looks up branch names if partial-name can be identified
with a unique category (because there is only one completion, or it is
an exact match).  It then removes that category from the possible
completion list."
  (arx-do-completion
   partial-name 
   ;; Get the branch associated with this category
   (let ((stripped-name (car (arx-split-names partial-name))))
     (append
      (let ((category-list (arx-complete-categories stripped-name
						      predicate t)))
	(if (= 1 (length category-list))
	    (arx-get-branches (car category-list))
	  (if (arx-complete-categories stripped-name predicate 'lambda)
	      (arx-get-branches stripped-name))))
      ;; Get all possible category completions, but remove a category
      ;; if it is already being completed above.
      (let ((categories (arx-complete-categories partial-name predicate t)))
	(if (not (and (= 1 (length categories))
		      (string= partial-name stripped-name)))
	    (remove partial-name categories)))))
   predicate flag))
