;; Set or display the default archive

;; Copyright (C) 2002, 2003 Walter Landry and the Regents of the University
;; of California

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

(put 'arx-my-default-archive-error
     'error-conditions
     '(error arx-error arx-my-default-archive-error))

(put 'arx-my-default-archive-error 'error-message
     "arx-my-default-archive failed")

(defun arx-my-default-archive (archive)
  "Set or display the default archive"
  (interactive (list (completing-read "Archive name: "
				      'arx-complete-archives)))
  (save-current-buffer
    (set-buffer (get-buffer-create "*arx*"))
    (erase-buffer))
  (let ((output
         (if (string= "" archive)
             (if (= 0 (call-process "arx" nil "*arx*" t "param"
                                    "default-archive"))
                 (car (save-current-buffer
                        (set-buffer "*arx*")
                        (split-string (buffer-string) "\n")))
               (progn (display-buffer "*arx*" t)
                      (signal 'arx-my-default-archive (list archive))))
           (if (= 0 (call-process "arx" nil "*arx*" t "param"
                                  "default-archive" archive))
               (car (save-current-buffer
                      (set-buffer "*arx*")
                      (split-string (buffer-string) "\n")))
             (progn (display-buffer "*arx*" t)
                    (signal 'arx-my-default-archive (list archive)))))))
           
    ;; Clear up the category, branch, revision caches if using a new
    ;; archive.
    (if (not (string= "" archive))
	(progn
	  (setq arx-category-list nil)
	  (setq arx-branch-alist nil)
	  (setq arx-revision-alist nil)))
    (if (interactive-p)
	(message output)
      output)))


    
