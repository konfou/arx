;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Emacs-lisp -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Filename        : arx-menu.el
;; Description     : Add menu support to arx-mode.  Not tested under XEmacs
;; 
;; 
;; Author          : Mark A. Flacy
;; Created On      : Sat May 18 00:54:50 2002
;; Last Modified By: Walter Landry
;; Last Modified On: July 10, 2005
;; Update Count    : 20
;; 
;; 
;; HISTORY
;; 19-May-2002		Mark A. Flacy	
;;    Last Modified: Sun May 19 17:46:56 2002 #17 (Mark A. Flacy)
;;    Added sub-menus for arx-mode and new menus for arx-log-mode and
;;    arx-branch-log-mode. 
;; 18-May-2002		Mark A. Flacy	
;;    First version
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;
;;; Primary arx mode menu support
;;;

(defvar arx-user-menu
  '("User commands"
    ["Set/show default archive" arx-my-default-archive t]
    ["Set/show your arx id" arx-my-id t]
    )
  "ArX user menu")

(defvar arx-project-tree-menu
  '("Project tree commands"
    ["Show tree branch" arx-tree-branch t]
    )
  "ArX project tree menu")

(defvar arx-project-tree-inventory-menu
  '("Tree inventory commands"
    ["Show inventory report" arx-inventory t]
    ["Show/set tagging method" arx-tagging-method t]
    ["Run tree lint" arx-tree-lint t]
    )
  "arx Project Tree Inventory Commands")

(defvar arx-archive-menu
  '("Archive commands"
    ["Browse" arx-browse t]
    ["Cat archive log" arx-cat-archive-log t]
    ["Cache revision" arx-archive-cache-revision t]
    ["Show cached revisions" arx-archive-cached-revisions t]
    ["Uncache revision" arx-archive-uncache-revision t]
    )
  "arx archive menu")

(defvar arx-patch-menu
  '("Patch commands"
    ["Commit" arx-commit t] ;; yes, this is a duplicate.
    ["Add log" arx-add-log t]
    ["Log ls summary" arx-log-ls-summary t]
    ["Changelog" arx-changelog t]
    )
  "arx patch commands")

(defvar arx-transaction-menu
  '("Transactions"
    ["Get archive" arx-get t]
    )
  "arx transaction menu")

(defvar arx-branching-merging-menu
  '("Branching/merging"
    ["Fork" arx-fork t]
    ["Missing" arx-missing t]
    ["Tag" arx-tag t]
    )
  "arx branching and merging menu")

(defvar arx-cache-menu
  '("Local cache commands"
    ["Show changes of entire tree" arx-diff t]
    ["Show diff for one file" arx-file-diffs t]
    ["Undo edit of one file" arx-file-undo t]
    )
  "arx local cache commands")

(defvar arx-web-menu
  '("Web commands"
    ["Update distribution" arx-update-distributions t]
    )
  "arx web related commands")

(defvar arx-mode-menu
  (list "ArX"
    ["Add file to arx version control" arx-add t]
    ["Make a file editable" arx-edit t]
    ["Remove file from arx version control" arx-rm t]
    ["Move file" arx-mv t]
    ["Commit" arx-commit t]
    "---"
    arx-user-menu
    arx-project-tree-menu
    arx-project-tree-inventory-menu
    arx-archive-menu
    arx-patch-menu
    arx-transaction-menu
    arx-branching-merging-menu
    arx-cache-menu
    arx-web-menu
    )
  "ArX-mode menu")

(defun add-arx-mode-menu()
  "Add the arx mode menu to the menu bar."
  (cond (window-system
	 (easy-menu-define menubar-mode arx-mode-map "ArX" arx-mode-menu)
	 (easy-menu-add arx-mode-menu 'arx-mode-map))))

(add-hook 'arx-mode-hook 'add-arx-mode-menu)

;;;
;;; Support for arx-log-mode
;;;

(defvar arx-log-mode-menu
  '("ArX-Log"
    ["Commit" arx-commit t]
    )
  "arx log mode menu")

(defun add-arx-log-mode-menu ()
  "Add the arx log mode menu to the menu bar"
  (cond (window-system
	 (easy-menu-define menubar-mode arx-log-mode-map "ArX-Log" arx-log-mode-menu)
	 (easy-menu-add arx-log-mode-menu 'arx-log-mode-map))))

(add-hook 'arx-log-mode-hook 'add-arx-log-mode-menu)

;;;
;;; Support for arx-branch-log-mode
;;;

(defvar arx-branch-log-mode-menu
  '("ArX-Branch-Log"
    ["Finish branch" arx-finish-branch t]
    )
  "arx-branch-log-mode menu")

(defun add-arx-branch-log-mode-menu ()
  "Add the arx branch log mode menu to the menu bar."
  (cond (window-system
	 (easy-menu-define menubar-mode arx-branch-log-mode-map "ArX-Branch-Log" arx-branch-log-mode-menu)
	 (easy-menu-add arx-branch-log-mode-menu 'arx-branch-log-mode-map))))

(add-hook 'arx-branch-log-mode-hook 'add-arx-branch-log-mode-menu)


(provide 'arx-menu)

