#!/usr/bin/env python

# This is a script to convert archives from the old ArX-1.0 format to
# the new 2.0 format.  It is not a complete transformation.  Logs
# added in the 1.0 format will not be properly added in the 2.0
# format.  The logs will still be preserved in the {arch} directory,
# but they will show not show up in the New-patches header, but rather
# in the New-files header.

# It also does not work if there are any tagged revisions besides the
# original base-0 revision.

# Copyright (C) 2004 Walter Landry

import sys
import os
import re
import string

def runcmd(comm,args):
    cmd=comm
    for a in args:
        cmd+=" " + a
    proc=os.popen(cmd)
    lines=proc.readlines()
    if(proc.close()!=None):
        print "Bad exit value from",cmd
        sys.exit(1)
    return lines


def commit_with_log(old,new,rev):
    log=runcmd(old,["revisions --all",rev])
    f=open(",,log","w")
    for line in log:
        f.write(line)
    f.close()
    runcmd(new,["commit --log-file ,,log"])

if(len(sys.argv)!=5):
    sys.stderr.write("Wrong number of arguments.  Invoke as:\n\t"
                     + "dump_restore.py old new archive branch\n"
                     + "where old and new are the complete paths to the old and new versions of ArX.");
    exit(1)

old=sys.argv[1]
new=sys.argv[2]
archive=sys.argv[3]
branch=sys.argv[4]
new_branch=archive + "/" + re.sub("--",".",branch)

# Initial get

runcmd(old,["get",archive + "/" + branch + "--base-0",branch])

#Initiate the new branch

os.chdir(branch)
runcmd(new,["init-tree",new_branch])

# Delete unneeded control files, but keep patch logs
runcmd(new,["rm","--id","-R","-f","{arch}"])
runcmd(new,["add {arch}"])
runcmd(new,["add -R {arch}/" + re.split("--",branch)[0]])

source_files=runcmd(new,["inventory -S"])
for f in source_files:
    if(re.search("\.arch-ids",f)):
        runcmd(new,["rm","--id",re.sub("\n","",f)])

# Commit with the old log file.
commit_with_log(old,new,archive + "/" + branch + "--base-0")

os.chdir("..")

revisions=runcmd(old,["revisions",archive+"/"+branch])

for rev in revisions:
    rev=re.sub("\n","",rev)
    if(rev!="base-0"):
        full=archive+"/"+branch+"--"+rev
        print "Converting " + full
        runcmd(old,["get-patch",full])
        runcmd(old,["dopatch","--delete-removed",branch+"--"+rev+".patches",
                    branch])
        patch_report=runcmd(old,["patch-report --control",
                                 branch+"--"+rev+".patches"])

        os.chdir(branch)

        size=len(patch_report)

        # Remove paths
        for i in range(0,size):
            if(patch_report[i]=="Removed Files:\n"
               or patch_report[i]=="Removed Directories:\n"):
                for j in range(i+2,size):
                    i=j
                    if(patch_report[j]=="\n"):
                        break
                    else:
                        if(not re.search("\.arch-ids",patch_report[j])):
                            temp=re.sub("\n","",patch_report[j])
##                             print "delete",temp
                            runcmd(new,["rm --id",temp])

        # Add new paths
        for i in range(0,size):
            if(patch_report[i]=="Added Files:\n"
               or patch_report[i]=="Added Directories:\n"):
                for j in range(i+2,size):
                    i=j
                    if(patch_report[j]=="\n"):
                        break
                    else:
                        if(not re.search("\.arch-ids",patch_report[j])):
                            temp=re.sub("\n","",patch_report[j])
##                             print "add",temp
                            runcmd(new,["add",temp])


        # Rename paths
        for i in range(0,size):
            if(patch_report[i]=="Renamed Files:\n"
               or patch_report[i]=="Renamed Directories:\n"):
                for j in range(i+2,size,3):
                    i=j
                    if(patch_report[j]=="\n"):
                        break
                    else:
                        if(not re.search("\.arch-ids",patch_report[j])):
                            before=re.sub("\s*","",re.sub("\n","",
                                                          patch_report[j]))
                            after=re.sub("\s*","",
                                         re.sub("==>","",
                                                re.sub("\n","",
                                                       patch_report[j+1])))
                            j=j+1
##                             print "move",before,after
                            # We have to handle directory moves
                            # specially, because all of the
                            # subdirectories are not mentioned in the
                            # patch log.
                            if(os.path.isdir(after)):
                                if(os.path.exists(before)):
                                    os.rename(before,",,temp")

                                # Create any extra dirs needed to move the
                                # path back to its original position.
                                branch_dir=os.path.dirname(before)
                                if(branch_dir!=""
                                   and os.path.exists(branch_dir)!=True):
                                    while not os.path.exists(branch_dir):
                                        branch_dir=os.path.dirname(branch_dir)
                                    os.makedirs(os.path.dirname(before))
                                else:
                                    branch_dir=0

                                os.rename(after,before)
                                runcmd(new,["mv --quiet",before,after])

                                # Delete any extra directories that were
                                # just created.
                                if(branch_dir):
                                    temp=os.path.dirname(before)
                                    while temp!=branch_dir:
                                        os.rmdir(temp)
                                        temp=os.path.dirname(temp)
                        
                    
                                if(os.path.exists(",,temp")):
                                    os.rename(",,temp",before)
                            else:
                                runcmd(new,["mv","--id",before,after])

        # Finally, commit
        commit_with_log(old,new,full)

        os.chdir("..")
                              
