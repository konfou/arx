Summary: ArX version control system
Name: ArX
Version: 2.2.2
Release: 1
URL: http://www.nongnu.org/arx/
Source0: %{name}-%{version}.tar.gz
License: GPL
Group: Development/Tools
BuildRoot: %{_tmppath}/%{name}-root
Requires: diffutils patchutils tar gnome-vfs2

%description
ArX is a flexible, portable, high performance, distributed revision
control system featuring whole-tree atomic changesets, easy branching,
and sophisticated merging.

%prep
%setup -q

%build
./configure --prefix=%{_prefix}
make

%install
rm -rf %{buildroot}
prefix=%{?buildroot:%{buildroot}}%{_prefix} mandir=%{buildroot}%{_mandir} make install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/arx
%doc docs/ArX.pdf
%doc docs/ArX.html
%doc docs/ArX001.png
%doc docs/ArX002.png
%doc docs/ArX003.png
%doc docs/ArX004.png
%doc docs/bob_orig.png
%doc docs/alice_branch.png
%doc docs/alice_update.png
%doc docs/bob_star_merge.png
%doc COPYING
%doc README
%doc CREDITS
%doc NEWS
%{_mandir}/*/*

%changelog
* Mon Apr 25 2005 Walter Landry
- Updated for 2.2.2

* Mon May 3 2004 Walter Landry
- Updated for the all-C++ version of ArX

* Mon Jan 1 2003 Pekka Enberg
* Thu Apr  3 2003 Constantine Sapuntzakis <csapuntz@stanford.edu>
- Initial build.


