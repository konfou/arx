/* Annotate a file

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "tree_root.hpp"
#include <boost/tokenizer.hpp>
#include "read_file_into_string.hpp"
#include "annotate_file.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int annotate(list<string> &argument_list, const command &cmd);

static command_initializer annotate_init(command("annotate",
"Prints out a complete annotation of a file",
"usage: annotate [options] file",
"Print out an annotated version of a file, listing which revision each\n\
line comes from.",
annotate,"Basic",true));

int annotate(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        parse_unknown_options(argument_list);
            break;
      }

  path file;
  if(argument_list.empty())
    {
      throw arx_error("Need at least one file name");
    }
  else
    {
      file=path(*argument_list.begin());
      argument_list.pop_front();
    }

  check_extra_args(argument_list,info);  

  pair<vector<string>,vector<string> > annotations(annotate_file(file));

  for(unsigned int i=0; i<annotations.first.size(); ++i)
    {
      cout << annotations.first[i] << ": " << annotations.second[i] << "\n";
    }
  return 0;
}
