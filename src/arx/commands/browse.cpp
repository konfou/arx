/* Print an outline describing what is available in an archive.

  Copyright (C) 2001, 2002, 2003 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include <iostream>
#include "boost/filesystem/operations.hpp"
#include "get_option_from_file.hpp"
#include "Parsed_Name.hpp"
#include "get_config_option.hpp"
#include "package_level.hpp"
#include "Revision_List.hpp"
#include "patch_level.hpp"
#include "gvfs.hpp"
#include "recursive_browse.hpp"
#include <sstream>
#include <limits>
#include "remote_browse.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int browse(list<string> &argument_list, const command &cmd);

static command_initializer browse_init(command("browse",
"Print an outline of the what is in an archive",
"usage: browse [options] [limit]",
" --level LEVEL          Only recurse down LEVEL levels\n\
 --readmes              Print out README's for categories etc.\n\
 --full                 Print out the full name of each component\n\
\n\
Print out an outline of the contents of an archive.  With no\n\
options and no argument, print out all branches and revisions present\n\
in the default archive.  The LIMIT argument limits the output to only\n\
that branch or revision.  So\n\
\n\
    arx browse foo@bar/baz.bat\n\
\n\
would only print the sub-branches and revisions of foo@bar/baz.bat, and\n\
\n\
    arx browse -l 2 foo@bar/baz\n\
\n\
would only print out the direct sub-branches of foo@bar/baz as well as\n\
its revisions, but not sub-sub-branches.  To specify just an archive,\n\
append the archive name with a slash \"/\", as in\n\
\n\
    arx browse foo@bar/\n\
\n\
For detailed information about the revisions, see \"arx log --help\".",
browse,"Basic",true));

int browse(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool readmes(false), full(false);
  unsigned int level(numeric_limits<unsigned int>::max());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--level")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --level");
              }
            stringstream s(*(argument_list.begin()));
            s >> level;
            argument_list.pop_front();            
          }
        else if(*(argument_list.begin())=="--readmes")
          {
            readmes=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--full")
          {
            full=true;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  /* Get the revision to add, delete or query. */
  
  string input_limit;
  if(!argument_list.empty())
    {
      input_limit=*(argument_list.begin());
      argument_list.pop_front();
    }
  check_extra_args(argument_list,info);

  /* Now get and print the revisions. */

  if(input_limit.empty())
    input_limit=get_config_option("default-archive") + "/";
  Parsed_Name name(input_limit);

  gvfs::Init();
  
  gvfs::uri archive_uri(name.archive_location());
  
  remote_browse remote(name,readmes,level,full);
  recursive_browse(archive_uri/name.branch_path_no_archive(),
                   name,remote);
  return 0;
}
