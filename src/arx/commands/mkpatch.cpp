/* Create a patch between two source trees.  This just parses the
   arguments and passes it on to make_patch.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include <list>
#include "make_patch.hpp"
#include "fill_path_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int mkpatch(list<string> &argument_list, const command &cmd);

static command_initializer mkpatch_init(command("mkpatch",
"Create an ArX patch between two source trees",
"usage: mkpatch [options] original modified destination [path ...]",
" --paths-file file        read the list of paths from FILE\n\
\n\
Create a patch tree between ORIGINAL and MODIFIED in DESTINATION.\n\
DESTINATION must not already exist.\n\
\n\
The patch can be restricted to certain paths with extra arguments\n\
on the command line or the --paths-file option.  See \"commit\" for a\n\
description of the limitations.\n\
\n\
See also \"dopatch --help\"",
mkpatch,"Advanced Patch",true));

int mkpatch(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  Path_List path_list;
  string paths_file;

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--paths-file")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --paths-file",2);
              }
            paths_file=*(argument_list.begin());
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  path original, modified, destination;
  if(argument_list.size()<3)
    {
      string arg_list;
      for(list<string>::iterator i=argument_list.begin();
          i!=argument_list.end(); ++i)
        {
          arg_list+="\n\t" + *i;
        }
      throw arx_error("You need at least three arguments to mkpatch.  The arguments you gave are:"
                      + arg_list,2);
    }
  else
    {
      original=path(*(argument_list.begin()));
      argument_list.pop_front();
      modified=path(*(argument_list.begin()));
      argument_list.pop_front();
      destination=path(*(argument_list.begin()));
      argument_list.pop_front();
    }

  /* Catch and rethrow any arx_error's because we have to change the
     error code to 2. */
  bool changed(false);
  try
    {
      fill_path_list(modified,paths_file,argument_list,path_list);
      changed=make_patch(original,modified,destination,path_list);
    }
  catch (arx_error &arx_err)
    {
      throw arx_error(arx_err.what(),2);
    }
  return (changed ? 1 : 0);
}
