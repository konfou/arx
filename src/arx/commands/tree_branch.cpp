/* Get or set the default branch of a project tree.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "tree_root.hpp"
#include "Parsed_Name.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "valid_package_name.hpp"
#include "get_option_from_file.hpp"
#include "set_option_in_file.hpp"
#include "tree_branch.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int tree_branch(list<string> &argument_list, const command &cmd);

static command_initializer tree_branch_init(command("tree-branch",
"print or set the default branch for a source tree",
"usage: tree-branch [options] [branch]",
" --dir DIR             look for the project tree in DIR\n\
With no argument print the default branch for a project tree.\n\
\n\
With an argument, record BRANCH as the default branch for a project tree",
tree_branch,"Administrative",true));

int tree_branch(list<string> &argument_list, const command &cmd)
{
  bool set_branch(false);
  Command_Info info(cmd);

  path tree_path(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        list<string>::iterator arg=argument_list.begin();
        if(*arg=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              throw arx_error("Need an argument for the --dir option");
            tree_path=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        parse_unknown_options(argument_list);
        break;
      }

  Parsed_Name new_branch;
  if(!argument_list.empty())
    {
      new_branch=Parsed_Name(*(argument_list.begin()));
      set_branch=true;
      argument_list.pop_front();
    }
  check_extra_args(argument_list,info);

  path root=tree_root(tree_path) / "_arx";
  if(set_branch)
    {
      tree_branch(root,new_branch);
    }
  else
    {
      Parsed_Name branch(tree_branch(root));
      if(!branch.empty())
        {
          cout << branch.complete_branch() << endl;
        }
      else
        {
          throw arx_error("No default branch set for "
                          + fs::current_path().native_file_string());
        }
    }
  return 0;
}

