/* Print out the changes to a file.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002  Karel Gardas
  Copyright (C) 2003 Walter Landry and the Regents
                     of the University of California
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "config.h"
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include "tree_root.hpp"
#include "find_or_make_pristine.hpp"
#include "boost/filesystem/operations.hpp"
#include "Spawn.hpp"
#include "get_config_option.hpp"
#include "Temp_Directory.hpp"
#include "tempdir.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int file_diff(list<string> &argument_list, const command &cmd);

static command_initializer file_diff_init(command("file-diff",
"Print the changes to a file",
"usage: file-diff [options] file [revision]",
" --gui       Use a gui tool to display the original and modified files.\n\
\n\
Print the changes between FILE and the corresponding file in a a cached\n\
copy of REVISION.  If no revision is given, it uses the last patch level\n\
of the branch given by tree-branch.\n\
\n\
With the --gui option, the guidiff command (as set by 'arx param') is run\n\
with the old and new files.",
file_diff,"Miscellaneous Advanced",true));

int file_diff(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  Parsed_Name cached_revision;
  bool gui(false);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        list<string>::iterator arg=argument_list.begin();
        if(*arg=="--gui")
          {
            gui=true;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  /* Get the file to be diff'd. */
  if(argument_list.empty())
    throw arx_error("Need at least one argument");

  path file_path;
  file_path=*(argument_list.begin());
  file_path=system_complete(file_path);
  argument_list.pop_front();

  const path root(tree_root(file_path.branch_path()));

  const path relative_file_path=relative_path(file_path,root);

  /* Get the revision */
  if(!argument_list.empty())
    {
      cached_revision=Parsed_Name(*(argument_list.begin()));
      argument_list.pop_front();
    }

  check_extra_args(argument_list,info);

  Temp_Directory temp_dir(tempdir(),",,pristine",false);
  const path pristine(find_or_make_pristine(root,temp_dir.path,
                                            cached_revision));

  if(!lexists(pristine/relative_file_path))
    {
      throw arx_error("Can't find this file in the pristine tree\n\t"
                      + file_path.native_file_string() + "\n\t"
                      + (pristine/relative_file_path).native_file_string());
    }

  /* Make sure that we're dealing with files. */

  if((lexists(file_path) && (symbolic_link_exists(file_path)
                             || is_directory(file_path)))
     || (symbolic_link_exists(pristine/relative_file_path)
         || is_directory(pristine/relative_file_path)))
    {
      throw arx_error("Can't use file-diffs on symlinks or directories.  Try diff instead.\n\t" + file_path.native_file_string());
    }

  /* Print out the diff */

  Spawn s;
  if(!gui)
    {
      s << ARXDIFF << "-u";
    }
  else
    {
      s << get_config_option("guidiff");
    }
  s << (pristine/relative_file_path).native_file_string()
    << file_path.native_file_string();

  int exit_status;
  if(s.execute(exit_status,true))
    return exit_status;
  /* Return 2 if there is a problem executing diff. */
  return 2;
}
