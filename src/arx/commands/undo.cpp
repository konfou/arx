/* Temporarily undo changes in working directory in order to do
   another checkin on the clean revision first

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002 Alexander Deruwe
   Copyright (C) 2003 Walter Landry and the Regents
                      of the University of California
   Copyright (C) 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include "fill_path_list.hpp"
#include "tree_root.hpp"
#include "boost/filesystem/operations.hpp"
#include "undo.hpp"
#include "Temp_Path.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int undo(list<string> &argument_list, const command &cmd);

static command_initializer undo_init(command("undo",
"Temporarily undo changes to a project tree",
"usage: undo [options] [path ...]",
" --paths-file file       read the list of paths from FILE\n\
 -o --output UNDO-DIR    store the patch in UNDO-DIR\n\
 --dir DIR               change to DIR first\n\
\n\
Create a patch of the local tree against the last revision, and then\n\
undo those changes.  The patch will be stored in UNDO-DIR, or \",,undo-\"\n\
with a numerical index attached.\n\
\n\
You can also undo only some of the changes with extra command line\n\
arguments or the --paths-file option.  See \"commit\" for a description\n\
of the limitations.\n\
\n\
Afterwards, the changes can be restored with \"redo\"",
undo,"Miscellaneous Advanced",true));

int undo(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  Path_List path_list;
  string paths_file;
  path undo_dir;
  path tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--paths-file")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --paths-file");
              }
            paths_file=*(argument_list.begin());
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --dir");
              }
            tree_directory=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="-o"
                || *(argument_list.begin())=="--output")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for -o and --output");
              }
            undo_dir=path(*(argument_list.begin()));
            argument_list.pop_front();
            if(lexists(undo_dir))
              {
                throw arx_error("The undo directory already exists: "
                                + undo_dir.native_file_string());
              }
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  /* Get the undo_dir */

  const path root(tree_root(tree_directory));

  if(undo_dir.string().empty())
    undo_dir=Temp_Path(root,",,undo");
  
  /* Fill up the path list. */

  fill_path_list(tree_directory,paths_file,argument_list,path_list);

  check_extra_args(argument_list,info);

  undo(tree_directory,undo_dir,path_list);

  return 0;
}
