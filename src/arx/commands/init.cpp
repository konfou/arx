/* Initialize the current tree

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry
                           and the Regents of the University of California
  Copyright (C) 2004, 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "Parsed_Name.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "init.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int init(list<string> &argument_list, const command &cmd);

static command_initializer init_init(command("init",
"Initialize the current tree",
"usage: init [options] branch",
" --dir DIR                      cd to DIR first\n\
 --no-add                       Don't add files\n\
\n\
Initialize the current directory so that it can be commit'ed\n\
as a completely new line of development.  For example,\n\
\n\
  arx init foo/bar\n\
\n\
will initialize the current directory as the project \"bar\" stored\n\
in the archive \"foo\".  If you have set your default archive to \"foo\"\n\
(see arx param -H), then it is just\n\
\n\
  arx init bar\n\
\n\
ArX will automatically (unless given the --no-add option) recursively\n\
add all of the files and directories so that they will be included in\n\
the archive.",
init,"Basic",true));

int init(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool add_paths(true);
  path tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --dir");
              }
            tree_directory=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--no-add")
          {
            add_paths=false;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(argument_list.empty())
    throw arx_error("Not enough arguments");
  Parsed_Name input_branch(*(argument_list.begin()));
  argument_list.pop_front();
  check_extra_args(argument_list,info);

  init(input_branch,tree_directory,add_paths);

  return 0;
}
