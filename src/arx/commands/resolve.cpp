/* List or resolve conflicts in a tree.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "Conflicts.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int resolve(list<string> &argument_list, const command &cmd);

static command_initializer resolve_init(command("resolve",
"List or resolve conflicts in a tree",
"usage: resolve [options] [path ...]",
" --all         Resolve all conflicts\n\
 --dir DIR     cd to DIR first\n\
\n\
Without any arguments, this will list all paths with conflicts in the\n\
project tree.  Specifying a path will tell ArX that the conflict is\n\
resolved, and delete any files that ArX may have made (e.g. .rej files).",
resolve,"Branching and Merging",true));

int resolve(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool all(false);
  path tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--all")
          {
            argument_list.pop_front();
            all=true;
          }
        else if(*(argument_list.begin())=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --dir");
              }
            tree_directory=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(argument_list.empty())
    {
      if(all)
        {
          Conflicts::resolve_all(tree_directory);
        }
      else
        {
          Conflicts conflicts(tree_directory,true);
          cout << conflicts;
        }
    }
  else
    {
      if(all)
        throw arx_error("No arguments allowed with the --all option.");
      while(!argument_list.empty())
        {
          path conflict_path(*argument_list.begin());
          Conflicts::resolve(tree_directory,conflict_path);
        }
    }
  return 0;
}
