/* Add an inventory id for a path.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "get_config_option.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include <cstdio>
#include "Checksums.hpp"
#include "tree_root.hpp"
#include "read_checksums.hpp"
#include "add_path_to_list.hpp"
#include "add_path_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int add(list<string> &argument_list, const command &cmd);

static command_initializer add_init(command("add",
"Add an inventory id for a path",
"usage: add [options] source...",
" -R --recursive      recursively add the elements in a directory\n\
\n\
Add an inventory id for a file or directory, thus officially including\n\
it in a project tree.  By default, it is not recursive.",
add,"Basic",true));

int add(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool recursive(false);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="-R"
           || *(argument_list.begin())=="--recursive")
          {
            argument_list.pop_front();
            recursive=true;
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(argument_list.empty())
    {
      throw arx_error("Need at least one file name");
    }

  Checksums checksums;
  path root(tree_root(fs::current_path()));
  read_checksums(root,checksums);

  list<pair<file_attributes,sha256> > add_list;
  for(list<string>::iterator file=argument_list.begin();
      file!=argument_list.end(); ++file)
    {
      const string relative_filename(relative_path(*file,root).string());

      bool dont_add(false);

      if(!fs::lexists(*file))
        {
          dont_add=true;
          if(Command_Info::verbosity>=default_output)
            cerr << "Can't add\n\t"
                 << *file
                 << "\nIt doesn't exist\n";
        }

      if(is_control(relative_filename))
        {
          dont_add=true;
          if(Command_Info::verbosity>=default_output)
            cerr << "Can't add\n\t"
                 << *file
                 << "\nYou are not allowed to add anything in the _arx directory.\n";
        }
      
      if(!dont_add)
        {
          add_path_to_list(add_list,root,*file,checksums,recursive);
        }
    }
  if(!add_list.empty())
    add_path_list(root,add_list);
  return 0;
}
