/* Delete a file or empty directory and its inventory id.  It should
   have the same semantics and most of the same options as rm(1).

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "delete_element.hpp"
#include "Checksums.hpp"
#include "read_checksums.hpp"
#include "tree_root.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int rm(list<string> &argument_list, const command &cmd);

static command_initializer move_init(command("rm",
"Delete a path and its inventory id.",
"usage: rm [options] path...",
" --id                 only delete the inventory id, not the actual path\n\
 -i --interactive     prompt before each removal\n\
 -R --recursive       remove the contents of directories recursively\n\
 -f --force           ignore nonexistent files, never prompt\n\
\n\
Delete a file or empty directory and its inventory id. With\n\
the --id option, only delete the external inventory id, not the file\n\
or directory itself.  The usage should be almost exactly the same as rm(1).",
rm,"Basic",true));

int rm(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);

  bool recursive(false), interactive(false), force(false), id_only(false);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="-R"
           || *(argument_list.begin())=="--recursive")
          {
            recursive=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="-i"
                || *(argument_list.begin())=="--interactive")
          {
            interactive=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="-f"
                || *(argument_list.begin())=="--force")
          {
            force=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--id")
          {
            id_only=true;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(argument_list.empty())
    {
      throw arx_error("Need at least one file or directory name");
    }

  Checksums checksums;
  read_checksums(tree_root(fs::current_path()),checksums);
  
  for(list<string>::iterator file=argument_list.begin();
      file!=argument_list.end(); ++file)
    {
      delete_element(*file,checksums,interactive,force,recursive,
                     id_only);
    }
  return 0;
}
