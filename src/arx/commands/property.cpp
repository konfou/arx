/* List, set or unset a property for a path.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "get_config_option.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include <cstdio>
#include "Checksums.hpp"
#include "tree_root.hpp"
#include "read_checksums.hpp"
#include "property_action.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int property(list<string> &argument_list, const command &cmd);

static command_initializer property_init(command("property",
"Set or unset a property for a path",
"usage: property [options] key path1 [path2...]\n\
       property --all path1 [path2...]\n\
       property --set value key path1 [path2...]\n\
       property --unset key path1 [path2...]",
" --all                    Show all of the properties of the listed paths\n\
 --set value              Set the property KEY to VALUE\n\
 --unset                  Unset the property KEY\n\
\n\
With no options, list the values of the properties for the various paths.\n\
Use --all to see all properties for a path, and --set and --unset to\n\
set or unset the listed propery.  The property and value may be arbitrary,\n\
but the following properties have predefined meanings:\n\
\n\
arx:user-read\n\
arx:user-write\n\
arx:user-exec\n\
arx:group-read\n\
arx:group-write\n\
arx:group-exec\n\
arx:other-read\n\
arx:other-write\n\
arx:other-exec\n\
\n\
If any of these properties are set to true or false, then when that path\n\
is checked out (e.g. with get), the appropriate permission bit is set\n\
(assuming the file system can accomodate it).\n\
\n\
Note that, while the properties can be arbitrary, they are designed to\n\
work well when they are small.",
property,"Miscellaneous Advanced",true));

int property(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool all(false), set_prop(false), unset_prop(false);
  string value;

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--set")
          {
            argument_list.pop_front();
            set_prop=true;
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --set");
              }
            value=*(argument_list.begin());
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--unset")
          {
            argument_list.pop_front();
            unset_prop=true;
          }
        else if(*(argument_list.begin())=="--all")
          {
            argument_list.pop_front();
            all=true;
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if((set_prop && (unset_prop || all)) || (unset_prop && all))
    throw arx_error("Can't use more than one of --set, --unset, or --all");

  if(all)
    {
      if(argument_list.empty())
        throw arx_error("Need at least one path");
    }
  else if(argument_list.size()<2)
    {
      throw arx_error("Need a key and at least one path");
    }
  const string key(*argument_list.begin());
  if(!all)
    argument_list.pop_front();

  Checksums checksums;
  path root(tree_root(fs::current_path()));
  read_checksums(root,checksums);

  for(list<string>::iterator file=argument_list.begin();
      file!=argument_list.end(); ++file)
    {
      const string relative_filename(relative_path(*file,root).string());
      
      Checksums::iterator i=
        find_if(checksums.begin(),checksums.end(),
                bind2nd(ptr_fun(checksums_file_path_eq_string),
                        relative_filename));
      if(i==checksums.end())
        {
          throw arx_error("This path does not exist in the manifest.  You must add it before you\ncan set any properties.\n\t"
                          + *file);
        }
      if(set_prop)
        {
          property_action(root,relative_filename,"set",key,value);
        }
      else if(unset_prop)
        {
          if(i->first.properties.find(key)==i->first.properties.end())
            {
              if(Command_Info::verbosity>=default_output)
                cerr << *file << ": " << key << " - property not set" << endl;
            }
          else
            {
              property_action(root,relative_filename,"unset",key,"");
            }
        }
      else
        {
          cout << *file << ": ";
          if(!all)
            {
              if(i->first.properties.find(key)==i->first.properties.end())
                {
                  cout << key << " - property not set" << endl;
                }
              else
                {
                  cout << key << " -> "
                       << i->first.properties[key]
                       << endl;
                }
            }
          else
            {
              for(map<string,string>::iterator j=i->first.properties.begin();
                  j!=i->first.properties.end(); ++j)
                {
                  cout << "\n\t" << j->first << " -> " << j->second << endl;
                }
              cout << endl;
            }
        }
    }
  return 0;
}
