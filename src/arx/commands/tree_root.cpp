/* Print out the root of a project tree.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "tree_root.hpp"
#include "boost/filesystem/operations.hpp"
#include <iostream>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int tree_root(list<string> &argument_list, const command &cmd);

static command_initializer tree_root_init(command("tree-root",
"Print the root of a project tree",
"usage: tree-root [options] [dir]",
"Print out the root of a project tree which contain DIR (or the\n\
current directory).",
tree_root,"Administrative",true));

int tree_root(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        parse_unknown_options(argument_list);
        break;
      }

  path dir(fs::current_path());
  if(!argument_list.empty())
    {
      dir=path(*(argument_list.begin()));
      if(!exists(dir) || !is_directory(dir))
        {
          throw arx_error("Argument must be a directory: "
                          + *(argument_list.begin()));
        }
      argument_list.pop_front();
    }
  check_extra_args(argument_list,info);

  cout << tree_root(dir).native_file_string() << endl;

  return 0;
}

