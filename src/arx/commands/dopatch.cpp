/* Apply a patch to a source tree.  This just parses the
   arguments and passes it on to do_patch.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include <list>
#include "do_patch.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int dopatch(list<string> &argument_list, const command &cmd);

static command_initializer dopatch_init(command("dopatch",
"Apply an ArX patch to a source tree",
"usage: dopatch [options] patchdir sourcedir",
" -r --reverse                 apply the patch in reverse\n\
 --delete-removed             remove all deleted files\n\
Apply the patch in PATCHDIR to the source tree SOURCEDIR.\n\
PATCHDIR is not modified.\n\
\n\
See also \"mkpatch --help\"",
dopatch,"Advanced Patch",true));

int dopatch(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool reverse(false), delete_removed(false);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="-r"
           || *(argument_list.begin())=="--reverse")
          {
            reverse=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--delete-removed")
          {
            delete_removed=true;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  path patch, source;
  if(argument_list.size()!=2)
    {
      string arg_list;
      for(list<string>::iterator i=argument_list.begin();
          i!=argument_list.end(); ++i)
        {
          arg_list+="\n\t" + *i;
        }
      throw arx_error("Wrong number of arguments to dopatch.  Arguments are"
                      + arg_list);
    }
  else
    {
      patch=path(*(argument_list.begin()));
      argument_list.pop_front();
      source=path(*(argument_list.begin()));
      argument_list.pop_front();
    }
  Conflicts conflicts(do_patch(patch,source,reverse,delete_removed));
  if(!conflicts.empty() && Command_Info::verbosity>=quiet)
    cerr << conflicts;

  return 0;
}
