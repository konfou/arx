/* Set or display the ignore regex for a tree.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "get_config_option.hpp"
#include "set_config_option.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "tree_root.hpp"
#include "get_option_from_file.hpp"
#include "set_option_in_file.hpp"
#include "file_attributes.hpp"
#include "unsafe_delete_inventory_id.hpp"
#include "add_path_and_checksum.hpp"
#include "edit_path.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int ignore_cmd(list<string> &argument_list, const command &cmd);

static command_initializer ignore_cmd_init(command("ignore",
"print or change the ignore regexp for a project tree",
"usage: ignore [options] [regexp]",
" -d --delete                   unspecify your ignore regexp\n\
\n\
With no argument, and without --delete, print the regular expression that\n\
determines whether tree-lint should ignore a path.\n\
\n\
With an argument, record REGEXP as the ignore regexp for the current\n\
project tree.  For example, to ignore all files ending with '.o', '.bak',\n\
or '~'\n\
\n\
  arx ignore \"^.*(\\.o|\\.bak|~)$\"\n\
\n\
With the option --delete and no argument, ensure that\n\
there is no ignore regexp.",
ignore_cmd,"Miscellaneous Advanced",true));


int ignore_cmd(list<string> &argument_list, const command &cmd)
{
  bool delete_option(false), set_option(false);
  Command_Info info(cmd);

  bool done=false;
  while(!argument_list.empty() && !done)
    {
      if(!parse_common_options(argument_list,info))
        {
          list<string>::iterator arg=argument_list.begin();
          if(*arg=="-d" || *arg=="--delete")
            {
              delete_option=true;
              argument_list.pop_front();
            }
          else
            {
              parse_unknown_options(argument_list);
              done=true;
            }
        }
    }
  string new_option;
  if(!delete_option && !argument_list.empty())
    {
      new_option=*(argument_list.begin());
      set_option=true;
      argument_list.pop_front();
    }
  check_extra_args(argument_list,info);
  
  const path root(tree_root(fs::current_path()));
  const path ignore_path(root/"_arx/ignore");

  /* When deleting, also remove the inventory id. */
  if(delete_option)
    {
      if(lexists(ignore_path))
        {
          unsafe_delete_inventory_id
            (root,file_attributes("_arx/ignore","_arx/ignore"),false);
          fs::remove(ignore_path);
        }
    }
  /* When setting, also add to the manifest if the ignore file doesn't
     already exist. */
  else if(set_option)
    {
      if(lexists(ignore_path))
        {
          set_option_in_file(ignore_path,new_option);
          if(exists(root/"_arx/++edit"))
            edit_path(root,"_arx/ignore");
        }
      else
        {
          set_option_in_file(ignore_path,new_option);
          add_path_and_checksum
            (root,file_attributes("_arx/ignore","_arx/ignore"),
             sha256(ignore_path));
        }
    }
  else
    {
      if(lexists(ignore_path))
        {
          cout << get_option_from_file(ignore_path) << endl;
        }
    }
  return 0;
}
