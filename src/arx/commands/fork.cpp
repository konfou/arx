/* Convert a project tree into a new branch

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry
                           and the Regents of the University of California
  Copyright (C) 2004, 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include "latest_tree_revision.hpp"
#include "Parsed_Name.hpp"
#include "fork.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int fork(list<string> &argument_list, const command &cmd);

static command_initializer fork_init(command("fork",
"Convert a project tree into a new branch",
"usage: fork [options] new-branch",
" --dir DIR         cd to DIR first\n\
\n\
Convert a given project tree to NEW-BRANCH.  The current project tree will\n\
be modified in place.  For example, if you have a project \"foo\" in the\n\
current directory, forking to a project \"bar\" is\n\
\n\
  arx fork bar\n\
\n\
This command can be used to branch any revision to any branch, even\n\
between two branches within the same branch.  So if you had revisions 0-5\n\
in project foo, then to create a new revision which does not have the last\n\
two patches, you can type\n\
\n\
  arx get foo,3 foo-project\n\
  cd foo-project\n\
  arx fork foo\n\
  arx commit -s \"Skipped patches 4 and 5\"\n\
\n\
As in this example, \"fork\" is often followed by \"arx commit\",\n\
which actually creates a new branch in an archive.",
fork,"Branching and Merging",true));

int fork(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  path tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --dir");
              }
            tree_directory=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  Parsed_Name new_input_branch, old_input_revision;

  if(argument_list.empty())
    throw arx_error("Not enough arguments");
  new_input_branch=Parsed_Name(*(argument_list.begin()));
  argument_list.pop_front();
  old_input_revision=latest_tree_revision(tree_directory);

  check_extra_args(argument_list,info);  

  /* Now just call the back end */
  fork(old_input_revision,new_input_branch,tree_directory);
  
  return 0;
}
