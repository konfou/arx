/* Remove a branch and all sub-branches etc. from an archive.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Parsed_Name.hpp"
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "valid_package_name.hpp"
#include "remote_browse.hpp"
#include "recursive_browse.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int delete_branch(list<string> &argument_list, const command &cmd);

static command_initializer delete_branch_init(command("delete-branch",
"IRREVOCABLY delete a branch within an archive",
"usage: delete-branch [options] branch",
" --force        delete without prompting\n\
\n\
IRREVOCABLY delete a branch within an archive, along with all\n\
associated sub-branches and revisions.  Unless --force is specified, it will\n\
display all of the branches and revisions that will be deleted and prompt\n\
for confirmation",
delete_branch,"Administrative",true));

int delete_branch(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);

  bool done(false), force(false);

  while(!argument_list.empty() && !done)
    {
      if(!parse_common_options(argument_list,info))
        {
          if(*(argument_list.begin())=="--force")
            {
              force=true;
              argument_list.pop_front();
            }
          else
            {
              parse_unknown_options(argument_list);
              done=true;
            }
        }
    }

  if(argument_list.size()<1)
    {
      throw arx_error ("Not enough arguments");
    }
  Parsed_Name name(*(argument_list.begin()));
  argument_list.pop_front();

  check_extra_args(argument_list,info);

  valid_package_name(name,Branch);

  /* Get consent from the user */
  if(!force)
    {
      gvfs::Init();
      gvfs::uri archive_uri(name.archive_location());
      remote_browse remote(name,false,numeric_limits<unsigned int>::max(),
                           false);
      recursive_browse(archive_uri/name.branch_path_no_archive(),
                       name,remote);
      cout << "Delete all of these branches and revisions? (y/n) ";
      string response;
      cin >> response;
      if(response=="y" || response=="Y")
        force=true;
    }

  if(force)
    {
      gvfs::Init();
      gvfs::uri location(name.archive_location());

      gvfs::remove_all(location / name.branch_path_no_archive());
    }
  return 0;
}
