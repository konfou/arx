/* Audit a source tree for problems

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "tree_root.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "check_tree.hpp"
#include <iostream>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int tree_lint(list<string> &argument_list, const command &cmd);

static command_initializer tree_lint_init(command("tree-lint",
"Audit a source tree for problems",
"usage: tree-lint [options] [dir]",
" --strict              Treat warnings as errors\n\
 --no-unrecognized     Don't check for unrecognized files\n\
\n\
Audit a source tree for missing files, duplicate ids,\n\
and files not matching recognized naming conventions.",
tree_lint,"Basic",true));

int tree_lint(list<string> &argument_list, const command &cmd)
{
  bool strict(false), no_unrecognized(false);
  Command_Info info(cmd);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--strict")
          {
            strict=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--no-unrecognized")
          {
            no_unrecognized=true;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  path dir("");
  if(!argument_list.empty())
    {
      dir=path(*(argument_list.begin()));
      if(!exists(dir) || !is_directory(dir))
        {
          throw arx_error("Argument must be a directory: "
                          + *(argument_list.begin()));
        }
      argument_list.pop_front();
    }
  check_extra_args(argument_list,info);

  string warnings, errors;
  bool result(check_tree(dir,strict,no_unrecognized,warnings,errors));
  if(Command_Info::verbosity>=default_output
     || (Command_Info::verbosity>=quiet && strict))
    cerr << warnings;
  if(Command_Info::verbosity>=quiet)
    cerr << errors;


  return (result ? 0 : 1);
}

