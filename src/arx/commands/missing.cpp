/* Show which patches are missing from a project tree.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include <iostream>
#include "tree_root.hpp"
#include "get_option_from_file.hpp"
#include "Parsed_Name.hpp"
#include "valid_package_name.hpp"
#include "Revision_List.hpp"
#include "list_patch_logs.hpp"
#include "list_archive_revisions.hpp"
#include "tree_branch.hpp"
#include "recursively_add_tags.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int missing(list<string> &argument_list, const command &cmd);

static command_initializer missing_init(command("missing",
"Show which patches are missing from a project tree",
"usage: missing [options] [branch]",
" --dir DIR                    Change to DIR first\n\
 -r --reverse                 Sort in reverse order\n\
\n\
Show which patches are stored in the archive but missing from\n\
a project tree.  This can be piped to \"arx log\" to get more\n\
info about the patches.  For example,\n\
\n\
  arx missing | xargs -n 1 arx log --remote\n\
\n\
will print the patch number and summary of all missing patches.",
missing,"Branching and Merging",true));

int missing(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);

  bool reverse(false);
  path tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="-r"
           || *(argument_list.begin())=="--reverse")
          {
            reverse=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="-d"
                || *(argument_list.begin())=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for -d and --dir");
              }
            tree_directory=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }
  
  /* Get the branch to look at. */
  
  Parsed_Name input_branch;

  if(!argument_list.empty())
    {
      input_branch=Parsed_Name(*(argument_list.begin()));
      argument_list.pop_front();
    }
  else
    {
      input_branch=tree_branch(tree_directory);
    }
  check_extra_args(argument_list,info);
      
  valid_package_name(input_branch,Branch);

  list<pair<Parsed_Name,path> > input_list;

  recursively_add_tags(input_list,input_branch,tree_directory);

  for(list<pair<Parsed_Name,path> >::iterator i=input_list.begin();
      i!=input_list.end(); ++i)
    {
      const Parsed_Name branch=i->first;
      const path branch_directory=i->second;
      
      Revision_List tree_list(list_patch_logs(branch_directory,branch)),
        archive_list(list_archive_revisions(branch));
      
      if(archive_list.empty())
        throw arx_error("This branch has no revisions in the archive.\n\t"
                        + branch.complete_name());
      
      if(reverse)
        {
          tree_list.reverse();
          archive_list.reverse();
        }
      
      Revision_List::iterator i(tree_list.begin()), j(archive_list.begin());
      while(i!=tree_list.end() && j!=archive_list.end())
        {
          if(*i==*j)
            {
              ++i;
              ++j;
            }
          else if((*i<*j && !reverse) || (*i>*j && reverse))
            {
              ++i;
            }
          else
            {
              cout << branch.complete_branch() + patch_level(*j)
                   << endl;
              ++j;
            }
        }
      
      for(;j!=archive_list.end();++j)
        cout << branch.complete_branch() + patch_level(*j)
             << endl;
    }
  return 0;
}
