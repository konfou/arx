/* Update the .listing files in an archive.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include "update_listing.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int update_listing(list<string> &argument_list, const command &cmd);

static command_initializer update_listing_init(command("update-listing",
"update the .listing files in an archive",
"usage: update-archive-listing --add archive_uri\n\
       update-archive-listing --delete archive_uri",
" -a --add           add .listing files to an archive and ensure that\n\
                    they are kept up to date\n\
 -d --delete        stop updating .listing files in an archive\n\
\n\
.listing files are required in order to make archives available on a\n\
plain http web host.\n\
\n\
With --add, ArX will update all of the .listing files in an archive and\n\
ensure that they are kept up to date.  With --delete, ArX will stop\n\
updating the .listing files.\n\
\n\
For example, to make the archive located at sftp://foo@bar//public_html/archive\n\
available via a plain web server\n\
\n\
  arx update-listing -a sftp://foo@bar//public_html/archive",
update_listing,"Administrative",true));

int update_listing(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool add(false), remove(false);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="-d"
           || *(argument_list.begin())=="--delete")
          {
            remove=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="-a"
           || *(argument_list.begin())=="--add")
          {
            add=true;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }
  if(add && remove)
    throw arx_error("You may not specify both --add and --delete.");
  if(!add && !remove)
    throw arx_error("You must specify either --add or --delete.");


  /* Get the version to look at. */
  
  Parsed_Name archive;
  if(!argument_list.empty())
    {
      string archive_uri(*(argument_list.begin()));
      if(*(archive_uri.rbegin())!='/')
        archive_uri+="/";
      archive=Parsed_Name(archive_uri);
      argument_list.pop_front();
    }
  else
    {
      throw arx_error("Need an argument for update_listing");
    }
  check_extra_args(argument_list,info);

  gvfs::Init();
  if(add)
    {
      gvfs::out_file out(archive.archive_location()
                         /",meta-info/update-listing");
      out.write("update");
      update_listing(archive);
    }
  else
    {
      gvfs::remove(archive.archive_location()/",meta-info/update-listing");
    }

  return 0;
}
