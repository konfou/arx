/* Print out a nicely formatted description of a patch.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Minor bug fix 2003 Stig Brautaset
  Minor bug fix 2003 Junio C Hamano
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include <list>
#include "Patch_Info.hpp"
#include "check_extra_args.hpp"
#include "Temp_Directory.hpp"
#include "Current_Path.hpp"
#include "Spawn.hpp"
#include "get_revision_patch.hpp"
#include "../../config.h"
#include "tempdir.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int patch_report(list<string> &argument_list, const command &cmd);

static command_initializer patch_report_init(command("patch-report",
"Print out a nicely formatted description of a patch",
"usage: patch-report [options] target",
" --diffs                      include diffs in the output\n\
 --control                    include control files\n\
 --html                       generate html output\n\
 --title TITLE                set the report title\n\
 --link-root ROOT             specify root to link against (html only)\n\
\n\
Print out a nicely formatted description of a patch.  If TARGET is\n\
a directory, it is assumed that it has an expanded directory inside it.\n\
If TARGET is a file, then it is assumed to be a patch tarball.  Otherwise,\n\
TARGET is assumed to be a revision name.  If TARGET is a revision name, then\n\
ArX will fetch the patch from the archive and report on its contents.\n\
\n\
The --link-root option only works with the --html option.  It specifies\n\
where the patch directory is, so that patches as well as new and deleted\n\
files can be linked to within the html.  Otherwise, it uses the patch\n\
directory.\n\
\n\
Note that links to patches and new and deleted files will only be valid\n\
if TARGET is a patch directory.",
patch_report,"Advanced Patch",true));

int patch_report(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool diffs(false), html(false), control(false);
  string title, link_root;

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--diffs")
          {
            diffs=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--html")
          {
            html=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--control")
          {
            control=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--title")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --title");
              }
            title=*(argument_list.begin());
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--link-root")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --link-root");
              }
            link_root=*(argument_list.begin());
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(!html && !link_root.empty())
    throw arx_error("--link_root needs --html");

  path patch;
  Temp_Directory temp_dir(tempdir(),",,patch-report");
  if(!argument_list.empty())
    {
      const string target=*(argument_list.begin());
      if(fs::exists(target))
        {
          if(fs::is_directory(target))
            {
              patch=target;
            }
          else
            {
              const path complete_target=fs::system_complete(target);
              Current_Path current(temp_dir.path);
              Spawn untar;
              untar << ARXTAR << "-zxf"
                    << complete_target.native_file_string();
              if(!untar.execute(true))
                {
                  throw arx_error("Can not execute untar for "
                                  + complete_target.native_file_string()); 
                }
              patch=temp_dir.path/path(target).leaf()
                .substr(0,path(target).leaf().size()-7);
            }
        }
      else
        {
          get_revision_patch(target,temp_dir.path/"patch");
          patch=temp_dir.path/"patch";
        }

      if(link_root.empty())
        link_root=patch.native_file_string();
      argument_list.pop_front();
      if(!exists(patch) || !is_directory(patch))
        throw arx_error("Patch directory does not exist or is not a directory\n\t"
                        + patch.native_file_string());
    }
  else
    {
      throw arx_error("Need a patch directory.");
    }
  check_extra_args(argument_list,info);

  cout << Patch_Info(patch,diffs,html,title,link_root,control);
  return 0;
}
