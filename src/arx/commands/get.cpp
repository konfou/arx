/* get a revision from an archive, making a project tree

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include "boost/filesystem/operations.hpp"
#include "get_config_option.hpp"
#include "Parsed_Name.hpp"
#include "get_revision.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int get(list<string> &argument_list, const command &cmd);

static command_initializer get_init(command("get",
"get a revision from an archive, making a project tree",
"usage: get [options] revision [dir]",
" --no-pristine          don't save a pristine copy\n\
 --cache DIR            cache directory for locally cached revisions\n\
 --no-edit              Mark all files as non-editable.\n\
 --link-tree            Hard link the tree (for use with --no-edit)\n\
\n\
Extract REVISION from an archive, creating the new project tree\n\
DIR.   If DIR is not specified, store the working copy in a subdirectory\n\
of the current directory, giving it the name of the revision.\n\
Either way, the project tree must not already exist.\n\
\n\
With the --no-edit option, all of the files and directories in the\n\
project tree are marked read-only.  You must use \"arx edit\" in order\n\
to edit them.  This is a mode of operation for large trees.  In conjunction\n\
with --no-edit, --link-tree will attempt to hard link the tree to an\n\
existing tree, giving potential savings in time and space.\n\
WARNING:  The --link-tree option will make versioning write properties\n\
(e.g. arx property arx:user-write) unreliable.",
get,"Basic",true));

int get(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool pristine(true), no_edit(get_config_option("no-edit")=="true"),
    link_tree(get_config_option("link-tree")=="true");
  path target_dir, cache_dir(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--no-pristine")
          {
            pristine=false;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--no-edit")
          {
            no_edit=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--link-tree")
          {
            link_tree=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--cache")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --cache");
              }
            cache_dir=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  /* Get the version to look at. */
  
  Parsed_Name input_revision;
  if(!argument_list.empty())
    {
      input_revision=Parsed_Name(*(argument_list.begin()));
      argument_list.pop_front();

      if(!argument_list.empty())
        {
          target_dir=path(*(argument_list.begin()));
          argument_list.pop_front();
        }
    }
  else
    {
      throw arx_error("Need an argument for get");
    }
  check_extra_args(argument_list,info);

  /* Now get the revision. */

  get_revision(input_revision,target_dir,cache_dir,no_edit,pristine,link_tree);
  return 0;
}
