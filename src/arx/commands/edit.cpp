/* Make a path editable.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "tree_root.hpp"
#include "edit_path.hpp"
#include "inventory_types.hpp"
#include "read_checksums.hpp"
#include "get_option_from_file.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int edit(list<string> &argument_list, const command &cmd);

static command_initializer move_init(command("edit",
"Make a path editable",
"usage: edit [options] source...",
"If a project tree has been downloaded with the --no-edit option, make\n\
individual paths editable.\n\
\n\
This is an option mostly useful for large trees.  When you get a revision\n\
using the --no-edit option, you are promising that you will not modify\n\
files without running \"arx edit\".  If you do not keep that promise, then\n\
any changes to that file will probably (though not necessarily) go\n\
unnoticed.\n\
\n\
To help keep that promise, when you get a revision using the --no-edit\n\
option, ArX makes all of the paths read-only.  When you run \"arx edit\",\n\
ArX changes the path back to its original state.\n\
\n\
The command does not work (and is not needed) on directories.",
edit,"Miscellaneous Advanced",true));

int edit(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  path tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        parse_unknown_options(argument_list);
        break;
      }

  if(argument_list.empty())
    {
      throw arx_error("Need at least one file name");
    }

  const path root(tree_root(tree_directory));
  if(!exists(root/"_arx/++edit"))
    throw arx_error("This project tree does not require you to run \"arx edit\" before editing a file.");

  Checksums checksums;

  for(list<string>::iterator file=argument_list.begin();
      file!=argument_list.end(); ++file)
    {
      const string relative_filename(relative_path(*file,root).string());

      bool dont_edit(false);

      if(!fs::lexists(*file))
        {
          dont_edit=true;
          if(Command_Info::verbosity>=default_output)
            cerr << "Can't edit\n\t"
                 << *file
                 << "\nIt doesn't exist\n";
        }
      else if(!fs::symbolic_link_exists(*file) && fs::is_directory(*file))
        {
          dont_edit=true;
          if(Command_Info::verbosity>=default_output)
            cerr << "Can't edit\n\t"
                 << *file
                 << "\nIt is a directory.\n";
        }
      else if(is_control(relative_filename))
        {
          dont_edit=true;
          if(Command_Info::verbosity>=default_output)
            cerr << "Can't edit\n\t"
                 << *file
                 << "\nYou are not allowed to edit anything in the _arx directory.\n";
        }
      /* Make sure that the file is in the manifest.  Otherwise, we
         can end up with a corrupt tree. */
      else
        {
          dont_edit=true;
          if(checksums.empty())
            read_checksums(root,checksums);
          for(Checksums::iterator i=checksums.begin(); i!=checksums.end(); 
              ++i)
            {
              if(i->first.file_path.string()==relative_filename)
                {
                  dont_edit=false;
                  break;
                }
            }
          if(dont_edit && Command_Info::verbosity>=default_output)
            cerr << "This path is not in the manifest.  Not marking it for editing."
                 << "\n\t"
                 << (*file) << endl;
        }
      if(!dont_edit)
        {
          edit_path(root,relative_filename);
       }
    }
  return 0;
}
