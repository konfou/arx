/* A generic mechanism to get or set configuration variables (my-id, etc.)

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "get_config_option.hpp"
#include "set_config_option.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"

using namespace std;

int param(list<string> &argument_list, const command &cmd);

static command_initializer param_init(command("param",
"print or change an ArX parameter",
"usage: param [options] key [value]",
" -d --delete                   unspecify a parameter\n\
\n\
This command will print or change an ArX parameter.  There are several\n\
different possibilities for the key:\n\
\n\
id:\n\
      This parameter tells ArX who you are.  It is used to record who\n\
      has a lock on an archive and who commited a change to an archive.\n\
\n\
default-archive:\n\
      This parameter tells ArX what to assume as the archive\n\
      if no archive is specified.\n\
\n\
no-edit:\n\
      If set to true, then get will always use the --no-edit option.\n\
\n\
link-tree:\n\
      If set to true, then get will always use the --link-tree option.\n\
      WARNING: setting this option will make versioning of write permissions\n\
      (e.g. arx property arx:user-write) unreliable.\n\
\n\
browser:\n\
      This parameter is used when displaying HTML patch reports,\n\
      either directly with patch-report or indirectly with diff.\n\
\n\
guidiff:\n\
      This parameter is used for a nicer output with file-diff.\n\
\n\
gpg:\n\
      The name of the gpg executable along with any extra arguments.  You\n\
      can specify an agent to avoid typing your password all the time.  So\n\
      to use the agpg agent and turn off input from the terminal, set gpg to\n\
      \"agpg --no-tty\".\n\
\n\
gpg-key:\n\
      By default, all archives will be signed with this public key.\n\
\n\
With no value and without -d, print the value of KEY.\n\
\n\
With an argument, record KEY's value as VALUE.\n\
\n\
With the option -d (--delete) and no argument, unset KEY's value.",
param,"Basic",true));

int param(list<string> &argument_list, const command &cmd)
{
  bool delete_option(false);
  Command_Info info(cmd);

  bool done=false;
  while(!argument_list.empty() && !done)
    {
      if(!parse_common_options(argument_list,info))
        {
          list<string>::iterator arg=argument_list.begin();
          if(*arg=="-d" || *arg=="--delete")
            {
              delete_option=true;
              argument_list.pop_front();
            }
          else
            {
              parse_unknown_options(argument_list);
              done=true;
            }
        }
    }
  string key, value;

  if(argument_list.empty())
    {
      throw arx_error("Need at least one argument for param.");
    }
  else if(!argument_list.empty())
    {
      key=*argument_list.begin();
      argument_list.pop_front();
      if(key!="browser" && key!="default-archive"
         && key!="id" && key!="guidiff" && key!="no-edit" && key!="link-tree"
         && key!="sign" && key!="gpg" && key!="gpg-key")
        {
          throw arx_error("Bad key: " + key
                          + "\nThe key can only be one of\n\tbrowser\n\tdefault-archive\n\tid\n\tguidiff\n\tno-edit\n\tlink-tree\n\tgpg\n\tgpg-key");
        }
      if(argument_list.empty())
        {
          if(delete_option)
            set_config_option(key,value);
          else if(Command_Info::verbosity>=default_output)
            {
              value=get_config_option(key);
              if(value.empty())
                cout << "No value set for " << key << endl;
              else
                cout << value << endl;
            }
        }
      else
        {
          value=*argument_list.begin();
          argument_list.pop_front();
          check_extra_args(argument_list,info);
          set_config_option(key,value);
        }
    }
  return 0;
}
