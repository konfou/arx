/* Retrieve a patch set from an archive.  The heavy lifting is
   delegated to get_revision_patch.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include "get_revision_patch.hpp"
#include "boost/filesystem/operations.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int get_patch(list<string> &argument_list, const command &cmd);

static command_initializer get_patch_init(command("get-patch",
"Retrieve a patch from an archive",
"usage: get-patch [options] revision [dir]",
"Obtain the patch set for REVISION and store it in the DIR\n\
(or a directory named REVISION.patches).  DIR must not already exist.",
get_patch,"Advanced Patch",true));

int get_patch(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  path directory;

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        parse_unknown_options(argument_list);
        break;
      }

  /* Get the revision to look at. */
  
  Parsed_Name input_revision;
  if(!argument_list.empty())
    {
      input_revision=Parsed_Name(*(argument_list.begin()));
      argument_list.pop_front();

      if(!argument_list.empty())
        {
          directory=path(*(argument_list.begin()));
          argument_list.pop_front();
        }
    }
  else
    {
      throw arx_error("Need an argument for get-patch");
    }
  check_extra_args(argument_list,info);

  /* Now get the revisions. */

  get_revision_patch(input_revision,directory);

  return 0;
}
