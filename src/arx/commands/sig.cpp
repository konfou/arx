/* Manage cryptographic signatures

  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <list>
#include <string>
#include "boost/filesystem/operations.hpp"
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "Parsed_Name.hpp"
#include "add_key_to_archive.hpp"
#include "delete_key_from_archive.hpp"
#include "list_archive_keys.hpp"
#include "delete_signature.hpp"
#include "verify_signature.hpp"
#include "sign_revision.hpp"
#include "gvfs.hpp"
#include "get_config_option.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int sig(list<string> &argument_list, const command &cmd);

static command_initializer sig_init(command("sig",
"Manage cryptographic signatures",
"usage: sig [options] REVISION [...]",
" -a --add          Add your own signature\n\
 -d --delete       Remove signatures\n\
 --archive         Add your signature to the authorized list of signatures\n\
                   for an archive\n\
 --key KEY         Use KEY to add, delete, or list\n\
\n\
Manage the signatures in an archive.  Without any options, ArX will list\n\
and verify who signed REVISION.  With the --add option, it will sign\n\
REVISION with your private gpg key.  It will prompt for a passphrase\n\
if needed and overwrite any existing signature.  With the --delete option,\n\
the signature for REVISION is deleted.\n\
\n\
You may also specify only a branch, in which case ArX will recursively\n\
add, delete, or verify all of the revision of that branch and sub-branches.\n\
For example, if you have the branches\n\
\n\
  foo/bar\n\
  foo/bar.baz\n\
\n\
then\n\
\n\
  sig foo/bar\n\
\n\
will verify all of the revision in foo/bar and foo/bar.baz.\n\
\n\
With the --archive option, ArX will list, add, or delete a key from the\n\
archive.  When archive keys are changed, everyone must reregister the\n\
archive to update their keys.",
sig,"Administrative",true));

int sig(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);

  bool add(false), remove(false), archive(false);
  string gpg_key(get_config_option("gpg-key"));

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--add"
                || *(argument_list.begin())=="-a")
          {
            add=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--delete"
                || *(argument_list.begin())=="-d")
          {
            remove=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--archive")
          {
            archive=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--key")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --key");
              }
            gpg_key=*(argument_list.begin());
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }
  
  /* Make sure that we don't have an illegal combination of
     options. */
  if(add && remove)
    throw arx_error("Can't specify both --add and --delete");

  if((add || remove) && gpg_key.empty())
    throw arx_error("You must specify a key when adding or deleting keys.");
  
  /* Get the revisions to look at. */

  if(argument_list.empty())
    throw arx_error("Need at least one archive, branch, or revision");

  list<Parsed_Name> sig_list;

  while(!argument_list.empty())
    {
      sig_list.push_back(Parsed_Name(*(argument_list.begin())));
      argument_list.pop_front();
    }
  check_extra_args(argument_list,info);
            
  for(list<Parsed_Name>::iterator i=sig_list.begin(); i!=sig_list.end(); ++i)
    {
      /* Add, delete, and list archive keys. */
      if(archive)
        {
          if(add)
            {
              add_key_to_archive(*i,gpg_key);
            }
          else if(remove)
            {
              delete_key_from_archive(*i,gpg_key);
            }
          else
            {
              list_archive_keys(*i,gpg_key);
            }
          /* Update the list of public keys. */
          if(add || remove)
            {
              path home(getenv("HOME"));
              path location_path(home / ".arx/archives");
              gvfs::Init();
              gvfs::uri location(i->archive_location());
              if(gvfs::exists(location / ",meta-info/public_keys"))
                {
                  gvfs::copy(location / ",meta-info/public_keys",
                             (location_path/i->archive()/"public_keys")
                             .native_file_string());
                }
              else
                {
                  fs::remove(location_path/i->archive()/"public_keys");
                }
            }
        }
      /* Add, delete, and verify revision signatures. */
      else
        {
          if(add)
            {
              sign_revision(*i,gpg_key);
            }
          else if(remove)
            {
              delete_signature(*i);
            }
          else
            {
              verify_signature(*i);
            }
        }
    }
  return 0;
}
