/* Print out an inventory of a project tree.  This mostly parses
   options and then passes the work to inventory_directory and
   output_inventory.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "Inventory_Flags.hpp"
#include "inventory_directory.hpp"
#include "file_attributes.hpp"
#include <list>
#include "Inventory.hpp"
#include "read_checksums.hpp"
#include "tree_root.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void output_inventory(Inventory &inventory_result,
                             const Inventory_Flags &flags);

int inventory(list<string> &argument_list, const command &cmd);

static command_initializer inventory_init(command("inventory",
"Print out an inventory of a project tree",
"usage: inventory [options] [dir]",
" -S --source                   list source paths\n\
 -I --ignore                   list ignored paths\n\
 -U --unrecognized             list unrecognized paths\n\
 -T --trees                    list roots of nested trees\n\
\n\
 -D --directories              list directories\n\
 -F --files                    list non-directories\n\
 --kind                        show file kinds\n\
 --control                     include ArX control files\n\
 --nested                      include nested trees\n\
 --include-subdir              include subdirectories of ignore\n\
                               and unrecognized directories.\n\
\n\
 --ids                         list with inventory ids\n\
                               (only applies to source)\n\
\n\
With no arguments, print a human-readable inventory report.\n\
\n\
The options -D and -F, stack on each other.  Thus \"inventory -D -F\"\n\
is the same as \"inventory\".\n\
\n\
Subdirectories of ignore and unrecognized directories are not\n\
printed out unless --include-subdir is specified.",
inventory,"Miscellaneous Advanced",true));

int inventory(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool done=false;
  Inventory_Flags flags;
  int disambiguate=-1;

  Command_Info::verbosity=report;

  while(!argument_list.empty() && !done)
    {
      if(!parse_common_options(argument_list,info))
        {
          list<string>::iterator arg=argument_list.begin();
          if(*arg=="-S" || *arg=="--source")
            {
              flags.source=true;
              argument_list.pop_front();
              ++disambiguate;
            }
          else if (*arg=="-I" || *arg=="--ignore")
            {
              flags.ignore=true;
              argument_list.pop_front();
              ++disambiguate;
            }
          else if (*arg=="-U" || *arg=="--unrecognized")
            {
              flags.unrecognized=true;
              argument_list.pop_front();
              ++disambiguate;
            }
          else if (*arg=="-T" || *arg=="--trees")
            {
              flags.trees=true;
              argument_list.pop_front();
              ++disambiguate;
            }
          else if (*arg=="-D" || *arg=="--directories")
            {
              flags.directories=true;
              argument_list.pop_front();
            }
          else if (*arg=="-F" || *arg=="--files")
            {
              flags.files=true;
              argument_list.pop_front();
            }
          else if (*arg=="--kind")
            {
              flags.kind=true;
              argument_list.pop_front();
            }
          else if (*arg=="--control")
            {
              flags.control=true;
              argument_list.pop_front();
            }
          else if (*arg=="--nested")
            {
              flags.nested=true;
              argument_list.pop_front();
            }
          else if (*arg=="--ids")
            {
              flags.ids=true;
              argument_list.pop_front();
            }
          else if (*arg=="--include-subdir")
            {
              flags.include_subdir=true;
              argument_list.pop_front();
            }
          else
            {
              parse_unknown_options(argument_list);
              done=true;
            }
        }
    }
  
  path directory("");
  if(!argument_list.empty())
    {
      directory=path(*(argument_list.begin()));
      if(!exists(directory) || !is_directory(directory))
        {
          throw arx_error("Argument must be a directory: "
                          + *(argument_list.begin()));
        }
      argument_list.pop_front();
    }
  check_extra_args(argument_list,info);

  /* If nothing is specified, print out everything */
    
  if(Command_Info::verbosity>=report)
    {
      flags.disambiguate=false;
    }
  else
    {
      flags.disambiguate=disambiguate!=0;
    }

  if(!flags.source && !flags.ignore && !flags.unrecognized
     && !flags.trees)
    {
      flags.source=flags.ignore=flags.unrecognized=flags.trees=true;
      flags.disambiguate=false;
    }
  if(!flags.directories && !flags.files)
    {
      flags.directories=flags.files=true;
    }

  flags.regexes.set(directory);

  Inventory inventory_result;

  /* Traverse the directory */

  Checksums checksums;
  read_checksums(directory,checksums);

  path root(tree_root(directory));
  string prefix=fs::relative_path(directory,root).string();
  if(!prefix.empty())
    prefix+="/";
  inventory_directory(directory,inventory_result,flags,checksums,prefix);
  
  /* and print it all out */

  if(Command_Info::verbosity>=default_output)
    output_inventory(inventory_result,flags);

  return 0;
}
