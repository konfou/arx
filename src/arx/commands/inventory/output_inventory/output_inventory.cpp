/* This actually prints the inventory of a project tree

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <list>
#include <string>
#include "Inventory_Flags.hpp"
#include "file_attributes.hpp"
#include "inventory_types.hpp"
#include "Inventory.hpp"

using namespace std;

extern void output_category(Inventory &inventory_result,
                            const string &report_title,
                            const char &prefix,
                            const inventory_types &type,
                            const Inventory_Flags &flags,
                            bool nested_tree=false);

void output_inventory(Inventory &inventory_result,
                      const Inventory_Flags &flags)
{
  if(flags.source)
    output_category(inventory_result,"Source",'S',source,flags);

  if(flags.control)
    output_category(inventory_result,"Control",'S',control,flags);

  if(flags.ignore)
    output_category(inventory_result,"Ignore",'N',ignore,flags);

  if(flags.unrecognized)
    output_category(inventory_result,"Unrecognized",'U',unrecognized,flags);

  if(flags.trees && flags.directories)
    output_category(inventory_result,"Nested Project Trees:",'T',nested_tree,
                    flags,true);
}
