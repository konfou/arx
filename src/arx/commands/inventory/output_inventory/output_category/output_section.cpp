/* Prints out one section of an inventory (e.g. Source Files, Precious
   Directories, etc.)

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <vector>
#include <list>
#include <string>
#include "Inventory_Flags.hpp"
#include "file_attributes.hpp"
#include "inventory_types.hpp"
#include "arx_error.hpp"
#include <iostream>

using namespace std;

void output_section(list<file_attributes> &section,
                    const string &report_title,
                    const char &prefix,
                    const bool &is_file,
                    const inventory_types &type,
                    const Inventory_Flags &flags)
{
  if(Command_Info::verbosity>=report)
    {
      cout << endl << report_title << endl;
    }
  section.sort();

  for(list<file_attributes>::iterator i=section.begin(); i!=section.end(); ++i)
    {
      if(flags.disambiguate)
        {
          cout << prefix << " ";
        }
      if(flags.kind)
        {
          if(i->link)
            {
              cout << "> ";
            }
          else if(is_file)
            {
              cout << "r ";
            }
          else
            {
              cout << "d ";
            }
        }
      cout << i->file_path.native_file_string();
      if(flags.ids)
        if(type==source || type==control)
          {
            cout << "\t" << i->inventory_id;
          }
        else
          {
            cout << "\t???";
          }
      cout << endl;
    }
  if(Command_Info::verbosity>=report)
    {
      cout << endl;
    }

}
