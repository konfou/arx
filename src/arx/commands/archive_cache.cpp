/* Query, add or delete cached revisions in an archive

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "list_archive_cached_revisions.hpp"
#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "Parsed_Name.hpp"
#include "valid_package_name.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include <list>
#include <iostream>
#include "patch_level.hpp"
#include "boost/filesystem/operations.hpp"
#include "tree_root.hpp"
#include "get_option_from_file.hpp"
#include "latest_tree_revision.hpp"
#include "get_revision.hpp"
#include "Current_Path.hpp"
#include "../../config.h"
#include "gvfs.hpp"
#include "Temp_Directory.hpp"
#include "Spawn.hpp"
#include "tree_branch.hpp"
#include "recursively_add_tags.hpp"
#include "tempdir.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int archive_cache(list<string> &argument_list, const command &cmd);

static command_initializer cached_revision_init(command("archive-cache",
"Query, add or delete cached revisions in an archive",
"usage: archive-cache [options] [branch]\n\
   or: archive-cache [options] --add [revision]\n\
   or: archive-cache [options] --delete revision",
" -a --add             Add a cached revision to the archive\n\
 -d --delete          Delete a cached revision from the archive\n\
 --dir DIR            change to DIR first\n\
 --cache DIR          cache directory for locally cached revisions\n\
\n\
Without any options, print out a list of all cached revisions of\n\
the indicated branch.  If no revision is specified, it uses the output\n\
of \"tree-branch\".\n\
\n\
With --add, create a complete copy of REVISION and add it to the archive.\n\
If none is specified, use the latest revision in the project tree of the\n\
branch given by \"tree-branch\".\n\
\n\
With --delete, remove the indicated cached revision from the archive,\n\
freeing up space.",
archive_cache,"Administrative",true));

int archive_cache(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool remove(false), add(false);
  path cache_dir, tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="-d"
           || *(argument_list.begin())=="--delete")
          {
            remove=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="-a"
                || *(argument_list.begin())=="--add")
          {
            add=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--cache")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --cache");
              }
            cache_dir=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --dir");
              }
            tree_directory=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(cache_dir.empty())
    cache_dir=tree_directory;

  if(add && remove)
    throw arx_error("You may not both add and delete a cached revision.");

  /* Get the revision to cache. */
  
  Parsed_Name name;
  if(!argument_list.empty())
    {
      name=Parsed_Name(*(argument_list.begin()));
      argument_list.pop_front();
      if(add || remove)
        valid_package_name(name,Revision);
      else
        valid_package_name(name,Branch);        
    }
  else if(!remove)
    {
      if(add)
        name=latest_tree_revision(tree_directory);
      else
        name=tree_branch(tree_directory);
    }
  else
    {
      throw arx_error("You must explicitly specify a revision when deleting a cached revision.");
    }

  check_extra_args(argument_list,info);

  list<pair<Parsed_Name,path> > input_list;
  
  recursively_add_tags(input_list,name,tree_directory);

  for(list<pair<Parsed_Name,path> >::iterator i=input_list.begin();
      i!=input_list.end(); ++i)
    {
      const Parsed_Name current_branch=i->first;
  
      /* See if the revision is already there.  This is a little
         wasteful, since sometimes it lists all of the contents of a
         directory to see if one is there.  It may matter when you
         have a large number of revisions. */
      
      Revision_List cached_revisions(list_archive_cached_revisions(current_branch));
      /* Add a revision */
      if(add)
        {
          if(!cached_revisions.empty())
            {
              if(info.verbosity>=default_output)
                cerr << "This revision is already cached in the archive\n\t"
                     << current_branch.full_name() << endl;
            }
          else
            {
              Temp_Directory cache_revision_dir(tempdir(),",,cache-revision");
              get_revision(current_branch,cache_revision_dir.path/current_branch.revision(),
                           cache_dir);
              {
                Current_Path current(cache_revision_dir.path);
                
                Spawn s;
                s << ARXTAR << "-zcf" << (current_branch.revision()+".tar.gz")
                  << current_branch.revision();
                if(!s.execute(true))
                  {
                    throw arx_error("Error when tar'ing the full revision");
                  }
              }

              gvfs::Init();
              
              gvfs::uri location(current_branch.archive_location()
                                 /current_branch.branch_path_no_archive()
                                 / ",cache");
              if(!gvfs::exists(location))
                gvfs::make_directory(location);
              gvfs::copy(fs::system_complete
                         (cache_revision_dir.path/(current_branch.revision()+".tar.gz"))
                         .native_file_string(),
                         location / (current_branch.revision() + ".tar.gz"));
            }
        }
      /* Delete a revision */
      else if(remove)
        {
          if(cached_revisions.empty())
            {
              if(info.verbosity>=default_output)
                cerr << "This branch is not cached in the archive\n\t"
                     << current_branch.full_name() << endl;
            }
          else
            {
              gvfs::Init();
              gvfs::remove(current_branch.archive_location()
                           / current_branch.branch_path_no_archive()
                           / ",cache" / (current_branch.revision() + ".tar.gz"));
            }
        }
      else
        /* List the revisions */
        {
          if(info.verbosity>=default_output)
            for(Revision_List::iterator i=cached_revisions.begin();
                i!=cached_revisions.end(); ++i)
              cout << cached_revisions.name.complete_branch()
                   << patch_level(*i) << endl;
        }
    }

  return 0;
}
