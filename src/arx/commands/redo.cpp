/* Redo changes in working directory that were undone with undo.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002 Alexander Deruwe
   Copyright (C) 2003 Walter Landry and the Regents
                      of the University of California
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "do_patch.hpp"
#include "tree_root.hpp"
#include "boost/filesystem/operations.hpp"
#include <list>
#include <string>
#include "check_tree.hpp"
#include <iostream>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int redo(list<string> &argument_list, const command &cmd);

static command_initializer redo_init(command("redo",
"Redo changes in a working directory",
"usage: redo [options] [undodir]",
" --dir DIR             change to DIR first\n\
\n\
Redo changes that were undone with \"undo\".  If given, it uses\n\
the changes in UNDODIR.  Otherwise, it uses \",,undo.index\", with\n\
index being the highest found.\n\
\n\
See also \"arx undo --help\"",
redo,"Miscellaneous Advanced",true));

int redo(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  path undo_dir;
  path tree_directory(fs::current_path());

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--dir")
          {
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --dir");
              }
            tree_directory=path(*(argument_list.begin()));
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  /* Get the undo_dir */

  const path root(tree_root(tree_directory));

  if(!argument_list.empty())
    {
      undo_dir=*(argument_list.begin());
      argument_list.pop_front();
      if(!exists(undo_dir))
        {
          throw arx_error("The undo directory does not exist: "
                          + undo_dir.native_file_string());
        }
    }
  else
    {
      list<path> root_dir((fs::directory_iterator(root)),
                          fs::directory_iterator());

      string largest=",,undo";
      for(list<path>::iterator i=root_dir.begin(); i!=root_dir.end(); ++i)
        {
          if(i->leaf()!=largest
             && (i->leaf().size()>8 && i->leaf().substr(0,7)==",,undo."
                 && atoi(i->leaf().substr(8).c_str())
                 > atoi(largest.substr(8).c_str())))
            largest=i->leaf();
        }
      if(!exists(root/largest))
        throw arx_error("Can not find an undo directory in \n\t"
                        + root.native_file_string());
      undo_dir=root/largest;
    }

  check_extra_args(argument_list,info);

  {
    /* Abort if the tree is bad. */
    string warnings, errors;
    if(!check_tree(root,false,false,warnings,errors))
      {
        if(Command_Info::verbosity>default_output)
          cerr << warnings;
        if(Command_Info::verbosity>=quiet)
          cerr << errors;
        throw arx_error("Problems with the tree");
      }
  }

  /* Compute the patch between the pristine tree and the current tree. */

  Conflicts conflicts(do_patch(undo_dir,root,false,true));
  if(!conflicts.empty() && Command_Info::verbosity>=quiet)
    cerr << conflicts;
  fs::remove_all(undo_dir);

  return 0;
}
