/* Create and register an archive

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004,2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "get_config_option.hpp"
#include "make_archive.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int make_archive(list<string> &argument_list, const command &cmd);

static command_initializer make_archive_init(command("make-archive",
"Create and register an archive",
"usage: make-archive [options] name directory",
" --mirror                Make the location a mirror\n\
 --key KEY               Put KEY in the archive\n\
\n\
NAME is the global name for the archive.  You should choose something\n\
unique, such as your email address followed by something descriptive.\n\
\n\
An archive with that name must not already be registered.  It will create\n\
the directory if it does not already exist.\n\
\n\
With the --mirror option, the new location will be made a mirror of the\n\
master location.  To actually populate it, see \"arx mirror --help\".\n\
\n\
With the --key option, the new archive will have a copy of the public\n\
gpg key for KEY.  This may also be set with the gpg-key parameter in param,\n\
in which case you can make an unsigned archive by giving an empty argument\n\
to --key.\n\
\n\
See \"arx sig --help\" for details on how signed archives work.",
make_archive,"Basic",true));

int make_archive(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  string gpg_key(get_config_option("gpg-key"));
  bool set_key(false), mirror(false);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--mirror")
          {
            mirror=true;
            argument_list.pop_front();
          }
        else if(*(argument_list.begin())=="--key")
          {
            set_key=true;
            argument_list.pop_front();
            if(argument_list.empty())
              {
                throw arx_error("Need an argument for --key");
              }
            gpg_key=*(argument_list.begin());
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(mirror && set_key)
    throw arx_error("You may not specify both --mirror and --key");

  if(argument_list.size()<2)
    {
      throw arx_error ("Not enough arguments");
    }
  string name(*(argument_list.begin()));
  argument_list.pop_front();

  string directory(*(argument_list.begin()));
  argument_list.pop_front();
  check_extra_args(argument_list,info);

  make_archive(name,directory,mirror,gpg_key);

  return 0;
}
