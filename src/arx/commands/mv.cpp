/* Move files and, if they have them, their ids.  If there are no
   ids, it still moves them.  The intention is to be able to
   transparently replace "mv" with "arx move".

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "parse_common_options.hpp"
#include "parse_unknown_options.hpp"
#include "check_extra_args.hpp"
#include "command_initializer.hpp"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "atomic_move.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

int mv(list<string> &argument_list, const command &cmd);

static command_initializer mv_init(command("mv",
"Rename a path and its inventory id.",
"usage: mv [options] source target\n   or: mv [options] source... directory",
" --id             only move the inventory id, not the actual path\n\
\n\
Rename a file or directory and its explicit inventory id.  Normally, The\n\
semantics should be equivalent to mv(1).\n\
\n\
This command can also move paths between project trees.\n\
\n\
The --id option only moves the inventory id, and it only works\n\
within a project tree.\n\
\n\
WARNING: This command will not work properly if you move paths\n\
through symlinks.",
mv,"Basic",true));

int mv(list<string> &argument_list, const command &cmd)
{
  Command_Info info(cmd);
  bool id_only(false);

  while(!argument_list.empty())
    if(!parse_common_options(argument_list,info))
      {
        if(*(argument_list.begin())=="--id")
          {
            id_only=true;
            argument_list.pop_front();
          }
        else
          {
            parse_unknown_options(argument_list);
            break;
          }
      }

  if(argument_list.size()<2)
    {
      throw arx_error("Need at least two arguments");
    }
  else if(argument_list.size()==2)
    /* Moving a file to a file, a file to a directory, or a directory
       to a directory. */
    {
      list<string>::iterator arg(argument_list.begin());
      path source(*arg);
      ++arg;
      path target(*arg);
      atomic_move(source,target,id_only);
    }
  else
    /* Moving a bunch of files and/or directories into another directory. */
    {
      if(id_only)
        throw arx_error("Only two arguments allowed with the --id option");

      list<string>::iterator destination=argument_list.end();
      --destination;
      if(!(exists(path(*destination)) && is_directory(path(*destination))))
        {
          throw arx_error("when moving multiple files, last argument must\nbe an existing directory");
        }
      else
        {
          for(list<string>::iterator i=argument_list.begin();
              i!=destination;++i)
            {
              atomic_move(*i,*destination,id_only);
            }
        }
    }
  return 0;
}
