%module arx
%{
#include "Branch_Lock.hpp"
#include "Checksums.hpp"
#include "Command_Info.hpp"
#include "Component_Regexes.hpp"
#include "Conflicts.hpp"
#include "Current_Path.hpp"
#include "Inventory.hpp"
#include "Inventory_Flags.hpp"
#include "Inventory_Regexes.hpp"
#include "Merge_Algo.hpp"
#include "Parsed_Name.hpp"
#include "Patch_Log.hpp"
#include "Path_List.hpp"
#include "Revision_List.hpp"
#include "Spawn.hpp"
#include "Temp_Directory.hpp"
#include "Temp_Path.hpp"
#include "Temp_File.hpp"
#include "Verbosity.hpp"
#include "add_key_to_archive.hpp"
#include "add_path_and_checksum.hpp"
#include "add_path_list.hpp"
#include "add_path_to_list.hpp"
#include "add_tree_cache.hpp"
#include "arx_error.hpp"
#include "atomic_move.hpp"
#include "check_extra_args.hpp"
#include "check_if_local_path.hpp"
#include "check_symlink_hierarchy.hpp"
#include "check_tree.hpp"

#include "command_initializer.hpp"
#include "commit_revision.hpp"
#include "compute_latest_continuation.hpp"
#include "consolidate_manifest.hpp"
#include "convert_inventory_to_checksums.hpp"
#include "copy_tree.hpp"
#include "create_archive_registration.hpp"
#include "delete_archive_registration.hpp"
#include "delete_element.hpp"
#include "delete_key_from_archive.hpp"
#include "delete_signature.hpp"
#include "do_patch.hpp"
#include "edit_path.hpp"
#include "ensure_branch_exists_in_archive.hpp"
#include "ensure_key_in_archive.hpp"
#include "escape_string.hpp"
#include "file_attributes.hpp"
#include "fill_path_list.hpp"
#include "find_closest_revision.hpp"
#include "find_new_and_removed_patches.hpp"
#include "find_or_make_pristine.hpp"
#include "fork.hpp"
#include "get_archived_revision.hpp"
#include "get_config_option.hpp"
#include "get_local_revision.hpp"
#include "get_option_from_file.hpp"
#include "get_revision.hpp"
#include "get_revision_patch.hpp"
#include "gvfs.hpp"
#include "init.hpp"
#include "inventory_directory.hpp"
#include "inventory_types.hpp"
#include "invoke_hook.hpp"
#include "is_a_tag.hpp"
#include "is_continuation.hpp"
#include "is_revision.hpp"
#include "key_location.hpp"
#include "latest_archive_revision.hpp"
#include "latest_tree_revision.hpp"
#include "list_archive_cached_revisions.hpp"
#include "list_archive_keys.hpp"
#include "list_archive_revisions.hpp"
#include "list_local_cached_revisions.hpp"
#include "list_patch_logs.hpp"
#include "list_tree_branches.hpp"
#include "list_tree_cached_revisions.hpp"
#include "make_non_writeable.hpp"
#include "make_archive.hpp"
#include "make_patch.hpp"
#include "match_uri_fragment.hpp"
#include "merge_branches.hpp"
#include "output_changelog.hpp"
#include "package_level.hpp"
#include "parse_common_options.hpp"
#include "parse_rfc822.hpp"
#include "parse_unknown_options.hpp"
#include "patch_level.hpp"
#include "patch_level_cmp.hpp"
#include "patch_number.hpp"
#include "property_action.hpp"
#include "push_branch_on_list.hpp"
#include "push_revision_on_list.hpp"
#include "read_and_sort_index.hpp"
#include "read_archive_locations.hpp"
#include "read_checksums.hpp"
#include "read_config.hpp"

#include "reconstitute_manifest.hpp"
#include "recursive_browse.hpp"
#include "recursively_add_tags.hpp"
#include "registered_archive.hpp"
#include "remote_recursive_browse.hpp"
#include "remote_browse.hpp"
#include "remove_path_from_checksums.hpp"
#include "set_config_option.hpp"
#include "set_option_in_file.hpp"
#include "sha256.hpp"
#include "signed_archive.hpp"
#include "tar_and_archive_patch.hpp"
#include "tree_branch.hpp"
#include "tree_root.hpp"
#include "undo.hpp"
#include "unsafe_delete_inventory_id.hpp"
#include "unsafe_move_inventory_id.hpp"
#include "valid_package_name.hpp"
#include "verify_manifest_checksum.hpp"
#include "verify_signature.hpp"
#include "verify_tag.hpp"
#include "write_archive_locations.hpp"
#include "write_manifest.hpp"
#include "write_property_diffs.hpp"
#include "xdelta.hpp"
%}

%include "std_string.i"
%include "std_list.i"
%include "std_map.i"

# We have to include our own version of boost/filesystem/path.hpp
# because the included definition has a nested class.


namespace boost
{
  namespace filesystem
  {
    class directory_iterator;


  //  path -------------------------------------------------------------------//

    class path
    {
    public:
      typedef bool (*name_check)( const std::string & name );

      // compiler generates copy constructor, copy assignment, and destructor

      path(){}

      path( const std::string & src );
      path( const char * src );

      path( const std::string & src, name_check checker );
      path( const char * src, name_check checker );

      // append operations:
      path & operator /=( const path & rhs );
      path operator /( const path & rhs ) const
        { return path( *this ) /= rhs; }

      // modification functions:
      path & normalize();

      // conversion functions:
      const std::string & string() const { return m_path; }
      std::string native_file_string() const;
      std::string native_directory_string() const;

      // decomposition functions:
      path         root_path() const;
      std::string  root_name() const;
      std::string  root_directory() const;
      path         relative_path() const;
      std::string  leaf() const;
      path         branch_path() const;

      // query functions:
      bool empty() const { return m_path.empty(); } // name consistent with std containers

      bool is_complete() const;

      bool has_root_path() const;
      bool has_root_name() const;
      bool has_root_directory() const;
      bool has_relative_path() const;
      bool has_leaf() const { return !m_path.empty(); }
      bool has_branch_path() const;

      // default name_check mechanism:
      static bool default_name_check_writable(); 
      static void default_name_check( name_check new_check );
      static name_check default_name_check();

    private:
      // Note: This is an implementation for POSIX and Windows, where there
      // are only minor differences between generic and system-specific
      // constructor input formats.  Private members might be quite different
      // in other implementations, particularly where there were wide
      // differences between generic and system-specific argument formats,
      // or between native_file_string() and native_directory_string() formats.

      std::string  m_path;

      friend class directory_iterator;
      // Was qualified; como433beta8 reports:
      //    warning #427-D: qualified name is not allowed in member declaration 
    public: // should be private, but friend functions don't work for me
      void m_path_append( const std::string & src, name_check checker );
      void m_replace_leaf( const char * new_leaf );
    };

  //  path non-member functions  ---------------------------------------------//

    inline path operator / ( const char * lhs, const path & rhs )
      { return path( lhs ) /= rhs; }

    inline path operator / ( const std::string & lhs, const path & rhs )
      { return path( lhs ) /= rhs; }
   
  //  path::name_checks  ---------------------------------------------------//

    bool portable_posix_name( const std::string & name );
    bool windows_name( const std::string & name );
    bool portable_name( const std::string & name );
    bool portable_directory_name( const std::string & name );
    bool portable_file_name( const std::string & name );
    bool no_check( const std::string & name );   // always returns true
    bool native( const std::string & name );
      // native(name) must return true for any name which MIGHT be valid
      // on the native platform.

  } // namespace filesystem
} // namespace boost


%template(attrlist) std::list<file_attributes>;
%template(ppsssslist) std::list<std::pair<std::pair<std::string,std::string>,
                      std::pair<std::string,std::string> > >;
%template(pspsslist) std::list<std::pair<std::string,
                                         std::pair<std::string,
                                                   std::string> > >;
%template(psslist) std::list<std::pair<std::string,std::string> >;

%ignore do_patch(const boost::filesystem::path&, const boost::filesystem::path&, const bool&, const bool&);
%ignore do_patch(const boost::filesystem::path&, const boost::filesystem::path&, const bool&, const bool&, bool);

%include "Branch_Lock.hpp"
%include "Checksums.hpp"
%include "Command_Info.hpp"
%include "Component_Regexes.hpp"
%include "Conflicts.hpp"
%include "Current_Path.hpp"
%include "Inventory.hpp"
%include "Inventory_Flags.hpp"
%include "Inventory_Regexes.hpp"
%include "Merge_Algo.hpp"
%include "Parsed_Name.hpp"
%include "Patch_Log.hpp"
%include "Path_List.hpp"
%include "Revision_List.hpp"
%include "Spawn.hpp"
%include "Temp_Directory.hpp"
%include "Temp_Path.hpp"
%include "Temp_File.hpp"
%include "Verbosity.hpp"
%include "add_key_to_archive.hpp"
%include "add_path_and_checksum.hpp"
%include "add_path_list.hpp"
%include "add_path_to_list.hpp"
%include "add_tree_cache.hpp"
%include "arx_error.hpp"
%include "atomic_move.hpp"
%include "check_extra_args.hpp"
%include "check_if_local_path.hpp"
%include "check_symlink_hierarchy.hpp"
%include "check_tree.hpp"

%include "command_initializer.hpp"
%include "commit_revision.hpp"
%include "compute_latest_continuation.hpp"
%include "consolidate_manifest.hpp"
%include "convert_inventory_to_checksums.hpp"
%include "copy_tree.hpp"
%include "create_archive_registration.hpp"
%include "delete_archive_registration.hpp"
%include "delete_element.hpp"
%include "delete_key_from_archive.hpp"
%include "delete_signature.hpp"
%include "do_patch.hpp"
%include "edit_path.hpp"
%include "ensure_branch_exists_in_archive.hpp"
%include "ensure_key_in_archive.hpp"
%include "escape_string.hpp"
%include "file_attributes.hpp"
%include "fill_path_list.hpp"
%include "find_closest_revision.hpp"
%include "find_new_and_removed_patches.hpp"
%include "find_or_make_pristine.hpp"
%include "fork.hpp"
%include "get_archived_revision.hpp"
%include "get_config_option.hpp"
%include "get_local_revision.hpp"
%include "get_option_from_file.hpp"
%include "get_revision.hpp"
%include "get_revision_patch.hpp"
%include "gvfs.hpp"
%include "init.hpp"
%include "inventory_directory.hpp"
%include "inventory_types.hpp"
%include "invoke_hook.hpp"
%include "is_a_tag.hpp"
%include "is_continuation.hpp"
%include "is_revision.hpp"
%include "key_location.hpp"
%include "latest_archive_revision.hpp"
%include "latest_tree_revision.hpp"
%include "list_archive_cached_revisions.hpp"
%include "list_archive_keys.hpp"
%include "list_archive_revisions.hpp"
%include "list_local_cached_revisions.hpp"
%include "list_patch_logs.hpp"
%include "list_tree_branches.hpp"
%include "list_tree_cached_revisions.hpp"
%include "make_non_writeable.hpp"
%include "make_archive.hpp"
%include "make_patch.hpp"
%include "match_uri_fragment.hpp"
%include "merge_branches.hpp"
%include "output_changelog.hpp"
%include "package_level.hpp"
%include "parse_common_options.hpp"
%include "parse_rfc822.hpp"
%include "parse_unknown_options.hpp"
%include "patch_level.hpp"
%include "patch_level_cmp.hpp"
%include "patch_number.hpp"
%include "property_action.hpp"
%include "push_branch_on_list.hpp"
%include "push_revision_on_list.hpp"
%include "read_and_sort_index.hpp"
%include "read_archive_locations.hpp"
%include "read_checksums.hpp"
%include "read_config.hpp"

%include "reconstitute_manifest.hpp"
%include "recursive_browse.hpp"
%include "recursively_add_tags.hpp"
%include "registered_archive.hpp"
%include "remote_recursive_browse.hpp"
%include "remote_browse.hpp"
%include "remove_path_from_checksums.hpp"
%include "set_config_option.hpp"
%include "set_option_in_file.hpp"
%include "sha256.hpp"
%include "signed_archive.hpp"
%include "tar_and_archive_patch.hpp"
%include "tree_branch.hpp"
%include "tree_root.hpp"
%include "undo.hpp"
%include "unsafe_delete_inventory_id.hpp"
%include "unsafe_move_inventory_id.hpp"
%include "valid_package_name.hpp"
%include "verify_manifest_checksum.hpp"
%include "verify_signature.hpp"
%include "verify_tag.hpp"
%include "write_archive_locations.hpp"
%include "write_manifest.hpp"
%include "write_property_diffs.hpp"
%include "xdelta.hpp"
