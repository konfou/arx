/* Output a brief help command for all of the commands.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include <list>
#include <iostream>
#include "command.hpp"
#include "Singleton.h"
#include <map>

typedef Loki::SingletonHolder<std::list<command> > cmd_list;

using namespace std;

bool command_group_ordering(const command &c1, const command &c2)
{
  map<string,int> ordering;
  ordering["Basic"]=1;
  ordering["Branching and Merging"]=2;
  ordering["Miscellaneous Advanced"]=3;
  ordering["Advanced Patch"]=4;
  ordering["Administrative"]=5;

  if(ordering[c1.command_group] < ordering[c2.command_group])
    {
      return true;
    }
  else if(c1.command_group == c2.command_group)
    {
      return c1.name < c2.name;
    }
  return false;
}

void help_commands()
{
  cmd_list::Instance().sort(command_group_ordering);
  const int indentation=18;
  string group;

  cout << "                        arx sub-commands\n"
       << "                        ----------------\n";
  for(list<command>::iterator i=cmd_list::Instance().begin();
      i!=cmd_list::Instance().end(); ++i)
    {
      if(i->visible_by_default)
        {
          if(group!=i->command_group)
            {
              group=i->command_group;
              cout << "\n* " << group << " Commands\n\n";
            }
          cout << "   " << i->name;
          for(unsigned int j=0;j<(indentation - i->name.size());++j)
            cout << " ";
          cout << i->brief_help << "\n";
        }
    }
}

