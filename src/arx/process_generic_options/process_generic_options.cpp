/* Process generic options (--help, --version, etc.).  If there are no
   arguments, then print out a usage command.  Return false if the
   caller shouldn't process any more arguments.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <list>
#include <string>
#include <iostream>
#include "Spawn.hpp"

extern void generic_help();
extern void version_info();
extern void help_commands();

using namespace std;

bool process_generic_options(const list<string> &argument_list)
{
  list<string>::const_iterator arg=argument_list.begin();
  if(argument_list.empty() || *arg=="--help" || *arg=="-h" || *arg=="-H")
    {
      generic_help();
      return false;
    }
  else if(*arg=="--version" || *arg=="-V")
    {
      version_info();
      return false;
    }
  else if((*arg).empty())
    {
      cerr << "empty arguments are not allowed" << endl;
      exit(1);
    }
  else if(*(*arg).begin()=='-')
    {
      cerr << "unknown option: " << *argument_list.begin() << endl;
      exit(1);
    }
  return true;
}
