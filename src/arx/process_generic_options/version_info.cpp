/* Output the version info for ArX

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "../../config.h"
#include <iostream>

using namespace std;

void version_info()
{
  cout << PACKAGE_STRING << endl
       << "Built " << __TIME__ << " " << __DATE__ << " "
#ifdef ARXGPG
       << "with gpg support" << endl
#else
       << "without gpg support" << endl
#endif
       << endl <<
"Copyright 2001-2005 by various contributors.  See CREDITS for details.\n\
\n\
This is free software; see the source for copying conditions.\n\
There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\n\
PARTICULAR PURPOSE.\n\
\n\
Report bugs to <arx-users@nongnu.org>." << endl;
}

