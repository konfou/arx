/* The generic help message for ArX

   Copyright 2003, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <iostream>
#include "command_initializer.hpp"
#include "parse_unknown_options.hpp"
#include "arx_error.hpp"

using namespace std;

void generic_help();

int help(list<string> &argument_list, const command &cmd)
{
  while(!argument_list.empty())
    {
      parse_unknown_options(argument_list);
      break;
    }
  if(!argument_list.empty())
    {
      string subcommand=*argument_list.begin();
      
      /* If we've implemented this command in C++, then run its
         help */
      list<command>::iterator
        action=find(cmd_list::Instance().begin(),
                    cmd_list::Instance().end(),subcommand);
      if(action!=cmd_list::Instance().end())
        {
          list<string> new_arguments;
          new_arguments.push_back("-H");
          action->eval(new_arguments);
        }
      else
        throw arx_error("No such command: " + subcommand);
    }
  else
    {
      generic_help();
    }
  return 0;
}

static command_initializer help_init(command("help",
"Print out a generic help message",
"usage: help",
"",help,"Basic",false));

extern void version_info();
extern void help_commands();

void generic_help()
{
  version_info();
  cout << "usage: arx command [options] [arguments]\n\n";
  help_commands();
  cout << endl <<
"All commands take the following options:\n\
 -h -H --help                  print a help message specific to that command\n\
 -v                            increase the amount of output\n\
 -q                            decrease the amount of output\n\
 --                            mark the end of options\n\
\n\
The -- option is useful for explicitly ending the list of options. This\n\
is useful if a filename, for example, might be mistaken for an option.\n\
\n\
The -v and -q options increase or decrease the amount of output.  Repeating\n\
the option (e.g. -v -v) will make ArX even more/less chatty.\n";
}
