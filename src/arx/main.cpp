/* The is the main file for ArX, a distributed revision control
   system. 

   Copyright 2001, 2002 Tom Lord
   Copyright 2002, 2003 Walter Landry and the Regents of the
   University of California.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include <list>
#include <cstdio>
#include <string>
#include <cassert>
#include <iostream>
#include "command.hpp"
#include "arx_error.hpp"
#include "Singleton.h"
#include "boost/filesystem/path.hpp"
#include "boost/filesystem/exception.hpp"
#include "boost/archive/archive_exception.hpp"
#include "gvfs.hpp"

using namespace std;

Verbosity Command_Info::verbosity(default_output);

extern bool process_generic_options(const list<string> &argument_list);

/* A list of commands that files can independently add themselves to. */

typedef Loki::SingletonHolder<list<command> > cmd_list;

int main(int argv, char *argc[])
{
  /* Never check for valid filenames */
  boost::filesystem::path::default_name_check(boost::filesystem::no_check);

  list<string> argument_list;
  for(int i=1;i<argv;++i)
    {
      argument_list.push_back(string(argc[i]));
    }

  /* Process generic options (--help, --version, etc.).  If there are
     arguments but no such options, then continue. */

  if(process_generic_options(argument_list))
    {
      /* Now actually do the command */
      assert(!argument_list.empty());
      string subcommand=*argument_list.begin();
      
      /* If we've implemented this command in C++, then do it */
      list<command>::iterator action=find(cmd_list::Instance().begin(),
                                          cmd_list::Instance().end(),subcommand);

      if(action!=cmd_list::Instance().end())
        {
          /* Remove the command argument */
          argument_list.pop_front();

          char * no_exceptions=getenv("ARX_NO_EXCEPTIONS");
          if(no_exceptions!=NULL)
            {
              exit(action->eval(argument_list));
            }
          else
            {
              try {
                exit(action->eval(argument_list));
              }
              catch (arx_error &err)
                {
                  if(Command_Info::verbosity>silent)
                    cerr << "ERROR: " << err.what() << endl;
                  exit(err.error_code);
                }
              catch (boost::filesystem::filesystem_error &fs_error)
                {
                  if(Command_Info::verbosity>silent)
                    cerr << "ERROR: Problems with a filesystem call\n"
                         << fs_error.who() + "\n"
                         << fs_error.path1().native_file_string() + "\n"
                         << fs_error.path2().native_file_string() + "\n";
                  exit(3);
                }
              catch (boost::archive::archive_exception &err)
                {
                  if(Command_Info::verbosity>silent)
                    cerr << "ERROR: Problems with an serialization call\n"
                         << err.what() << "\n";
                  exit(4);
                }
              catch (gvfs::exception &gvfs_err)
                {
                  if(Command_Info::verbosity>silent)
                    cerr << "ERROR: Problems with a Gnome-VFS call\n"
                         << gvfs_err.what() << "\n"
                         << gvfs_err.uri1.string() << "\n"
                         << gvfs_err.uri2.string() << "\n";
                }
              catch (std::exception &err)
                {
                  if(Command_Info::verbosity>silent)
                    cerr << "ERROR: Unhandled exception\n"
                         << err.what() << "\n";
                  exit(5);
                }
            }
        }
      else
        {
          cerr << "No such command: "
               << subcommand
               << endl;
          exit(1);
        }
    }
  exit(0);
}

