/* Generate a new ++manifest file from a list of files in both trees,
   files only in the new or old tree, and the old manifest.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "Checksums.hpp"
#include "Parsed_Name.hpp"
#include "write_manifest.hpp"
#include "read_checksums.hpp"
#include "remove_path_from_checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void generate_new_manifest(const path &new_manifest_path,
                           const path &root,
                           const Parsed_Name &name,
                           const Checksums &orig_only,
                           const Checksums &mod_only,
                           const list<pair<pair<file_attributes,sha256>,
                           pair<file_attributes,sha256> > > &in_both)
{
  /* First read the old manifest */

  Checksums new_manifest;
  if(!root.empty())
    read_checksums(root,new_manifest);
  new_manifest.name=name;
  
  /* Now modify it by deleting, updating, and adding elements */

  for(Checksums::const_iterator i=orig_only.begin(); i!=orig_only.end(); ++i)
    remove_path_from_checksums(new_manifest,i->first);

  for(list<pair<pair<file_attributes,sha256>,pair<file_attributes,sha256> > >
        ::const_iterator k=in_both.begin(); k!=in_both.end(); ++k)
    {
      Checksums::iterator j(new_manifest.end());
      for(Checksums::iterator i=new_manifest.begin();
          i!=new_manifest.end(); ++i)
        {
          if(i->first.inventory_id==k->first.first.inventory_id)
            {
              j=i;
              break;
            }
        }
      
      /* Sanity checks */
      if(j==new_manifest.end())
        throw arx_error("INTERNAL ERROR: The patch contains an update for a path whose inventory id is not in the manifest.\n\toriginal path: "
                        + k->first.first.file_path.native_file_string()
                        + "\n\tdestination path: "
                        + k->second.first.file_path.native_file_string()
                        + "\n\tinventory id: "
                        + k->first.first.inventory_id);
      if(j->first.file_path.string()!=k->first.first.file_path.string())
        throw arx_error("INTERNAL ERROR: The patch contains a rename for a path with a particular\ninventory id, but the inventory id and path do not match in the manifest.\npath in manifest: "
                        + j->first.file_path.native_file_string()
                        + "\n\toriginal path in the patch: "
                        + k->first.first.file_path.native_file_string()
                        + "\n\tdestination path in ++changes: "
                        + k->second.first.file_path.native_file_string()
                        + "\n\tinventory id: "
                        + k->second.first.inventory_id);
      
      /* Actually replaces the element in the list */
      *j=k->second;
    }

  for(Checksums::const_iterator i=mod_only.begin(); i!=mod_only.end(); ++i)
    new_manifest.push_back(*i);

  /* Finally, write the new manifest. */

  write_manifest(new_manifest_path,new_manifest);
}
