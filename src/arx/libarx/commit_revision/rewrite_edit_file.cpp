/* Rewrite the ++edit file with the contents of a new path list.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "Path_List.hpp"
#include "Temp_File.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void rewrite_edit_file(const path &root, const Path_List &path_list)
{
  Temp_File edit_path(root,",,edit");
  fs::ofstream edit_file(edit_path.path);
  {
    archive::text_oarchive edit_archive(edit_file);
  }
  edit_file << " ";
  
  archive::text_oarchive edit_archive(edit_file,archive::no_header);
  
  for(Path_List::const_iterator i=path_list.begin(); i!=path_list.end(); ++i)
    edit_archive << i->string();

  if(rename(edit_path.path.native_file_string().c_str(),
            (root/"_arx/++edit").native_file_string().c_str())!=0)
    throw arx_error("Can't rename " + edit_path.path.native_file_string()
                    + " to "
                    + (root/"_arx/++edit").native_file_string());
}
