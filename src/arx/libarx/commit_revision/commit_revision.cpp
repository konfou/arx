/* Do the actual work of committing the changes to a tree to the archive

  Copyright (C) 2001, 2002, 2003 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/filesystem/exception.hpp"
#include "boost/filesystem/convenience.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "Temp_Directory.hpp"
#include <map>
#include "tree_root.hpp"
#include "Revision_List.hpp"
#include "list_archive_revisions.hpp"
#include "patch_level.hpp"
#include "get_option_from_file.hpp"
#include "set_option_in_file.hpp"
#include "get_config_option.hpp"
#include "Checksums.hpp"
#include "make_patch.hpp"
#include "do_patch.hpp"
#include "get_revision.hpp"
#include "ensure_branch_exists_in_archive.hpp"
#include "tar_and_archive_patch.hpp"
#include "gvfs.hpp"
#include "find_new_and_removed_patches.hpp"
#include "Parsed_Name.hpp"
#include "valid_package_name.hpp"
#include "inventory_types.hpp"
#include "Temp_File.hpp"
#include "consolidate_manifest.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/serialization/map.hpp"
#include "boost/serialization/list.hpp"
#include "add_path_and_checksum.hpp"
#include "make_non_writeable.hpp"
#include "compute_latest_continuation.hpp"
#include "fill_path_list.hpp"
#include "is_a_tag.hpp"

using namespace std;
using namespace boost;
using namespace boost::posix_time;
namespace fs=boost::filesystem;
using fs::path;

namespace {
  void remove_header(map<string,string> &headers, const string &field)
  {
    map<string,string>::iterator i=headers.find(field);
    if(i!=headers.end())
      headers.erase(i);
  }
}

extern void fill_log(map<string,list<string> > &file_headers,
                     const string &field,
                     Checksums &checksums, const int &type);

extern void generate_new_manifest(const path &new_manifest_path,
                                  const path &root,
                                  const Parsed_Name &name,
                                  const Checksums &orig_only,
                                  const Checksums &mod_only,
                                  const list<pair<pair<file_attributes,sha256>,
                                  pair<file_attributes,sha256> > > &in_both);

extern void rewrite_edit_file(const path &root, const Path_List &path_list);

void commit_revision(const path &tree_directory, Parsed_Name name,
                     const Path_List &path_list,
                     map<string,string> &log_headers, const string &log_body,
                     const bool &unchanged_ok, const bool &out_of_date_ok,
                     const bool &partial_commit, const string &gpg_key)
{
  if(!name.revision().empty())
    throw arx_error("INTERNAL ERROR: commit_revision passed a revision, not a branch\n\t" + name.full_name());

  if(!Conflicts::all_resolved(tree_directory))
    throw arx_error("There are unresolved conflicts in the tree.  See the \"resolve\" command.");

  /* Remove any of these headers that might be present in the
     log file since ArX will compute what should go into these
     things. */
  remove_header(log_headers,"Removed-files");
  remove_header(log_headers,"Removed-directories");
  remove_header(log_headers,"New-files");
  remove_header(log_headers,"New-directories");
  remove_header(log_headers,"Modified-files");
  remove_header(log_headers,"Modified-directories");
  remove_header(log_headers,"Renamed-files");
  remove_header(log_headers,"Renamed-directories");
  remove_header(log_headers,"New-patches");
  remove_header(log_headers,"Continuation-of");

  gvfs::Init();
  
  ensure_branch_exists_in_archive(name);
  
  path root(tree_root(tree_directory));
  
  /* If doing an initial import, do that. */
  
  path pristine_path;
  bool branching(false);
  Parsed_Name previous;
  if(lexists(root / "_arx/++import"))
    {
      name.set_revision(0);
    }
  else
    {
      Revision_List revision_list(list_archive_revisions(name));
      
      unsigned int latest_level(UINT_MAX), next_level(UINT_MAX);
      
      if(!revision_list.empty())
        {
          latest_level=*(revision_list.rbegin());
          next_level=latest_level+1;
        }
      else
        {
          next_level=0;
        }
      
      name.set_revision(next_level);
      
      /* Check whether we're branching. */
      
      if(lexists(root/"_arx/++branch-revision"))
        {
          previous=Parsed_Name(get_option_from_file
                               (root/"_arx/++branch-revision"));
          branching=true;
          log_headers["Continuation-of"]=previous.complete_revision();
        }
      else
        {
          if(revision_list.empty())
            throw arx_error("INTERNAL ERROR: no revisions for this branch found in the archive\n\t" + name.complete_branch());
          
          previous=Parsed_Name(name).set_revision(latest_level);
        }
      
      /* Parse the previous revision */
      
      valid_package_name(previous,Revision);
      
      /* Make sure that the previous revision was not a tag. */
      
      if(is_a_tag(previous))
        throw arx_error("You may not commit to a tag\n\t"
                        + previous.complete_revision());
      
      /* Find a tree to diff against. */
      
      pristine_path=(root/"_arx/++cache")/ previous.revision_path();
      if(!lexists(pristine_path))
        {
          create_directories(pristine_path.branch_path());
          get_revision(previous,pristine_path,fs::current_path());
        }
    }
  
  /* Diff the two trees and make sure something actually happened. */
  Temp_Directory commit_dir(tree_directory,",,commit");
  path patch_dir(commit_dir.path/(name.revision() + ".patches"));
  
  list<pair<pair<file_attributes,sha256>,pair<file_attributes,sha256> > > 
    in_both;
  Checksums patched, orig_only, mod_only;
  
  if(!make_patch(pristine_path,root,patch_dir,path_list,
                 orig_only,mod_only,patched,in_both)
     && !(unchanged_ok || branching))
    {
      remove_all(patch_dir);
      throw arx_error("Nothing has changed from the previous revision.  Try --unchanged-ok.");
    }
  
  /* Fill all of the necessary log fields */
  
  log_headers["Revision"]=name.revision();
  log_headers["Archive"]=name.archive();
  /* We don't change these headers, since they may have been set
     for a reason.  They are only informative anyway, not really
     needed for most operations (except changelog). */
  if(log_headers.find("Creator")==log_headers.end())
    log_headers["Creator"]=get_config_option("id");
  if(log_headers.find("Date")==log_headers.end())
    log_headers["Date"]=to_simple_string(second_clock::local_time());
  if(log_headers.find("Standard-date")==log_headers.end())
    log_headers["Standard-date"]=
      to_simple_string(second_clock::universal_time());
  
  map<string,list<string> > header_lists;
  fill_log(header_lists,"Removed-files",orig_only,arx_file);
  fill_log(header_lists,"Removed-directories",orig_only,arx_dir);
  fill_log(header_lists,"New-files",mod_only,arx_file);
  fill_log(header_lists,"New-directories",mod_only,arx_dir);
  fill_log(header_lists,"Modified-files",patched,arx_file);
  fill_log(header_lists,"Modified-directories",patched,arx_dir);
  
  map<string,list<pair<string,string> > > rename_lists;
  
  for(list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator
        i=in_both.begin(); i!=in_both.end(); ++i)
    if(i->first.first.file_path.string()
       !=i->second.first.file_path.string())
      {
        string header, separator;
        if(i->first.second.empty())
          {
            header="Renamed-directories";
          }
        else
          {
            header="Renamed-files";
          }
        rename_lists[header]
          .push_back(make_pair(i->first.first.file_path.string(),
                               i->second.first.file_path.string()));
      }
  
  header_lists["New-patches"].push_back(name.complete_revision());
  find_new_and_removed_patches(patch_dir,header_lists,out_of_date_ok);
  
  /* If this isn't a continuation and it isn't an initial import,
     then we record when was the last continuation. */
  if(log_headers.find("Continuation-of")==log_headers.end()
     && !previous.empty())
    log_headers["Latest-continuation"]=
      compute_latest_continuation(tree_directory,previous);
  
  /* Create the new log. */
  
  path patch_log_path(commit_dir.path/"log");
  {
    fs::ofstream patch_log(patch_log_path);
    archive::text_oarchive patch_archive(patch_log);
    
    patch_archive << log_headers << header_lists << rename_lists
                  << log_body;
  }
  sha256 log_hash(patch_log_path);
  
  /* Add the new log to the patch list */
  
  path new_log_path=path("_arx/patch-log")/name.revision_path();

  if(!lexists(patch_dir/"added"))
    create_directory(patch_dir/"added");

  fs::create_directories((patch_dir/"added"/new_log_path)
                         .branch_path());
  fs::copy(patch_log_path,patch_dir/"added"/new_log_path);
  {
    fs::ofstream mod_files_index(patch_dir / "mod-files-index",ios::app);
    mod_files_index << " ";
    archive::text_oarchive mod_files_archive
      (mod_files_index,archive::no_header);
           
    /* Explicitly specify the inventory id.  It is just the path,
       since this is the control directory. */
    mod_files_archive << new_log_path.string()
                      << new_log_path.string();
  }

  /* Add the new log to the manifest */      
  {
    file_attributes f(patch_log_path);
    f.file_path=new_log_path;
    f.inventory_id=new_log_path.string();
    mod_only.push_back(make_pair(f,log_hash));
  }
      
  if(branching)
    {
      set_option_in_file(commit_dir.path/"CONTINUATION",
                         previous.complete_revision());
    }

  /* Create the new manifest and put its hash into the patch */

  const Temp_File temp_manifest(tree_directory,",,manifest");
  generate_new_manifest(temp_manifest.path,pristine_path,
                        name,orig_only,mod_only,in_both);
  {
    fs::ofstream sha_file(commit_dir.path/"sha256");
    sha_file << sha256(temp_manifest.path) << endl;
  }

  tar_and_archive_patch(commit_dir.path,previous,name,branching,root,
                        gpg_key);

  /* We add the new patch log to tree after the patch is archived,
     just in case we couldn't get a revision lock or similar
     thing. */

  fs::create_directories((root/new_log_path).branch_path());
  fs::copy(patch_log_path,root/new_log_path);
  add_path_and_checksum
    (root,file_attributes(new_log_path,new_log_path.string()),sha256("0"));

  /* The revision is archived, now delete the marker files and any
     ++changes files, and rename the new manifest. */
      
  remove(root / "_arx/++import");
  remove(root/"_arx/++branch-revision");

  /* Finally, create a new pristine by applying the patch. */

  path temp_pristine(commit_dir.path/"++cache");
  path new_pristine(root/"_arx/++cache"/ name.revision_path());
  if(pristine_path.empty())
    {
      create_directories(temp_pristine);
    }
  else
    {
      rename(pristine_path,temp_pristine);
    }

  do_patch(patch_dir,temp_pristine,false);

  /* Fix up the manifest */

  remove(temp_pristine/"_arx/++changes");
  remove(temp_pristine/"_arx/++manifest");
  remove(temp_pristine/"_arx/++sha256");
  fs::rename(temp_manifest.path,temp_pristine/"_arx/++manifest");
  fs::copy(commit_dir.path/"sha256",temp_pristine/"_arx/++sha256");

  create_directories(new_pristine.branch_path());
  fs::rename(temp_pristine,new_pristine);

  /* If we are not doing a partial commit, then the pristine
     manifest should match the current tree manifest. */

  if(!partial_commit)
    {
      fs::remove(root/"_arx/++manifest");
      fs::remove(root/"_arx/++changes");
      fs::remove(root/"_arx/++sha256");
      fs::copy(new_pristine/"_arx/++manifest",root/"_arx/++manifest");
      fs::copy(new_pristine/"_arx/++sha256",root/"_arx/++sha256");

      /* If a no-edit tree, clear the ++edit file and set all of
         the permissions to read-only */
      if(exists(root/"_arx/++edit"))
        {
          rewrite_edit_file(root,Path_List());

          for(Path_List::const_iterator i=path_list.begin();
              i!=path_list.end(); ++i)
            if(!is_control(*i) && exists(root/(*i))
               && !is_directory(root/(*i)))
              make_non_writeable(root/(*i));
        }
    }

  /* If we are doing a partial commit in an edit tree, then we
     have to update the manifest so that non-partial commits can
     work.  This means munging the ++edit file to make sure that we
     have everything.. */
  else if(exists(root/"_arx/++edit"))
    {
      Path_List complete_path_list;
      list<string> empty_list;
      fill_path_list(root,string(),empty_list,complete_path_list);

      /* Have to remove any files that have been deleted.
         Otherwise, diff will complain that it can't find these
         files in the manifest. */
      for(Checksums::iterator i=orig_only.begin(); i!=orig_only.end(); ++i)
        {
          Path_List::iterator j=find(complete_path_list.begin(),
                                     complete_path_list.end(),
                                     i->first.file_path);
          if(j!=complete_path_list.end())
            complete_path_list.erase(j);
        }
                                         
      rewrite_edit_file(root,complete_path_list);
      consolidate_manifest(root,name);
    }
}
