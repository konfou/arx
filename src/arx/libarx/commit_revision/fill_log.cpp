/* Fill up the log header with the contents of lists that are passed
   in.  It only puts in directories or files, not both.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <string>
#include <list>
#include <map>
#include "Checksums.hpp"
#include "inventory_types.hpp"

using namespace std;

void fill_log(map<string,list<string> > &file_headers, const string &field,
              Checksums &checksums, const int &type)
{
  list<string> body;

  for(Checksums::iterator i=checksums.begin(); i!=checksums.end(); ++i)
    {
      if((!is_control(i->first.file_path)
          || i->first.file_path.string()=="_arx/ignore")
         && i->second.empty()==(type==arx_dir))
        body.push_back(i->first.file_path.string());
    }
  if(!body.empty())
    file_headers[field]=body;
}
