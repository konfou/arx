/* Return a list of all of the patch logs for a branch in a
   project tree.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Revision_List.hpp"
#include "patch_level_cmp.hpp"
#include "get_config_option.hpp"
#include <string>
#include <list>
#include "arx_error.hpp"
#include "patch_number.hpp"
#include "tree_root.hpp"
#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include <cctype>
#include "is_revision.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

Revision_List list_patch_logs(const path &tree_directory,
                              const Parsed_Name &P)
{
  if(P.branch().empty())
    throw arx_error("This argument needs to specify a branch: "
                    + P.full_name());
  Revision_List result;

  path patch_log_dir(tree_root(tree_directory)/"_arx/patch-log"
                     /P.branch_path());

  if(lexists(patch_log_dir))
    for(fs::directory_iterator i(patch_log_dir); i!=fs::directory_iterator();
        ++i)
      {
        if(is_revision(i->leaf())
           && (P.revision().empty() || P.short_revision_path()==i->leaf()))
          {
            result.push_back(patch_number(i->leaf()));
          }
      }

  result.sort();
  return result;
}
