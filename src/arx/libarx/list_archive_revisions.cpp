/* Return a list of all of the revisions for a version in an
   archive.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "patch_level_cmp.hpp"
#include <string>
#include <list>
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "patch_number.hpp"
#include "Component_Regexes.hpp"
#include "valid_package_name.hpp"

using namespace std;

Revision_List list_archive_revisions(const Parsed_Name &name)
{
  valid_package_name(name,Branch);

  Revision_List revisions(name);
  
  gvfs::Init();
  
  gvfs::uri location(name.archive_location()
                     / name.branch_path_no_archive());
  
  if(gvfs::exists(location))
    {
      list<string> revision_list;
      
      Component_Regexes reg;
      
      gvfs::fill_directory_list(revision_list,location);
      revision_list.sort(patch_level_cmp);
      for(list<string>::iterator i=revision_list.begin();
          i!=revision_list.end(); ++i)
        {
          if(regex_match(*i,reg.revision)
             && (name.revision().empty() || name.short_revision_path()==*i))
            {
              revisions.push_back(patch_number(*i));
            }
        }
    }
  revisions.sort();
  return revisions;
}
