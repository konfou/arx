/* Make sure that a particular key in the list of public keys for an
   archive.  useful to prevent commits of keys that are not already
   listed in the archive.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "Spawn.hpp"
#include "boost/filesystem/operations.hpp"
#include "get_gpg_name.hpp"
#include "key_location.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void ensure_key_in_archive(const Parsed_Name &name, const string &gpg_key)
{
  list<string> gpg=get_gpg_name();
  if(!gpg.empty())
    {
      Spawn gpg_list;
      gpg_list.output="/dev/null";
      gpg_list.error="/dev/null";

      for(list<string>::iterator i=gpg.begin(); i!=gpg.end(); ++i)
        gpg_list << *i;
      gpg_list << "--no-default-keyring"
               << "--keyring" << (key_location(name)).native_file_string()
               << "--list-keys" << gpg_key;

      int return_status;
      if(!gpg_list.execute(return_status,true))
        throw arx_error("Problems when executing gpg to list keys");
      if(return_status)
        throw arx_error("This signature\n\t"
                        + gpg_key
                        + "\nis not listed in your local copy of the keys for this archive\n\t"
                        + name.archive()
                        + "\nYou must add that key to the archive before you can commit with it.");
    }
}
