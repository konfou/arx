/* Find all of the new and removed patch logs in the tree.  If there
   are any removed patches, then flag it as an error unless
   --out-of-date-ok is true.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include <map>
#include <string>
#include "push_revision_on_list.hpp"
#include "recursive_browse.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void find_new_and_removed_patches(const path &patch_dir,
                                  map<string,list<string> > &header_lists,
                                  const bool &out_of_date_ok)
{
  /* Compute the new patches. */

  list<Revision_List> new_patches;

  if(lexists(patch_dir / "added/_arx/patch-log"))
    for(fs::directory_iterator
          archive(patch_dir / "added/_arx/patch-log");
        archive!=fs::directory_iterator(); ++archive)
      {
        push_revision_on_list push(new_patches,Parsed_Name());
        recursive_browse(*archive,Parsed_Name(archive->leaf() + "/"),push);
      }

  for(list<Revision_List>::iterator i=new_patches.begin();
      i!=new_patches.end(); ++i)
    {
      for(Revision_List::iterator j=i->begin(); j!=i->end(); ++j)
        {
          header_lists["New-patches"].push_back(i->name.complete_branch()
                                                + patch_level(*j));
        }
    }

  /* Compute the removed patches.  Complain if any patches are missing
     and out-of-date is not specified, because it is indicative of a
     tree that is not up to date. */


  list<Revision_List> deleted_patches;

  if(lexists(patch_dir / "deleted/_arx/patch-log"))
    for(fs::directory_iterator
          archive(patch_dir / "deleted/_arx/patch-log");
        archive!=fs::directory_iterator(); ++archive)
      {
        push_revision_on_list push(deleted_patches,Parsed_Name());
        recursive_browse(*archive,Parsed_Name(archive->leaf() + "/"),push);
      }

  for(list<Revision_List>::iterator i=deleted_patches.begin();
      i!=deleted_patches.end(); ++i)
    {
      for(Revision_List::iterator j=i->begin(); j!=i->end(); ++j)
        {
          header_lists["Removed-patches"].push_back(i->name.complete_branch()
                                                    + patch_level(*j));
        }
    }

  if(!out_of_date_ok
     && header_lists.find("Removed-patches")!=header_lists.end())
    {
      string message("These patch logs are not present in the project tree.  This is usually\nindicative of a project tree that is not up to date with the archive.\nIf the removal of these logs is ok (e.g. you deleted them\nwith \"arx remove-history\"), then use the --out-of-date-ok flag to\nforce the commit.");
      for(list<string>::iterator i=header_lists["Removed-patches"].begin();
          i!=header_lists["Removed-patches"].end(); ++i)
        message+="\n\t" + *i;
      throw arx_error(message);
    }
}

  
