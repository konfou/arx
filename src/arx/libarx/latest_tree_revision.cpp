/* Return the latest revision present in the patch logs.

  Copyright (C) 2003, 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "tree_root.hpp"
#include "get_option_from_file.hpp"
#include "Parsed_Name.hpp"
#include "Revision_List.hpp"
#include "list_patch_logs.hpp"
#include "patch_level.hpp"
#include "valid_package_name.hpp"
#include "tree_branch.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

Parsed_Name latest_tree_revision(const path &tree_dir, const Parsed_Name &P)
{
  valid_package_name(P,Branch);

  Revision_List revisions(list_patch_logs(tree_dir,P));

  if(revisions.empty())
    return P;
  else
    return Parsed_Name(P).set_revision(*(revisions.rbegin()));
}

/* A simpler version that just uses the default branch of the tree. */
Parsed_Name latest_tree_revision(const path &tree_dir)
{
  return latest_tree_revision(tree_dir,tree_branch(tree_dir));
}
