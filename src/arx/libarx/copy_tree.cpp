/* Recursively copy one project tree to another.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include <list>
#include "tree_root.hpp"
#include "Inventory_Flags.hpp"
#include "Inventory.hpp"
#include "Current_Path.hpp"
#include "inventory_directory.hpp"
#include "file_attributes.hpp"
#include <string>
#include "read_checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void copy_tree(const path &source, const path &target)
{
  path root(tree_root(source));

  Inventory result;
  {
    Current_Path current(root);
    Checksums checksums;
    read_checksums(fs::current_path(),checksums);

    Inventory_Flags flags(fs::current_path());
    
    
    flags.include_subdir=flags.source=
      flags.unrecognized=flags.trees=
      flags.directories=flags.files=flags.kind=flags.ids=
      flags.disambiguate=true;
    flags.nested=flags.control=flags.ignore=false;

    inventory_directory("",result,flags,checksums);
  }
  list<file_attributes> complete_list;
  for(int i=0;i<num_inventory_types;++i)
    if(i!=ignore)
      for(int j=0;j<2;++j)
        {
          complete_list.splice(complete_list.begin(),result(i,j));
        }
  complete_list.sort();

  /* Copy everything except the control files. */
  copy_directory(root,target);
  for(list<file_attributes>::iterator i=complete_list.begin();
      i!=complete_list.end(); ++i)
    {
      copy(root/i->file_path,target/i->file_path);
    }
  /* Now copy the control files */
  copy(root/"_arx",target/"_arx",true);
}
