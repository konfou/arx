/* Match a uri fragment from the locations listed for an archive.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include <list>
#include "Parsed_Name.hpp"
#include "read_archive_locations.hpp"

using namespace std;

string match_uri_fragment(const string &fragment, const Parsed_Name &Name)
{
  list<string> locations(read_archive_locations(Name));
  string saved_item;
  for(list<string>::iterator i=locations.begin(); i!=locations.end(); ++i)
    {
      if(fragment==i->substr(0,fragment.size()))
        {
          if(saved_item.empty() || fragment==*i)
            {
              saved_item=*i;
            }
          else if(fragment!=saved_item)
            {
              throw arx_error("You must more fully expand this fragment\n\t"
                              + fragment
                              + "\nIt matches both of these uri's\n\t"
                              + (*i) + "\n\t"
                              + saved_item);
            }
        }
    }
  if(saved_item.empty())
    throw arx_error("This fragment did not match any of the locations for this archive\n\t"
                    + fragment + "\n\t"
                    + Name.archive());

  return saved_item;
}
