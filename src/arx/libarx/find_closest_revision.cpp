/* Find the closest revision to the target revision.  Right now it
   just counts patches, though in the future it should probably look
   at the size of the patches as well.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Revision_List.hpp"
#include "arx_error.hpp"

unsigned int find_closest_revision(const Revision_List &revs,
                                   const unsigned int &target)
{
  if(revs.empty())
    throw arx_error("INTERNAL ERROR: Empty revision list passed to find_closest_revision");
  unsigned int upper(target), lower(target+1);
  for(Revision_List::const_iterator i=revs.begin(); i!=revs.end(); ++i)
    {
      if(*i <= target)
        {
          lower=*i;
        }
      else
        {
          upper=*i;
          break;
        }
    }
  if(upper==target)
    {
      return lower;
    }
  else if(lower==target+1)
    {
      return upper;
    }
  else
    {
      if(target-lower < upper-target)
        return lower;
      return upper;
    }
}
