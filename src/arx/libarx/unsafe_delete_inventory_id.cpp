/* Rewrite the ++changes file to delete an inventory id.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "file_attributes.hpp"
#include "Temp_File.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void unsafe_delete_inventory_id(const path &root, const file_attributes &f,
                                const bool &is_directory)
{
  const Temp_File temp_changes(root,",,changes");
  if(lexists(root/"_arx/++changes"))
    copy_file(root/"_arx/++changes",temp_changes.path);
  {
    fs::ofstream changes(temp_changes.path,ios::app|ios::out);
    changes << "\ndelete\t";
    f.output(changes,is_directory);
  }
  if(rename(temp_changes.path.native_file_string().c_str(),
            (root/"_arx/++changes").native_file_string().c_str())!=0)
    throw arx_error("Unable to rename:\n\t"
                    + temp_changes.path.native_file_string()
                    + "\n\t"
                    + (root/"_arx/++changes").native_file_string());
}

