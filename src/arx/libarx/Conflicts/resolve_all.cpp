/* Resolve all conflicts in the tree.

  Copyright (C) 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Conflicts.hpp"
#include "tree_root.hpp"

using namespace boost;
namespace fs=boost::filesystem;
using fs::path;
using namespace std;

void Conflicts::resolve_all(const path &tree_directory)
{
  path root(tree_root(tree_directory));
  if(!exists(root/"_arx/++conflicts"))
    return;

  Conflicts conflicts(root/"_arx/++conflicts",true);

  for(list<pair<string,pair<string,string> > >::iterator
        i=conflicts.patch.begin(); i!=conflicts.patch.end(); ++i)
    {
      resolve_patch(root,*i);
    }

  for(list<pair<string,string> >::iterator i=conflicts.xdelta.begin();
      i!=conflicts.xdelta.end(); ++i)
    {
      resolve_xdelta(root,*i);
    }

  for(list<pair<pair<string,string>,pair<string,string> > >::iterator
        i=conflicts.merge.begin(); i!=conflicts.merge.end(); ++i)
    {
      resolve_merge(root,*i);
    }

  remove(root/"_arx/++conflicts");
}
