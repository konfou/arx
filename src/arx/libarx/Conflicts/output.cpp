/* Print out all conflicts

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Conflicts.hpp"

using namespace boost;
namespace fs=boost::filesystem;
using fs::path;
using namespace std;

std::ostream & operator<<(std::ostream &s, const Conflicts &conflicts)
{
  const path root(conflicts.tree_directory);
  if(!conflicts.move_target.empty())
    {
      s << "Move target conflicts.  The following paths tried to move to where an\nexisting path is.  The original has been moved out of the way." << endl;
      for(list<pair<string,string> >::const_iterator
            i=conflicts.move_target.begin();
          i!=conflicts.move_target.end(); ++i)
        {
          s << "T\t" << (root/i->first).native_file_string()
            << "\t" << (root/i->second).native_file_string() << endl;
        }
    }
  if(!conflicts.move_parent.empty())
    {
      s << "Move parent conflicts.  The parent directory of the path in the tree is\nincompatible with where the patch wants to move the path.  The path has\nbeen moved, but you should check that it is in the correct place." << endl;
      for(list<pair<string,pair<string,string> > >::const_iterator
            i=conflicts.move_parent.begin();
          i!=conflicts.move_parent.end(); ++i)
        {
          s << "P\t" << (root/i->first).native_file_string()
            << "\t" << (root/i->second.first).native_file_string()
            << " => " << (root/i->second.second).native_file_string()
            << endl;
        }
    }
  if(!conflicts.rename.empty())
    {
      s << "Rename conflicts.  The name of the path in the tree is incompatible\nwith the name that the patch wants to see.  The path has been moved,\nbut you should check that it is in the correct place." << endl;
      for(list<pair<string,pair<string,string> > >::const_iterator
            i=conflicts.rename.begin(); i!=conflicts.rename.end(); ++i)
        {
          s << "R\t" << (root/i->first).native_file_string()
            << "\t" << (root/i->second.first).native_file_string()
            << " => " << (root/i->second.second).native_file_string()
            << endl;
        }
    }
  if(!conflicts.deleted_parent.empty())
    {
      s << "Deleted parent conflicts.  The following paths had their parent\ndirectories deleted by the patch.  They have been moved to the location\ngiven by the patch, but you need to put them in the correct place." << endl;
      for(list<pair<string,string> >::const_iterator
            i=conflicts.deleted_parent.begin();
          i!=conflicts.deleted_parent.end(); ++i)
        {
          s << "D\t" << (root/i->first).native_file_string()
            << " => " << (root/i->second).native_file_string()
            << endl;
        }
    }
  if(!conflicts.no_parent.empty())
    {
      s << "No parent conflicts.  The following paths do not have existing parent\ndirectories.  They have been moved to the location\ngiven by the patch, but you need to put them in the correct place." << endl;
      for(list<pair<string,string> >::const_iterator i=conflicts.no_parent.begin();
          i!=conflicts.no_parent.end(); ++i)
        {
          s << "N\t" << (root/i->first).native_file_string()
            << " => " << (root/i->second).native_file_string()
            << endl;
        }
    }
  if(!conflicts.patch.empty())
    {
      s << "Patch conflicts.  The following is a list of the files and where the originals and rejected hunks are." << endl;
      for(list<pair<string,pair<string,string> > >::const_iterator
            i=conflicts.patch.begin(); i!=conflicts.patch.end(); ++i)
        {
          s << "P\t" << (root/i->first).native_file_string()
            << "\t" << (root/i->second.first).native_file_string()
            << "\t" << (root/i->second.second).native_file_string()
            << endl;
        }
    }
  if(!conflicts.xdelta.empty())
    {
      s << "XDelta patch conflicts.  The following is a list of the files and where the originals are." << endl;
      for(list<pair<string,string> >::const_iterator i=conflicts.xdelta.begin();
          i!=conflicts.xdelta.end(); ++i)
        {
          s << "P\t" << (root/i->first).native_file_string()
            << "\t" << (root/i->second).native_file_string()
            << endl;
        }
    }
  if(!conflicts.missing_patches.empty())
    {
      s << "The following paths have patches but do not exist." << endl;
      for(list<string>::const_iterator i=conflicts.missing_patches.begin();
          i!=conflicts.missing_patches.end(); ++i)
        {
          s << "E\t" << (root/ *i).native_file_string() << endl;
        }
    }
  if(!conflicts.missing_moves.empty())
    {
      s << "The following paths have renames but do not exist." << endl;
      for(list<pair<pair<string,string>,string> >::const_iterator
            i=conflicts.missing_moves.begin();
          i!=conflicts.missing_moves.end(); ++i)
        {
          s << "S\t" << (root/i->first.first).native_file_string()
            << "\t" << (root/i->first.second).native_file_string()
            << "\t" << i->second
            << endl;
        }
    }
  if(!conflicts.add.empty())
    {
      s << "The following paths were added, but they have the same inventory id as a\npath already in the inventory.  Perhaps you are trying to apply a patch\nthat has already been applied?" << endl;
      for(list<pair<pair<string,string>,string> >::const_iterator
            i=conflicts.add.begin(); i!=conflicts.add.end(); ++i)
        {
          s << "A\t" << (root/i->first.first).native_file_string()
            << "\t" << (root/i->first.second).native_file_string()
            << "\t" << i->second << endl;
        }
    }
  if(!conflicts.merge.empty())
    {
      s << "Merge conflicts.  The following is a list of the files, the original\ntree version, the ancestor version, and the sibling version."
        << endl;
      for(list<pair<pair<string,string>,pair<string,string> > >::const_iterator
            i=conflicts.merge.begin(); i!=conflicts.merge.end(); ++i)
        s << "M\t" << (root/i->first.first).native_file_string()
          << "\t"  << (root/i->first.second).native_file_string()
          << "\t"  << (root/i->second.first).native_file_string()
          << "\t"  << (root/i->second.second).native_file_string()
          << endl;
    }
  if(!conflicts.directory_loop.empty())
    {
      s << "Directory loop conflict.  The following paths and associated moves end\nup with one or more directory loops." << endl;
      for(list<pair<pair<string,string>,pair<string,string> > >::const_iterator
            i=conflicts.directory_loop.begin();
          i!=conflicts.directory_loop.end(); ++i)
        {
          s << "L\t" << (root/i->first.first).native_file_string()
            << " : " << (root/i->second.first).native_file_string()
            << " --> " << (root/i->second.second).native_file_string()
            << endl;
        }
    }
  return s;
}
