/* Resolve a conflict to a single file.

  Copyright (C) 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Conflicts.hpp"
#include "tree_root.hpp"
#include "Temp_Path.hpp"
#include <cstdio>

using namespace boost;
namespace fs=boost::filesystem;
using fs::path;
using namespace std;

void Conflicts::resolve(const path &tree_directory, const path &conflict_path)
{
  path root(tree_root(tree_directory));
  if(!exists(root/"_arx/++conflicts"))
    return;

  Conflicts conflicts(root/"_arx/++conflicts",true);

  const string local(relative_path(system_complete(conflict_path),
                                   system_complete(root)).string());

  for(list<pair<string,string> >::iterator i=conflicts.move_target.begin();
      i!=conflicts.move_target.end(); ++i)
    if(i->first==local || i->second==local)
      {
        conflicts.move_target.erase(i);
        break;
      }        

  for(list<pair<string,pair<string,string> > >::iterator
        i=conflicts.move_parent.begin(); i!=conflicts.move_parent.end(); ++i)
    if(i->first==local)
      {
        conflicts.move_parent.erase(i);
        break;
      }

  for(list<pair<string,pair<string,string> > >::iterator
        i=conflicts.rename.begin(); i!=conflicts.rename.end(); ++i)
    if(i->first==local)
      {
        conflicts.rename.erase(i);
        break;
      }

  for(list<pair<string,string> >::iterator i=conflicts.deleted_parent.begin();
      i!=conflicts.deleted_parent.end(); ++i)
    if(i->first==local)
      {
        conflicts.deleted_parent.erase(i);
        break;
      }

  for(list<pair<string,string> >::iterator i=conflicts.no_parent.begin();
      i!=conflicts.no_parent.end(); ++i)
    if(i->first==local)
      {
        conflicts.no_parent.erase(i);
        break;
      }

  for(list<pair<string,pair<string,string> > >::iterator
        i=conflicts.patch.begin(); i!=conflicts.patch.end(); ++i)
    if(i->first==local)
      {
        resolve_patch(root,*i);
        conflicts.patch.erase(i);
        break;
      }

  for(list<pair<string,string> >::iterator i=conflicts.xdelta.begin();
      i!=conflicts.xdelta.end(); ++i)
    if(i->first==local)
      {
        resolve_xdelta(root,*i);
        conflicts.xdelta.erase(i);
        break;
      }

  for(list<pair<pair<string,string>,string> >::iterator
        i=conflicts.missing_moves.begin();
      i!=conflicts.missing_moves.end(); ++i)
    if(i->first.first==local)
      {
        conflicts.missing_moves.erase(i);
        break;
      }

  for(list<string>::iterator i=conflicts.missing_patches.begin();
      i!=conflicts.missing_patches.end(); ++i)
    if(*i==local)
      {
        conflicts.missing_patches.erase(i);
        break;
      }

  for(list<pair<pair<string,string>,string> >::iterator
        i=conflicts.add.begin(); i!=conflicts.add.end(); ++i)
    if(i->first.first==local || i->first.second==local)
      {
        conflicts.add.erase(i);
        break;
      }
  for(list<pair<pair<string,string>,pair<string,string> > >::iterator
        i=conflicts.merge.begin(); i!=conflicts.merge.end(); ++i)
    if(i->first.first==local)
      {
        resolve_merge(root,*i);
        conflicts.merge.erase(i);
        break;
      }

  for(list<pair<pair<string,string>,pair<string,string> > >::iterator
        i=conflicts.directory_loop.begin();
      i!=conflicts.directory_loop.end(); ++i)
    if(i->first.first==local)
      {
        conflicts.directory_loop.erase(i);
        break;
      }
  conflicts.save_to_conflicts_file(false);
}
