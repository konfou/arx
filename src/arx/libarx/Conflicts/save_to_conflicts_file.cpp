/* Append all conflicts to the list of conflicts in a tree.

  Copyright (C) 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Conflicts.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/serialization/list.hpp"
#include "boost/serialization/utility.hpp"
#include "arx_error.hpp"
#include "Temp_Path.hpp"
#include <cstdio>

using namespace boost;
namespace fs=boost::filesystem;
using fs::path;
using namespace std;

void Conflicts::save_to_conflicts_file(const bool append)
{
  path conflict_path(tree_directory/"_arx/++conflicts");

  path new_conflicts(Temp_Path(tree_directory,",,conflicts"));

  Conflicts local_copy(*this);
  if(append)
    {
      Conflicts conflicts(tree_directory,true);
      
      local_copy.move_target.splice(local_copy.move_target.end(),
                                    conflicts.move_target);
      local_copy.move_parent.splice(local_copy.move_parent.end(),
                                    conflicts.move_parent);
      local_copy.rename.splice(local_copy.rename.end(),conflicts.rename);
      local_copy.deleted_parent.splice(local_copy.deleted_parent.end(),
                                       conflicts.deleted_parent);
      local_copy.no_parent.splice(local_copy.no_parent.end(),
                                  conflicts.no_parent);
      local_copy.patch.splice(local_copy.patch.end(),conflicts.patch);
      local_copy.xdelta.splice(local_copy.xdelta.end(),conflicts.xdelta);
      local_copy.missing_moves.splice(local_copy.missing_moves.end(),
                                      conflicts.missing_moves);
      local_copy.missing_patches.splice(local_copy.missing_patches.end(),
                                        conflicts.missing_patches);
      local_copy.add.splice(local_copy.add.end(),conflicts.add);
      local_copy.merge.splice(local_copy.merge.end(),conflicts.merge);
    }
      
  {
    fs::ofstream conflict_file(new_conflicts);
    archive::text_oarchive conflict_archive(conflict_file);
    conflict_archive << local_copy;
  }
  if(std::rename(new_conflicts.native_file_string().c_str(),
              conflict_path.native_file_string().c_str())!=0)
    throw arx_error("Can't rename " + new_conflicts.native_file_string()
                    + " to "
                    + conflict_path.native_file_string());
}
