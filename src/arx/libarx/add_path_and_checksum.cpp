/* Add a specified file and checksum to the manifest.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "sha256.hpp"
#include "file_attributes.hpp"
#include <list>
#include "add_path_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void add_path_and_checksum(const path &dir, const file_attributes &f,
                           const sha256 &checksum)
{
  list<pair<file_attributes,sha256> > add_list;
  add_list.push_back(make_pair(f,checksum));
  add_path_list(dir,add_list);
}
