/* Delete one element, either a file or directory.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "tree_root.hpp"
#include "Checksums.hpp"
#include <list>
#include <string>
#include "unsafe_delete_inventory_id.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void delete_element(const path &pathname, Checksums &checksums,
                    const bool &interactive,
                    const bool &force, const bool &recursive,
                    const bool &ids_only)
{
  const path root(tree_root(fs::current_path()));
  string relative_pathname(relative_path(pathname,root).string());
      
  bool id_exists(false);
  Checksums::iterator j=checksums.end();
  for(Checksums::iterator i=checksums.begin();
      i!=checksums.end() && !id_exists; ++i)
    {
      if(i->first.file_path.string()==relative_pathname)
        {
          id_exists=true;
          j=i;
        }
    }
  if(!lexists(pathname) && !id_exists && !force)
    {
      throw arx_error("Neither this path nor its inventory id exist\n\t"
                      + pathname.native_file_string());
    }
      
  if(lexists(pathname) && !symbolic_link_exists(pathname)
     && is_directory(pathname))
    {
      list<path> dir((fs::directory_iterator(pathname)),
                     fs::directory_iterator());
      if(recursive)
        {
          for(list<path>::iterator i=dir.begin(); i!=dir.end(); ++i)
            {
              delete_element(*i,checksums,interactive,force,recursive,
                             ids_only);
            }
        }
      if(!ids_only
         && fs::directory_iterator(pathname)!=fs::directory_iterator())
        {
          throw arx_error("Not deleting this directory because it is not empty\n\t" + pathname.native_file_string());
        }
    }
      
  if(interactive && !force)
    {
      string response;
      cout << "remove ";
      if(symbolic_link_exists(pathname))
        cout << "link '";
      else if(is_directory(pathname))
        cout << "directory '";
      else
        cout << "file '";
      cout << pathname.native_file_string() << "'? ";
          
      cin >> response;
      if(response!="y" && response!="yes"
         && response!="Y" && response!="YES")
        return;
    }
      
  /* If the id exists, write the delete marker into ++changes */  
  if(id_exists)
    {
      unsafe_delete_inventory_id(root,j->first,j->second.empty());
    }

  /* remove the path itself */
  if(!ids_only && fs::lexists(pathname))
    {
      remove(pathname);
    }
}
