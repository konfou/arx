/* Atomically move paths.  You can move paths between project tree, or
   only move the inventory ids within a single tree.

   Copyright 2003, 2004  Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "Checksums.hpp"
#include "tree_root.hpp"
#include <cstdio>
#include "Current_Path.hpp"
#include "Temp_File.hpp"
#include "read_checksums.hpp"
#include "unsafe_move_inventory_id.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void move_inventory_ids(const path &source_root,
                                const path &target_root,
                                const Checksums &source_checksums,
                                const Checksums &target_checksums,
                                const path &source,
                                const path &target,
                                const path &source_changes,
                                const path &target_changes);

inline path canonical_dir(const path &p)
{
  Current_Path current(p);
  return fs::current_path();
}


void atomic_move(const path &source, const path &destination,
                 const bool &ids_only)
{
  path target(destination), source_root, target_root, relative_source,
    relative_target;
  Checksums source_checksums, target_checksums;

  /* If we're moving both files and inventory ids */
  if(!ids_only)
    {
      if(lexists(destination))
        {
          if(!(exists(destination) && is_directory(destination))
             || lexists(destination / source.leaf()))
            {
              throw arx_error("can't move " + source.native_file_string()
                              + " to " + destination.native_file_string()
                              + "\n.  Destination already exists");
            }
          target=canonical_dir(destination) / source.leaf();
        }
      else
        {
          target=canonical_dir(destination.branch_path())/destination.leaf();
        }
          
      try
        {
          const path canonical_source(canonical_dir(source.branch_path()));
          source_root=tree_root(canonical_source);
          relative_source=relative_path(canonical_source/source.leaf(),
                                        source_root);
        }
      catch(arx_error &err) {}
      if(!source_root.empty())
        read_checksums(source_root,source_checksums);
          
      try {
        target_root=tree_root(target.branch_path());
        relative_target=relative_path(target,target_root);
      }
      catch(arx_error &err) {}
      if(!target_root.empty())
        read_checksums(target_root,target_checksums);
          
      if(target_root.empty()!=source_root.empty())
        throw arx_error("Either both the source and target must be in a project tree, or neither are.\n\tsource: "
                        + source.native_file_string()
                        + "\n\ttarget: "
                        + target.native_file_string());
          
      /* If we are not in a project tree, it is just a simple
         rename. */
      if(source_checksums.empty())
        {
          fs::rename(source,target);
        }
      /* Otherwise, there is lots of singing and dancing to make sure
         that a path and its external id are either both moved or
         neither are.  It isn't perfectly transactional, but it should
         at least complain if the tree is broken and give directions
         on how to fix it. */
      else
        {
          /* Copy the ++changes files if they exist */
          const Temp_File source_temp(source_root,",,move1"),
            target_temp(target_root,",,move2");
          if(lexists(source_root/"_arx/++changes"))
            copy(source_root/"_arx/++changes",source_temp.path);
          if(source_root.string()!=target_root.string()
             && lexists(target_root/"_arx/++changes"))
            copy(target_root/"_arx/++changes",target_temp.path);

          move_inventory_ids(source_root,target_root,source_checksums,
                             target_checksums,relative_source,
                             relative_target, source_temp.path,
                             target_temp.path);
          fs::rename(source,target);

          if(source_root.string()!=target_root.string()
             && 0!=rename(target_temp.path.native_file_string().c_str(),
                          (target_root/"_arx/++changes")
                          .native_file_string().c_str()))
            {
              try {
                fs::rename(target,source);
              }
              catch (fs::filesystem_error &err)
                {
                  throw arx_error("Problem when renaming\n\t"
                                  + target_temp.path.native_file_string()
                                  + "\n\t"
                                  + (target_root/"_arx/++changes")
                                  .native_file_string()
                                  + "\nCan't move the target back to the source.  Please manually move it back\nor your tree will be corrupted.\n\tsource: "
                                  + source.native_file_string()
                                  + "\n\ttarget :"
                                  + target.native_file_string());
                }
              throw arx_error("Can't rename\n\t"
                              + target_temp.path.native_file_string()
                              + "\n\t"
                              + (target_root/"_arx/++changes")
                              .native_file_string());

            }
          else if(exists(source_temp.path)
                  && 0!=rename(source_temp.path.native_file_string().c_str(),
                               (source_root/"_arx/++changes")
                               .native_file_string().c_str()))
            {
              try {
                fs::rename(target,source);
              }
              catch (fs::filesystem_error &err)
                {
                  throw arx_error("Problem when renaming\n\t"
                                  + source_temp.path.native_file_string()
                                  + "\n\t"
                                  + (source_root/"_arx/++changes")
                                  .native_file_string()
                                  + "\nCan't move the target back to the source.  Please manually move it back\nor your tree will be corrupted.\n\tsource: "
                                  + source.native_file_string()
                                  + "\n\ttarget :"
                                  + target.native_file_string());
                }
              throw arx_error("Can't rename\n\t"
                              + source_temp.path.native_file_string()
                              + "\n\t"
                              + (source_root/"_arx/++changes")
                              .native_file_string());
            }
        }
    }
  /* Moving just inventory ids */
  else
    {
      source_root=tree_root(source.branch_path());
      target_root=tree_root(target.branch_path());
      if(target_root.string()!=source_root.string())
        throw arx_error("When moving ids, both source and target must be in the same project tree.\n\tsource tree root: "
                        + source_root.native_file_string()
                        + "\n\ttarget tree root: "
                        + target_root.native_file_string());
      relative_source=relative_path(source,source_root);
      relative_target=relative_path(target,source_root);
      read_checksums(source_root,source_checksums);
          
      Checksums::iterator i
        =find_if(source_checksums.begin(),
                 source_checksums.end(),
                 std::bind2nd(std::ptr_fun(checksums_file_path_eq_string),
                              source.string()));
          
          
      if(i==source_checksums.end())
        throw arx_error("Can't find this path in the manifest\n\t"
                        + source.native_file_string());
      unsafe_move_inventory_id(source_root,relative_source,relative_target,
                               i->first.inventory_id,i->second.empty());
    }
}
