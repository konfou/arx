/* Move just the inventory ids, recursively is necessary.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Checksums.hpp"
#include "sha256.hpp"
#include "boost/archive/text_oarchive.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void move_inventory_ids(const path &source_root,
                        const path &target_root,
                        const Checksums &source_checksums,
                        const Checksums &target_checksums,
                        const path &source,
                        const path &target,
                        const path &source_changes,
                        const path &target_changes)
{
  Checksums::const_iterator i
    =find_if(source_checksums.begin(),
             source_checksums.end(),
             std::bind2nd(std::ptr_fun(checksums_file_path_eq_string),
                          source.string()));

  Checksums::const_iterator j
    =find_if(target_checksums.begin(),
             target_checksums.end(),
             std::bind2nd(std::ptr_fun(checksums_file_path_eq_string),
                          target.string()));

  if(j!=target_checksums.end())
    if(i!=source_checksums.end())
      {
        throw arx_error("Source and target both already exists in the manifest\n\t"
                        + (source_root/source).native_file_string() + "\n\t"
                        + (target_root/target).native_file_string());
      }
    else
      {
        if(Command_Info::verbosity>=default_output)
          cerr << "Target already exists in the manifest and source does not, so not moving any inventory ids"
               << "\n\t"
               << (source_root/source).native_file_string()
               << "\n\t"
               << (target_root/target).native_file_string()
               << endl;
      }

  if(i!=source_checksums.end())
    if(source_root.string()==target_root.string())
      {
        fs::ofstream changes(source_changes,ios::app|ios::out);
        changes << "\nmove\t";
        i->first.output(changes,i->second);
        changes << " ";
        archive::text_oarchive changes_archive(changes,
                                               boost::archive::no_header);
        changes_archive << target.string();
      }
    else
      {
        fs::ofstream source_temp(source_changes,ios::app|ios::out);
        source_temp << "\ndelete\t";
        i->first.output(source_temp,i->second);
        
        file_attributes f(i->first);
        f.file_path=target;
        fs::ofstream target_temp(target_changes,ios::app|ios::out);
        target_temp << "\nadd\t";
        f.output(target_temp,i->second);
      }

  if(!symbolic_link_exists(source_root/source)
     && is_directory(source_root/source))
    {
      for(fs::directory_iterator i(source_root/source);
          i!=fs::directory_iterator(); ++i)
        move_inventory_ids(source_root,target_root,source_checksums,
                           target_checksums,source/i->leaf(),target/i->leaf(),
                           source_changes,target_changes);
    }
}
