/* Get a particular full-tree revision from a local cache.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "Parsed_Name.hpp"
#include "get_config_option.hpp"
#include "valid_package_name.hpp"
#include <iostream>
#include "Command_Info.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void get_local_revision(const Parsed_Name &name, const path &target_dir,
                        const path &cache_dir, const bool &hard_link)
{
  valid_package_name(name,Revision);

  path start_dir(cache_dir);
  if(start_dir.empty())
    {
      start_dir=fs::current_path().branch_path();
      if(start_dir.empty())
        start_dir=fs::current_path();
    }

  path arx_dir;
  bool done(false);
  for(fs::directory_iterator i(start_dir);
      i!=fs::directory_iterator() && !done; ++i)
    {
      if(i->leaf()=="_arx")
        {
          arx_dir=*i;
        }
      else
        {
          if(!fs::symbolic_link_exists(*i) && fs::is_directory(*i))
            for(fs::directory_iterator j(*i); j!=fs::directory_iterator(); ++j)
              {
                if(j->leaf()=="_arx")
                  {
                    arx_dir=*j;
                    break;
                  }
              }
        }
      if(!arx_dir.empty())
        {
          path revision_path(arx_dir / "++cache"/ name.revision_path());
          if(fs::lexists(revision_path))
            {
              if(Command_Info::verbosity>=report)
                cout << "Copying "
                     << revision_path.native_file_string() << endl;
              fs::copy(revision_path,target_dir,true,hard_link);
              done=true;
            }
        }
    }
  if(!done)
    throw arx_error("Couldn't find a copy of the revision\n\t"
                    + name.full_name()
                    + "\nin the cache directory\n\t"
                    + cache_dir.native_file_string());
}

      
