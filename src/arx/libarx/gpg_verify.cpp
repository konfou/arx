/* Verify the signature on a file using the archive keyring.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/path.hpp"
#include "Parsed_Name.hpp"
#include "../../config.h"
#include "Spawn.hpp"
#include "key_location.hpp"
#include <iostream>
#include "get_gpg_name.hpp"
#include "gvfs.hpp"
#include "Temp_File.hpp"
#include "sha256.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void gpg_verify(const boost::filesystem::path &file_name,
                const Parsed_Name &name)
{
  list<string> gpg=get_gpg_name();

  /* Check whether the local and remote public key rings are the same. */

  if(Command_Info::verbosity>=default_output)
    {
      gvfs::Init();
      Temp_File remote_public_keys(",,public_key");
      gvfs::copy(name.archive_location() / ",meta-info/public_keys",
                 gvfs::uri(remote_public_keys.path.native_file_string()));
      if(sha256(remote_public_keys.path)!=sha256(key_location(name)))
        cerr << "The local and remote list of authorized public keys for this archive are\ndifferent.\n\t"
             << name.archive() << endl
             << "It may be that a new developer has been granted write access\nto the archive, or an old developer has been removed.  However, it is also\npossible that someone is attempting to add an unauthorized key.  To use\nthe new public key list, you will have to unregister and reregister\nthe archive.\n";
    }

  if(!gpg.empty())
    {
      Spawn gpg_verify;
      for(list<string>::iterator i=gpg.begin(); i!=gpg.end(); ++i)
        gpg_verify << *i;
      gpg_verify << "-q" << "--trust-model" << "always"
                 << "--no-default-keyring"
                 << "--keyring" << (key_location(name)).native_file_string()
                 << "--verify" << file_name.native_file_string();

      int return_status;
      if(!gpg_verify.execute(return_status,true))
        throw arx_error("Problems when executing gpg to verify a signature");
      if(return_status)
        throw arx_error("gpg signature did not verify.  There is probably some corruption in the\narchive.  It may also be the case that someone has tampered with the\narchive.  Finally, the archive owner may be using a new key to sign revisions.\nIf you are certain the the archive owner is using a new key, then you\nshould unregister and then reregister the archive.");
    }
  else if(Command_Info::verbosity>=default_output)
    cerr << "WARNING: You can not verify signatures, because ArX has not been compiled\nwith gpg support and you did not specify a gpg program with \"arx param\".\n";
}
