/* Pushes a file onto the source list if it has an inventory id.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/regex.hpp"
#include "Inventory_Flags.hpp"
#include "file_attributes.hpp"
#include "inventory_types.hpp"
#include "Inventory.hpp"
#include "Checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

bool push_if_file_has_id(const path &file_path,
                         Inventory &result,
                         const Inventory_Flags &flags,
                         Checksums &checksums,
                         const string &prefix)
{
  string relative_name(prefix+file_path.leaf());
  for(Checksums::iterator i=checksums.begin(); i!=checksums.end(); ++i)
    {
      if(i->first.file_path.string()==relative_name)
        {
          if(flags.source)
            {
              file_attributes f(file_path);
              f.inventory_id=i->first.inventory_id;
              f.properties=i->first.properties;
              result(source,arx_file).push_back(f);
            }
          checksums.erase(i);
          return true;
        }
    }
  return false;
}
