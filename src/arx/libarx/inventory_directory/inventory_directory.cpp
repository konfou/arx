/* Inventories an individual path.  Looks recursively through a
   directory, categorizing what is there.  When looking at
   directories, it asks, in order:

   1) Is it a nested tree
   2) Is it a control directory (_arx)
   3) Does it have an inventory id
   4) Does it match the regex for ignore

   If none of these things are true, then it categorizes the directory
   as unrecognized.

   For files, it is the same except there are no such thing as nested
   trees.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include "boost/filesystem/operations.hpp"
#include "Inventory.hpp"
#include "Inventory_Flags.hpp"
#include "inventory_types.hpp"
#include "file_attributes.hpp"
#include "inventory_directory_as.hpp"
#include "Checksums.hpp"
#include <string>
#include "read_checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;


extern bool inventory_control_directory(const path &file_path,
                                        Inventory &result,
                                        const Inventory_Flags &flags,
                                        Checksums &checksums);

extern bool push_if_directory_has_id(const path &file,
                                     Inventory &result,
                                     const Inventory_Flags &flags,
                                     Checksums &checksums,
                                     const string &prefix);

extern bool push_if_file_has_id(const path &file_path,
                                Inventory &result,
                                const Inventory_Flags &flags,
                                Checksums &checksums,
                                const string &prefix);

void inventory_directory(const path &directory,
                         Inventory &result,
                         const Inventory_Flags &flags,
                         Checksums &checksums, string prefix="")
{
  fs::directory_iterator end;
  for(fs::directory_iterator i(directory); i!=end; ++i)
    {
      /* If it is a directory, we may have to recurse through it.
         Symbolic links are always considered files, even if they
         point to directories. */
      
      if(!symbolic_link_exists(*i) && is_directory(*i))
        {
          /* If it is a nested project tree and we are doing a nested
             inventory, then we need to use new inventory flags. */
          
          if(lexists(*i / "_arx"))
            {
              if(flags.trees && flags.directories)
                {
                  result(nested_tree,arx_dir).push_back(*i);
                }
              if(flags.nested)
                {
                  Inventory_Flags new_flags(flags);
                  new_flags.regexes=Inventory_Regexes();
                  Checksums tree_checksums;
                  read_checksums(*i,tree_checksums);
                  inventory_directory(*i,result,new_flags,tree_checksums);
                }
            }
          else if(inventory_control_directory(*i,result,flags,checksums))
            {}
          else if(push_if_directory_has_id(*i,result,flags,checksums,prefix))
            {
              inventory_directory(*i,result,flags,checksums,
                                  prefix + i->leaf() + "/");
            }
          else if(flags.regexes.match(i->leaf()))
            {
              if(flags.ignore && flags.directories)
                {
                  result(ignore,arx_dir).push_back(*i);
                  /* If we're inventorying everything, then all files in a
                     ignore directory are ignore. */
                  if(flags.include_subdir)
                    inventory_directory_as(*i,result,ignore,flags);
                }
            }
          else
            {
              if(flags.unrecognized && flags.directories)
                {
                  result(unrecognized,arx_dir).push_back(*i);
                  /* If we're inventorying everything, then all files in an
                     unrecognized directory are unrecognized. */
                }
              if(flags.include_subdir)
                inventory_directory_as(*i,result,unrecognized,flags);
            }
        }
      /* It must be a file or symbolic link.  In this case, files and
         symlinks can never be control or temp, because they must be
         within a control or temp directory.  In that case, they won't
         be inventoried with this code. */
      else if(flags.files)
        {
          if(push_if_file_has_id(*i,result,flags,checksums,prefix))
            {
            }
          else if(flags.regexes.match(i->leaf()))
            {
              if(flags.ignore)
                result(ignore,arx_file).push_back(*i);
            }
          else
            {
              if(flags.unrecognized)
                result(unrecognized,arx_file).push_back(*i);
            }
        }
    }
}
