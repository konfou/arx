/* Inventories a directory, but puts everything into the a particular
   category (ignore, unrecognized, control).  Useful for
   inventorying a directory that has already been determined to be
   ignore, unrecognized, or control.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "inventory_types.hpp"
#include "Inventory_Flags.hpp"
#include "file_attributes.hpp"
#include "Inventory.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void inventory_directory_as(const path &directory, Inventory &result,
                            const inventory_types &type,
                            const Inventory_Flags &flags, string prefix="")
{
  fs::directory_iterator end;

  for(fs::directory_iterator i(directory); i!=end; ++i)
    {
      /* If it is a directory, we have to recurse through it.
         Symbolic links are always considered files, even if they
         point to directories. */
      
      if(!symbolic_link_exists(*i) && is_directory(*i))
        {
          if(prefix.empty())
            {
              if(flags.directories)
                result(type,arx_dir).push_back(*i);
              inventory_directory_as(*i,result,type,flags);
            }
          else
            {
              if(flags.directories)
                result(type,arx_dir).push_back(file_attributes
                                                (*i,prefix + i->leaf()));
              inventory_directory_as(*i,result,type,flags,
                                     prefix + i->leaf() + "/");
            }
        }
      else if(flags.files)
        {
          if(prefix.empty())
            {
              result(type,arx_file).push_back(*i);
            }
          else
            {
              result(type,arx_file).push_back(file_attributes
                                               (*i,prefix + i->leaf()));
            }
        }
    }
}
