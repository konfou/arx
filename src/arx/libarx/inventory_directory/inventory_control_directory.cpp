/* Checks whether a particular directory is categorized as control.
   If so, and we are inventorying control directories, then recurse
   through it.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/regex.hpp"
#include "Inventory_Flags.hpp"
#include "file_attributes.hpp"
#include "inventory_types.hpp"
#include "inventory_directory.hpp"
#include "inventory_directory_as.hpp"
#include "Inventory.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

bool inventory_control_directory(const path &file_path,
                                 Inventory &result,
                                 const Inventory_Flags &flags,
                                 Checksums &checksums)
{
  if(file_path.leaf()=="_arx")
    {
      if(flags.control)
        {
          /* In _arx directories, everything that doesn't start with
             a plus '+' is source. */
          regex ignore_regex("^(\\+.*)|(,,.*)$");

          if(flags.directories)
            result(control,arx_dir)
              .push_back(file_attributes(file_path,file_path.leaf()));

          Inventory_Flags new_flags(flags);
          for(fs::directory_iterator i=fs::directory_iterator(file_path);
              i!=fs::directory_iterator(); ++i)
            {
              /* Directories */
              if(!symbolic_link_exists(*i) && is_directory(*i))
                {
                  if(!regex_match(i->leaf(),ignore_regex))
                    {
                      if(flags.directories)
                        result(control,arx_dir)
                          .push_back(file_attributes(*i,"_arx/"+i->leaf()));
                      inventory_directory_as(*i,result,control,new_flags,
                                             "_arx/"+i->leaf()+"/");
                    }
                  else
                    {
                      if(flags.directories && flags.ignore)
                        result(ignore,arx_dir).push_back(*i);
                      if(new_flags.include_subdir)
                        inventory_directory_as(*i,result,ignore,new_flags);
                    }
                }
              /* Files */
              else
                {
                  if(flags.files)
                    {
                      /* A path is either source or ignored. */
                      if(!regex_match(i->leaf(),ignore_regex))
                        {
                          result(control,arx_file)
                            .push_back(file_attributes(*i,"_arx/"+i->leaf()));
                        }
                      else 
                        {
                          if(flags.ignore)
                            result(ignore,arx_file).push_back(*i);
                        }
                    }
                }
            }
        }
      return true;
    }
  return false;
}
