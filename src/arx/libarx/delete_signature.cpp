/* Delete a signature for a revision

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "gvfs.hpp"
#include "recursive_browse.hpp"
#include "remote_recursive_browse.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

class recursive_delete_signature: public remote_recursive_browse
{
public:
  recursive_delete_signature(const Parsed_Name &Name):
    remote_recursive_browse(Name) {}
  void revision_hook(const Parsed_Name &branch, const Revision_List &rev_list,
                     const gvfs::uri &branch_location)
  {
    for(Revision_List::const_iterator i=rev_list.begin(); i!=rev_list.end();
        ++i)
      {
        if(limit_name.revision().empty() || limit_name.patch_number()==*i)
          {
            gvfs::uri location(branch_location/patch_level(*i));
            gvfs::remove(location/(branch.branch() + patch_level(*i)
                                   + ".patches.tar.gz.sig"));
            gvfs::remove(location/"sha256.sig");
          }
      }
  }
};

void delete_signature(const Parsed_Name &name)
{
  recursive_delete_signature remote(name);
  recursive_browse(name.archive_location()
                   /name.branch_path_no_archive(), name, remote);
}
