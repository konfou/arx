/* List all of the public keys in an archive.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Parsed_Name.hpp"
#include "Spawn.hpp"
#include "key_location.hpp"
#include "signed_archive.hpp"
#include "../../config.h"
#include <iostream>
#include "get_gpg_name.hpp"

using namespace std;

void list_archive_keys(const Parsed_Name &name, const string key="")
{
  if(signed_archive(name))
    {
      list<string> gpg=get_gpg_name();
      if(!gpg.empty())
        {
          Spawn gpg_list;
          
          for(list<string>::iterator i=gpg.begin(); i!=gpg.end(); ++i)
            gpg_list << *i;
          gpg_list << "--no-default-keyring"
                   << "--keyring" << (key_location(name)).native_file_string()
                   << "--fingerprint";
          if(!key.empty())
            gpg_list << key;
          if(!gpg_list.execute(true))
            throw arx_error("Problems when executing gpg to list keys");
        }
      else if(Command_Info::verbosity>=quiet)
        cerr << "WARNING: Can not list archive keys because ArX has not been compiled with\ngpg support and you did not specify a gpg program with \"arx param\"." << endl;
    }
}
