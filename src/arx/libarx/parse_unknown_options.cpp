/* parse any options that might be there that shouldn't be there, as
   well as empty arguments.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <list>
#include <string>
#include "arx_error.hpp"

using namespace std;

void parse_unknown_options(list<string> &argument_list)
{
  if(!argument_list.empty())
    {
      list<string>::iterator arg=argument_list.begin();
      if((*arg).empty() || (*arg)=="--")
        {
          argument_list.pop_front();
        }
      else if(*(*arg).begin()=='-')
        {
          throw arx_error("unknown option: " + *arg);
        }
    }
}
