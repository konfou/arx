/* Parse options that are common to all commands (--help and
   verbosity).  If an option is parsed, then it returns true.
   Otherwise, it returns false.

   Copyright 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include <list>
#include <iostream>
#include "Command_Info.hpp"
#include "Verbosity.hpp"

using namespace std;

bool parse_common_options(list<string> &argument_list, Command_Info &info)
{
  if(argument_list.empty())
    return false;
  list<string>::iterator arg=argument_list.begin();

  if(*arg=="-h" || *arg=="--help" || *arg=="-H")
    {
      cout << info.cmd.brief_help << endl
           << info.cmd.usage << endl << endl
           << info.cmd.long_help << endl;
      exit(0);
    }
  else if(*arg=="-v")
    {
      int v=info.verbosity;
      ++v;
      if(v<=verbose)
        info.verbosity=static_cast<Verbosity>(v);
    }
  else if(*arg=="-q")
    {
      int v=info.verbosity;
      --v;
      if(v>=silent)
        info.verbosity=static_cast<Verbosity>(v);
    }
  else
    {
      return false;
    }
  argument_list.pop_front();
  return true;
}
