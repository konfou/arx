/* Parse a range.  A valid range is one of

   XX-YY
   XX-
   -YY

   Copyright 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/regex.hpp"
#include <string>
#include "arx_error.hpp"
#include <climits>

using namespace std;
using namespace boost;

pair<unsigned int, unsigned int> parse_range(const string &range)
{
  unsigned int lower, upper;
  if(regex_match(range,regex("[0-9]+-[0-9]+")))
    {
      match_results<string::const_iterator> m;
      regex_search(range,m,regex("-"));
      lower=atoi(range.substr(0,m[-1].length()).c_str());
      upper=atoi(range.substr(m[-1].length()+1).c_str())+1;
    }
  else if(regex_match(range,regex("(-)([0-9]+)")))
    {
      lower=0;
      upper=atoi(range.substr(1).c_str())+1;
    }
  else if(regex_match(range,regex("([0-9]+)(-)")))
    {
      lower=atoi(range.substr(0,range.size()-1).c_str());
      upper=numeric_limits<unsigned int>::max();
    }
  else
    {
      throw arx_error("Bad range " + range);
    }
  if(upper<=lower)
      throw arx_error("Bad range " + range);    
  return make_pair(lower,upper);
}
