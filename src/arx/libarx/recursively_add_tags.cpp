/* Recursively add all of the branches and their locations of a tag to
   a list.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Parsed_Name.hpp"
#include "Patch_Log.hpp"
#include "boost/filesystem/operations.hpp"
#include "latest_archive_revision.hpp"
#include "check_symlink_hierarchy.hpp"
#include "check_if_local_path.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void recursively_add_tags(list<pair<Parsed_Name,path> > &input_list,
                          const Parsed_Name &tag_name,
                          const path &directory)
{
  Parsed_Name temp_name;
  if(tag_name.revision().empty())
    {
      temp_name=latest_archive_revision(tag_name);
    }
  else
    {
      temp_name=tag_name;
    }
  Patch_Log log(temp_name);

  map<string,list<pair<string,string> > >::iterator i;
  i=log.rename_lists.find("Tags");
  if(i!=log.rename_lists.end())
    {
      for(list<pair<string,string> >::iterator j=i->second.begin();
          j!=i->second.end(); ++j)
        {
          /* We check here to make sure that we are not doing things
             through symlinks or that the paths try to go somewhere
             they shouldn't. */
          check_symlink_hierarchy(directory,directory/j->second);
          check_if_local_path(j->second);
          recursively_add_tags(input_list,j->first,directory/j->second);
        }
    }
  else
    {
      input_list.push_back(make_pair(tag_name,directory));
    }
}
