/* Convert a project tree into a new branch

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry
                           and the Regents of the University of California
  Copyright (C) 2004, 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include "arx_error.hpp"
#include "tree_root.hpp"
#include "patch_level.hpp"
#include "Parsed_Name.hpp"
#include "tree_branch.hpp"
#include "is_a_tag.hpp"
#include "latest_archive_revision.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void fork(const Parsed_Name &old_input_revision,
          const Parsed_Name &new_input_branch,
          const path &tree_directory)
{
  Parsed_Name input_revision(old_input_revision);

  if(input_revision.revision().empty())
    input_revision=latest_archive_revision(input_revision);

  if(is_a_tag(input_revision))
    throw arx_error("You may not fork from a tag\n\t"
                    + input_revision.complete_revision());

  valid_package_name(new_input_branch,Branch);
  if(!new_input_branch.revision().empty())
    throw arx_error("You may not specify a revision\n\t"
                    + new_input_branch.full_name());

  /* Create the patch directories */
  path root(tree_root(tree_directory));
  create_directories(root/"_arx/patch-log"/new_input_branch.branch_path());

  /* set the default branch */
  tree_branch(root,new_input_branch);

  /* Write the ++branch file */
  set_option_in_file(root/"_arx/++branch-revision",
                     input_revision.complete_revision());
}
