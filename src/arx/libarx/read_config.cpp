/* Read and parse a configuration file

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Parsed_Name.hpp"
#include "boost/tokenizer.hpp"
#include <list>
#include <utility>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

list<pair<path,Parsed_Name> > read_config(const path &config_path)
{
  if(!exists(config_path))
    throw arx_error("This config file does not exist\n\t"
                    + config_path.native_file_string());

  list<pair<path,Parsed_Name> > result;

  fs::ifstream config_file(config_path);
  string s;
  while(getline(config_file,s))
    {
      char_separator<char> sep("\t ");
      typedef tokenizer<char_separator<char> > tokenize;
      tokenize tokens(s,sep);
      
      if(tokens.begin()!=tokens.end())
        {
          tokenize::iterator j=tokens.begin();
          tokenize::iterator k=j;
          ++k;

          if((*j)[0]!='#')
            {
              /* I'm not really sure that includes are even a good
                 idea, so I'm leaving it commented out for now. */
//               if(*j=="%include")
//                 {
//                   if(tokens.size()!=2)
//                     throw arx_error("Wrong number of arguments for %include\n\t" + s);
//                   result.splice(read_config(config_path.branch_path()/(*k)));
//                 }
//               else
                {
                  result.push_back(make_pair(*j,*k));
                  ++k;
                  if(k!=tokens.end())
                    throw arx_error("Badly formatted line: too many elements\n\t" + s);
                }
            }
        }
    }
  return result;
}
