/* A simple function to read a complete file into a string

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "gvfs.hpp"

namespace gvfs
{
  std::string read_file_into_string(const uri &location)
  {
    Capture_Error capture;

    char *file_string;
    int file_size;

    GnomeVFSResult error_code=
      gnome_vfs_read_entire_file(location.escaped_string().c_str(),
                                 &file_size,&file_string);
    if(error_code!=GNOME_VFS_OK)
      throw exception("Read a complete file",error_code,location);

    std::string result(file_string,file_size);
    g_free(file_string);
    return result;
  }
}
