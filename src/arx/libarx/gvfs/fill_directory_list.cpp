/* A function to flll up a list with directory entries.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "gvfs.hpp"
#include <cstring>
#include <string>

namespace gvfs
{
  void fill_directory_list(std::list<std::string> &dir_list,
                           const uri &location)
  {
    Capture_Error capture;

    GList *dir_glist(NULL);
    GnomeVFSResult result=
      gnome_vfs_directory_list_load(&dir_glist,
                                    location.escaped_string().c_str(),
                                    GNOME_VFS_FILE_INFO_DEFAULT);

    if(result!=GNOME_VFS_OK)
      /* For http, if there is a listing file, then use it. */
      if(location.string().substr(0,5)=="http:"
         && exists(location / ".listing"))
        {
          in_file listing(location / ".listing");
          std::string element;
          while(listing())
            {
              element=listing.read_line();
              if(!element.empty())
                dir_list.push_back(element);
            }
          return;
        }
      else
        {
          throw exception("Opening a Directory",result,location);
        }
      
    GList *i;
    for(i=dir_glist; i!=NULL; i=i->next)
      {
        std::string name(static_cast<GnomeVFSFileInfo *>(i->data)->name);
        if(name!="." && name!="..")
          dir_list.push_back(static_cast<GnomeVFSFileInfo *>(i->data)->name);
      }
  }
}
