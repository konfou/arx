/* Copies one file to another.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "gvfs.hpp"

namespace gvfs
{
  void copy(const uri &source, const uri &target)
  {
    Capture_Error capture;

    GnomeVFSURI *source_uri, *target_uri;
    bool throw_error(false);

    source_uri=gnome_vfs_uri_new(source.escaped_string().c_str());
    target_uri=gnome_vfs_uri_new(target.escaped_string().c_str());

    GnomeVFSResult error_code=
      gnome_vfs_xfer_uri(source_uri,target_uri,GNOME_VFS_XFER_DEFAULT,
                         GNOME_VFS_XFER_ERROR_MODE_ABORT,
                         GNOME_VFS_XFER_OVERWRITE_MODE_REPLACE,0,0);
    if(error_code!=GNOME_VFS_OK)
      throw_error=true;

    gnome_vfs_uri_unref(source_uri);
    gnome_vfs_uri_unref(target_uri);

    if(throw_error)
      throw exception("Transferring Files",error_code,source,target);
  }
}
