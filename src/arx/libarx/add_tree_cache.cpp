/* Add a cached revision to a tree.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include "tree_root.hpp"
#include "Parsed_Name.hpp"
#include "get_revision.hpp"
#include "Temp_Directory.hpp"
#include "is_a_tag.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void add_tree_cache(const path &tree_directory, const path &cache_dir,
                    const Parsed_Name &input_revision)
{
  if(is_a_tag(input_revision))
    throw arx_error("You can not add a tree cache for a tag\n\t"
                    + input_revision.complete_revision());

  /* Need to create the target host directory */
  
  const path target((tree_root(tree_directory)/ "_arx/++cache")
                    / input_revision.revision_path());
  
  create_directories(target.branch_path());
  get_revision(input_revision,target,cache_dir,true);
  
  /* Make sure that ++default-branch doesn't get copied
     over. */
  
  fs::remove(target/"_arx/++default-branch");
}
