/* A simple utility to convert a list of checksums to an inventory.
   It just puts everything into source, since we don't checksum
   non-source paths.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Checksums.hpp"
#include "Inventory.hpp"

void convert_checksums_to_inventory(const Checksums &checksums,
                                    Inventory &inv)
{
  for(Checksums_const_iterator i=checksums.begin(); i!=checksums.end(); ++i)
    {
      inventory_types inv_type(source);
      if(i->first.is_control())
        inv_type=control;

      int path_type;
      if(i->second.empty())
        path_type=arx_dir;
      else
        path_type=arx_file;

      inv[inv_type][path_type].push_back(i->first);
    }
}
