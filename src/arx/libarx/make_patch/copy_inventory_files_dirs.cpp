/* Copy all of the files in an inventory located in "original" into
   "destination".

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Checksums.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/filesystem/convenience.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/serialization/map.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void write_properties(const map<string,string> &properties,
                      const path &prop_path)
{
  if(!properties.empty())
    {
      create_directories(prop_path.branch_path());
      fs::ofstream prop_file(prop_path);
      archive::text_oarchive prop_archive(prop_file);
      prop_archive << properties;
    }
}

void copy_inventory_files_dirs(const path &source,
                               const path &destination,
                               const path &prop_dir,
                               Checksums &checksums)
{
  /* First, sort the directories so that parents are created before
     children. */
  checksums.sort(checksums_file_path_cmp);

  for(Checksums::const_iterator i=checksums.begin();i!=checksums.end(); ++i)
    {
      create_directories((destination/i->first.file_path).branch_path());
      if(i->first.link)
        {
          /* For symlinks, just create a new symlink. */
          symlink(readlink(source/i->first.file_path),
                  destination/i->first.file_path);
          write_properties(i->first.properties,prop_dir
                           /(i->first.file_path.native_file_string()
                             + ".prop"));
        }
      else if(i->second.empty())
        {
          copy_directory(source/i->first.file_path,
                         destination/i->first.file_path);
          write_properties(i->first.properties,
                           prop_dir/i->first.file_path/"dir-prop");
        }
      else
        {
          copy_file(source/i->first.file_path,destination/i->first.file_path);
          write_properties(i->first.properties,
                           prop_dir/(i->first.file_path
                                     .native_file_string() + ".prop"));
        }
    }
}
