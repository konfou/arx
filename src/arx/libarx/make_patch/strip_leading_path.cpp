/* Strip a leading directory from all of the paths of an inventory,
   making them into relative paths.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <list>
#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"
#include "Inventory.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void strip_leading_path(const path &root, Inventory &inv)
{
  const int root_size=root.string().size()+1;
  for(int l=0;l<num_inventory_types;++l)
    for(int m=0;m<2;++m)
      for(list<file_attributes>::iterator i=inv(l,m).begin();
          i!=inv(l,m).end();++i)
        i->file_path=i->file_path.string().substr(root_size);
}
