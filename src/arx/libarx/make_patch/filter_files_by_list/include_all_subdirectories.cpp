/* Include all of the contents of a directory on an explicit list of
   paths to include in mkpatch.

  Copyright (C) 2003, 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"
#include <list>
#include <utility>
#include <string>
#include <functional>
#include "Checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void include_all_subdirectories(const string &dir,
                                Checksums &orig_only,
                                Checksums &mod_only,
                                list<pair<pair<file_attributes,sha256>,
                                pair<file_attributes,sha256> > > &in_both,
                                Checksums &new_orig_only,
                                Checksums &new_mod_only,
                                list<pair<pair<file_attributes,sha256>,
                                pair<file_attributes,sha256> > > &new_in_both)
{
  const int size(dir.size());

  /* We just compare the prefix of each inventoried file to see if it
     is part of this directory. */

  for(list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator
        i=in_both.begin(); i!=in_both.end();++i)
    {
      if(i->second.first.file_path.string().compare(0,size,dir)==0)
        new_in_both.push_back(*i);
    }
      
  for(Checksums::iterator i=mod_only.begin(); i!=mod_only.end();++i)
    {
      if(i->first.file_path.string().compare(0,size,dir)==0)
        new_mod_only.push_back(*i);
    }
      
  for(Checksums::iterator i=orig_only.begin(); i!=orig_only.end();++i)
    {
      if(i->first.file_path.string().compare(0,size,dir)==0)
        new_orig_only.push_back(*i);
    }
}
