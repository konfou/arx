/* Make sure that a restricted commit includes any renames away from
   destinations that are already included.
   
   Copyright (C) 2003, 2004 Walter Landry
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 dated June, 1991.
   
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include <list>
#include <utility>
#include <functional>
#include "Checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void check_renames_included
(const path &p,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &in_both,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &new_in_both)
{
  list<pair<pair<file_attributes,sha256>,
    pair<file_attributes,sha256> > >::iterator
    i=find_if(in_both.begin(),in_both.end(),
              bind2nd(ptr_fun(checksums_first_file_path_eq_string),
                      p.string()));
  if(i!=in_both.end())
    {
      if(find(new_in_both.begin(),new_in_both.end(),*i)==new_in_both.end())
        {
          throw arx_error
            ("This path\n\t"
             + p.native_file_string()
             + "\nrequires the rename\n\t"
             + i->first.first.file_path.native_file_string()
             + " --> "
             + i->second.first.file_path.native_file_string());
        }
    }
}
