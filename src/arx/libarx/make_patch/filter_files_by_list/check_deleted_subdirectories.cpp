/* Make sure that a restricted commit includes any subdirectories of a
   directory that has been moved.

  Copyright (C) 2003, 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include <list>
#include <utility>
#include "Checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void check_deleted_subdirectories
(const path &dir, Checksums &new_orig_only,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &new_in_both,
 Checksums &orig_only,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &in_both)
{
  const string dir_with_separator(dir.string() + "/");
  const int size(dir_with_separator.size());

  for(list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator
        i=in_both.begin(); i!=in_both.end(); ++i)
    {
      if(i->first.first.file_path.string()
         .compare(0,size,dir_with_separator)==0
         && find(new_in_both.begin(),new_in_both.end(),*i)==new_in_both.end())
        {
          throw arx_error("This delete\n\t"
                          + dir.native_file_string()
                          + "\nrequires this rename\n\t"
                          + i->first.first.file_path.native_file_string()
                          + " --> "
                          + i->second.first.file_path.native_file_string());
        }
    }
  
  /* This part should be satisfied automatically, since we already
     added all subdirectories previously.  For now, better safe
     than sorry. */
  
  for(Checksums::iterator i=orig_only.begin(); i!=orig_only.end(); ++i)
    {
      if(i->first.file_path.string().compare(0,size,dir_with_separator)==0
         && find_if(new_orig_only.begin(),new_orig_only.end(),
                    bind2nd(ptr_fun(checksums_file_path_eq_string),
                            i->first.file_path.string()))
         ==new_orig_only.end())
        {
          throw arx_error("This delete\n\t"
                          + dir.native_file_string()
                          + "\nrequires this delete\n\t"
                          + i->first.file_path.native_file_string());
        }
    }
}

