/* Make that there aren't any files being moved to or created in
   directories that either don't exist or have changed, unless that
   directory is also included in the files.

   Copyright (C) 2003, 2004 Walter Landry
  
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 dated June, 1991.
   
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include <list>
#include <utility>
#include <functional>
#include "Checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void check_path_directory
(const path &p,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &new_in_both,
 Checksums &new_mod_only,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &in_both)
{
  const path dir=p.branch_path();
  if(dir.string()!=""
     && !is_control(p)
     && find_if(new_in_both.begin(), new_in_both.end(),
                bind2nd(ptr_fun(checksums_second_file_path_eq_string),
                        dir.string()))==new_in_both.end()
     && find_if(new_mod_only.begin(), new_mod_only.end(),
                bind2nd(ptr_fun(checksums_file_path_eq_string),
                        dir.string()))==new_mod_only.end())
    {
      list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator
        l=find_if(in_both.begin(),in_both.end(),
                  bind2nd(ptr_fun(checksums_second_file_path_eq_string),
                          dir.string()));
      if(l==in_both.end() || l->first!=l->second)
        {
          throw arx_error("This path\n\t"
                          + p.native_file_string()
                          + "\nis in a directory\n\t"
                          + dir.native_file_string()
                          + "\nthat needs to be included in the list of paths for a patch.");
        }
    }
}
