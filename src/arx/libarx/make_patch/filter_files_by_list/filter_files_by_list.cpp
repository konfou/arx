/* Filter the various lists of paths (orig_only, mod_only, in_both) so
   that they only apply to paths in a particular list (path_list).
   This restricts the commit to only those files.

   We have to be careful.  If it replaces a file that has been
   renamed, we require the renamed file to be present as well.  Also,
   if a file has been renamed to a different directory, then if that
   directory is new, or has been renamed, then it has to be in the
   list.  Finally, if a directory has been moved, then all of the old
   contents of that directory have to be in the list.

   Copyright (C) 2003, 2004 Walter Landry
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; version 2 dated June,
   1991.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include <list>
#include <utility>
#include "Checksums.hpp"
#include "Path_List.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;


extern void check_renamed_subdirectories
(const path &old_dir, const path &new_dir,
 Checksums &new_orig_only,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &new_in_both,
 Checksums &orig_only,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &in_both);


extern void check_deleted_subdirectories
(const path &dir, Checksums &new_orig_only,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &new_in_both,
 Checksums &orig_only,
 list<pair<pair<file_attributes,sha256>,
 pair<file_attributes,sha256> > > &in_both);


extern void check_renames_included(const path &p,
                                   list<pair<pair<file_attributes,sha256>,
                                   pair<file_attributes,sha256> > > &in_both,
                                   list<pair<pair<file_attributes,sha256>,
                                   pair<file_attributes,sha256> > > &new_in_both);

extern void check_path_directory(const path &p,
                                 list<pair<pair<file_attributes,sha256>,
                                 pair<file_attributes,sha256> > > &new_in_both,
                                 Checksums &new_mod_only,
                                 list<pair<pair<file_attributes,sha256>,
                                 pair<file_attributes,sha256> > > &in_both);

extern void include_all_subdirectories(const string &dir,
                                       Checksums &orig_only,
                                       Checksums &mod_only,
                                       list<pair<pair<file_attributes,sha256>,
                                       pair<file_attributes,sha256> > > &in_both,
                                       Checksums &new_orig_only,
                                       Checksums &new_mod_only,
                                       list<pair<pair<file_attributes,sha256>,
                                       pair<file_attributes,sha256> > > &new_in_both);


void filter_files_by_list(const Path_List &path_list,
                          Checksums &orig_only,
                          Checksums &mod_only,
                          list<pair<pair<file_attributes,sha256>,
                          pair<file_attributes,sha256> > > &in_both)
{
  /* Make sure a path is in the modified inventory. */

  Checksums new_orig_only, new_mod_only;
  
  list<pair<pair<file_attributes,sha256>,
    pair<file_attributes,sha256> > > new_in_both;

  for(Path_List::const_iterator i=path_list.begin(); i!=path_list.end(); ++i)
    {
      bool found(false), deleted(false);
      list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator j;

      j=find_if(in_both.begin(),in_both.end(),
                bind2nd(ptr_fun(checksums_second_file_path_eq_string),
                        i->string()));
      if(j!=in_both.end())
        {
          new_in_both.push_back(*j);
          found=true;
        }

      if(!found)
        {
          Checksums::iterator k;
          
          k=find_if(mod_only.begin(),mod_only.end(),
                    bind2nd(ptr_fun(checksums_file_path_eq_string),
                            i->string()));
          if(k!=mod_only.end())
            {
              new_mod_only.push_back(*k);
              found=true;
            }
          if(!found)
            {
              k=find_if(orig_only.begin(),orig_only.end(),
                        bind2nd(ptr_fun(checksums_file_path_eq_string),
                                i->string()));
              if(k!=orig_only.end())
                {
                  new_orig_only.push_back(*k);
                  found=true;
                  deleted=true;
                  
                  /* If it is a directory, then include all
                     subdirectories. */
                  if(k->second.empty())
                    include_all_subdirectories(i->string()+"/",
                                               orig_only,mod_only,
                                               in_both,new_orig_only,
                                               new_mod_only,new_in_both);
                }
              if(!found)
                {
                  throw arx_error("This path\n\t" + i->native_file_string()
                                  +"\nis not in the inventory.  Did you forget to add it or its parent directory?");
                }
            }
        }
      
      if(!deleted)
        {
          /* Check if it replaces something that has been deleted. */
          Checksums::iterator
            l=find_if(orig_only.begin(),orig_only.end(),
                      bind2nd(ptr_fun(checksums_file_path_eq_string),
                              i->string()));
          if(l!=orig_only.end())
            new_orig_only.push_back(*l);

          /* If a path is a directory, recursively include all of the
             contents of the directory in the patch. */
          if(!symbolic_link_exists(*i) && is_directory(*i))
            {
              include_all_subdirectories(i->string()+"/",orig_only,mod_only,
                                         in_both,new_orig_only,new_mod_only,
                                         new_in_both);
            }
        }
    }
  
  /* Make that there aren't any files being moved to or created in
     directories that either don't exist or have changed, unless that
     directory is also included in the files. */

  for(Checksums::iterator i=new_mod_only.begin(); i!=new_mod_only.end(); ++i)
    check_path_directory(i->first.file_path,new_in_both,new_mod_only,in_both);

  for(list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator i=new_in_both.begin();
      i!=new_in_both.end(); ++i)
    check_path_directory(i->second.first.file_path,new_in_both,new_mod_only,
                         in_both);

  /* Make sure that any new files or renames include any files
     that have been moved from the destination.  Deletes are
     already taken care of. */
  
  for(Checksums::iterator i=new_mod_only.begin(); i!=new_mod_only.end(); ++i)
    check_renames_included(i->first.file_path,in_both,new_in_both);


  for(list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator i=new_in_both.begin();
      i!=new_in_both.end(); ++i)
    check_renames_included(i->second.first.file_path,in_both,new_in_both);

  /* If we are moving or deleting a directory, are all of the old
     subdirectories going as well? */
  for(list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator
        i=new_in_both.begin(); i!=new_in_both.end(); ++i)
    if(i->first.second.empty())
      check_renamed_subdirectories(i->first.first.file_path,
                                   i->second.first.file_path,
                                   new_orig_only, new_in_both,
                                   orig_only, in_both);
  
  for(Checksums::iterator i=new_orig_only.begin();
      i!=new_orig_only.end(); ++i)
    if(i->second.empty())
      check_deleted_subdirectories(i->first.file_path, new_orig_only,
                                   new_in_both, orig_only, in_both);

  /* Uniquify all of the lists and set the real lists to the new
     lists. */

  new_orig_only.sort(checksum_inventory_id_cmp);
  new_orig_only.unique();
  orig_only=new_orig_only;

  new_mod_only.sort(checksum_inventory_id_cmp);
  new_mod_only.unique();
  mod_only=new_mod_only;
  
  new_in_both.sort();
  new_in_both.unique();
  in_both=new_in_both;
}
