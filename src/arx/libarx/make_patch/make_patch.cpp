/* Create a patch between two source trees.  If one of the input patch
   tree paths is empty, then it will create a patch which adds or
   deletes everything.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Copyright (C) 2004 Walter Landry

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/filesystem/exception.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include "Inventory_Flags.hpp"
#include "Inventory.hpp"
#include "inventory_directory.hpp"
#include <list>
#include <utility>
#include "Current_Path.hpp"
#include "read_checksums.hpp"
#include "convert_inventory_to_checksums.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "Path_List.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void filter_files_by_list(const Path_List &path_list,
                                 Checksums &orig_only,
                                 Checksums &mod_only,
                                 list<pair<pair<file_attributes,sha256>,
                                 pair<file_attributes,sha256> > > &in_both);

extern void check_duplicate_ids(const Checksums &checksums);
extern void cull_changed_id_directories(Checksums &orig_only,
                                        Checksums &mod_only);

extern void copy_inventory_files_dirs(const path &source,
                                      const path &destination,
                                      const path &prop_dir,
                                      Checksums &checksums);

extern void compare_paths(const path &original,
                          const path &modified,
                          list<pair<pair<file_attributes,sha256>,
                          pair<file_attributes,sha256> > > &in_both,
                          Checksums &changed_orig, Checksums &changed_mod,
                          Checksums &patched, const path &destination);

extern void add_parents(const Checksums &orig_only, const Checksums &mod_only,
                        Checksums &changed_orig, Checksums &changed_mod,
                        list<pair<pair<file_attributes,sha256>,
                        pair<file_attributes,sha256> > > &in_both,
                        list<pair<pair<file_attributes,sha256>,
                        pair<file_attributes,sha256> > > &complete_in_both);

/* A more complicated version that returns information about the
   patch. */

bool make_patch(const path &original, const path &modified,
                const path &destination, const Path_List &path_list,
                Checksums &orig_only, Checksums &mod_only,
                Checksums &patched,
                list<pair<pair<file_attributes,sha256>,
                pair<file_attributes,sha256> > > &in_both)
{
  bool status;
  if(!original.empty() && !exists(original))
    {
      throw arx_error("Original does not exist: "
                      + original.native_file_string());
    }
  if(!modified.empty() && !exists(modified))
    {
      throw arx_error("Modified does not exist: "
                      + modified.native_file_string());
    }
  if(lexists(destination))
    {
      throw arx_error("Destination already exists: "
                      + destination.native_file_string());
    }

  /* Inventory the two directories, setting the appropriate
     flags.  We set and reset the directories.  This is a
     performance hack. */
     
  Checksums original_checksums, modified_checksums;

  if(!original.empty())
    {
      read_checksums(original,original_checksums);
    }
  if(!modified.empty())
    {
      read_checksums(modified,modified_checksums);

      /* Make sure that everything exists and is the appropriate type
         (file, directory, or link) */

      for(Checksums::iterator i=modified_checksums.begin();
          i!=modified_checksums.end(); ++i)
        {
          if(!lexists(modified/i->first.file_path))
            throw arx_error("There is an inventory id for this path, but the path itself does not exist.\n\t" + i->first.file_path.native_file_string());
          i->first.link=symbolic_link_exists(modified/i->first.file_path);
        }
    }

  /* Sort the checksums and check for duplicate ids */
      
  original_checksums.sort(checksum_inventory_id_cmp);
  modified_checksums.sort(checksum_inventory_id_cmp);

  check_duplicate_ids(original_checksums);
  check_duplicate_ids(modified_checksums);

  /* Figure out which files are only in original or modified. */
      
  /* It is probably more efficient to splice elements from one
     list to the other, but this is easier for now. */
      
  Checksums changed_orig, changed_mod;
      
  Checksums::iterator i(original_checksums.begin()),
    j(modified_checksums.begin());
  while(i!=original_checksums.end() && j!=modified_checksums.end())
    {
      if(i->first.inventory_id==j->first.inventory_id)
        {
          in_both.push_back(make_pair(*i,*j));
          ++i;
          ++j;
        }
      else if(i->first.inventory_id < j->first.inventory_id)
        {
          orig_only.push_back(*i);
          ++i;
        }
      else
        {
          mod_only.push_back(*j);
          ++j;
        }
    }
  for(;i!=original_checksums.end();++i)
    {
      orig_only.push_back(*i);
    }
  for(;j!=modified_checksums.end();++j)
    {
      mod_only.push_back(*j);
    }

  /* We need an extra copy for add_parents, because in_both can get
     mangled in filter_files_by_list. */
  list<pair<pair<file_attributes,sha256>,pair<file_attributes,sha256> > >
    complete_in_both(in_both);

  /* We only use the path_list if we are not doing an ordinary diff
     (not a manually created path_list) in a no-edit tree and the
     path_list is not empty, or we are in a no-edit tree and we are
     diffing against the same revision.  Otherwise, we have to do the
     full diff. */
  if((!path_list.empty() && !path_list.no_edit_diff)
     || (path_list.no_edit_diff
         && original_checksums.name==modified_checksums.name))
    {
      Current_Path curr_path(modified);
      filter_files_by_list(path_list,orig_only,mod_only,in_both);
    }

  /* Compute checksums for all mod-only files */
  for(Checksums::iterator i=mod_only.begin(); i!=mod_only.end(); ++i)
    {
      if(!i->second.empty())
        {
          if(i->first.link)
            i->second=sha256((readlink(modified/i->first.file_path))
                             .string());
          else
            i->second=sha256(modified/i->first.file_path);
        }
    }

  /* Don't create the directory until now because we may have
     aborted due to exceptions thrown by filter_files_by_list,
     just leaving an empty directory. */

  create_directory(destination);

  /* Remove directories that appear to be deleted and created with
     the same name.  This is just a directory that has had it's id
     changed. */
  cull_changed_id_directories(orig_only,mod_only);

  /* Save copies of deleted and new files and their properties. */
  copy_inventory_files_dirs(original, destination / "deleted",
                            destination / "deleted-prop", orig_only);
  copy_inventory_files_dirs(modified, destination / "added",
                            destination / "added-prop", mod_only);
      
  {
    Checksums orig_temp(orig_only), mod_temp(mod_only);
        
    changed_orig.splice(changed_orig.begin(),orig_temp);
    changed_mod.splice(changed_mod.begin(),mod_temp);
  }

  /* Compare files and directories, writing out patches. */
      
  create_directory(destination / "patches");
  compare_paths(original, modified, in_both, changed_orig, changed_mod,
                patched, destination / "patches");

  /* Sort and uniquify the changed lists */
      
  changed_orig.sort(checksum_inventory_id_cmp);
  changed_orig.unique();
      
  /* Need to uniquify patched[] because it gets passed back. */
      
  patched.sort(checksum_inventory_id_cmp);
  patched.unique();
      
  /* Add in the list of new patched files.  This involves some copying
     that is not really needed if patched[] is never looked at. I
     don't think this is actually required, since in every place where
     something is put in patched, it is also put in changed_mod. */

  Checksums temp(patched);
  changed_mod.splice(changed_mod.end(),temp);

  changed_mod.sort(checksum_inventory_id_cmp);
  changed_mod.unique();

  add_parents(orig_only,mod_only,changed_orig,changed_mod,in_both,
              complete_in_both);

  /* Write out the indices.  These contain files and directories
     that have changed in some way.  */

  {
    fs::ofstream orig_files_index(destination / "orig-files-index");
    fs::ofstream orig_dirs_index(destination / "orig-dirs-index");
    archive::text_oarchive orig_files_archive(orig_files_index);
    archive::text_oarchive orig_dirs_archive(orig_dirs_index);
    for(Checksums::iterator i=changed_orig.begin();
        i!=changed_orig.end(); ++i)
      if(i->second.empty())
        {
          orig_dirs_archive << i->first.file_path.string()
                            << i->first.inventory_id;
        }
      else
        {
          orig_files_archive << i->first.file_path.string()
                             << i->first.inventory_id;
        }
  }
  {
    fs::ofstream mod_files_index(destination / "mod-files-index");
    fs::ofstream mod_dirs_index(destination / "mod-dirs-index");
    archive::text_oarchive mod_files_archive(mod_files_index);
    archive::text_oarchive mod_dirs_archive(mod_dirs_index);
    for(Checksums::iterator i=changed_mod.begin();
        i!=changed_mod.end(); ++i)
      if(i->second.empty())
        {
          mod_dirs_archive << i->first.file_path.string()
                           << i->first.inventory_id;
        }
      else
        {
          mod_files_archive << i->first.file_path.string()
                            << i->first.inventory_id;
        }
  }
  status=!(changed_orig.empty() && changed_mod.empty());
  return status;
}


/* A simple version which returns no information. */

bool make_patch(const path &original, const path &modified,
                const path &destination, const Path_List &path_list)
{
  list<pair<pair<file_attributes,sha256>,
    pair<file_attributes,sha256> > > in_both;

  Checksums patched, orig_only, mod_only;
  
  return make_patch(original,modified,destination,path_list,orig_only,
                    mod_only,patched,in_both);
}

