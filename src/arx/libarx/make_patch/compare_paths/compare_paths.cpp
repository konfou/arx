/* Compare the paths that are in both the original and modified trees.
   We are really just dividing things up into files and directories
   here.

  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include "inventory_types.hpp"
#include <list>
#include <utility>
#include "Checksums.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void compare_files(const path &original,
                          const path &modified,
                          const pair<file_attributes,sha256> &orig,
                          pair<file_attributes,sha256> &mod,
                          Checksums &changed_orig,
                          Checksums &changed_mod,
                          Checksums &patched,
                          const path &destination);

extern void compare_directories(pair<file_attributes,sha256> &orig,
                                pair<file_attributes,sha256> &mod,
                                Checksums &changed_orig,
                                Checksums &changed_mod,
                                Checksums &patched,
                                const path &destination);

extern void remove_empty_directories(const path &directory);

void compare_paths(const path &original,
                   const path &modified,
                   list<pair<pair<file_attributes,sha256>,
                   pair<file_attributes,sha256> > > &in_both,
                   Checksums &changed_orig, Checksums &changed_mod,
                   Checksums &patched, const path &destination)
{
  for(list<pair<pair<file_attributes,sha256>,
        pair<file_attributes,sha256> > >::iterator
        i=in_both.begin(); i!=in_both.end(); ++i)
    {
      pair<file_attributes,sha256> &orig(i->first), &mod(i->second);
      
      if(orig.second.empty()!=mod.second.empty())
        {
          throw arx_error("INTERNAL ERROR: Two paths have the same inventory id, but one is a directory and the other is a file or symlink\n\tfirst path: "
                          + orig.first.file_path.string()
                          + "\n\tsecond path: "
                          + mod.first.file_path.string());
        }
      else if(orig.second.empty())
        {
          compare_directories(orig,mod,changed_orig,changed_mod,patched,
                              destination);
        }
      else
        {
          compare_files(original,modified,orig,mod,
                        changed_orig,changed_mod,patched,destination);
        }
    }
  /* Delete empty directories.  Those are the results of empty diffs. */
  remove_empty_directories(destination);
}
