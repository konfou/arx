/* Compare the files in two inventories, writing patches as needed.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Copyright (C) 2004, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "config.h"
#include "Checksums.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/filesystem/convenience.hpp"
#include <list>
#include <utility>
#include "arx_error.hpp"
#include "Spawn.hpp"
#include "set_option_in_file.hpp"
#include "write_property_diffs.hpp"
#include "read_file_into_string.hpp"
#include "xdelta.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void compare_files(const path &original,
                   const path &modified,
                   const pair<file_attributes,sha256> &orig,
                   pair<file_attributes,sha256> &mod,
                   Checksums &changed_orig,
                   Checksums &changed_mod,
                   Checksums &patched,
                   const path &destination)
{
  create_directories(destination/mod.first.file_path.branch_path());
  
  if(orig.first.link)
    {
      /* If they are both symlinks */
      if(mod.first.link)
        {
          const path mod_link(readlink(modified/mod.first.file_path));

          mod.second=sha256(mod_link.string());

          if(orig.second!=mod.second)
            {
              /* Make sure that the original tree is not corrupted */
              const path orig_link(readlink(original/orig.first.file_path));
              if(orig.second!=sha256(orig_link.string()))
                throw arx_error("INTERNAL ERROR: The pristine tree is corrupted.  This link points to a\ndifferent place than is recorded in the manifest.\n\tpristine path: " + orig.first.file_path.string()
                                + "\n\twrong link: "
                                + orig_link.native_file_string()
                                + "\n\tcorrect hash: "
                                + orig.second.string()
                                + "\n\twrong hash:   "
                                + sha256(orig_link.string()).string());

              set_option_in_file(destination/
                                 (mod.first.file_path.string()
                                  + ".link-orig"),
                                 orig_link.native_file_string());
              set_option_in_file(destination/
                                 (mod.first.file_path.string()
                                  + ".link-mod"),
                                 mod_link.native_file_string());
              
              changed_orig.push_back(orig);
              changed_mod.push_back(mod);
              patched.push_back(mod);
            }
        }
      /* Orig is a link, mod is a file */
      else
        {
          /* Make sure that the original tree is not corrupted */
          const path orig_link(readlink(original/orig.first.file_path));
          if(orig.second!=sha256(orig_link.string()))
            throw arx_error("INTERNAL ERROR: The pristine tree is corrupted.  This link points to a\ndifferent place than is recorded in the manifest.\n\tpristine path: " + orig.first.file_path.string()
                            + "\n\twrong link: "
                            + orig_link.native_file_string()
                            + "\n\tcorrect hash: "
                            + orig.second.string()
                            + "\n\twrong hash:   "
                            + sha256(orig_link.string()).string());

          mod.second=sha256(modified/mod.first.file_path);

          set_option_in_file(destination/
                             (mod.first.file_path.string()
                              + ".link-orig"),
                             orig_link.native_file_string());
          copy_file(modified/mod.first.file_path,
                    destination/(mod.first.file_path.string() + ".mod"));
          changed_orig.push_back(orig);
          changed_mod.push_back(mod);
          patched.push_back(mod);
        }
    }
  else
    {
      /* Orig is a file, mod is a link */
      if(mod.first.link)
        {
          /* Make sure that the original tree is not corrupted */
              if(orig.second!=sha256(original/orig.first.file_path))
                throw arx_error("INTERNAL ERROR: The pristine tree is corrupted.  This file's contents hash\nto a different value than is recorded in the manifest.\n\tpristine path: " + orig.first.file_path.native_file_string()
                                + "\n\tcorrect hash: "
                                + orig.second.string()
                                + "\n\twrong hash:   "
                                + sha256(original/orig.first.file_path).string());

          const path mod_link(readlink(modified/mod.first.file_path));
          mod.second=sha256(mod_link.string());

          set_option_in_file(destination/(mod.first.file_path.string()
                                          + ".link-mod"),
                             mod_link.native_file_string());
          copy_file(original/orig.first.file_path,
                    destination/(mod.first.file_path.string() + ".orig"));
          changed_orig.push_back(orig);
          changed_mod.push_back(mod);
          patched.push_back(mod);
        }
      /* Both are files */
      else
        {
          bool files_different(false);
          
          /* Have properties changed? */
          if(orig.first.properties!=mod.first.properties)
            {
              write_property_diffs
                (orig.first.file_path, mod.first.file_path,
                 orig.first.properties, mod.first.properties,
                 destination/(mod.first.file_path.string() + ".prop-orig"),
                 destination/(mod.first.file_path.string() + ".prop-mod"));
              files_different=true;
            }
          
          /* Compute and set the new checksum.  If it is different,
             try to diff. */

          mod.second=sha256(modified/mod.first.file_path);
          if(orig.second!=mod.second)
            {
              /* Make sure that the original tree is not corrupted */
              if(orig.second!=sha256(original/orig.first.file_path))
                throw arx_error("INTERNAL ERROR: The pristine tree is corrupted.  This file's contents hash\nto a different value than is recorded in the manifest.\n\tpristine path: " + orig.first.file_path.native_file_string()
                                + "\n\tcorrect hash: "
                                + orig.second.string()
                                + "\n\twrong hash:   "
                                + sha256(original/orig.first.file_path).string());

              files_different=true;
              
              Spawn s;
              s << ARXDIFF << "--binary" << "-u"
                << (original/orig.first.file_path).native_file_string()
                << (modified/mod.first.file_path).native_file_string();
              s.output=(destination/mod.first.file_path).native_file_string()
                + ".patch";
              s.error="/dev/null";
              
              int diffstat;
              if(!s.execute(diffstat,true))
                throw arx_error("Problem when executing diff");
              
              /* If there is no difference, there is something
                 wrong. */
              
              if(diffstat==0)
                {
                  throw arx_error("Hashes don't match, but diff says there isn't a difference.  You have\neither a corrupted hash or corrupted files.\n\t"
                                  + (original/orig.first.file_path)
                                  .native_file_string() + "\n\t\t"
                                  + orig.second.string() + "\n\t"
                                  + (modified/mod.first.file_path)
                                  .native_file_string() + "\n\t\t"
                                  + mod.second.string());
                }
              /* If the diff failed, use xdelta. */
              if(diffstat==2)
                {
                  remove(destination/(mod.first.file_path.string() +".patch"));
                  
                  string orig_string(read_file_into_string
                                     (original/orig.first.file_path));
                  string mod_string(read_file_into_string
                                    (modified/mod.first.file_path));
                  string delta_orig, delta_mod;

                  sha256 orig_hash(orig_string), mod_hash(mod_string);

                  compute_xdelta(orig_string,mod_string,delta_orig);
                  compute_xdelta(mod_string,orig_string,delta_mod);

                  fs::ofstream delta_orig_file
                    (destination/(mod.first.file_path.string() +".xorig"));
                  delta_orig_file << orig_hash;
                  fs::ofstream delta_mod_file
                    (destination/(mod.first.file_path.string() +".xmod"));
                  delta_mod_file << mod_hash;

                  if(!delta_orig_file.write(delta_orig.c_str(),
                                            delta_orig.size())
                     || !delta_mod_file.write(delta_mod.c_str(),
                                              delta_mod.size()))
                    throw arx_error("Can't write out a binary diff\n\t"
                                    + (destination/
                                       (mod.first.file_path.string()
                                        +".xorig")).native_file_string()
                                    + "\n\t"
                                    + (destination/
                                       (mod.first.file_path.string()
                                        +".xmod")).native_file_string());
                }
            }
          
          /* If there are any differences, add these files to
             the list of changed files. */
          if(files_different)
            {
              changed_orig.push_back(orig);
              changed_mod.push_back(mod);
              patched.push_back(mod);
            }
        }
    }

  /* Check for renames.  We have to do this at the end because
     mod.second may have been modified by patching. */

  if(orig.first.file_path.string()!=mod.first.file_path.string())
    {
      changed_orig.push_back(orig);
      changed_mod.push_back(mod);
    }
}
