/* Compare the directories that are in both the original and modified
   trees, looking only at properties and renames.  Write out
   the appropriate files in the patch-directory if they are different.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003, 2004 Walter Landry and the Regents
                           of the University of California
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include "Checksums.hpp"
#include <list>
#include "write_property_diffs.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void compare_directories(pair<file_attributes,sha256> &orig,
                         pair<file_attributes,sha256> &mod,
                         Checksums &changed_orig,
                         Checksums &changed_mod,
                         Checksums &patched,
                         const path &destination)
{
  bool changed(false);
  /* Check for renames */
  if(orig.first.file_path.string()!=mod.first.file_path.string())
    {
      changed=true;
    }
  
  /* Check properties */
  if(orig.first.properties!=mod.first.properties)
    {
      create_directories(destination/mod.first.file_path);
      write_property_diffs(orig.first.file_path,mod.first.file_path,
                           orig.first.properties,mod.first.properties,
                           destination/mod.first.file_path/"dir-prop-orig",
                           destination/mod.first.file_path/"dir-prop-mod");
      changed=true;
      patched.push_back(mod);            
    }

   if(changed)
     {
      changed_orig.push_back(orig);
      changed_mod.push_back(mod);
    }
}
