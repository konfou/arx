/* Remove all empty directories below the current directory.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void remove_empty_directories(const path &directory)
{
  if(lexists(directory))
    {
      fs::directory_iterator end;
      for(fs::directory_iterator i(directory); i!=end; ++i)
        {
          if(!symbolic_link_exists(*i) && is_directory(*i))
            {
              remove_empty_directories(*i);
              if(fs::is_empty(*i))
                remove(*i);
            }
        }
    }
}
