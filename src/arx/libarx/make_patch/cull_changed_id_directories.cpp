/* Remove directories that appear to be deleted and created with the
   same name.  This is just a directory that has had its id changed.
   The directory can stay, though the external inventory id will
   change.

  Copyright (C) 2003, 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Checksums.hpp"

using namespace std;

void cull_changed_id_directories(Checksums &orig_only, Checksums &mod_only)
{
  /* We only care about things that go into the archive, so only do
     source and control directories. */

  Checksums::iterator i(orig_only.begin()), j(mod_only.begin());
    
  while(i!=orig_only.end() && j!=mod_only.end())
    {
      if(i->first.inventory_id==j->first.inventory_id)
        {
          Checksums::iterator i_old(i), j_old(j);
          ++i;
          ++j;
          orig_only.erase(i_old);
          mod_only.erase(j_old);
        }
      else if(i->first.inventory_id < j->first.inventory_id)
        {
          ++i;
        }
      else
        {
          ++j;
        }
    }
}
