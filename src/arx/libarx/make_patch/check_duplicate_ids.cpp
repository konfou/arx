/* Check whether a Checksum has duplicate ids.  This assumes that the
   inventory is already sorted by ids.

  Copyright (C) 2003, 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "file_attributes.hpp"
#include "Checksums.hpp"
#include <list>
#include "arx_error.hpp"

using namespace std;

void check_duplicate_ids(const Checksums &checksums)
{
  if(!checksums.empty())
    {
      Checksums::const_iterator i(checksums.begin());
      ++i;
      for(Checksums::const_iterator j(checksums.begin()); i!=checksums.end();
          ++j, ++i)
        {
          if(checksum_inventory_id_eq(*i,*j))
            throw arx_error("Duplicate inventory ids.  These two paths have the same inventory ids.\n\t" + i->first.file_path.native_file_string()
                            + "\n\t"
                            + j->first.file_path.native_file_string());
        }
    }
}
