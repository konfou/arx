/* Add in all of the parents of the moved, added, and deleted files so
   that they can be put in the proper place, even in the presence of
   renames.

  Copyright (C) 2005 Walter Landry

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Checksums.hpp"
#include "arx_error.hpp"
#include <list>
#include <utility>
#include <functional>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void add_parents_for_one_element
(const file_attributes &file_attrib,
 const Checksums &changed_master,
 Checksums &changed_orig, Checksums &changed_mod,
 list<pair<pair<file_attributes,sha256>,pair<file_attributes,sha256> > >
 &complete_in_both,
 bool cmp(const pair<pair<file_attributes,sha256>,
          pair<file_attributes,sha256> > a,
          const string b))
{
  /* We don't need to check control paths, because they never move */
  if(file_attrib.is_control())
    return;

  path parent_path=file_attrib.file_path.branch_path();
  /* This is the slow, inefficient way to do it.  Ideally, we
     would only add a path once, and then we would later check
     that that path had its parents.  So each moved path would not
     have to iterate up the whole chain. */
  while(!parent_path.empty())
    {
      string parent=parent_path.string();
      if(find_if(changed_master.begin(),changed_master.end(),
                 bind2nd(ptr_fun(checksums_file_path_eq_string),parent))
         ==changed_master.end())
        {
          list<pair<pair<file_attributes,sha256>,
            pair<file_attributes,sha256> > >::iterator j=
            find_if(complete_in_both.begin(),complete_in_both.end(),
                    bind2nd(ptr_fun(cmp),parent));
          if(j==complete_in_both.end())
            throw arx_error("INTERNAL ERROR: I can not find the parent of this file in the manifest:\n\t" + file_attrib.file_path.native_file_string());
            
          changed_orig.push_back(j->first);
          changed_mod.push_back(j->second);
        }
      parent_path=parent_path.branch_path();
    }
}

void add_parents(const Checksums &orig_only, const Checksums &mod_only,
                 Checksums &changed_orig, Checksums &changed_mod,
                 list<pair<pair<file_attributes,sha256>,
                 pair<file_attributes,sha256> > > &in_both,
                 list<pair<pair<file_attributes,sha256>,
                 pair<file_attributes,sha256> > > &complete_in_both)
{
  for(Checksums::const_iterator i=orig_only.begin(); i!=orig_only.end(); ++i)
    add_parents_for_one_element(i->first,changed_orig,changed_orig,
                                changed_mod,complete_in_both,
                                checksums_first_file_path_eq_string);
  for(Checksums::const_iterator i=mod_only.begin(); i!=mod_only.end(); ++i)
    add_parents_for_one_element(i->first,changed_mod,changed_orig,
                                changed_mod,complete_in_both,
                                checksums_second_file_path_eq_string);

  for(list<pair<pair<file_attributes,sha256>,pair<file_attributes,sha256> > >::
        iterator i=in_both.begin(); i!=in_both.end(); ++i)
    {
      /* If we have a rename */
      if(i->first.first.file_path.string()!=i->second.first.file_path.string())
        {
          add_parents_for_one_element(i->first.first,
                                      changed_orig,changed_orig,changed_mod,
                                      complete_in_both,
                                      checksums_first_file_path_eq_string);
          add_parents_for_one_element(i->second.first,
                                      changed_mod,changed_orig,changed_mod,
                                      complete_in_both,
                                      checksums_second_file_path_eq_string);
        }
    }
}
