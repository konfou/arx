/* Consolidate a tree's ++manifest and ++changes into a single
   ++manifest file, updating the ++sha256 file in the process.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Checksums.hpp"
#include "Parsed_Name.hpp"
#include "tree_root.hpp"
#include "read_checksums.hpp"
#include "write_manifest.hpp"
#include "Temp_File.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void consolidate_manifest(const path &tree_directory, const Parsed_Name &name)
{
  const path root(tree_root(tree_directory));

  Checksums checksums;
  read_checksums(root,checksums);
  checksums.name=name;

  const Temp_File temp_manifest(root,",,consolidate");
  write_manifest(temp_manifest.path,checksums);
  if(rename(temp_manifest.path.native_file_string().c_str(),
            (root/"_arx/++manifest").native_file_string().c_str())!=0)
    throw arx_error("Can't rename "
                    + temp_manifest.path.native_file_string()
                    + " to "
                    + (root/"_arx/++manifest").native_file_string());
  remove(root/"_arx/++changes");
  remove(root/"_arx/++sha256");
  fs::ofstream sha_file(root/"_arx/++sha256");
  sha_file << sha256(root/"_arx/++manifest") << endl;
}

