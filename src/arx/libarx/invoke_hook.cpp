/* Invoke user defined hooks.  Pre-commit hooks are checked for return
   value.  Post-commit hooks are fire and forget.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "boost/filesystem/operations.hpp"
#include <string>
#include "arx_error.hpp"
#include "Spawn.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void invoke_hook(const Parsed_Name &previous,
                 const Parsed_Name &spec, const bool &pre_commit, path root)
{
  char * home_env=getenv("HOME");
  if(home_env!=NULL)
    {
      path home(home_env);
      if(fs::lexists(home / ".arx/hooks"))
        {
          Spawn s;

          if(!previous.empty())
            {
              s.add_env("ARX_PREVIOUS_ARCHIVE",previous.archive());
              s.add_env("ARX_PREVIOUS_ARCHIVE_URI",
                        previous.archive_location().string());
              s.add_env("ARX_PREVIOUS_BRANCH",previous.complete_branch());
              s.add_env("ARX_PREVIOUS_REVISION",previous.complete_revision());
            }
          s.add_env("ARX_ARCHIVE",spec.archive());
          s.add_env("ARX_ARCHIVE_URI",spec.archive_location().string());
          s.add_env("ARX_BRANCH",spec.complete_branch());
          s.add_env("ARX_REVISION",spec.complete_revision());
          s.add_env("ARX_TREEROOT",root.native_file_string());

          s << home.string() + "/.arx/hooks";

          if(pre_commit)
            {
              s << "pre";
            }
          else
            {
              s << "post";
            }
          
          string action;
          if(spec.revision().empty())
            {
              action="make-branch";
            }
          else
            {
              action="make-revision";
            }

          s << action;

          if(!s.execute(pre_commit))
            {
              if(pre_commit)
                {
                  throw arx_error("Pre-commit hook failed when trying to "
                                  + action);
                }
              else
                {
                  throw arx_error("Error in executing the post-commit hook");
                }
            }
        }
    }
}


      
    
