/* Return a list of all of the cached revisions for a branch in local
   caches.  It starts looking one directory up, then looks for _arx
   directories in that directory and everything one level down.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "patch_level_cmp.hpp"
#include "get_config_option.hpp"
#include <string>
#include <list>
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "patch_number.hpp"
#include "boost/filesystem/operations.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

Revision_List list_local_cached_revisions(const Parsed_Name &name,
                                          const path &cache_dir)
{
  if(name.branch().empty())
    throw arx_error("This argument needs to specify a branch: "
                    + name.full_name());

  Revision_List cached_revisions(name);

  /* Get the directory to start looking from.  If we are in the root
     directory "/", then we just start from there. */
  path start_dir(cache_dir);
  if(start_dir.empty())
    {
      start_dir=fs::current_path().branch_path();
      if(start_dir.empty())
        start_dir=fs::current_path();
    }
      
  for(fs::directory_iterator i(start_dir); i!=fs::directory_iterator(); ++i)
    {
      path arx_dir;
      if(i->leaf()=="_arx")
        {
          arx_dir=*i;
        }
      else
        {
          if(!fs::symbolic_link_exists(*i) && fs::is_directory(*i))
            for(fs::directory_iterator j(*i); j!=fs::directory_iterator(); ++j)
              {
                if(j->leaf()=="_arx")
                  {
                    arx_dir=*j;
                    break;
                  }
              }
        }
      if(!arx_dir.empty())
        {
          path revision_path(arx_dir / "++cache"/ name.branch_path());
          if(lexists(revision_path))
            {
              for(fs::directory_iterator k(revision_path);
                  k!=fs::directory_iterator(); ++k)
                {
                  if(is_revision(k->leaf())
                     && (name.revision().empty()
                         || name.short_revision_path()==k->leaf()))
                    {
                      if(Command_Info::verbosity>=verbose)
                        cout << "local cache at "
                             << k->native_file_string() << endl;
                      cached_revisions.push_back(patch_number(k->leaf()));
                    }
                }
            }
        }
    }
  cached_revisions.sort();
  return cached_revisions;
}
