/* Create a complete ++manifest file by recomputing all of the
   checksums.  We have to recompute the checksums because ordinary
   patches to files won't update the ++manifest.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Checksums.hpp"
#include "Parsed_Name.hpp"
#include "tree_root.hpp"
#include "read_checksums.hpp"
#include "write_manifest.hpp"
#include "convert_inventory_to_checksums.hpp"
#include "Current_Path.hpp"
#include "inventory_directory.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void reconstitute_manifest(const path &tree_directory, const Parsed_Name &name)
{
  const path root(tree_root(tree_directory));

  Checksums checksums;
  read_checksums(root,checksums);
  checksums.name=name;

  {
    Current_Path current(root);

    Inventory result;
    Inventory_Flags flags(root);
    flags.source=flags.ignore=flags.unrecognized=
      flags.trees=flags.control=true;
    flags.disambiguate=false;
    flags.directories=flags.files=true;

    inventory_directory("",result,flags,checksums);

    convert_inventory_to_checksums(result,checksums);

    for(Checksums::iterator i=checksums.begin(); i!=checksums.end(); ++i)
      if(!i->second.empty())
        if(i->first.link)
          {
            i->second=sha256(readlink(i->first.file_path).string());
          }
        else
          {
            i->second=sha256(i->first.file_path);            
          }
  }
  remove(root/"_arx/++manifest");
  remove(root/"_arx/++changes");
  write_manifest(root/"_arx/++manifest",checksums);

  remove(root/"_arx/++sha256");
  fs::ofstream sha_file(root/"_arx/++sha256");
  sha_file << sha256(root/"_arx/++manifest") << endl;
}

