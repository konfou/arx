/* Annotate a file from a patch directory.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include <iostream>
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "read_file_into_string.hpp"
#include <vector>
#include <map>
#include "Parsed_Name.hpp"
#include "boost/spirit.hpp"

using namespace std;
using namespace boost;
using namespace boost::spirit;
namespace fs=boost::filesystem;
using fs::path;

extern int process_hunk(const int &begin_atat,
                        vector<string> &patch_lines,
                        vector<string> &file_lines,
                        vector<string> &annotations,
                        vector<int> &file_offsets,
                        vector<int> &new_offsets,
                        const string &name);

void annotate_from_patch_dir(const string &file_name,
                             vector<string> &file_lines,
                             vector<string> &annotations,
                             vector<int> &file_offsets,
                             const string &annotate_string,
                             const path &patch_dir)
{
  path patch_file(patch_dir/"patches"/(file_name+".patch"));

  vector<string> patch_lines;
  {
    fs::ifstream infile(patch_file);
    string s;
    while(getline(infile,s))
      {
        patch_lines.push_back(s);
      }
  }
  vector<int> new_offsets(file_offsets);
  for(unsigned int i=0; i<patch_lines.size(); ++i)
    {
      if(patch_lines[i].substr(0,4)!="--- "
         && patch_lines[i].substr(0,4)!="+++ ")
        {
          if(patch_lines[i].substr(0,2)=="@@")
            i=process_hunk(i,patch_lines,file_lines,annotations,file_offsets,
                           new_offsets,annotate_string);
        }
    }
  file_offsets=new_offsets;
}
