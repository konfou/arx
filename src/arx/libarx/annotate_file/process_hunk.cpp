/* Process a single hunk in a patch.  Returns the location of the next
   hunk.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include <iostream>
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "read_file_into_string.hpp"
#include <vector>
#include <map>
#include "Parsed_Name.hpp"
#include "Temp_Directory.hpp"
#include "get_revision_patch.hpp"
#include "boost/spirit.hpp"

using namespace std;
using namespace boost;
using namespace boost::spirit;
namespace fs=boost::filesystem;
using fs::path;

int process_hunk(const int &begin_atat,
                 vector<string> &patch_lines,
                 vector<string> &file_lines,
                 vector<string> &annotations,
                 vector<int> &file_offsets,
                 vector<int> &new_offsets,
                 const string &name)
{
  unsigned int start_orig, orig_length, start_mod, mod_length;
  if(!parse(patch_lines[begin_atat].c_str(),str_p("@@")
            >> '-' >> uint_p[assign_a(start_orig)]
            >> ',' >> uint_p[assign_a(orig_length)]
            >> '+' >> uint_p[assign_a(start_mod)]
            >> ',' >> uint_p[assign_a(mod_length)]
            >> str_p("@@")
            ,space_p).full)
    throw arx_error("Can't parse the atatline in the patch\n\t"
                    + patch_lines[begin_atat]);
  unsigned int mod_position(start_mod), orig_position(start_orig);
  unsigned int i(begin_atat+1);
  for(; i<patch_lines.size(); ++i)
    {
      if(patch_lines[i][0]=='+')
        {
          /* Removing lines, so need to decrease offsets and maybe
             annotate a line */
          unsigned int pos=0;
          for(unsigned int j=0; j<file_offsets.size(); ++j)
            {
              pos+=file_offsets[j];
              if(mod_position==pos)
                {
                  if(patch_lines[i]!="+"+file_lines[j])
                    throw arx_error("Bad match when parsing a patch\n\t"
                                    + patch_lines[i]
                                    + "\n\t" + file_lines[j]);
                  annotations[j]=name;
                  for(unsigned int k=j+1; k<file_offsets.size(); ++k)
                    if(new_offsets[k]!=0)
                      {
                        new_offsets[k]+=new_offsets[j]-1;
                        break;
                      }
                  new_offsets[j]=0;
                  break;
                }
              else if(mod_position<pos)
                {
                  --new_offsets[j];
                  if(new_offsets[j]<0)
                    throw arx_error("Bad patch.  Line offsets are less than zero.\n\t"
                                    + name);
                  break;
                }
            }
          ++mod_position;
        }
      else if(patch_lines[i][0]=='-')
        {
          /* Adding in lines, so need to increase offsets */
          unsigned int pos=0;
          for(unsigned int j=0; j<file_offsets.size(); ++j)
            {
              pos+=file_offsets[j];
              if(mod_position<=pos)
                {
                  ++new_offsets[j];
                  break;
                }
            }
          ++orig_position;
        }
      else if(patch_lines[i].substr(0,2)=="@@")
        {
          break;
        }
      else
        {
          ++orig_position;
          ++mod_position;
        }
    }

  /* Sanity check of the hunk */
  if(start_mod+mod_length!=mod_position
     || start_orig+orig_length!=orig_position)
    {
      throw arx_error("Error when parsing a patch.  The numbers in the @@ lines do not match the\nlines in the patch itself.\n\t"
                      + name + "\n\t"
                      + patch_lines[begin_atat]);
    }

  return i-1;
}
