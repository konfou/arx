/* Annotate a file.  It takes a file as a list of strings and prepends
   those lines with what revision it came from.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <vector>
#include <utility>
#include "Parsed_Name.hpp"
#include "latest_tree_revision.hpp"
#include "Temp_Directory.hpp"
#include "find_or_make_pristine.hpp"
#include "make_patch.hpp"
#include "Patch_Info.hpp"
#include "follow_path.hpp"
#include "Annotate.hpp"
#include "tree_root.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

pair<vector<string>, vector<string> > annotate_file(const path &file)
{
  if(!fs::exists(file) || fs::symbolic_link_exists(file) || fs::is_directory(file))
    throw arx_error("This file either does not exist or is not a regular file.\n\t"
                    + file.native_file_string());

  path root(tree_root(file));

  vector<string> file_lines;
  {
    fs::ifstream infile(file);
    string s;
    while(getline(infile,s))
      file_lines.push_back(s);
  }

  vector<string> annotations(file_lines.size());

  Parsed_Name name(latest_tree_revision(root));

  string current_name(relative_path(file,root).string());

  vector<int> file_offsets(file_lines.size(),1);

  /* Get annotations from all of the regular patches. */
  Annotate annotate(annotations, file_lines, file_offsets);
  follow_path(annotate,current_name,name,root,arx_file);
  return make_pair(annotations,file_lines);
}
