/* A simple class that annotates a file whenever it is patched or added.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_ANNOTATE_HPP
#define ARX_ANNOTATE_HPP

#include "Parsed_Name.hpp"
#include "Temp_Directory.hpp"
#include "get_revision_patch.hpp"
#include <vector>
#include "tempdir.hpp"

extern void annotate_from_patch_dir(const std::string &file_name,
                                    std::vector<std::string> &file_lines,
                                    std::vector<std::string> &annotations,
                                    std::vector<int> &file_offsets,
                                    const std::string &annotate_string,
                                    const boost::filesystem::path &patch_dir);

class Annotate
{
  std::vector<std::string> &annotations;
  std::vector<std::string> &file_lines;
  std::vector<int> &file_offsets;
  void add_all_lines(const std::string &def)
  {
    for(std::vector<std::string>::iterator i=annotations.begin();
        i!=annotations.end(); ++i)
      {
        if(i->empty())
          *i=def;
      }
  }

public:
  Annotate(std::vector<std::string> &Annotations,
           std::vector<std::string> &File_lines,
           std::vector<int> &File_offsets):
    annotations(Annotations), file_lines(File_lines),
    file_offsets(File_offsets) {}

  void add(const std::string &name)
  {
    add_all_lines(name);
  }
  void add(const Parsed_Name &name)
  {
    add_all_lines(name.complete_name());
  }
  /* There are two versions of patch().  This version downloads the
     patch for the annotation. */
  void patch(const std::string &current_name, const Parsed_Name &name)
  {
    /* parse the patch file */
    Temp_Directory patch_dir(tempdir(),"patch",false);
    get_revision_patch(name,patch_dir.path,false);
    patch(current_name,name.complete_name(),patch_dir.path);
  }
  /* This version takes an existing patch dir for the annotation.  It
     is used in the previous version as well as local
     modifications. */
  void patch(const std::string &current_name, const std::string &name,
             const boost::filesystem::path &patch_dir)
  {
    annotate_from_patch_dir(current_name,file_lines,annotations,
                            file_offsets,name,patch_dir);
  }
  void rename(const std::string &name) {}
  void rename(const Parsed_Name &name) {}
  void finish()
  {
    add_all_lines("Unknown");
  }
};

#endif
