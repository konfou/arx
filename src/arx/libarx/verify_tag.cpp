/* Verify that a tag's contents match the hash in the archive and that
   it is properly signed.  It returns the contents of the log.

  Copyright (C) 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "valid_package_name.hpp"
#include "sha256.hpp"
#include "Temp_Directory.hpp"
#include "signed_archive.hpp"
#include "gpg_verify.hpp"
#include "read_file_into_string.hpp"
#include "tempdir.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

string verify_tag(const Parsed_Name &name)
{
  valid_package_name(name,Revision);

  string log_string;

  gvfs::Init();
  
  gvfs::uri location(name.archive_location()
                     / name.revision_path_no_archive());
  if(!gvfs::exists(location/"sha256"))
    throw arx_error("CORRUPTED_ARCHIVE: There is no checksum file for this tag in the archive\n\t"
                    + name.full_name());
  
  Temp_Directory temp_dir(tempdir(),",,verify");
  gvfs::copy(location/"sha256",
             (temp_dir.path/"sha256").native_file_string());
  if(signed_archive(name))
    {
      gvfs::copy(location.string()+"/sha256.sig",
                 gvfs::uri((temp_dir.path/"sha256.sig")
                           .native_file_string()));
      gpg_verify(temp_dir.path/"sha256.sig",name);
    }
  
  log_string=gvfs::read_file_into_string(location/"log");
  
  if(sha256(log_string).string()
     !=read_file_into_string(temp_dir.path/"sha256").substr(0,64))
    throw arx_error("CORRUPT ARCHIVE: The log file and its hash do not match\n\t"
                    + (location/"log").string() + "\n\t"
                    + "log hash: " + sha256(log_string).string()
                    + "\n\tarchive hash: "
                    + read_file_into_string(temp_dir.path/"sha256"));

  return log_string;
}
