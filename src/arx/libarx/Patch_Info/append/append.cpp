/* Print out a nicely formatted description of a patch.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Command_Info.hpp"
#include <list>
#include <string>
#include <utility>
#include "read_and_sort_index.hpp"
#include <iostream>
#include "inventory_types.hpp"
#include "Patch_Info.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void classify_patches(const path &patch, list<string> patched[2],
                             const bool &diffs,
                             list<pair<string,string> > &regular_patched,
                             list<string> &symlink_patched,
                             list<string> &binary_patched,
                             list<string> &metadata_patched);

void Patch_Info::append(const path &patch, const string prefix)
{
  list<pair<string,string> > original[2], modified[2];
  
  read_and_sort_index(patch/"orig-files-index",original[arx_file]);
  read_and_sort_index(patch/"orig-dirs-index",original[arx_dir]);
  read_and_sort_index(patch/"mod-files-index",modified[arx_file]);
  read_and_sort_index(patch/"mod-dirs-index",modified[arx_dir]);

  std::list<std::pair<std::string,std::string> > Regular_Patched, Moved[2];
  std::list<std::string> Patched[2], Orig_Only[2], Mod_Only[2],
    Symlink_Patched, Binary_Patched, Metadata_Patched;

  for(int m=0;m<2;++m)
    {
      list<pair<string,string> >::const_iterator i(original[m].begin()),
        j(modified[m].begin());

      while(i!=original[m].end() && j!=modified[m].end())
        {
          if(i->second == j->second)
            {
              if(include_control || !is_control(j->first))
                Patched[m].push_back(j->first);

              if(i->first != j->first)
                {
                  if(include_control || !is_control(i->first))
                    Moved[m].push_back(make_pair(i->first,j->first));
                }
              ++i;
              ++j;
            }
          else if(i->second < j->second)
            {
              if(include_control || !is_control(i->first))
                Orig_Only[m].push_back(i->first);
              ++i;
            }
          else
            {
              if(include_control || !is_control(j->first))
                Mod_Only[m].push_back(j->first);
              ++j;
            }
        }
      for(;i!=original[m].end();++i)
        {
          if(include_control || !is_control(i->first))
            Orig_Only[m].push_back(i->first);
        }
      for(;j!=modified[m].end();++j)
        {
          if(include_control || !is_control(j->first))
            Mod_Only[m].push_back(j->first);
        }
    }

  classify_patches(patch,Patched,diffs,Regular_Patched,Symlink_Patched,
                   Binary_Patched,Metadata_Patched);

  /* Sort all of lists.  This will end up sorting the lists within a
     particular patch, but will not mix lists from different
     patches. */

  Regular_Patched.sort();
  Symlink_Patched.sort();
  Binary_Patched.sort();
  Metadata_Patched.sort();
  for(int m=0;m<2;++m)
    {
      Moved[m].sort();
      Patched[m].sort();
      Orig_Only[m].sort();
      Mod_Only[m].sort();
    }

  /* Add the prefix */

  if(!prefix.empty())
    {
      for(list<pair<string,string> >::iterator i=Regular_Patched.begin();
          i!=Regular_Patched.end(); ++i)
        i->first=prefix+i->first;
      for(list<string>::iterator i=Symlink_Patched.begin();
          i!=Symlink_Patched.end(); ++i)
        *i=prefix+*i;
      for(list<string>::iterator i=Binary_Patched.begin();
          i!=Binary_Patched.end(); ++i)
        *i=prefix+*i;
      for(list<string>::iterator i=Metadata_Patched.begin();
          i!=Metadata_Patched.end(); ++i)
        *i=prefix+*i;
      for(int m=0;m<2;++m)
        {
          for(list<pair<string,string> >::iterator i=Moved[m].begin();
              i!=Moved[m].end(); ++i)
            i->first=prefix+i->first;
          for(list<string>::iterator i=Patched[m].begin();
              i!=Patched[m].end(); ++i)
            *i=prefix+*i;
          for(list<string>::iterator i=Orig_Only[m].begin();
              i!=Orig_Only[m].end(); ++i)
            *i=prefix+*i;
          for(list<string>::iterator i=Mod_Only[m].begin();
              i!=Mod_Only[m].end(); ++i)
            *i=prefix+*i;
        }
    }

  /* Splice the lists onto the master lists. */

  regular_patched.splice(regular_patched.end(),Regular_Patched);
  symlink_patched.splice(symlink_patched.end(),Symlink_Patched);
  binary_patched.splice(binary_patched.end(),Binary_Patched);
  metadata_patched.splice(metadata_patched.end(),Metadata_Patched);
  for(int m=0;m<2;++m)
    {
      moved[m].splice(moved[m].end(),Moved[m]);
      patched[m].splice(patched[m].end(),Patched[m]);
      orig_only[m].splice(orig_only[m].end(),Orig_Only[m]);
      mod_only[m].splice(mod_only[m].end(),Mod_Only[m]);
    }

}

