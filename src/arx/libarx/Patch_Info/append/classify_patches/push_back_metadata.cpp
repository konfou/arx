/* Classify a list of possibly patched paths into regular, symlink,
   binary, or metadata changes.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Command_Info.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <map>
#include <list>
#include <string>
#include <utility>
#include "inventory_types.hpp"
#include <sstream>
#include "boost/archive/text_iarchive.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

map<string,string> read_all_properties(const path &prop_path)
{
  fs::ifstream prop_file(prop_path);
  archive::text_iarchive prop_archive(prop_file);

  string action, key, value;
  map<string,string> result;

  prop_archive >> action;
  while(prop_file)
    {
      prop_archive >> key >> value;
      if(action=="set")
        result[key]=value;
      else
        result[key]="";
      prop_archive >> action;
    }
  return result;
}

void push_back_metadata(const path &orig_path, const path &mod_path,
                        const string &filename,
                        list<string> &metadata_patched)
{
  map<string,string> properties_orig(read_all_properties(orig_path)),
    properties_mod(read_all_properties(mod_path));
  stringstream s;
  s << filename << "\n";
  map<string,string>::iterator i(properties_orig.begin()),
    j(properties_mod.begin());
  for(;i!=properties_orig.end(); ++i, ++j)
    {
      s << i->first << " : "
        << i->second << " -> "
        << j->second << "\n";
    }
  metadata_patched.push_back(s.str());
}  
