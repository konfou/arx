/* Classify a list of possibly patched paths into regular, symlink,
   binary, or metadata changes.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Command_Info.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <list>
#include <string>
#include <utility>
#include "inventory_types.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void push_back_metadata(const path &orig_path, const path &mod_path,
                               const string &filename,
                               list<string> &metadata_patched);

void classify_patches(const path &patch, list<string> patched[2],
                      const bool &diffs,
                      list<pair<string,string> > &regular_patched,
                      list<string> &symlink_patched,
                      list<string> &binary_patched,
                      list<string> &metadata_patched)
{
  for(int m=0;m<2;++m)
    {
      for(list<string>::iterator i=patched[m].begin(); i!=patched[m].end();
          ++i)
        {
          if(m==arx_dir)
            {
              if(lexists(patch/"patches"/(*i)/"dir-prop-mod")
                 && lexists(patch/"patches"/(*i)/"dir-prop-orig"))
                {
                  push_back_metadata(patch/"patches"/(*i)/"dir-prop-orig",
                                     patch/"patches"/(*i)/"dir-prop-mod",
                                     *i,metadata_patched);
                }
            }
          else
            {
              if(lexists(patch/"patches"/(*i + ".prop-orig"))
                 && lexists(patch/"patches"/(*i + ".prop-mod")))
                {
                  push_back_metadata(patch/"patches"/(*i + ".prop-orig"),
                                     patch/"patches"/(*i + ".prop-mod"),
                                     *i,metadata_patched);
                }
              if(lexists(patch/"patches"/(*i + ".patch"))
                 && !fs::is_empty(patch/"patches"/(*i + ".patch")))
                {
                  if(diffs)
                    {
                      fs::fstream patch_file(patch/"patches"/(*i + ".patch"));
                      string s, temp;

                      while(getline(patch_file,temp))
                        {
                          s+="    " + temp + "\n";
                        }
                      s+="\n";
                      regular_patched.push_back(make_pair(*i,s));
                    }
                  else
                    {
                      regular_patched.push_back(make_pair(*i,""));
                    }
                }
              if(lexists(patch/"patches"/(*i + ".mod"))
                 || lexists(patch/"patches"/(*i + ".orig"))
                 || lexists(patch/"patches"/(*i + ".xorig"))
                 || lexists(patch/"patches"/(*i + ".xmod")))
                {
                  binary_patched.push_back(*i);
                }
              if(lexists(patch/"patches"/(*i + ".link-mod"))
                 && lexists(patch/"patches"/(*i + ".link-orig")))
                {
                  symlink_patched
                    .push_back(*i + "\n      "
                               + (readlink(patch/"patches"/
                                              (*i + ".link-orig"))).string()
                               + " ==> "
                               + (readlink(patch/"patches"/
                                              (*i + ".link-mod"))).string());
                }
            }                  
        }
    }

}
