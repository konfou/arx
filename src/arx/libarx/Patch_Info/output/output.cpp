/* Print out a nicely formatted description of a patch.

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Command_Info.hpp"
#include <list>
#include <string>
#include <utility>
#include "read_and_sort_index.hpp"
#include <iostream>
#include "inventory_types.hpp"
#include "Patch_Info.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void print_patches(ostream &os,
                          const list<pair<string,string> > &section_list,
                          const string &section_name,
                          const bool &diffs, const bool &html,
                          const string &link_root);

extern void print_section(ostream &os, const list<string> &section_list,
                          const string &section_name,
                          const bool &html, const string &link_root,
                          const string hyperlink_prefix="");

extern void print_renames(ostream &os,
                          const list<pair<string,string> > &section_list,
                          const string &section_name, const bool &html);

ostream & operator<<(ostream &os, const Patch_Info &pi)
{
  if(Command_Info::verbosity>=default_output)
    {
      if(pi.html)
        os << "<html>\n"
           << "<head>\n"
           << "<title>" << pi.title << "</title>\n"
           << "</head>\n"
           << "<body>\n"
           << "<H1>" << pi.title << "</H1>\n";

      print_section(os,pi.orig_only[arx_file],"Removed Files:",pi.html,
                    pi.link_root,"deleted");
      print_section(os,pi.orig_only[arx_dir],"Removed Directories:",pi.html,
                    pi.link_root);
      print_section(os,pi.mod_only[arx_file],"Added Files:",pi.html,
                    pi.link_root,"added");
      print_section(os,pi.mod_only[arx_dir],"Added Directories:",pi.html,
                    pi.link_root);
      print_renames(os,pi.moved[arx_file],"Renamed Files:",pi.html);
      print_renames(os,pi.moved[arx_dir],"Renamed Directories:",pi.html);
      print_patches(os,pi.regular_patched,"Patched Files:",pi.diffs,pi.html,
                    pi.link_root);
      print_section(os,pi.symlink_patched,"Patched Symlinks:",pi.html,
                    pi.link_root);
      print_section(os,pi.binary_patched,"Patched Binary Files:",pi.html,
                    pi.link_root);
      print_section(os,pi.metadata_patched,"Metadata Changes:",pi.html,
                    pi.link_root);

      if(pi.html)
        os << "</body>\n";
    }
  return os;
}

