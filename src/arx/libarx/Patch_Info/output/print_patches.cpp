/* Print out a nicely formatted section describing all of the regular
   patches.

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <list>
#include <string>
#include <utility>
#include <iostream>

using namespace std;

void print_patches(ostream &os, const list<pair<string,string> > &section_list,
                   const string &section_name,
                   const bool &diffs, const bool &html,
                   const string &link_root)
{
  if(!section_list.empty())
    {
      if(html)
        {
          os << "<H2>" << section_name << "</H2>\n<ul>\n";
          for(list<pair<string,string> >::const_iterator
                i=section_list.begin(); i!=section_list.end(); ++i)
            {
              os << "<li><a href=\"" << link_root << "/patches/"
                   << i->first << ".patch\">"
                   << i->first << "</a>";
              if(diffs)
                os << "\n<pre>\n"
                     << i->second << "\n</pre>";
              os << "</li>\n";
            }
          os << "</ul>\n";
        }
      else
        {
          os << section_name << "\n\n";
          for(list<pair<string,string> >::const_iterator
                i=section_list.begin(); i!=section_list.end(); ++i)
            {
              if(diffs)
                os << "****";
              else
                os << "    ";
              os << i->first << "\n";
              if(diffs)
                os << "\n" << i->second << "\n";
            }
          os << "\n";
        }
    }
}
