/* Check whether a project tree is missing files or has duplicate ids.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents
                    of the University of California
   Minor Bug fix 2003 Robin Farine
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "inventory_directory.hpp"
#include "file_attributes.hpp"
#include "Inventory.hpp"
#include "Inventory_Flags.hpp"
#include <list>
#include <utility>
#include <string>
#include "read_checksums.hpp"
#include "tree_root.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

/* A convenience function to only output a header once. */

void output_once(string &errors, const string &message, bool &printed)
{
  if(!printed)
    errors+=message;
  printed=true;
}

bool check_tree(const path &dir, const bool &strict,
                const bool &no_unrecognized,
                string &warnings,
                string &errors)
{
  bool result(true);
  string error_string("WARNING");
  if(strict)
    error_string="ERROR";

  Inventory dir_inventory;

  const path root(tree_root(dir));
  Checksums checksums;
  read_checksums(root,checksums);

  /* Remove control files and directories */
  for(Checksums::iterator i=checksums.begin(); i!=checksums.end();)
    {
      if(i->first.is_control())
        {
          Checksums::iterator j(i);
          ++i;
          checksums.erase(j);
        }
      else
        {
          ++i;
        }
    }

  Inventory_Flags flags(root);
  flags.control=flags.source=
    flags.ignore=flags.unrecognized=flags.trees=true;
  flags.directories=flags.files=true;

  inventory_directory(root,dir_inventory,flags,checksums);

  /* Check for unrecognized paths */

  if(!no_unrecognized)
    {
      if(!dir_inventory(unrecognized,arx_file).empty() ||
         !dir_inventory(unrecognized,arx_dir).empty())
        {
          if(strict)
            result=false;
          warnings+=error_string + ": Unrecognized paths\nThese files or directories are unrecognized.\n\n";
          for(int m=0;m<2;++m)
            for(Inventory::iterator
                  i=dir_inventory(unrecognized,m).begin();
                i!=dir_inventory(unrecognized,m).end(); ++i)
              {
                warnings+="\t" + i->file_path.native_file_string()
                  + "\n";
              }
          warnings+="\n";
        }
    }              
  
  /* Check for external ids with no file. */
  
  bool printed(false);

  for(Checksums::iterator i=checksums.begin(); i!=checksums.end(); ++i)
    {
      if(!i->first.is_control())
        {
          result=false;
          output_once(errors,"ERROR: Missing files\nInventory ids exist for these files, but the files themselves are\nmissing or are in directories not categorized as source.\n\n",
                      printed);
          errors+="\t" + i->first.file_path.native_file_string() + "\n";
        }
    }
  printed=!printed;
  output_once(errors,"\n",printed);
  
  printed=false;
  
  /* Check for duplicate ids */
  
  for(int m=0;m<2;++m)
    {
      dir_inventory(source,m).sort(inventory_id_cmp);

      Inventory::iterator i=dir_inventory(source,m).begin();
      if(i!=dir_inventory(source,m).end())
        {
          Inventory::iterator j(i);
          ++j;
          for(;j!=dir_inventory(source,m).end(); ++i, ++j)
            {
              if(i->inventory_id==j->inventory_id)
                {
                  result=false;
                  output_once(errors,"INTERNAL ERROR: Corrupt tree\nEach group of paths in this list have the same inventory id.\nThis should never happen.\n\n",printed);
                  errors+="\t" + i->file_path.native_file_string() + "\n\t"
                    + j->file_path.native_file_string() + "\n\n";
                }
            }
        }
    }
  printed=!printed;
  output_once(errors,"\n",printed);

  return result;
}
