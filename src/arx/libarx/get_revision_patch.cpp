/* Do the actual work of retrieving a patch set from an archive

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Current_Path.hpp"
#include "Parsed_Name.hpp"
#include "patch_level_cmp.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "../../config.h"
#include "Temp_Directory.hpp"
#include "Spawn.hpp"
#include "valid_package_name.hpp"
#include "boost/filesystem/exception.hpp"
#include "gpg_verify.hpp"
#include "signed_archive.hpp"
#include "is_a_tag.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void get_revision_patch(const Parsed_Name &name, const path &destination,
                        const bool verify=true)
{
  if(is_a_tag(name))
    throw arx_error("You may not get a patch for a tag.  The tag is just a log file.\n\t"
                    + name.complete_revision());

  /* Get the revision to look at. */
  
  valid_package_name(name,Revision);

  path target(destination);
  if(target.empty())
    {
      target=path(name.revision() + ".patches");
    }
  if(fs::lexists(target))
    throw arx_error("Patch output directory already exists\n\t"
                    + name.revision() + ".patches");

  /* Now get the revisions. */

  gvfs::Init();
  
  gvfs::uri location(name.archive_location()
                     / name.revision_path_no_archive()
                     / (name.revision() + ".patches.tar.gz"));
  if(!gvfs::exists(location))
    throw arx_error("No such revision in the archive\n\t"
                    + name.full_name());
  Temp_Directory get_patch_dir(system_complete(destination).branch_path(),
                               ",,get-patch");
  path get_patch_file=get_patch_dir.path/(name.revision()
                                          + ".patches.tar.gz");
  gvfs::copy(location,
             (system_complete(get_patch_file)).native_file_string());
  
  if(verify && signed_archive(name))
    {
      /* Verify the signature */
      gvfs::copy(location.string()+".sig",
                 (system_complete(get_patch_file)).native_file_string()
                 + ".sig");
      gpg_verify((system_complete(get_patch_file)).native_file_string()
                 + ".sig",name);
    }
  {
    Current_Path current(get_patch_dir.path);
    
    Spawn untar;
    untar << ARXTAR << "-zxf" << get_patch_file.leaf();
    if(!untar.execute(true))
      throw arx_error("Error when unpacking a patch file\n\t"
                      + get_patch_file.native_file_string());
  }
  fs::rename(get_patch_dir.path/(name.revision() + ".patches"),target);
}
