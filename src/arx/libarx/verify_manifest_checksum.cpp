/* Verify that the checksum of a manifest matches the checksum stored
   in the archive.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "valid_package_name.hpp"
#include "sha256.hpp"
#include "Temp_Directory.hpp"
#include "signed_archive.hpp"
#include "gpg_verify.hpp"
#include "read_file_into_string.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

sha256 verify_manifest_checksum(const path &p, const Parsed_Name &name)
{
  sha256 revision_hash;
  
  valid_package_name(name,Revision);

  /* Now get the revisions. */

  gvfs::Init();
  
  gvfs::uri location(name.archive_location()
                     / name.revision_path_no_archive()
                     / "sha256");
  if(!gvfs::exists(location))
    throw arx_error("CORRUPTED_ARCHIVE: There is no checksum file for this revision in the archive\n\t"
                    + name.full_name());
  
  Temp_Directory temp_dir(p,",,verify");
  gvfs::copy(location,(temp_dir.path/"sha256").native_file_string());
  if(signed_archive(name))
    {
      gvfs::copy(location.string()+".sig",
                 gvfs::uri((temp_dir.path/"sha256.sig")
                           .native_file_string()));
      gpg_verify(temp_dir.path/"sha256.sig",name);
    }
  
  stringstream revision_hash_string(read_file_into_string
                                    (temp_dir.path/"sha256"));
  revision_hash_string >> revision_hash;
  
  if(revision_hash!=sha256(p/"_arx/++manifest"))
    throw arx_error("INTERNAL ERROR: The checksum of the local tree and the checksum in the archive do not match.\n\trevision: "
                    + name.full_name()
                    + "\n\tpath: "
                    + p.native_file_string()
                    + "\n\tarchive checksum: "
                    + revision_hash.string()
                    + "\n\tlocal checksum: "
                    + (sha256(p/"_arx/++manifest")).string());
  return revision_hash;
}
