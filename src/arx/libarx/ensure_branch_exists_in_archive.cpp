/* Make sure that a particular branch exists in the archive.  It
   calls hooks if anything is created.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <string>
#include "gvfs.hpp"
#include "Parsed_Name.hpp"
#include "arx_error.hpp"
#include "invoke_hook.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void ensure_branch_exists_in_archive(const Parsed_Name &p)
{
  gvfs::Init();
  
  /* Make sure that the archive is not a mirror. */
  if(exists(p.archive_location() /",meta-info/mirror"))
    throw arx_error("Can not commit: The archive is a mirror\n\t"
                    + p.archive());
  
  /* Make all of the necessary branches and sub-branches. */
  
  gvfs::uri location(p.archive_location());
  
  Parsed_Name current(p.archive()+"/");
  
  for(list<string>::const_iterator i=p.components.begin();
      i!=p.components.end(); ++i)
    {
      current.append_branch(*i);
      if(!exists(location/current.branch_path_no_archive()))
        {
          invoke_hook(Parsed_Name(), current.complete_branch(),true);
          gvfs::make_directory(location/current.branch_path_no_archive());
          invoke_hook(Parsed_Name(), current.complete_branch(),false);
        }
    }
}
