/* Temporarily undo changes in working directory in order to do
   another checkin on the clean revision first

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002 Alexander Deruwe
   Copyright (C) 2003 Walter Landry and the Regents
                      of the University of California
   Copyright (C) 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "make_patch.hpp"
#include "do_patch.hpp"
#include "tree_root.hpp"
#include "find_or_make_pristine.hpp"
#include "boost/filesystem/operations.hpp"
#include "Temp_Directory.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void undo(const path &tree_directory, const path &undo_dir,
          const Path_List path_list=Path_List())
{
  const path root(tree_root(tree_directory));
  
  /* Compute the patch between the pristine tree and the current tree. */

  try
    {
      Temp_Directory temp_dir(root,",,pristine",false);
      make_patch(find_or_make_pristine(tree_directory,temp_dir.path),root,
                 undo_dir,path_list);
    }
  catch (arx_error &error)
    {
      fs::remove_all(undo_dir);
      throw arx_error(error);
    }

  /* and apply it in reverse. */

  do_patch(undo_dir,root,true);
}
