/* Unregister an archive

   Copyright (C) 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "read_archive_locations.hpp"
#include "write_archive_locations.hpp"
#include "arx_error.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void delete_archive_registration(const string &repo,
                                 const string location_fragment=string())
{
  path home(getenv("HOME"));
  path config_path(home / ".arx/archives" / repo);
  
  if(!lexists(config_path))
    {
      if(Command_Info::verbosity>=default_output)
        cerr <<"Archive is not registered: " << repo << endl;
    }
  /* Delete a registration */
  else
    {
      if(location_fragment.empty())
        {
          remove_all(config_path);
        }
      else
        {
          list<string> locations=read_archive_locations(config_path/"uri");
          list<string>::iterator i=find(locations.begin(),locations.end(),
                                        location_fragment);
          if(i==locations.end())
            {
              throw arx_error("This location\n\t"
                              + location_fragment
                              + "\nis not registered with this archive\n\t"
                              + repo);
            }
          else
            {
              locations.erase(i);
              if(locations.empty())
                {
                  remove_all(config_path);
                }
              else
                {
                  write_archive_locations(config_path/"uri",locations);
                }
            }
        }
    }
}
