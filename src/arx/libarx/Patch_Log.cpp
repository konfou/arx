/* Constructors for Patch_Log

   Copyright 2003, 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "arx_error.hpp"
#include <map>
#include <string>
#include "Parsed_Name.hpp"
#include "valid_package_name.hpp"
#include "tree_root.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/serialization/map.hpp"
#include "boost/serialization/list.hpp"
#include "gvfs.hpp"
#include "Patch_Log.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

/* The actual function call is quite small. */

void Patch_Log::read_patch_log(istream &s)
{
  /* There is a wierd bug in boost::serialization that makes the
     first element of rename_lists empty if headers is read.  As a
     workaround, we create another archive and read the rest from
     there. */
  {
    archive::text_iarchive log_archive(s);
    log_archive >> headers >> header_lists;
  }
  {
    archive::text_iarchive log_archive(s,archive::no_header);
    log_archive >> rename_lists >> body;
  }
}    

/* Version that looks in a file */

Patch_Log::Patch_Log(const path &log_name)
{
  fs::ifstream log_file(log_name);
  read_patch_log(log_file);
}

/* Version just for strings. */

Patch_Log::Patch_Log(const string &log_string)
{
  stringstream log_stream(log_string);
  read_patch_log(log_stream);
}

/* Version that knows where the patch log is supposed to be in a
   project tree. */

Patch_Log::Patch_Log(const path &tree_directory, const Parsed_Name &name)
{
  valid_package_name(name,Revision);
  path root(tree_root(tree_directory));

  path log_name(root/"_arx/patch-log"/name.revision_path());
  if(lexists(log_name))
    {
      fs::ifstream log_file(log_name);
      read_patch_log(log_file);
    }
}

/* Version that just looks in the archive. */

Patch_Log::Patch_Log(const Parsed_Name &name)
{
  valid_package_name(name,Revision);

  gvfs::Init();
  gvfs::uri location(name.archive_location()
                     / name.revision_path_no_archive() / "log");
  
  string log_string;
  try {
    log_string=gvfs::read_file_into_string(location);
    stringstream log_stream(log_string);
    read_patch_log(log_stream);
  } catch (gvfs::exception &err) {
    throw arx_error("Can't read the log message in the archive\n\t"
                    + location.string());
  }
}

