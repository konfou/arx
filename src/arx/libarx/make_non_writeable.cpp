/* Make a file non-writeable.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include <sys/stat.h>
#include "arx_error.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void make_non_writeable(const path &file)
{
  if(!symbolic_link_exists(file) && !is_directory(file))
    {
      struct stat path_stat;
      if(::stat(file.native_file_string().c_str(),&path_stat))
        throw arx_error("Can't stat " + file.native_file_string());
      
      path_stat.st_mode&=~(S_IWUSR | S_IWGRP | S_IWOTH);
      if(::chmod(file.native_file_string().c_str(),path_stat.st_mode))
        throw arx_error("Can't chmod " + file.native_file_string());
    }
}
                   
