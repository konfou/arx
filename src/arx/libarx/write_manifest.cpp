/* Generate a new manifest file given the checksums and the revision name.
   It sorts the manifest first.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Checksums.hpp"
#include "Parsed_Name.hpp"
#include "manifest_version.hpp"
#include "boost/archive/text_oarchive.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void write_manifest(const path &new_manifest_path,
                    Checksums &new_manifest)
{
  fs::ofstream manifest_file(new_manifest_path);
  {
    archive::text_oarchive manifest_archive(manifest_file);

    manifest_archive << manifest_version
                     << new_manifest.name.complete_revision();
  }
  new_manifest.sort(checksums_file_path_cmp);

  for(Checksums::const_iterator i=new_manifest.begin();
      i!=new_manifest.end(); ++i)
    i->first.output(manifest_file,i->second);
}
