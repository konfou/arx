/* A simple utility to convert an inventory to a list of checksums.
   For files, it creates a dummy checksum, for directories an empty
   one.  We can use a dummy checksum because it is never actually
   used.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Checksums.hpp"
#include "Inventory.hpp"

using namespace std;

void convert_inventory_to_checksums(const Inventory &inv,
                                    Checksums &checksums)
{
  checksums.clear();
  sha256 dummy("0");

  for(Inventory::const_iterator i=inv(source,arx_file).begin();
      i!=inv(source,arx_file).end(); ++i)
    checksums.push_back(make_pair(*i,dummy));

  for(Inventory::const_iterator i=inv(control,arx_file).begin();
      i!=inv(control,arx_file).end(); ++i)
    checksums.push_back(make_pair(*i,dummy));

  for(Inventory::const_iterator i=inv(source,arx_dir).begin();
      i!=inv(source,arx_dir).end(); ++i)
    checksums.push_back(make_pair(*i,sha256()));

  /* We don't convert control dirs, because they should never be in a
     checksum. */
}

