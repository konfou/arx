/* Implementations of the input and output routines for file_attributes.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "file_attributes.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/serialization/map.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void file_attributes::output(std::ostream &os, const bool &is_directory) const
{
  if(is_directory)
    output(os,sha256());
  else
    output(os,sha256("0"));
}

void file_attributes::output(std::ostream &os, const sha256 &checksum) const
{
  os << " ";
  boost::archive::text_oarchive oa(os,boost::archive::no_header);
  
  if(link)
    {
      oa << std::string("link")
         << file_path.string()
         << properties
         << inventory_id;
      checksum.serialize(os);
    }
  else if(checksum.empty())
    {
      oa << std::string("dir")
         << file_path.string()
         << properties
         << inventory_id;
    }
  else
    {
      if(is_control())
        {
          oa << std::string("control")
             << file_path.string();
          checksum.serialize(os);
        }
      else
        {
          oa << std::string("file")
             << file_path.string()
             << properties
             << inventory_id;
          checksum.serialize(os);
        }
    }
}

void file_attributes::input(std::istream &is, sha256 &checksum)
{
  boost::archive::text_iarchive ia(is,boost::archive::no_header);
  std::string temp, name;
  ia >> temp;
  if(!is)
    return;
  ia >> name;
  file_path=boost::filesystem::path(name);

  if(temp=="link")
    {
      link=true;
      ia >> properties >> inventory_id;
      checksum.deserialize(is);
    }
  else if(temp=="dir")
    {
      link=false;
      ia >> properties >> inventory_id;
    }
  else if(temp=="control")
    {
      link=false;
      checksum.deserialize(is);
      inventory_id=file_path.string();
    }
  else if(temp=="file")
    {
      link=false;
      ia >> properties >> inventory_id;
      checksum.deserialize(is);
    }
  else
    throw arx_error("Bad path type: " + temp);
}
