/* Set a config option, overwriting whatever is there.  So if the
   input string is empty, it unsets the option.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "arx_error.hpp"
#include "set_option_in_file.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void set_config_option(const string &filename, const string &input)
{
  path home(getenv("HOME"));
  if(!lexists(home / ".arx"))
    {
      try
        {
          create_directory(home / ".arx");
        }
      catch (fs::filesystem_error &fs_error)
        {
          throw arx_error("Can't create directory "
                          + (home / ".arx").native_file_string());
        }
    }
  set_option_in_file(home / ".arx" / filename,input);
}
