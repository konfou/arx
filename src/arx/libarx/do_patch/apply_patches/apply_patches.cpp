/* Apply patches to files and directories.  There are versions for
   exact and inexact patching.

  Copyright (C) 2003, 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "boost/filesystem/operations.hpp"
#include <string>
#include "arx_error.hpp"
#include "null_patch.hpp"
#include "Moved_Path.hpp"
#include "inventory_types.hpp"
#include "Inventory.hpp"
#include "copy_missing_patch.hpp"
#include "check_symlink_hierarchy.hpp"
#include <iostream>
#include "Command_Info.hpp"
#include "Conflicts.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void patch_directory(const path &root, const path &patch_dir,
                            const path &destination, const string &orig,
                            const string &mod);

extern void patch_file(const path &patch_file, const path &root,
                       const path &file_path,
                       const string &orig, const string &mod,
                       const string &reverse,
                       Conflicts &conflicts,
                       const bool &apply_content_patches);

/* A generic version for exact and inexact patching. */

void apply_patches(list<pair<pair<string,string>,string> > patched[2],
                   const list<Moved_Path> &moved,
                   const Inventory &source_inventory,
                   const bool &reverse,
                   const path &patch, const path &destination,
                   Conflicts &conflicts,
                   bool exact_patching,
                   const bool &apply_content_patches,
                   const bool &record_missing)
{
  string orig("orig"), mod("mod"), reverse_option;
  if(reverse)
    {
      reverse_option="--reverse";
      orig="mod";
      mod="orig";
    }

  for(int m=0; m<2; ++m)
    {
      for(list<pair<pair<string,string>,string> >::iterator
            i=patched[m].begin(); i!=patched[m].end(); ++i)
        {
          if(Command_Info::verbosity>=verbose)
            cout << "patching " << i->first.first << endl;

          if(!exact_patching)
            {
              /* Find the path with the same id.  First check in the
                 list of moved paths, and the original inventory. */
          
              inventory_types inv_type(is_control(i->first.first)
                                       ? control : source);
              list<Moved_Path>::const_iterator
                k(find_if(moved.begin(),moved.end(),
                          bind2nd(ptr_fun(Moved_Path_inventory_id_eq_string),
                                  i->first.second)));
              if(k!=moved.end())
                {
                  if(k->deleted)
                    throw arx_error("INTERNAL ERROR: This ArX patch is trying to both patch and delete a path.\n\t" + k->local_path.native_file_string());
                  i->second=k->destination.string();
                }
              else
                {
                  Inventory::const_iterator
                    j(find_if(source_inventory(inv_type,m).begin(),
                              source_inventory(inv_type,m).end(),
                              bind2nd(ptr_fun(inventory_id_eq_string),
                                      i->first.second)));
                  
                  if(j==source_inventory(inv_type,m).end())
                    {
                      /* Handle non-existent destinations for patches. */
                      
                      if(record_missing)
                        {
                          conflicts.missing_patches.push_back(i->first.first);
                          copy_missing_patch(i->first.first,patch,
                                             destination/",,missing-patches",
                                             m==arx_dir);
                          i->second=string();
                        }
                    }
                  else
                    {
                      i->second=j->file_path.string();
                    }
                }
            }

          /* Don't worry about empty patches. */
          
          if(!null_patch((patch/i->first.first).string(),m==arx_dir))
            {
              
              if(!i->second.empty())
                {
                  /* Make sure we aren't applying patches through
                     symlinks */
                  
                  check_symlink_hierarchy(destination,
                                          destination/i->second);
                  if(m==arx_dir)
                    {
                      /* The only thing that can happen is metadata
                         changes.  These can never conflict. */
                      patch_directory(destination,patch/i->first.first,
                                      i->second,orig,mod);
                    }
                  else
                    {
                      /* Don't apply ordinary patches to symlinks */
                      if(((fs::exists(patch/(i->first.first+".patch"))
                           && !fs::is_empty(patch/(i->first.first+".patch")))
                          || fs::exists(patch/(i->first.first+".xorig"))
                          || fs::exists(patch/(i->first.first+".xmod")))
                         && symbolic_link_exists(destination/i->second))
                        {
                          if(exact_patching)
                            throw arx_error("INTERNAL ERROR: Attempting to apply an exact patch to a symlink.");
                          conflicts.missing_patches.push_back(i->first.first);
                          copy_missing_patch(i->first.first,patch,
                                             destination/",,missing-patches",
                                             m==arx_dir);
                        }
                      else
                        {
                          patch_file(patch/i->first.first,destination,
                                     i->second,
                                     orig,mod,reverse_option,conflicts,
                                     apply_content_patches);
                        }
                    }
                }
            }
        }
    }
}

/* A version for exact patching */

void apply_patches(list<pair<pair<string,string>,string> > patched[2],
                   const bool &reverse,
                   const path &patch, const path &destination)
{
  Inventory source_inventory;
  list<Moved_Path> moved;
  Conflicts conflicts(destination);

  apply_patches(patched,moved,source_inventory,reverse,patch,destination,
                conflicts,true,true,true);
  if(!conflicts.empty())
    throw arx_error("INTERNAL ERROR: Patch conflicts when exact patching");
}

/* A version for inexact patching */

void apply_patches(list<pair<pair<string,string>,string> > patched[2],
                   const list<Moved_Path> &moved,
                   const Inventory &source_inventory,
                   const bool &reverse,
                   const path &patch, const path &destination,
                   Conflicts &conflicts,
                   const bool &apply_content_patches,
                   const bool &record_missing)
{
  apply_patches(patched,moved,source_inventory,reverse,patch,destination,
                conflicts,false,apply_content_patches,record_missing);
}
