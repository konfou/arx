/* Apply a patch to one directory.  The only thing that can happen is
   metadata changes.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include <string>
#include "arx_error.hpp"
#include <bitset>
#include "../set_properties_from_file.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void patch_directory(const path &root, const path &patch_dir,
                     const path &destination, const string &orig,
                     const string &mod)
{
  /* Make sure we have all of the files. */
  if(!lexists(patch_dir/("dir-prop-" + orig))
     || !lexists(patch_dir/("dir-prop-" + mod)))
    {
      throw arx_error("Bad patch: Both of the following files must exist:\n\t"
                      + (patch_dir/("dir-prop-" + orig))
                      .native_file_string()
                      + "\n\t"
                      + (patch_dir/("dir-prop-" + mod))
                      .native_file_string());
    }
  set_properties_from_file(root,destination,patch_dir/("dir-prop-" + mod));
}
