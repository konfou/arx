/* Copy all the files for a missing patch to a new place.
   
  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include <string>

#include <iostream>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

inline void move_if_exists(const path &patch_dir, const path &source,
                           const string &patch_file)
{
  if(lexists(patch_dir/patch_file))
    rename(patch_dir/patch_file,source/patch_file);
}

void copy_missing_patch(const string &patch_file,
                        const path &patch_dir,
                        const path &source, const bool &is_directory)
{
  const path destination_dir((source/patch_file).branch_path());

  fs::create_directories(destination_dir);
  if(is_directory)
    {
      fs::create_directories(source/patch_file);
      fs::rename(patch_dir/(patch_file + "/dir-prop-mod"),
                 source/(patch_file + "/dir-prop-mod"));
      fs::rename(patch_dir/(patch_file + "/dir-prop-orig"),
                 source/(patch_file + "/dir-prop-orig"));
    }
  else
    {
      move_if_exists(patch_dir,source,patch_file+".patch");
      move_if_exists(patch_dir,source,patch_file+".xorig");
      move_if_exists(patch_dir,source,patch_file+".xmod");
      move_if_exists(patch_dir,source,patch_file+".orig");
      move_if_exists(patch_dir,source,patch_file+".mod");
      move_if_exists(patch_dir,source,patch_file+".link-orig");
      move_if_exists(patch_dir,source,patch_file+".link-mod");
      move_if_exists(patch_dir,source,patch_file+".prop-orig");
      move_if_exists(patch_dir,source,patch_file+".prop-mod");
    }
}
