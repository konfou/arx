/* Check whether a particular file actually patches anything.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include <string>
#include <utility>

inline bool null_patch(const std::string &dest, const bool &is_directory)
{
  return (is_directory
          && !boost::filesystem::lexists(dest + "/dir-prop-mod"))
    || (!is_directory
        && ((!boost::filesystem::lexists(dest + ".patch")
             || boost::filesystem::is_empty(dest + ".patch"))
            && !boost::filesystem::lexists(dest + ".xorig")
            && !boost::filesystem::lexists(dest + ".xmod")
            && !boost::filesystem::lexists(dest + ".link-orig")
            && !boost::filesystem::lexists(dest + ".link-mod")
            && !boost::filesystem::lexists(dest + ".mod")
            && !boost::filesystem::lexists(dest + ".orig")
            && !boost::filesystem::lexists(dest + ".prop-orig")
            && !boost::filesystem::lexists(dest + ".prop-mod")));
}
