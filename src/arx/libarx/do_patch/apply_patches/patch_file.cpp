/* Apply a patches to one file.

  Copyright (C) 2003-2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "config.h"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <string>
#include "Command_Info.hpp"
#include "arx_error.hpp"
#include "get_option_from_file.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include "Spawn.hpp"
#include "tree_root.hpp"
#include "../set_properties_from_file.hpp"
#include "xdelta.hpp"
#include "read_file_into_string.hpp"
#include "Conflicts.hpp"
#include "Temp_Path.hpp"

#include <sys/stat.h>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void patch_file(const path &patch_file, const path &root,
                const path &file_path,
                const string &orig, const string &mod,
                const string &reverse_option,
                Conflicts &conflicts,
                const bool &apply_content_patches)
{
  const path destination(root/file_path);

  if(apply_content_patches)
    {
      /* Apply regular old patches.  If the patch exists but is empty,
         we skip it */
      if(fs::lexists(patch_file.string()+".patch")
         && !fs::is_empty(patch_file.string()+".patch"))
        {

          struct stat path_stat;
          if(::stat(destination.native_file_string().c_str(),&path_stat))
            throw arx_error("Can't stat " + destination.native_file_string());

          /* Apply the patches out-of-place, in case of hard links */
          path temp_location(Temp_Path(destination.branch_path(),",,dopatch"));
          fs::rename(destination,temp_location);
          fs::remove(",,patch_output");

          path rej(Temp_Path(root,file_path.string() + ".rej"));

          Spawn s;
          s << ARXPATCH;
          if(!reverse_option.empty())
            s << reverse_option;
          s << "--binary" << "-f" << "-s" << "--posix" << "-i"
            << (patch_file.native_file_string() + ".patch")
            << "-o"
            << destination.native_file_string()
            << "-r"
            << (root/rej).native_file_string()
            << temp_location.native_file_string();

          s.output=",,patch_output";
          s.error=s.output;

          int patch_stat;

          if(!s.execute(patch_stat,true))
            patch_stat=-1;

          /* Reset permissions because they might be mangled by patch */
          if(::chmod(destination.native_file_string().c_str(),path_stat.st_mode))
            throw arx_error("Can't chmod " + destination.native_file_string());

          switch(patch_stat)
            {
            case 0:
              fs::remove(temp_location);
              fs::remove(",,patch_output");
              break;
            case 1:
              /* Handle patch conflicts. */
              {
                path orig(Temp_Path(root,file_path.string() + ".orig"));
                orig=relative_path(orig,root);
                rename(temp_location,root/orig);

                conflicts.patch
                  .push_back(make_pair(file_path.string(),
                                       make_pair(orig.string(),rej.string())));
              }
              if(Command_Info::verbosity>=verbose)
                {
                  string temp;
                  fstream patch_output(",,patch_output");
                  while(getline(patch_output,temp))
                    {
                      cerr << temp << endl;
                    }
                }
              fs::remove(",,patch_output");
              break;
            default:
              /* Serious trouble */
              throw arx_error("INTERNAL ERROR: bad patch status with files\n\t"
                              + patch_file.native_file_string() + ".patch\n\t"
                              + destination.native_file_string() + "\n\t"
                              + temp_location.native_file_string()
                              + "\n\t,,patch_output");
              break;
            }
          fs::remove(",,patch_output");
        }

      /* Apply an xdelta if it exists */

      else if(fs::lexists(patch_file.string()+".x"+orig))
        {
          string orig_string(read_file_into_string(destination));
          sha256 orig_hash(orig_string), saved_hash;

          fs::ifstream delta_file(patch_file.string()+".x"+orig);
          delta_file >> saved_hash;

          if(orig_hash==saved_hash)
            {
              string delta_string(read_file_into_string(delta_file));
              string mod_string;

              apply_xdelta(orig_string,delta_string,mod_string);
              fs::remove(destination);
              fs::ofstream dest(destination);
              if(!dest.write(mod_string.c_str(),mod_string.size()))
                throw arx_error("Can't write out destination\n\t"
                                + destination.native_file_string());
            }
          else
            {
              /* Handle an xdelta patch that does not apply. */

              path xdel(Temp_Path(root,file_path.string() + ".xdelta"));
              fs::copy(patch_file.string()+".x"+orig,
                       root/xdel);
              conflicts.xdelta.push_back(make_pair(file_path.string(),
                                                   xdel.string()));
            }
        }
    }
  
  /* Handle symlinks and places where files are completely replaced
     instead of being patched. */
  
  if(fs::lexists(patch_file.string() + ".link-" + orig)
     || fs::lexists(patch_file.string() + "." + orig))
    {
      fs::remove(destination);
      if(fs::lexists(patch_file.string() + ".link-" + mod))
        {
          /* Replaced by a symlink */
          fs::symlink(destination,
                      get_option_from_file(patch_file.string()
                                           + ".link-" + mod));
        }
      else if(fs::lexists(patch_file.string() + "." + mod))
        {
          /* Replaced by a complete file */
          fs::rename(patch_file.string() + "." + mod,destination);
        }
      else
        {
          throw arx_error
            ("INTERNAL ERROR: Bad patch: One of\n\t"
             + patch_file.native_file_string() + ".link-" + orig + "\n\t"
             + patch_file.native_file_string() + "." + orig
             + "\nexists, but can't find either of\n\t"
             + patch_file.native_file_string() + ".link-" + mod + "\n\t"
             + patch_file.native_file_string() + "." + mod);
        }
    }
  
  /* Apply any property changes. */
  if(fs::lexists(patch_file.string() + ".prop-" + orig))
    {
      if(fs::lexists(patch_file.string() + ".prop-" + mod))
        {
          set_properties_from_file(root,file_path,
                                   patch_file.string() + ".prop-" + mod);
        }
      else
        {
          throw arx_error
            ("Bad patch: Either both or neither of the following files must exist:\n\t"
             + patch_file.native_file_string() + ".prop-" + orig
             + "\n\t"
             + patch_file.native_file_string() + ".prop-" + mod);
        }
    }
}
