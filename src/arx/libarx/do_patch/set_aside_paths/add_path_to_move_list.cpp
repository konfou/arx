/* Add a path to the list of moved files given its file_attributes
   and parent.

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Moved_Path.hpp"
#include <list>
#include "file_attributes.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

Moved_Path add_path_to_move_list(list<Moved_Path> &moved,
                                 const file_attributes &k,
                                 const Moved_Path &parent,
                                 const bool &is_directory)
{
  Moved_Path moved_path(k.file_path.string(),
                        (parent.destination/k.file_path.leaf()).string(),
                        k.inventory_id,is_directory);
  moved_path.local_path=k.file_path;
  moved_path.leaf=k.file_path.leaf();
  if(parent.deleted)
    {
      moved_path.destination="";
      moved_path.deleted=true;
    }
  moved_path.local_parent_id=parent.inventory_id;
  moved_path.patch_initial=parent.patch_initial/moved_path.leaf;

  moved.push_back(moved_path);
  return moved_path;
}
