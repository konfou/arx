/* Add a path to the list of moved files given its file_attributes
   and parent.

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_ADD_PATH_TO_MOVE_LIST_HPP
#define ARX_ADD_PATH_TO_MOVE_LIST_HPP

#include "Moved_Path.hpp"
#include <list>
#include "file_attributes.hpp"

Moved_Path add_path_to_move_list(std::list<Moved_Path> &moved,
                                 const file_attributes &k,
                                 const Moved_Path &parent,
                                 const bool &is_directory);

#endif
