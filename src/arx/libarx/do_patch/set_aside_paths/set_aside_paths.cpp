/* Set aside the deleted and renamed files and directories.  There are
   versions for exact and inexact patching.

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Command_Info.hpp"
#include <list>
#include <functional>
#include <sstream>
#include "Moved_Path.hpp"
#include "Inventory.hpp"
#include "file_attributes.hpp"
#include "check_symlink_hierarchy.hpp"
#include "unsafe_move_inventory_id.hpp"
#include "unsafe_delete_inventory_id.hpp"
#include "add_path_to_move_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;


extern Moved_Path add_parent(const path &moved_path,
                             const inventory_types &inv_type,
                             list<Moved_Path> &moved,
                             const Inventory &source_inventory);

/* A version for exact patching that doesn't do all of the
   checking. */

void set_aside_paths(const path &deleted_path, const path &source_path,
                     list<Moved_Path> &moved)
{
  /* We need to get a list of all of the directories that are moving,
     and move the bottom ones first.  Then, when moving things back
     for the rename, we move the ones that will be topmost first.  */

  /* Remove all of the non-existent moves from the move list. */  
  moved.remove_if(bind2nd(ptr_fun(Moved_Path_initial_eq_string),""));

  /* Sort based on the name that the file or directory has now.  Sort
     in reverse order, so that the bottom-most files appear before top
     directories. */
  
  moved.sort(not2(ptr_fun(Moved_Path_initial_cmp)));

  /* Now move them, starting from the bottom.  We save the new name in
     the original list.  */

  int renames(0), deletes(0);

  for(list<Moved_Path>::iterator i=moved.begin(); i!=moved.end(); ++i)
    {
      const path old_source=i->initial;
      /* Make sure we aren't moving things from symlinks. */
      check_symlink_hierarchy(source_path,source_path/old_source);

      if(i->destination.empty())
        {
          file_attributes f;
          f.file_path=i->initial;
          f.inventory_id=i->inventory_id;
          f.link=false;

          stringstream deleted;
          deleted << ",,delete-" << deletes;
          i->destination=deleted_path.leaf()/i->initial;
          i->initial=path(deleted.str());
          i->deleted=true;
          ++deletes;

          unsafe_delete_inventory_id(source_path,f,i->is_directory);
        }
      else
        {
          stringstream renamed;
          renamed << ",,rename-" << renames;
          i->initial=path(renamed.str());
          ++renames;
          unsafe_move_inventory_id(source_path,old_source,
                                   deleted_path.leaf()/i->initial,
                                   i->inventory_id);
        }
      if(Command_Info::verbosity>=verbose)
        cout << "Renaming: " << old_source.native_file_string() << "\t"
             << (deleted_path/i->initial).native_file_string() << endl;
      
      rename(source_path/old_source,deleted_path/i->initial);
    }
}

/* A more generic version that checks for conflicts. */

void set_aside_paths(const path &deleted_path, const path &source_path,
                     list<Moved_Path> &moved,
                     list<pair<pair<string,string>,string> > &missing_moves,
                     const Inventory &source_inventory,
                     const bool &record_missing)
{
  /* We need to get a list of all of the directories that are moving,
     and move the bottom ones first.  Then, when moving things back
     for the rename, we move the ones that will be topmost first.  */
  
  for(list<Moved_Path>::iterator i=moved.begin(); i!=moved.end(); ++i)
    {
      /* Find the path with the same id. */
      inventory_types inv_type=is_control(i->initial) ? control : source;
      int m=i->is_directory ? arx_dir : arx_file;

      Inventory::const_iterator
        j(find_if(source_inventory(inv_type,m).begin(),
                  source_inventory(inv_type,m).end(),
                  bind2nd(ptr_fun(inventory_id_eq_string),
                          i->inventory_id)));

      if(j==source_inventory(inv_type,m).end())
        {
          /* Handle non-existent moves.  Ignore any missing deleted files. */
          if(record_missing && !i->destination.empty())
            missing_moves.push_back(make_pair
                                    (make_pair(i->initial.string(),
                                               i->destination.string()),
                                     i->inventory_id));
          i->initial="";
        }
      else
        {
          /* Save the initial path according to the patch and the
             local tree, and the inventory id of the parent.  Needed
             for doing correct renaming in move_to_destination */
          i->patch_initial=i->initial;
          i->local_path=j->file_path;
          if(i->local_path.has_branch_path())
            {
              /* Finds the parent id */
              Inventory::const_iterator
                k(find_if(source_inventory(inv_type,arx_dir).begin(),
                          source_inventory(inv_type,arx_dir).end(),
                          bind2nd(ptr_fun(file_path_eq_string),
                                  i->local_path.branch_path().string())));
              if(k!=source_inventory(inv_type,arx_dir).end())
                i->local_parent_id=k->inventory_id;
            }
          i->initial=j->file_path;

          /* Add any children of the moved path that are not already
             in the list of moved paths. */
          for(int m=0;m<2;++m)
            {
              for(Inventory::const_iterator
                    k=source_inventory(inv_type,m).begin();
                  k!=source_inventory(inv_type,m).end(); ++k)
                {
                  if(k->file_path.string().substr(0,i->local_path.string().size()+1)
                     ==i->local_path.string() + "/"
                     && find_if(moved.begin(),moved.end(),
                                bind2nd
                                (ptr_fun(Moved_Path_inventory_id_eq_string),
                                 k->inventory_id))==moved.end())
                    {
                      Moved_Path parent(add_parent(k->file_path,inv_type,
                                                   moved,source_inventory));
                      add_path_to_move_list(moved,*k,parent,m==arx_dir);
                    }
                }
            } 
        }

    }
  /* Do the actual setting aside. */
  set_aside_paths(deleted_path,source_path,moved);
}

