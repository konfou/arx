/* Add the parent of a path that is being moved to the list of moved
   paths.  This is a recursive call, because the parents have parents
   etc.

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Moved_Path.hpp"
#include <list>
#include "Inventory.hpp"
#include "add_path_to_move_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

Moved_Path add_parent(const path &moved_path,
                      const inventory_types &inv_type,
                      list<Moved_Path> &moved,
                      const Inventory &source_inventory)
{
  for(Inventory::const_iterator
        parent=source_inventory(inv_type,arx_dir).begin();
      parent!=source_inventory(inv_type,arx_dir).end(); ++parent)
    {
      /* If this is the immediate parent, then check whether it is
         already in the list of moved paths.  If not, add its parent
         and then itself. */
      if(parent->file_path/moved_path.leaf()==moved_path)
        {
          list<Moved_Path>::iterator
            i=find_if(moved.begin(),moved.end(),
                      bind2nd(ptr_fun(Moved_Path_inventory_id_eq_string),
                              parent->inventory_id));
          if(i==moved.end())
            {
              Moved_Path grandparent(add_parent(parent->file_path,inv_type,moved,
                                                source_inventory));
              
              return add_path_to_move_list(moved,*parent,grandparent,true);
            }
          else
            {
              return *i;
            }
        }
    }
  throw arx_error("Can not find the parent for this path in the inventory.\n\t"
                  + moved_path.native_file_string());
}
