/* Set properties for a given path given a property file listing them.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_SET_PROPERTIES_FROM_FILE
#define ARX_SET_PROPERTIES_FROM_FILE

#include "boost/filesystem/operations.hpp"

extern void set_properties_from_file(const boost::filesystem::path &dir,
                                     const boost::filesystem::path &target,
                                     const boost::filesystem::path &prop_path);

#endif
