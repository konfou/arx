/* Move added files to a temp directory where they will later be moved
   to their final destination.  There are versions for exact and
   inexact patching.

  Copyright (C) 2003-2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <string>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/serialization/map.hpp"
#include <list>
#include <utility>
#include "Moved_Path.hpp"
#include <sstream>
#include "check_symlink_hierarchy.hpp"
#include "Command_Info.hpp"
#include <iostream>
#include "sha256.hpp"
#include "add_path_list.hpp"
#include "property_action.hpp"
#include "Inventory.hpp"
#include "inventory_types.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void read_properties(map<string,string> &properties, const path &prop_path)
{
  if(exists(prop_path))
    {
      fs::ifstream prop_file(prop_path);
      archive::text_iarchive prop_archive(prop_file);
      prop_archive >> properties;
    }
}

/* The version for exact patching, called by the inexact version after
   it checks for conflicts. */

void add_files_and_directories(list<Moved_Path> &new_paths,
                               const path &patch, const path &source,
                               const path &temp_path,
                               list<Moved_Path> &moved,
                               const string &new_files_string,
                               const string &mod,
                               const bool exact_patching=true)
{
  const path prop_dir(patch/(new_files_string+"-prop"));

  /* Sort so that the topmost directories are first. */

  new_paths.sort(Moved_Path_destination_cmp);
  
  /* Then flatten the directory hierarchy, starting with the
     bottom-most. */

  int adds(0);
  list<pair<file_attributes,sha256> > add_list;
  for(list<Moved_Path>::reverse_iterator i=new_paths.rbegin();
      i!=new_paths.rend(); ++i)
    {
      stringstream added;
      added << ",,added-" << adds;
      i->initial=added.str();

      /* If it is a directory, delete all of the contents.  The
         contents should only be directories, if anything. */

      const path location(patch/new_files_string/i->destination);

      /* Make sure we aren't moving things from symlinks. */
      check_symlink_hierarchy(patch,location);

      if(Command_Info::verbosity>=verbose)
        cout << "Deleting subdirectories: "
             << location.native_file_string() << endl;

      if(!symbolic_link_exists(location) && is_directory(location))
        {
          for(fs::directory_iterator j(location); j!=fs::directory_iterator();
              ++j)
            {
              remove_all(*j);
            }
        }


      if(Command_Info::verbosity>=verbose)
        cout << "Copying: " << location.native_file_string()
             << "\t" << (temp_path/i->initial).native_file_string() << endl;


      /* We hard link the files instead of copying if we are doing
         exact patching, since the patches are going to disappear
         anyway. */
      fs::copy(location,temp_path/i->initial,false,exact_patching);
      ++adds;

      /* If the inventory id has not been set to empty because of
         conflicts, then do the actual add. */

      file_attributes f;
      f.file_path=temp_path.leaf()/i->initial;
      f.inventory_id=i->inventory_id;
      
      if(symbolic_link_exists(source/f.file_path))
        {
          read_properties
            (f.properties,prop_dir
             /(i->destination.native_file_string() + ".prop"));

          if(!i->inventory_id.empty())
            add_list.push_back
              (make_pair(f,sha256(readlink(source/f.file_path).string())));
        }
      else if(is_directory(source/f.file_path))
        {
          read_properties(f.properties,prop_dir/i->destination/"dir-prop");
          if(!i->inventory_id.empty())
            add_list.push_back(make_pair(f,sha256()));
        }
      else
        {
          read_properties
            (f.properties,prop_dir
             /(i->destination.native_file_string() + ".prop"));
          if(!i->inventory_id.empty())
            add_list.push_back(make_pair(f,sha256(source/f.file_path)));
        }          
      /* Set the properties for paths not in the manifest? */
      if(i->inventory_id.empty())
        for(map<string,string>::iterator j=f.properties.begin();
            j!=f.properties.end(); ++j)
          property_action(source,f.file_path,"set",j->first,j->second);
    }
  /* Do the actual add and set the properties of paths in the manifest. */
  add_path_list(source,add_list);
  for(list<pair<file_attributes,sha256> >::iterator i=add_list.begin();
      i!=add_list.end(); ++i)
    for(map<string,string>::iterator j=i->first.properties.begin();
        j!=i->first.properties.end(); ++j)
      property_action(source,i->first.file_path,"set",j->first,j->second);

  /* Finally, splice the moved list into the total list of moved
     files. */
  moved.splice(moved.end(),new_paths);
}

/* A version for inexact patching.  We have to check for adding inventory ids
   that already exist. */

void add_files_and_directories(list<Moved_Path> &new_paths,
                               const path &patch, const path &source_path,
                               const path &temp_path,
                               list<Moved_Path> &moved,
                               const string &new_files_string,
                               const string &mod,
                               const Inventory &source_inventory,
                               list<pair<pair<string,string>,string> >
                               &add_conflicts)
{
  /* For inexact patching, make sure that we aren't adding a file with
     the same inventory id as another existing one. */

  list<Moved_Path>::iterator next=new_paths.begin();
  for(list<Moved_Path>::iterator i=new_paths.begin(); i!=new_paths.end();
      i=next)
    {
      ++next;

      /* Find the path with the same id. */
      inventory_types inv_type=((is_control(i->destination))
                                ? control : source);

      /* I am not sure that I have to loop over m here, but I am just
         being cautious */
      for(int m=0; m<2; ++m)
        {
          Inventory::const_iterator
            j(find_if(source_inventory(inv_type,m).begin(),
                      source_inventory(inv_type,m).end(),
                      bind2nd(ptr_fun(inventory_id_eq_string),
                              i->inventory_id)));
          if(j!=source_inventory(inv_type,m).end())
            {
              if(sha256(source_path/j->file_path)
                 !=sha256(patch/new_files_string/i->destination)
                 || j->file_path.string()!=i->destination.string())
                {
                  add_conflicts.
                    push_back(make_pair
                              (make_pair
                               (i->destination.string(),j->file_path.string()),
                               i->inventory_id));
                  i->inventory_id="";
                }
              else
                {
                  /* This is really the same element, so we need to
                     erase it from the list. */
                  new_paths.erase(i);
                }
            }
        }
    }
  add_files_and_directories(new_paths,patch,source_path,temp_path,moved,
                            new_files_string,mod,false);
}
