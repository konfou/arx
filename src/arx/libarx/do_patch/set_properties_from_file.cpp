/* Set properties for a given path given a property file listing them.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "tree_root.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "property_action.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void set_properties_from_file(const path &dir, const path &target,
                              const path &prop_path)
{
  const path root=tree_root(dir);
  fs::ifstream prop_file(prop_path);
  archive::text_iarchive prop_archive(prop_file);

  string action, key, value;

  prop_archive >> action;
  while(prop_file)
    {
      prop_archive >> key >> value;
      property_action(root,target,action,key,value);
      prop_archive >> action;
    }
}
