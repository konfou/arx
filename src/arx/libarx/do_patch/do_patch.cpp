/* Apply a patch to a source tree.  There are two versions, one for
   exact patching and one for inexact patching.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Conflicts.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include "Inventory_Flags.hpp"
#include "Inventory.hpp"
#include "inventory_directory.hpp"
#include <list>
#include <utility>
#include "Moved_Path.hpp"
#include "Current_Path.hpp"
#include "read_and_sort_index.hpp"
#include "read_checksums.hpp"
#include "edit_path.hpp"
#include "Temp_Path.hpp"

using namespace std;
using namespace boost;
using namespace boost::posix_time;
namespace fs=boost::filesystem;
using fs::path;

extern void apply_patches(list<pair<pair<string,string>,string> > patched[2],
                          const list<Moved_Path> &moved,
                          const Inventory &source_inventory,
                          const bool &reverse,
                          const path &patch, const path &destination,
                          Conflicts &conflicts,
                          const bool &apply_content_patches,
                          const bool &record_missing);

extern void move_to_destination(list<Moved_Path> &moved,
                                const path &deleted_path,
                                const path &source,
                                Conflicts &conflicts,
                                Inventory &source_inventory,
                                const list<pair<string,string> > original[2],
                                const list<pair<string,string> > modified[2]);

extern void set_aside_paths(const path &deleted_path, const path &source,
                            list<Moved_Path> &moved,
                            list<pair<pair<string,string>,string> >
                            &missing_moves,
                            const Inventory &source_inventory,
                            const bool &record_missing);

extern void categorize_files(const list<pair<string,string> > original[2],
                             const list<pair<string,string> > modified[2],
                             list<pair<pair<string,string>,string> > patched[2],
                             list<Moved_Path> &moved,
                             list<Moved_Path> &mod_only,
                             const bool &reversed);

extern void add_files_and_directories(list<Moved_Path> &new_paths,
                                      const path &patch, const path &source,
                                      const path &temp_path,
                                      list<Moved_Path> &moved,
                                      const string &new_files_string,
                                      const string &mod,
                                      const Inventory &source_inventory,
                                      list<pair<pair<string,string>,string> >
                                      &add_conflicts);

/* This version is the general version for inexact patching. */

Conflicts do_patch(const path &patch, const path &source, const bool &reverse,
                   const bool &delete_removed,
                   const bool record_missing=true,
                   const bool apply_content_patches=true)
{
  Conflicts conflicts(source);
  
  if(!exists(patch) || !is_directory(patch))
    {
      throw arx_error("Patch directory does not exist or is not a directory: "
                      + patch.native_file_string());
    }
  if(!exists(source) || !is_directory(source))
    {
      throw arx_error("Source directory does not exist or is not a directory: "
                      + source.native_file_string());
    }

  /* Strings to parametrize whether a patch is to be reversed. */
  string orig("orig"), mod("mod"), new_files_string("added");
  if(reverse)
    {
      orig="mod";
      mod="orig";
      new_files_string="deleted";
    }
          
  /* Inventory the source tree. */

  Inventory source_inventory;
  Checksums source_checksums;

  /* If the source directory is empty, don't inventory it.  This
     is useful for applying the very first patch. */

  if(fs::directory_iterator(source)!=fs::directory_iterator())
    {
      Current_Path current_path(source);
      Inventory_Flags flags(fs::current_path());
      flags.source=flags.control=true;
      flags.files=flags.directories=true;

      read_checksums(fs::current_path(),source_checksums);
      Checksums checksums(source_checksums);
      inventory_directory("", source_inventory, flags, checksums);
    }
  else
    {
      /* We have to create an _arx directory so that tree_root
         will work. */
      create_directory(source/"_arx");
    }

  /* Sort by ids to allow efficient searching later. */
  for(int m=0;m<2;++m)
    for(int l=0;l<num_inventory_types;++l)
      {
        source_inventory(l,m).sort(inventory_id_cmp);
      }

  /* Read the patch indices and categorize files. */

  list<pair<string,string> > original[2], modified[2];

  read_and_sort_index(patch/(orig + "-files-index"),original[arx_file]);
  read_and_sort_index(patch/(orig + "-dirs-index"),original[arx_dir]);
  read_and_sort_index(patch/(mod + "-files-index"),modified[arx_file]);
  read_and_sort_index(patch/(mod + "-dirs-index"),modified[arx_dir]);

  list<pair<pair<string,string>,string> > patched[2];
  list<Moved_Path> mod_only, moved;
  categorize_files(original,modified,patched,moved,mod_only,reverse);

  /* Do the real meat of deleting, renaming, adding and patching.
     We do the set asides because, among other things, a file may
     be renamed into a directory that has just been created or
     renamed.  */

  /* Create a directory where I can put deleted things and other
     things temporarily. */

  path deleted_path;
  deleted_path=Temp_Path(source,",,deleted");
  if(lexists(deleted_path))
    {
      throw arx_error("Deleted items directory already exists\n\t"
                      + deleted_path.native_file_string());
    }
  else
    {
      fs::create_directory(deleted_path);
    }

  set_aside_paths(deleted_path,source,moved,conflicts.missing_moves,
                  source_inventory,record_missing);
  add_files_and_directories(mod_only,patch,source,deleted_path,moved,
                            new_files_string,mod,source_inventory,
                            conflicts.add);
  move_to_destination(moved,deleted_path,source,conflicts,
                      source_inventory,original,modified);
  apply_patches(patched,moved,source_inventory,reverse,patch/"patches",
                source,conflicts,apply_content_patches,record_missing);

  if(apply_content_patches)
    conflicts.save_to_conflicts_file();

  /* Remove the deleted paths if the option is set or the
     directory is empty. */

  if(delete_removed || fs::directory_iterator(deleted_path)
     ==fs::directory_iterator())
    remove_all(deleted_path);

  /* If we are patching a non-edit tree, we update the ++edit file
     with the patched files. */

  if(fs::exists(source/"_arx/++edit"))
    {
      for(list<pair<pair<string,string>,string> >::iterator
            i=patched[arx_file].begin(); i!=patched[arx_file].end(); ++i)
        if(!i->second.empty())
          edit_path(source,i->second);
    }

  return conflicts;
}


/* Versions of functions for exact patching. */

extern void apply_patches(list<pair<pair<string,string>,string> > patched[2],
                          const bool &reverse,
                          const path &patch, const path &destination);

extern void move_to_destination(list<Moved_Path> &moved,
                                const path &deleted_path,
                                const path &source);

extern void set_aside_paths(const path &deleted_path, const path &source,
                            list<Moved_Path> &moved);

extern void add_files_and_directories(list<Moved_Path> &new_paths,
                                      const path &patch, const path &source,
                                      const path &temp_path,
                                      list<Moved_Path> &moved,
                                      const string &new_files_string,
                                      const string &mod,
                                      const bool exact_patching=true);

/* Do exact patching.  If the patch is not exact, this will barf in
   unexpected ways. */

void do_patch(const path &patch, const path &source, const bool &reverse)
{
  /* Strings to parametrize whether a patch is to be reversed. */
  string orig("orig"), mod("mod"), new_files_string("added");
  if(reverse)
    {
      orig="mod";
      mod="orig";
      new_files_string="deleted";
    }

  /* If the source directory is empty, create an _arx directory so
     that tree_root will work. */

  if(fs::directory_iterator(source)==fs::directory_iterator())
    {
      create_directory(source/"_arx");
    }

  /* Read the patch indices and categorize files. */

  list<pair<string,string> > original[2], modified[2];

  read_and_sort_index(patch/(orig + "-files-index"),original[arx_file]);
  read_and_sort_index(patch/(orig + "-dirs-index"),original[arx_dir]);
  read_and_sort_index(patch/(mod + "-files-index"),modified[arx_file]);
  read_and_sort_index(patch/(mod + "-dirs-index"),modified[arx_dir]);

  list<pair<pair<string,string>,string> > patched[2];
  list<Moved_Path> mod_only, moved;
  categorize_files(original,modified,patched,moved,mod_only,reverse);

  /* Do the real meat of deleting, renaming, adding and patching.
     We do the set asides because, among other things, a file may
     be renamed into a directory that has just been created or
     renamed.  */

  /* Create a directory where I can put deleted things and other
     things temporarily. */

  path deleted_path;
  deleted_path=Temp_Path(source,",,deleted");
  if(lexists(deleted_path))
    {
      throw arx_error("Deleted items directory already exists\n\t"
                      + deleted_path.native_file_string());
    }
  else
    {
      fs::create_directory(deleted_path);
    }

  set_aside_paths(deleted_path,source,moved);
  add_files_and_directories(mod_only,patch,source,deleted_path,moved,
                            new_files_string,mod);
  move_to_destination(moved,deleted_path,source);
  apply_patches(patched,reverse,patch/"patches",source);

  /* Remove the deleted paths */

  remove_all(deleted_path);
}
