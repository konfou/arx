/* Move all of the deletes and renames to their final resting place.

  Copyright (C) 2003, 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include "Moved_Path.hpp"
#include <list>
#include <sstream>
#include "Inventory.hpp"
#include "unsafe_move_inventory_id.hpp"
#include "Temp_Path.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

/* The root parameter is only if we have to move inventory ids. */

path move_original_away(const path &root,
                        const path &source_path,
                        Inventory &source_inventory,
                        list<pair<pair<path,path>,string> > &move_list)
{
  path orig(Temp_Path(root,source_path.string() + ".orig"));
  orig=relative_path(orig,root);

  for(int m=0; m<2; ++m)
    for(Inventory::iterator i=source_inventory(source,m).begin();
        i!=source_inventory(source,m).end(); ++i)
      {
        if(i->file_path.string()==source_path.string())
          {
            move_list.push_back(make_pair(make_pair(source_path,orig),
                                          i->inventory_id));
            i->file_path=orig;
            m=2;
            break;
          }
      }

  rename(root/source_path,root/orig);
  return orig;
}
