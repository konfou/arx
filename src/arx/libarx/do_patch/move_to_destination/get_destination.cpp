/* Gets the destination for a moved path.  It is assumed that the path
   already knows what it's parent's id is.  This requires finding the
   path associated with the id.  Since the parent may have also moved,
   this function has to be recursive.

  Copyright (C) 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Moved_Path.hpp"
#include "Inventory.hpp"
#include "Conflicts.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void get_destination(Moved_Path &moved_path,
                     list<Moved_Path> &moved,
                     const Inventory &source_inventory,
                     Conflicts &conflicts)
{
  /* The destination is already set.*/
  if(!moved_path.destination.empty())
    return;

  /* If the path is being deleted. */
  if(moved_path.deleted)
    {
      moved_path.destination=moved_path.patch_destination;
      return;
    }

  /* If we are in a directory loop, then try to put it back where it
     started.  This is not perfect, but getting the optimal answer
     would be a great deal of work for such an obscure boundary
     case. */
  if(moved_path.directory_loop)
    {
      /* First find out if we have a local parent id.  If not, fall
         back to the local parent path and hope for the best :( We end
         up clearing out the moved_path's destination parents and
         calling get_destination again.  It gives an extra level of
         recursion, but recursion is fun! */

      moved_path.destination_parent_ids.clear();
      if(!moved_path.local_parent_id.empty())
        {
          moved_path.destination_parent_ids
            .push_back(moved_path.local_parent_id);
        }
      moved_path.patch_destination=moved_path.local_path;
    }

  /* If we don't have any parent ids, then just use the patch's
     destination. */
  if(moved_path.destination_parent_ids.empty())
    {
      moved_path.destination=moved_path.patch_destination;
      return;
    }

  string id(*moved_path.destination_parent_ids.begin());
  list<Moved_Path>::iterator
    i(find_if(moved.begin(),moved.end(),
              bind2nd(ptr_fun(Moved_Path_inventory_id_eq_string),id)));

  if(i!=moved.end())
    {
      /* If the parent is also a moved path, then we have to find its
         destination. */

      if(i->deleted)
        {
          conflicts.deleted_parent.push_back
            (make_pair(moved_path.patch_initial.string(),
                       moved_path.patch_destination.string()));
          moved_path.destination=moved_path.patch_destination;
        }
      else
        {
          moved_path.directory_loop=true;

          get_destination(*i,moved,source_inventory,conflicts);
          /* Check for directory loops.  If we have a loop, just keep
             everything where it was before.  This may cause extra
             conflicts is the parent has been deleted. 

             What it really should do is move as many things as
             possible, breaking the loop in as few places as possible.
             That requires looking at the entire graph and deciding
             where to break it.  That is a bit too much work for now.
             Especially since this is probably not a common
             problem. */
          if(moved_path.destination.empty())
            {
              moved_path.destination=i->destination/moved_path.leaf;
              moved_path.directory_loop=i->directory_loop;
            }
          if(moved_path.directory_loop==true)
            {
              /* If we have a directory loop, add this move to the
                 list of conflicts.  Check for duplicates, because we
                 may have already added it to the list when processing
                 parents of conflicts. */
              list<pair<pair<string,string>,pair<string,string> > >::iterator
                duplicate=conflicts.directory_loop.begin();
              for(;duplicate!=conflicts.directory_loop.end();++duplicate)
                if(duplicate->first.second==moved_path.inventory_id)
                  break;
              
              if(duplicate==conflicts.directory_loop.end())
                conflicts.directory_loop.push_back
                  (make_pair(make_pair(moved_path.destination.string(),
                                       moved_path.inventory_id),
                             make_pair(moved_path.local_path.string(),
                                       (i->local_path/moved_path.leaf).string())));
            }
        }
    }
  else
    {
      path parent_leaf,
        parent_branch(moved_path.patch_destination.branch_path());
      bool conflict(false);
      for(list<string>::iterator k=moved_path.destination_parent_ids.begin();
          k!=moved_path.destination_parent_ids.end(); ++k)
        {
          /* Otherwise, look for the parent in the inventory.  */
          Inventory::const_iterator
            j(find_if(source_inventory(source,arx_dir).begin(),
                      source_inventory(source,arx_dir).end(),
                      bind2nd(ptr_fun(inventory_id_eq_string),*k)));
          if(j!=source_inventory(source,arx_dir).end())
            {
              moved_path.destination=j->file_path/parent_leaf/moved_path.leaf;
            }
          else
            {
              /* There is no match for the inventory id, so we have a
                 conflict.  Continue with the parent's parent id. */
              conflict=true;
              parent_leaf/=parent_branch.leaf();
              parent_branch=parent_branch.branch_path();
            }
        }

      /* If nothing can be found, then it goes in the original patch
         destination. */
      if(moved_path.destination.empty())
        moved_path.destination=moved_path.patch_destination;
      if(conflict)
        conflicts.no_parent.push_back
          (make_pair(moved_path.patch_initial.string(),
                     moved_path.patch_destination.string()));
    }
}
