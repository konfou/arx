/* Find the inventory id of the parent or parents of a path.

  Copyright (C) 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include <list>
#include <utility>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

/* This version only finds the immediate parent of a path. */

void find_parents(path &parent_path,
                  string &parent_id,
                  const path &branch_path,
                  const list<pair<string,string> > &path_list)
{
  for(list<pair<string,string> >::const_iterator i=path_list.begin();
      i!=path_list.end(); ++i)
    {
      if(i->first==branch_path.string())
        {
          parent_path=i->first;
          parent_id=i->second;
          break;
        }
    }
}

/* This version finds all of the parents for a path and puts it in a
   list. */

void find_parents(path &parent_path,
                  list<string> &parent_ids,
                  const path &branch_path,
                  const list<pair<string,string> > &path_list)
{
  path temp(branch_path), temp_parent;
  parent_path=path();
  while(!temp.empty())
    {
      string parent_id;
      find_parents(temp_parent,parent_id,temp,path_list);
      if(parent_path.empty())
        parent_path=temp_parent;
      parent_ids.push_back(parent_id);
      temp=temp.branch_path();
    }
}
