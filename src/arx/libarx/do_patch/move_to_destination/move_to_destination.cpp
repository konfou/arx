/* Move all of the deletes and renames to their final resting place.
   There are versions for exact and inexact patching.

  Copyright (C) 2003-2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include "Moved_Path.hpp"
#include <list>
#include "check_symlink_hierarchy.hpp"
#include "Command_Info.hpp"
#include "Inventory.hpp"
#include "Conflicts.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern path move_original_away(const path &root,
                               const path &source_path,
                               Inventory &source_inventory,
                               list<pair<pair<path,path>,string> > &move_list);

extern void find_parents(path &parent_path,
                         string &parent_id,
                         const path &branch_path,
                         const list<pair<string,string> > &path_list);
extern void find_parents(path &parent_path,
                         list<string> &parent_ids,
                         const path &branch_path,
                         const list<pair<string,string> > &path_list);
extern void get_destination(Moved_Path &moved_path,
                     list<Moved_Path> &moved,
                     const Inventory &source_inventory,
                     Conflicts &conficts);
extern void unsafe_move_list
(const path &root,
 const list<pair<pair<path,path>,string> > &move_list);

/* Generic version for exact and inexact patching. */

void move_to_destination(list<Moved_Path> &moved, const path &deleted_path,
                         const path &source_path,
                         Conflicts &conflicts,
                         Inventory &source_inventory,
                         const list<pair<string,string> > original[2],
                         const list<pair<string,string> > modified[2],
                         const bool &exact_patching)
{
  /* We need to find out where the true destination is, in case the
     parent directory has moved. */

  if(!exact_patching)
    {
      /* First, reconcile the inventory with the new locations of
         everything. */

      for(list<Moved_Path>::iterator i=moved.begin(); i!=moved.end(); ++i)
        if(!is_control(i->destination) && !is_control(i->local_path))
          {
            int m=(i->is_directory ? arx_dir : arx_file);
            if(i->initial.leaf().substr(0,7)==",,added")
              {
                file_attributes f;
                f.file_path=i->initial;
                source_inventory(source,m).push_front(f);
                i->inventory_iterator=source_inventory(source,m).begin();
              }
            else
              {
                Inventory::iterator
                  j=find_if(source_inventory(source,m).begin(),
                            source_inventory(source,m).end(),
                            bind2nd(ptr_fun(inventory_id_eq_string),
                                    i->inventory_id));

                if(j==source_inventory(source,m).end())
                  throw arx_error("INTERNAL ERROR: Trying to move or delete a path that does not exist in the inventory.\n\t"
                                  + i->local_path.native_file_string()
                                  + "\n\t"
                                  + i->inventory_id + "\n\t");
                i->inventory_iterator=j;
                j->file_path=i->initial;
              }
          }

      for(list<Moved_Path>::iterator i=moved.begin(); i!=moved.end(); ++i)
        if(!is_control(i->destination) && !is_control(i->local_path))
          {
            path initial_parent_path, destination_parent_path;
            string initial_parent_id;
            if(i->patch_initial.has_branch_path())
              {
                find_parents(initial_parent_path,initial_parent_id,
                             i->patch_initial.branch_path(),
                             original[arx_dir]);
              }
            if(i->destination.has_branch_path())
              {
                find_parents(destination_parent_path,
                             i->destination_parent_ids,
                             i->destination.branch_path(),
                             modified[arx_dir]);
              }
            
            /* Check for conflicts.  We check for rename (foo/a ->
               foo/b) conflicts separately from move (foo/a -> bar/a)
               conflicts.  Adds will not conflict at this point, since
               initial_parent_id, patch_initial, and local_parent_id are all
               empty strings. */
            
            /* Move conflicts. */
            string destination_parent_id;
            if(!i->destination_parent_ids.empty())
              destination_parent_id=*(i->destination_parent_ids.begin());
            if(i->patch_initial.empty()
               || initial_parent_id != destination_parent_id)
              {
                if(i->local_parent_id!=initial_parent_id
                   && i->local_parent_id!=destination_parent_id)
                  {
                    conflicts.move_parent.push_back
                      (make_pair
                       (i->local_path.string(),
                        make_pair(initial_parent_path.string(),
                                  destination_parent_path.string())));
                  }
              }
            else
              {
                /* The patch did not change the parent id, so we
                   accept the local parent id . */
                i->destination_parent_ids.clear();
                if(!i->local_parent_id.empty())
                  i->destination_parent_ids.push_back(i->local_parent_id);
              }
            
            /* Rename conflicts. */
            
            string leaf;
            if(i->patch_initial.empty()
               || i->patch_initial.leaf()!=i->destination.leaf())
              {
                leaf=i->destination.leaf();
                if(i->local_path.leaf()!=i->patch_initial.leaf()
                   && i->local_path.leaf()!=i->destination.leaf())
                  {
                    conflicts.rename.push_back
                      (make_pair(i->local_path.string(),
                                 make_pair(i->patch_initial.string(),
                                           i->destination.string())));
                  }
              }
            else
              {
                /* The patch did not change the name, so we accept the
                   local name. */
                leaf=i->local_path.leaf();
              }
            
            /* Save the destination */
            i->leaf=leaf;
            i->patch_destination=i->destination;
            i->destination=path();
          }
      
      /* Get the final destination. */

      for(list<Moved_Path>::iterator i=moved.begin(); i!=moved.end(); ++i)
        if(!is_control(i->destination) && !is_control(i->local_path))
          get_destination(*i,moved,source_inventory,conflicts);
    }

  /* Sort the moved list so that topmost destinations come first. */

  moved.sort(Moved_Path_destination_cmp);

  /* Now move everything (adds, renames and deletes). */

  list<pair<pair<path,path>,string> > move_list;
  for(list<Moved_Path>::iterator i=moved.begin(); i!=moved.end(); ++i)
    {
      if(!exact_patching)
        {
          /* If the file is going into a directory, that directory may
             already exist as a symlink or file.  So we need to move that
             symlink or file out of the way. */
          
          list<path> checked_list;
          path checked_path(i->destination.branch_path());
          while(!checked_path.empty())
            {
              checked_list.push_front(checked_path);
              checked_path=checked_path.branch_path();
            }
          
          for(list<path>::iterator j=checked_list.begin();
              j!=checked_list.end(); ++j)
            {
              if(lexists(source_path/(*j))
                 && (symbolic_link_exists(source_path/(*j))
                     || !is_directory(source_path/(*j))))
                {
                  path orig(move_original_away(source_path,*j,
                                               source_inventory,move_list));
                  conflicts.move_target.push_back(make_pair(j->string(),
                                                            orig.string()));
                }
            }
        }

      /* This check makes sure that we are not patching through a
         symlink.  It isn't really required for inexact patching,
         since any symlink directories should have been already
         removed.  But it is needed for exact patching, and it makes
         me sleep better in any case. */
      check_symlink_hierarchy(source_path,source_path/i->destination);

      if(Command_Info::verbosity>=verbose)
        cout << "Creating directories: "
             << (i->destination.branch_path()).native_file_string() << endl;

      fs::create_directories((source_path/i->destination).branch_path());

      /* Move stuff out of the way if necessary. */
      if(!exact_patching)
        {
          if(lexists(source_path/i->destination))
            {
              path orig(move_original_away(source_path,i->destination,
                                           source_inventory,move_list));
              conflicts.move_target
                .push_back(make_pair(i->destination.string(),orig.string()));
            }
        }

      if(Command_Info::verbosity>=verbose)
        cout << "Renaming: " << (deleted_path/i->initial).native_file_string()
             << "\t" << i->destination.native_file_string() << endl;

      /* Need to prune out the deleted files and files that had add
         conflicts so we don't try to move their inventory id */

      if(i->initial.leaf().substr(0,8)!=",,delete"
         && !i->inventory_id.empty())
        {
          move_list.push_back(make_pair(make_pair
                                        (deleted_path.leaf()/i->initial,
                                         i->destination),
                                        i->inventory_id));
        }
      rename(deleted_path/i->initial,source_path/i->destination);
      if(!exact_patching && !is_control(i->destination)
         && !is_control(i->local_path))
        i->inventory_iterator->file_path=i->destination;
    }
  unsafe_move_list(source_path,move_list);
}

/* Wrapper versions for the exact and inexact patching cases. */

void move_to_destination(list<Moved_Path> &moved, const path &deleted_path,
                         const path &source_path,
                         Conflicts &conflicts,
                         Inventory &source_inventory,
                         const list<pair<string,string> > original[2],
                         const list<pair<string,string> > modified[2])
{
  move_to_destination(moved,deleted_path,source_path,conflicts,
                      source_inventory,original,modified,false);
}

void move_to_destination(list<Moved_Path> &moved, const path &deleted_path,
                         const path &source_path)
{
  Conflicts conflicts(source_path);
  Inventory source_inventory;
  list<pair<string,string> > original[2], modified[2];
  move_to_destination(moved,deleted_path,source_path,conflicts,
                      source_inventory,original,modified,true);
}
