/* Move an inventory id from one place to another, marking it in the
   ,,changes file.  It doesn't check that the file is in the original
   manifest, so it is a little unsafe.  It assumes that the file has
   already been moved, because it has to stat it at the new location.

  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "sha256.hpp"
#include "tree_root.hpp"
#include "file_attributes.hpp"
#include "arx_error.hpp"
#include "Temp_File.hpp"
#include "boost/archive/text_oarchive.hpp"
#include <list>
#include <string>
#include <utility>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void unsafe_move_list(const path &root,
                      const list<pair<pair<path,path>,string> > &move_list)
{
  const Temp_File changes_path(root,"_arx/,,changes");
  if(lexists(root/"_arx/++changes"))
    copy_file(root/"_arx/++changes",changes_path.path);
  {
    fs::ofstream changes(changes_path.path,ios::app|ios::out);

    for(list<pair<pair<path,path>,string> >::const_iterator
          i=move_list.begin(); i!=move_list.end(); ++i)
      {
        changes << "\nmove\t";
        file_attributes f;
        f.file_path=i->first.first;
        f.inventory_id=i->second;
        f.output(changes,!symbolic_link_exists(root/i->first.second)
                 && is_directory(root/i->first.second));
        
        changes << " ";
        archive::text_oarchive changes_archive(changes,boost::archive::no_header);
        changes_archive << i->first.second.string();
      }
  }

  if(rename(changes_path.path.native_file_string().c_str(),
            (root/"_arx/++changes").native_file_string().c_str())!=0)
    throw arx_error("Can't rename:\n\t"
                    + changes_path.path.native_file_string()
                    + "\n\t"
                    + (root/"_arx/++changes").native_file_string());
}
