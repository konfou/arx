/* Categorize files and directories from a patch as to whether they
   are renamed, removed, or new

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <string>
#include <list>
#include <utility>
#include "Moved_Path.hpp"
#include "inventory_types.hpp"

using namespace std;

void categorize_files(const list<pair<string,string> > original[2],
                      const list<pair<string,string> > modified[2],
                      list<pair<pair<string,string>,string> > patched[2],
                      list<Moved_Path> &moved,
                      list<Moved_Path> &mod_only,
                      const bool &reverse)
{
  for(int m=0;m<2;++m)
    {
      list<pair<string,string> >::const_iterator i(original[m].begin()),
        j(modified[m].begin());
      while(i!=original[m].end() && j!=modified[m].end())
        {
          if(i->second == j->second)
            {
              if(!reverse)
                {
                  patched[m].push_back(make_pair(*j,j->first));
                }
              else
                {
                  patched[m].push_back(make_pair(*i,j->first));
                }                  
              if(i->first != j->first)
                {
                  moved.push_back(Moved_Path(i->first,j->first,i->second,
                                             m==arx_dir));
                }
              ++i;
              ++j;
            }
          else if(i->second < j->second)
            {
              moved.push_back(Moved_Path(i->first,"",i->second,m==arx_dir));
              ++i;
            }
          else
            {
              mod_only.push_back(Moved_Path("",j->first,j->second,m==arx_dir));
              ++j;
            }
        }
      for(;i!=original[m].end();++i)
        {
          moved.push_back(Moved_Path(i->first,"",i->second,m==arx_dir));
        }
      for(;j!=modified[m].end();++j)
        {
          mod_only.push_back(Moved_Path("",j->first,j->second,m==arx_dir));
        }
    }
}
