/* Find the latest revision that is a continuation in the history of a
   particular revision.

   Copyright 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include "gvfs.hpp"
#include "boost/filesystem/operations.hpp"
#include "Patch_Log.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

/* A version that just looks in the archive. */
      
string compute_latest_continuation(const Parsed_Name &previous)
{
  if(previous.short_revision()=="0")
    {
      return previous.short_revision();
    }
  else
    {
      Patch_Log log(previous);

      if(log.headers.find("Latest-continuation")!=log.headers.end())
        {
          return log.headers["Latest-continuation"];
        }
      else if(log.headers.find("Continuation-of")!=log.headers.end())
        {
          return previous.short_revision();
        }
      else
        {
          Parsed_Name temp(previous);
          --temp;
          return compute_latest_continuation(temp);
        }
    }
}

/* A version that looks in the project tree patch logs first if they
   exist, otherwise it bails to looking at the archive. */

string compute_latest_continuation(const path &root,
                                   const Parsed_Name &previous)
{
  if(previous.short_revision()=="0")
    {
      return previous.short_revision();
    }
  Patch_Log log(root,previous);
  if(!log.empty())
    {
      if(log.headers.find("Latest-continuation")!=log.headers.end())
        {
          return log.headers["Latest-continuation"];
        }
      else if(log.headers.find("Continuation-of")!=log.headers.end())
        {
          return previous.short_revision();
        }
      else
        {
          Parsed_Name temp(previous);
          --temp;
          return compute_latest_continuation(root,temp);
        }
    }
  else
    {
      return compute_latest_continuation(previous);
    }
}
