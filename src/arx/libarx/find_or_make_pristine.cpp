/* Find where a pristine tree is for a given tree.
   
  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                           University of California
  Minor bug fix 2003 Chris Gray
  Copyright (C) 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "get_option_from_file.hpp"
#include "get_config_option.hpp"
#include "tree_root.hpp"
#include "latest_tree_revision.hpp"
#include "valid_package_name.hpp"
#include "Parsed_Name.hpp"
#include <iostream>
#include "get_revision.hpp"
#include "is_a_tag.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

path find_or_make_pristine(const path &tree, const path &temp_path,
                           Parsed_Name P=Parsed_Name())
{
  /* Parse all of the names */

  const path control_root(tree_root(tree)/"_arx");

  if(P.empty())
    {
      P=latest_tree_revision(tree);
    }
  else
    {
      valid_package_name(P,Revision);
    }
      
  /* Check for the existence of the cached revisions */

  path pristine;
  if(lexists(control_root / "++cache" / P.revision_path()))
    {
      pristine=control_root / "++cache" / P.revision_path();
    }
  else
    {
      if(is_a_tag(P))
        throw arx_error("Trying to make a pristine tree of a tag\n\t"
                        + P.complete_revision());
      if(Command_Info::verbosity>=report)
        cout << "Can not find this revision in a local tree cache.\nBuilding it on demand." << endl;
      get_revision(P,temp_path,tree);
      pristine=temp_path;
    }
  return pristine;
}
