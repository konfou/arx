/* Sign a file with gpg using a particular key.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/path.hpp"
#include "Parsed_Name.hpp"
#include "../../config.h"
#include "Spawn.hpp"
#include "get_gpg_name.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void gpg_sign(const boost::filesystem::path &file_name,
              const std::string key)
{
  list<string> gpg=get_gpg_name();
  if(!gpg.empty())
    {
      Spawn gpg_sign;
      for(list<string>::iterator i=gpg.begin(); i!=gpg.end(); ++i)
        gpg_sign << *i;
      gpg_sign << "-b";
      if(!key.empty())
        gpg_sign << "--default-key" << key;
      gpg_sign << file_name.native_file_string();
      
      int exit_status;

      if(!gpg_sign.execute(exit_status,true) || exit_status!=0)
        throw arx_error("Problems when executing gpg to sign");
    }
  else
    {
      throw arx_error("ERROR: You can not sign revisions, because ArX has not been compiled with gpg\nsupport and you did not specify a gpg program with \"arx param\".");
    }
}
