/* Read a list of files and inventory ids from a file.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <list>
#include "arx_error.hpp"
#include "check_if_local_path.hpp"
#include "boost/archive/text_iarchive.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

inline bool second_element_cmp(const pair<string,string> &s1,
                               const pair<string,string> &s2)
{
  return s1.second < s2.second;
}

void read_and_sort_index(const path &file_path,
                         list<pair<string,string> > &file_list)
{
  if(lexists(file_path))
    {
      if(symbolic_link_exists(file_path))
        {
          throw arx_error("Bad patch.  Index file is a symlink, not a regular file.\n\t"
                          + file_path.native_file_string());
        }
      fs::ifstream infile(file_path);
      archive::text_iarchive inarchive(infile);
      try
        {
          while(infile)
            {
              /* I have to declare file_name and inventory_id here,
                 because their is some internal problems with
                 std::string that cause a string already placed in the
                 list to have it's contents changed when I change
                 inventory_id.  Here the string is reborn each time,
                 so it doesn't have this problem with rewriting.  */
              string file_name, inventory_id;
              inarchive >> file_name;
              if(infile)
                {
                  inarchive >> inventory_id;
                  check_if_local_path(file_name);
                  file_list.push_back(make_pair(file_name,inventory_id));
                }
            }
        }
      catch(archive::archive_exception &a)
        {
          throw arx_error("Error when reading an index file\n\t"
                          + file_path.native_file_string()
                          + "\n\t"
                          + a.what());
        }
    }
  file_list.sort(second_element_cmp);
}
