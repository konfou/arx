/* Tar's up and stores in the archive a patch.  The patch must be in
   the subdirectory temp_dir/revision.patches, the log is in
   temp_dir/log, and a continuation file is in temp_dir/CONTINUATION.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Current_Path.hpp"
#include "../../config.h"
#include "invoke_hook.hpp"
#include "Branch_Lock.hpp"
#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "Parsed_Name.hpp"
#include <string>
#include "Spawn.hpp"
#include "gpg_sign.hpp"
#include "signed_archive.hpp"
#include "ensure_key_in_archive.hpp"
#include "update_listing.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void tar_and_archive_patch(const path &temp_dir, const Parsed_Name &previous,
                           const Parsed_Name &name,
                           const bool &continuation, const path &root,
                           const string &gpg_key,
                           const bool log_only=false)
{
  valid_package_name(name,Revision);

  /* Tar up the patch */
  if(!log_only)
    {
      Current_Path current(temp_dir);
      
      Spawn s;
      s << ARXTAR << "-czf" << (name.revision() + ".patches.tar.gz")
        << (name.revision() + ".patches");
      
      if(!s.execute(true))
        {
          throw arx_error("Problems when tar'ing\n\t"
                          + (temp_dir / (name.revision() + ".patches"))
                          .native_file_string());
        }
    }
  
  /* run the precommit hook */
  invoke_hook(previous,name,true,system_complete(root));

  if(signed_archive(name))
    {
      ensure_key_in_archive(name,gpg_key);
      if(!log_only)
        gpg_sign(temp_dir / (name.revision() + ".patches.tar.gz"), gpg_key);
      gpg_sign(temp_dir / "sha256", gpg_key);
    }
  
  /* lock the branch */
  Branch_Lock lock(name);
  
  /* send the patch over */
  if(!log_only)
    gvfs::copy((temp_dir/(name.revision() + ".patches.tar.gz"))
               .native_file_string(),
               lock.uri()/(name.revision() + ".patches.tar.gz"));
  
  /* send the log over */
  gvfs::copy((temp_dir/"log").native_file_string(),lock.uri()/"log");
  
  /* send the hash over */
  gvfs::copy((temp_dir/"sha256").native_file_string(),lock.uri()/"sha256");
  
  /* Copy over the signatures. */
  if(signed_archive(name))
    {
      if(!log_only)
        gvfs::copy((temp_dir/(name.revision() + ".patches.tar.gz.sig"))
                   .native_file_string(),
                   lock.uri()/(name.revision() + ".patches.tar.gz.sig"));
      gvfs::copy((temp_dir/"sha256.sig").native_file_string(),
                 lock.uri()/"sha256.sig");
    }
  
  /* Copy over a CONTINUATION file if needed. */
  if(continuation)
    {
      gvfs::copy((temp_dir/"CONTINUATION").native_file_string(),
                 lock.uri()/"CONTINUATION");
    }
  /* Finally, rename the lock dir to the destination dir */
  gvfs::rename(lock.uri(),name.archive_location()
               /name.revision_path_no_archive());

  /* The revision is archived, now run the postcommit hook. */
  
  invoke_hook(previous,name,false,system_complete(root));

  if(gvfs::exists(name.archive_location()/",meta-info/update-listing"))
    update_listing(name);
}

