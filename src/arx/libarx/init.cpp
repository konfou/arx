/* Initialize the current tree

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry
                           and the Regents of the University of California
  Copyright (C) 2004, 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include "arx_error.hpp"
#include <list>
#include "tree_branch.hpp"
#include "read_checksums.hpp"
#include "add_path_to_list.hpp"
#include "add_path_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void init(const Parsed_Name &input_branch, const path &tree_directory,
          const bool &add_paths)
{
  valid_package_name(input_branch,Branch);
  if(!input_branch.revision().empty())
    throw arx_error("You may not specify a revision\n\t"
                    + input_branch.full_name());

  /* Write the ++import file */

  if(lexists(tree_directory/"_arx"))
    throw arx_error("This tree has already been initialized.  To start over, remove\nthe _arx directory.");
  path control_root(tree_directory/"_arx");
  create_directory(control_root);
  
  tree_branch(tree_directory,input_branch);
  
  set_option_in_file(control_root/"++import",
                     input_branch.complete_branch());

  Checksums checksums;
  read_checksums(tree_directory,checksums);
      
  list<pair<file_attributes,sha256> > add_list;
  if(add_paths)
    {
      for(fs::directory_iterator i(tree_directory);
          i!=fs::directory_iterator(); ++i)
        if(!is_control(i->leaf()))
          {
            add_path_to_list(add_list,tree_directory,*i,checksums,true);
          }
    }
  add_path_list(tree_directory,add_list);
}
