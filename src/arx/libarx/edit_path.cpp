/* Make a path editable.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/archive/text_oarchive.hpp"
#include <string>
#include "Temp_File.hpp"
#include <sys/stat.h>
#include "arx_error.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void edit_path(const path &root, const string &filename)
{
  /* Do all of the rigamarole associated with the ++edit file */
  Temp_File temp_edit(root,",,edit");
  copy_file(root/"_arx/++edit",temp_edit.path);
  {
    fs::ofstream edit_file(temp_edit.path,ios::app|ios::out);
    archive::text_oarchive edit_archive(edit_file,archive::no_header);
    edit_archive << filename;
  }
  if(rename(temp_edit.path.native_file_string().c_str(),
            (root/"_arx/++edit").native_file_string().c_str())!=0)
    throw arx_error("Can't rename " + temp_edit.path.native_file_string()
                    + " to "
                    + (root/"_arx/++edit").native_file_string());

  /* If it is not a symbolic link, then we can change permissions. */
  if(!symbolic_link_exists(root/filename))
    {
      /* Copy the file to break the link */
      copy_file(root/filename,temp_edit.path);
      if(rename(temp_edit.path.native_file_string().c_str(),
                (root/filename).native_file_string().c_str()))
        throw arx_error("Can't rename " + temp_edit.path.native_file_string()
                        + " to "
                        + (root/filename).native_file_string());

      /* chmod the result */
      struct stat path_stat;
      if(::stat((root/filename).native_file_string().c_str(),&path_stat))
        throw arx_error("Can't stat " + (root/filename).native_file_string());
      
      path_stat.st_mode|=S_IWUSR;
      if(::chmod((root/filename).native_file_string().c_str(),
                 path_stat.st_mode))
        throw arx_error("Can't chmod " + (root/filename).native_file_string());
    }
}
