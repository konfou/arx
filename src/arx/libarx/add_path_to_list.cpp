/* Add a path to a list of files to be added to the manifest.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "tree_root.hpp"
#include "sha256.hpp"
#include "file_attributes.hpp"
#include "Checksums.hpp"
#include "add_path_and_checksum.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using namespace boost::posix_time;
using fs::path;

void add_path_to_list(list<pair<file_attributes,sha256> > &add_list,
                      const path &dir, const path &file, Checksums &checksums,
                      bool recursive=false)
{
  path root(tree_root(dir));
  const string relative_filename(relative_path(file,root).string());
  bool dont_add(false);
  
  for(Checksums::iterator i=checksums.begin(); i!=checksums.end(); ++i)
    {
      if(i->first.file_path.string()==relative_filename)
        {
          dont_add=true;
          if(Command_Info::verbosity>=default_output)
            cerr << file.native_file_string() << " already added\n";
        }
    }
  
  /* I would like to use a microsecond clock here, but it
     doesn't seem to be supported in Windows */
  
  stringstream s;
  s << relative_filename << " "
    << to_iso_extended_string(ptime
#ifdef BOOST_DATE_TIME_HAS_GETTIMEOFDAY_HIGH_PRECISION_CLOCK
                              (microsec_clock::local_time()));
#else
                              (second_clock::universal_time()));
#endif
  
  sha256 checksum(s.str());
  
  file_attributes f(file);
  f.file_path=path(relative_filename);
  f.inventory_id=checksum.string();
  /* Only directories don't have checksums */
  if(tree_root(file.branch_path()).string()==root.string())
    {
      if(!fs::symbolic_link_exists(file) && fs::is_directory(file))
        {
          if(!exists(file/"_arx"))
            {
              if(!dont_add)
                add_list.push_back(make_pair(f,sha256()));
              if(recursive)
                for(fs::directory_iterator i(file);
                    i!=fs::directory_iterator(); ++i)
                  add_path_to_list(add_list,root,*i,checksums,recursive);
            }
          else if(!dont_add && Command_Info::verbosity>=default_output)
            {
              cerr << "Warning: Not adding this path because it is already a project tree.\n\t"
                   << file.native_file_string()
                   << endl;
            }
        }
      else if(!dont_add)
        {
          add_list.push_back(make_pair(f,checksum));
        }
    }
  else if(!dont_add)
    {
      throw arx_error("Can not add this file\n\t"
                      + file.native_file_string()
                      + "\nIt is already in the project tree at\n\t"
                      + tree_root(file.branch_path()).native_file_string());
    }
    
}
