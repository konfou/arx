/* Get a particular revision, starting with a full-tree revision
   cached somewhere (in a project tree or archive) and then patching.
   If hard_link is true, then it attempts to hard link the result.
   It returns the full name and hash of the revision that was fetched.

   Copyright 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <list>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include "arx_error.hpp"
#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "patch_level.hpp"
#include <functional>
#include "Temp_Directory.hpp"
#include "latest_archive_revision.hpp"
#include "tree_branch.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Inventory.hpp"
#include "Inventory_Flags.hpp"
#include "read_checksums.hpp"
#include "inventory_directory.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "make_non_writeable.hpp"
#include "Temp_Directory.hpp"
#include "get_config_option.hpp"
#include "is_a_tag.hpp"
#include "Patch_Log.hpp"
#include "verify_tag.hpp"
#include "check_symlink_hierarchy.hpp"
#include "check_if_local_path.hpp"
#include "read_file_into_string.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern void compute_revision_path(Parsed_Name &beginning_revision,
                                  list<pair<Parsed_Name,Parsed_Name> >
                                  &revision_path,
                                  const Parsed_Name &target,
                                  const path &cache_dir,
                                  const path &temp_destination,
                                  const bool &hard_link);

extern sha256 build_revision_by_patching
(const list<pair<Parsed_Name,Parsed_Name> > &revision_path,
 const Parsed_Name &beginning,
 const path &temp_destination,
 const path &temp_dir,
 const Parsed_Name &name);

/* A generic version that just gets a revision, perhaps via hard links.
   It does not use its own temp directories, instead assuming
   that it is passed a temp directory. */

pair<Parsed_Name,sha256> get_revision
(const Parsed_Name &target,
 const path &target_dir, const path &cache_dir,
 const bool hard_link=true)
{
  list<pair<Parsed_Name,Parsed_Name> > revision_path;
  
  Parsed_Name target_rev(latest_archive_revision(target));
  pair<Parsed_Name,sha256> result;
  result.first=target_rev;

  /* Handle tags, otherwise actually get the revision */

  if(is_a_tag(target_rev))
    {
      /* Download the log, hash, and signature and make sure it is valid */

      string log_string(verify_tag(target_rev));
      result.second=sha256(log_string);

      Patch_Log log(log_string);

      if(log.rename_lists.find("Checksums")!=log.rename_lists.end())
        {
          if(log.rename_lists["Tags"].size()
             !=log.rename_lists["Checksums"].size())
            throw arx_error("Corrupted log?  The list of tags and their checksums do not match up.\n\t" + target_rev.complete_revision());
          for(list<pair<string,string> >::iterator
                i=log.rename_lists["Tags"].begin(),
                j=log.rename_lists["Checksums"].begin();
              i!=log.rename_lists["Tags"].end(),
                j!=log.rename_lists["Checksums"].end();
              ++i, ++j)
            {
              if(i->first!=j->first)
                throw arx_error("Corrupted log?  The list of tags and their checksums do not match up.\n\t" + target_rev.complete_revision());
              
              /* We check here to make sure that we are not doing things
                 through symlinks or that the paths try to go somewhere
                 they shouldn't. */
              check_symlink_hierarchy(target_dir,target_dir/i->second);
              check_if_local_path(i->second);
              
              
              
              if(j->second!=get_revision(i->first,target_dir/i->second,
                                         cache_dir,hard_link).second.string())
                throw arx_error("INTERNAL ERROR: The checksum of the local tree and the checksum in the tag do not match.\n\trevision: "
                                + i->first
                                + "\n\ttag checksum:  "
                                + j->second
                                + "\n\ttree checksum: "
                                + result.second.string());
            }
        }
      else
        {
          /* If there are no checksums for the revisions */
          for(list<pair<string,string> >::iterator
                i=log.rename_lists["Tags"].begin();
              i!=log.rename_lists["Tags"].end(); ++i)
            {
              /* We check here to make sure that we are not doing things
                 through symlinks or that the paths try to go somewhere
                 they shouldn't. */
              check_symlink_hierarchy(target_dir,target_dir/i->second);
              check_if_local_path(i->second);
              get_revision(i->first,target_dir/i->second,cache_dir,hard_link);
            }
        }
    }
  else
    {
      revision_path.push_back(make_pair("",target_rev.complete_revision()));
      
      /* The default place to put the revision is in a directory named
         after the revision. */
      if(target_dir.empty())
        throw arx_error("INTERNAL ERROR: Empty target dir given to get_revision");
      
      Parsed_Name beginning_revision;
      
      if(lexists(target_dir))
        throw arx_error("Target directory for get already exists:\n\t"
                        + target_dir.native_file_string());
      
      compute_revision_path(beginning_revision,revision_path,
                            target_rev,cache_dir,target_dir,hard_link);
      Temp_Directory temp_dir(target_dir.branch_path(),",,get_rev");
      result.second=
        build_revision_by_patching(revision_path,beginning_revision,
                                   target_dir,temp_dir.path,target_rev);
      tree_branch(target_dir,target_rev);
    }
  return result;
}

/* A more specific version that handles pristine and no-edit options.
   This uses its own temp directory. */

pair<Parsed_Name,sha256> get_revision
(const Parsed_Name &target,
 const path &target_dir, const path &cache_dir,
 const bool &no_edit, const bool &pristine,
 const bool &link_tree)
{
  Parsed_Name target_rev(latest_archive_revision(target));

  path destination(target_dir);
  if(destination.empty())
    destination=target_rev.branch() + "." + target_rev.short_revision();
  if(lexists(destination))
    throw arx_error("Target directory already exists: "
                    + destination.native_file_string());

  Temp_Directory temp_dir(destination.branch_path(),",,get_revision");

  pair<Parsed_Name,sha256> result=
    get_revision(target,temp_dir.path/"rev",cache_dir,
                 (no_edit && link_tree) || pristine);

  if(pristine)
    {
      copy(temp_dir.path/"rev",temp_dir.path/"main",true,link_tree && no_edit);
      create_directories(temp_dir.path/"main/_arx/++cache"
                         /target_rev.branch_path());
      rename(temp_dir.path/"rev",temp_dir.path/"main/_arx/++cache"
             /target_rev.revision_path());
      rename(temp_dir.path/"main",temp_dir.path/"rev");
    }

  /* If the --no-edit flag is given, then change all of the
     permissions for all of the files but not directories.  We don't
     do that to directories because that would interfere with things
     like add, rm, and mv. */
  if(no_edit)
    {
      Checksums checksums;
      read_checksums(temp_dir.path/"rev",checksums);

      Inventory inventory_result;
      Inventory_Flags flags;
      flags.source=flags.files=flags.directories=true;
      inventory_directory(temp_dir.path/"rev",inventory_result,flags,
                          checksums);

      for(Inventory::iterator i=inventory_result(source,arx_file).begin();
          i!=inventory_result(source,arx_file).end(); ++i)
        make_non_writeable(i->file_path);
      /* Create an empty ++edit file */
      fs::ofstream edit_file(temp_dir.path/"rev/_arx/++edit");
      {
        archive::text_oarchive edit_archive(edit_file);
      }
      edit_file << " ";
    }
  rename(temp_dir.path/"rev",destination);
  return result;
}
