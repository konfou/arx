/* Build a revision by patching a full source tree using the path of
   revisions given in revision_path.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <list>
#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "Parsed_Name.hpp"
#include "patch_number.hpp"
#include "patch_level.hpp"
#include "get_revision_patch.hpp"
#include "do_patch.hpp"
#include <iostream>
#include "Command_Info.hpp"
#include "Temp_Directory.hpp"
#include "reconstitute_manifest.hpp"
#include "verify_manifest_checksum.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

sha256 build_revision_by_patching(const list<pair<Parsed_Name,Parsed_Name> >
                                  &revision_path,
                                  const Parsed_Name &beginning,
                                  const path &temp_destination,
                                  const path &temp_dir,
                                  const Parsed_Name &name)
{
  Parsed_Name start(beginning);
  
  /* Now we have a list of continuation revisions terminated by an
     archived revision.  So we now have to get the patches and
     archived revision. */

  for(list<pair<Parsed_Name,Parsed_Name> >::const_reverse_iterator
        i=revision_path.rbegin(); i!=revision_path.rend(); ++i)
    {
      /* Get all of the patches between target_revision and the
         current_revision. */

      /* If we're just starting to apply patches from the cached
         revision, we might have to apply some patches in reverse. */
      bool reverse=false;
      if(i->second<start)
        reverse=true;

      const int sign=(reverse ? -1 : 1);
      for(unsigned int j=start.patch_number() + (reverse ? 0 : 1);
          j!=i->second.patch_number() + (reverse ? 0 : 1);
          j+=sign)
        {
          if(Command_Info::verbosity>=report)
            cout << "applying patch "
                 << (start.complete_branch() + patch_level(j))
                 << endl;

          Temp_Directory patch_dir(temp_dir,",,patch",false);
          get_revision_patch(Parsed_Name(start).set_revision(j),
                             patch_dir.path,false);
          do_patch(patch_dir.path,temp_destination,reverse);
        }
      /* If we're not done, then we have to do the continuation patch.
         This is never reversed. */

      if(!i->first.empty())
        {
          if(Command_Info::verbosity>=report)
            cout << "applying continuation patch "
                 << i->first.full_name()
                 << endl;
          
          Temp_Directory patch_dir(temp_dir,",,patch",false);
          get_revision_patch(i->first,patch_dir.path,false);
          do_patch(patch_dir.path,temp_destination,false);
          start=i->first;
        }
    }
  reconstitute_manifest(temp_destination,name);
  return verify_manifest_checksum(temp_destination,name);
}

