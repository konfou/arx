/* Get the closest cached revision, putting it in destination, and its
   name in beginning_revision.  It destroys the Revision_List's.

   Copyright 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include <list>
#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "Revision_List.hpp"
#include "patch_level.hpp"
#include "get_archived_revision.hpp"
#include "get_config_option.hpp"
#include <iostream>
#include "Command_Info.hpp"
#include "get_local_revision.hpp"
#include "find_closest_revision.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void get_closest_full_revision(Revision_List &local_revisions,
                               Revision_List &archive_revisions,
                               const path &destination,
                               const Parsed_Name &current,
                               Parsed_Name &beginning_revision,
                               const path &cache_dir,
                               const bool &hard_link)
{
  bool local_archive(false);
  string location(current.archive_location().string());
  if(location[0]=='/' || location.substr(0,5)=="file:")
    local_archive=true;

  unsigned int closest_archive(0), closest_local(0);
  bool archived(!archive_revisions.empty()), local(!local_revisions.empty());
  if(archived)
    closest_archive=find_closest_revision(archive_revisions,
                                          current.patch_number());
  if(local)
    closest_local=find_closest_revision(local_revisions,
                                        current.patch_number());
  
  Revision_List all_revisions;
  all_revisions.merge(local_revisions);
  if(local_archive)
    all_revisions.merge(archive_revisions);

  if(!all_revisions.empty())
    {
      const unsigned int closest(find_closest_revision
                                 (all_revisions,current.patch_number()));
      beginning_revision=Parsed_Name(current).set_revision(closest);

      if(local && closest==closest_local)
        {
          if(Command_Info::verbosity>=report)
            cout << "Copying from the local cache "
                 << beginning_revision.full_name() << endl;
          get_local_revision(beginning_revision,destination,cache_dir,
                             hard_link);
        }
      else if(local_archive && archived && closest==closest_archive)
        {
          if(Command_Info::verbosity>=report)
            cout << "Getting a cached revision from the archive "
                 << beginning_revision.full_name() << endl;
          get_archived_revision(beginning_revision,destination);
        }
    }
  else
    {
      beginning_revision=Parsed_Name(current).set_revision(closest_archive);
      if(Command_Info::verbosity>=report)
        cout << "Getting a cached revision from the archive "
             << beginning_revision.full_name() << endl;
      get_archived_revision(beginning_revision,destination);
    }
}
