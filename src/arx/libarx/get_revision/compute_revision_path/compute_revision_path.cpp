/* Compute the path of revisions needed to get the target revision.
   It also gets the cached revision needed to start from for patching.

   Copyright 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include <list>
#include <map>
#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "list_archive_cached_revisions.hpp"
#include "list_local_cached_revisions.hpp"
#include "patch_number.hpp"
#include "patch_level.hpp"
#include "patch_level_cmp.hpp"
#include <functional>
#include "get_config_option.hpp"
#include "valid_package_name.hpp"
#include "compute_latest_continuation.hpp"
#include "gvfs.hpp"
#include "Patch_Log.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;


/* Remove revisions that have a continuation between itself and the
   current revision. */
void remove_continued_revisions(unsigned int &min_revision,
                                Revision_List &rev_list,
                                const Parsed_Name &current)
{
  /* Remove revisions cut out from below */
  rev_list.remove_if(bind2nd(less<unsigned int>(),min_revision));

  /* Remove revisions cut out from above.  We only save the closest
     one. */
  for(Revision_List::iterator j=rev_list.begin(); j!=rev_list.end(); ++j)
    {
      if(*j>current.patch_number())
        {
          if(static_cast<unsigned int>
             (atoi(compute_latest_continuation
                   (Parsed_Name(current).set_revision(*j)).c_str()))
             <=current.patch_number())
            ++j;
          rev_list.erase(j,rev_list.end());
          break;
        }
    }
}


extern void get_closest_full_revision(Revision_List &local_revisions,
                                      Revision_List &archive_revisions,
                                      const path &destination,
                                      const Parsed_Name &current,
                                      Parsed_Name &beginning_revision,
                                      const path &cache_dir,
                                      const bool &hard_link);

void compute_revision_path(Parsed_Name &beginning_revision,
                           list<pair<Parsed_Name,Parsed_Name> > &revision_path,
                           const Parsed_Name &target,
                           const path &cache_dir,
                           const path &temp_destination,
                           const bool &hard_link)
{
  Parsed_Name current(target);
  while(beginning_revision.empty())
    {
      /* Get all of the cached revisions.  Silently ignore archives
         that can not be reached. */
      Revision_List archived_revisions;
      try
        {
          archived_revisions=list_archive_cached_revisions
            (Parsed_Name(current).set_revision());
        }
      catch (const std::runtime_error &ex)
        {}
      Revision_List local_revisions(list_local_cached_revisions
                                    (current.complete_branch(),
                                     cache_dir));
      
      /* Now find the continuation revisions so that we can cull
         revisions from the lists of cached revisions */

      pair<Parsed_Name,Parsed_Name> continuation;

      /* If we don't have the exact revision in the cache or local archives,
         then we need to find the continuation revisions. */
            
      bool local_archive(false);
      string location(current.archive_location().string());
      if(location[0]=='/' || location.substr(0,5)=="file:")
        local_archive=true;

      if(find(local_revisions.begin(),local_revisions.end(),
                 current.patch_number())==local_revisions.end()
         && (!local_archive
             || find(archived_revisions.begin(),archived_revisions.end(),
                     current.patch_number())==archived_revisions.end()))
        {
          /* We need to cut out all of the cached revisions that have
             a continuation revision between itself and the target
             revision. */

          continuation.first=Parsed_Name(current)
            .set_revision(atoi(compute_latest_continuation(current).c_str()));
            
          unsigned int min_revision(atoi(continuation.first.
                                         short_revision().c_str()));
          remove_continued_revisions(min_revision,archived_revisions,current);
          remove_continued_revisions(min_revision,local_revisions,current);
        }
      
      /* If there are any revisions left, figure out which revision is
         best.  We look at local revisions first. */

      if(!(local_revisions.empty() && archived_revisions.empty()))
        {
          /* This destroys the revision lists. */
          get_closest_full_revision(local_revisions,
                                    archived_revisions,temp_destination,
                                    current,beginning_revision,cache_dir,
                                    hard_link);
        }
      /* compute_latest_continuation returns revision 0 if there are
         no other revisions, even if it is not a continuation
         revision.  However, if it is not a continuation revision,
         then it must have an archive cached revision, so we should
         never get to this point. */
      else
        {
          /* We need to follow another continuation. */
          string log_string;
          gvfs::Init();
          gvfs::uri location(current.archive_location()
                             /continuation.first.revision_path_no_archive()
                             / "log");
          log_string=gvfs::read_file_into_string(location);

          Patch_Log log(log_string);
          log.compress_lists();
          if(log.headers.find("Continuation-of")==log.headers.end())
            {
              /* We can't find anything. */
              string s("INTERNAL ERROR: Can not find any cached revisions or further continuations.\nThe list of revisions that were searched are:\n");
              for(list<pair<Parsed_Name,Parsed_Name> >::iterator
                    i=revision_path.begin();
                  i!=revision_path.end(); ++i)
                {
                  s+="\t"+ i->first.complete_revision() + " -> "
                    + i->second.complete_revision() + "\n";
                }
              throw arx_error(s);
            }
          continuation.second=log.headers["Continuation-of"];
          revision_path.push_back(continuation);
          if(current==continuation.second)
            {
              if(log.headers["Summary"]=="Deleted revision")
                throw arx_error("You may not get this revision, because it has been deleted.\n\t" + current.complete_name());
              else
                throw arx_error("INTERNAL ERROR: This revision continues from itself.\nYou may have corrupted your archive.\n\t" + current.complete_name());
            }
          current=continuation.second;
        }
    }
}
