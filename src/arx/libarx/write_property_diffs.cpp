/* Compare the properties of original and modified versions of a path,
   and write the original and modified properties in the supplied
   paths.
   
  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/archive/text_oarchive.hpp"
#include <map>
#include <string>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void write_property_diffs(const path &orig_filename,
                          const path &mod_filename,
                          const map<string,string> &orig,
                          const map<string,string> &mod,
                          const path &orig_path,
                          const path &mod_path)
{
  /* If the properties are the same, don't create the files. */
  if(orig==mod)
    return;

  fs::ofstream orig_file(orig_path), mod_file(mod_path);

  map<string,string>::const_iterator i(orig.begin()), j(mod.begin());

  archive::text_oarchive orig_archive(orig_file), mod_archive(mod_file);

  while(i!=orig.end() && j!=mod.end())
    {
      if(i->first==j->first)
        {
          if(i->second!=j->second)
            {
              orig_archive << string("set") << i->first << i->second;
              mod_archive << string("set") << j->first << j->second;
            }
          ++i;
          ++j;
        }
      else if(i->first>j->first)
        {
          orig_archive << string("unset") << j->first << j->second;
          mod_archive << string("set") << j->first << j->second;
          ++j;
        }
      else
        {
          orig_archive << string("set") << i->first << i->second;
          mod_archive << string("unset") << i->first << i->second;
          ++i;
        }
    }      
  while(i!=orig.end())
    {
      orig_archive << string("set") << i->first << i->second;
      mod_archive << string("unset") << i->first << i->second;
      ++i;
    }
  while(j!=mod.end())
    {
      orig_archive << string("unset") << j->first << j->second;
      mod_archive << string("set") << j->first << j->second;
      ++j;
    }
}
