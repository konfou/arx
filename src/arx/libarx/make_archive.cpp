/* Create and register an archive

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "create_archive_registration.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "Component_Regexes.hpp"
#include "add_key_to_archive.hpp"
#include "signed_archive.hpp"
#include "registered_archive.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void make_archive(const string &name, const gvfs::uri &location,
                  const bool &mirror, const string gpg_key)
{
  const string ArX_archive_version("ArX archive, format version 1");

  if(!regex_match(name,Component_Regexes().archive))
    throw arx_error("Invalid archive name: " + name);

  if(!mirror && registered_archive(name+"/"))
    throw arx_error("You already have an archive registered with the same name.  You must either\nchoose a different name, or use the --mirror option.\n\t"
                    + name);

  gvfs::Init();
  
  /* Create directories if needed */
  gvfs::uri dir_path(location);
  
  if(!gvfs::exists(dir_path))
    {
      gvfs::make_directory(dir_path);
    }
  else if(!gvfs::is_directory(dir_path))
    {
      throw arx_error("The argument: " + location.string()
                      + "\n is not a directory");
    }
  
  gvfs::uri meta_path(dir_path / ",meta-info");
  if(!exists(meta_path))
    {
      gvfs::make_directory(meta_path);
    }
  else if(!is_directory(meta_path))
    {
      throw arx_error("Corrupt archive\n" + meta_path.string()
                      + "\n is not a directory");
    }
  
  /* Enter the characteristics of the archive. */
  
  gvfs::write_archive(meta_path / "name",name);
  gvfs::write_archive(dir_path / ",archive_version",ArX_archive_version);
  if(mirror)
    gvfs::write_archive(meta_path / "mirror",name);

  /* Copy over public keys if this is a mirror and the master archive
     is signed. */

  if(mirror)
    {
      if(signed_archive(name + "/"))
        gvfs::copy(key_location(name + "/").native_file_string(),
                   meta_path / "public_keys");
    }
  /* Otherwise, add the gpg_key if necessary. */
  else if(!gpg_key.empty())
    {
      add_key_to_archive(dir_path.string() + "/",gpg_key);
      gvfs::copy(meta_path / "public_keys",
                 key_location(name + "/").native_file_string());
      
    }
  
  /* Register the new archive. */
  create_archive_registration(name + "/",dir_path.string());
}
