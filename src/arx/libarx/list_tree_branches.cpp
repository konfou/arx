/* List all of the branches in a project tree.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <list>
#include "Parsed_Name.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "tree_root.hpp"
#include "Component_Regexes.hpp"
#include "recursive_browse.hpp"
#include "push_branch_on_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

list<Parsed_Name> list_tree_branches(const path &tree_directory,
                                     Parsed_Name limit=Parsed_Name())
{
  list<Parsed_Name> result;

  /* Find where the logs are. */

  path patch_dir(tree_root(tree_directory)/"_arx/patch-log");
  if(!exists(patch_dir))
    return result;

  list<path> archive_dir
    ((fs::directory_iterator(patch_dir)),fs::directory_iterator());
  
  for(list<path>::iterator l=archive_dir.begin();
      l!=archive_dir.end(); ++l)
    {
      if(limit.archive().empty() || limit.archive()==l->leaf())
        {
          push_branch_on_list push(result,limit);
              recursive_browse(*l,Parsed_Name(l->leaf() + "/"),push);
        }
    }
  result.sort(Parsed_Name::Branch_less);
  return result;
}
 
