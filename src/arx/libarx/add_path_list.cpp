/* Add a list of files and checksums to the manifest.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include <cstdio>
#include "tree_root.hpp"
#include "sha256.hpp"
#include "file_attributes.hpp"
#include "Temp_File.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void add_path_list(const path &dir,
                   const list<pair<file_attributes,sha256> > &add_list)
{
  const path root(tree_root(dir));

  Temp_File temp_changes(root,",,changes");
  
  if(lexists(root/"_arx/++changes"))
    copy_file(root/"_arx/++changes",temp_changes.path);
  {
    fs::ofstream changes(temp_changes.path,ios::app|ios::out);
  
    for(list<pair<file_attributes,sha256> >::const_iterator i=add_list.begin();
        i!=add_list.end(); ++i)
      {
        changes << "\nadd\t";
        i->first.output(changes,i->second);
      }
  }
  if(rename(temp_changes.path.native_file_string().c_str(),
            (root/"_arx/++changes").native_file_string().c_str())!=0)
    throw arx_error("Can't rename " + temp_changes.path.native_file_string()
                    + " to "
                    + (root/"_arx/++changes").native_file_string());
}
