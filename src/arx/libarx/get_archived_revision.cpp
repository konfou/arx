/* Get a particular full-tree revision from an archive.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Current_Path.hpp"
#include <string>
#include <list>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "arx_error.hpp"
#include "../../config.h"
#include "gvfs.hpp"
#include "Parsed_Name.hpp"
#include "patch_number.hpp"
#include "Temp_Directory.hpp"
#include "do_patch.hpp"
#include "Spawn.hpp"
#include "valid_package_name.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void get_archived_revision(const Parsed_Name &name, const path &target_dir)
{
  valid_package_name(name,Revision);

  gvfs::Init();
  
  gvfs::uri location(name.archive_location()
                     / name.branch_path_no_archive()
                     / ",cache" / (name.revision() + ".tar.gz"));
  Temp_Directory get_revision_dir(system_complete(target_dir).branch_path(),
                                  ",,get_archived_revision");
  path get_revision_file=get_revision_dir.path/(name.revision()
                                                + ".tar.gz");
  
  if(name.patch_number()==0 && !gvfs::exists(location))
    {
      location=
        gvfs::uri(name.archive_location()
                  / name.revision_path_no_archive()
                  / (name.revision() + ".src.tar.gz"));
    }
  
  /* If we are patching from the beginning, then get the -0
     patch and apply it to get -0. */
  if(name.patch_number()==0 && !gvfs::exists(location))
    {
      if(!gvfs::exists(name.archive_location()
                       / name.revision_path_no_archive()
                       / "CONTINUATION"))
        {
          path get_revision_patch=get_revision_dir.path
            /(name.revision() + ".patches.tar.gz");
          gvfs::copy(name.archive_location()
                     / name.revision_path_no_archive()
                     / (name.revision() + ".patches.tar.gz"),
                     system_complete(get_revision_patch)
                     .native_file_string());
          {
            Current_Path current(get_revision_dir.path);
            
            Spawn s;
            s << ARXTAR << "-zxf" << get_revision_patch.leaf();
            if(!s.execute(true))
              throw arx_error("Error when upacking a patch file\n\t"
                              + get_revision_patch.native_file_string());
            fs::create_directory(name.revision());
            do_patch(name.revision()+".patches",name.revision(),false);
          }
        }
      else
        {
          throw arx_error("INTERNAL ERROR: Trying to get an archived revision which doesn't exist in\nthe archive.  It is a continuation revision, not a patch from the void.\n\t" + name.complete_revision());
        }
    }
  /* We are just unpacking a tar ball. */
  else
    {
      gvfs::copy(location,system_complete(get_revision_file)
                 .native_file_string());
      {
        Current_Path current(get_revision_dir.path);
        
        Spawn s;
        s << ARXTAR << "-zxf" << get_revision_file.leaf();
        if(!s.execute(true))
          throw arx_error("Error when upacking a cached revision tar file\n\t"
                          + get_revision_file.native_file_string());
      }
    }
  fs::rename(get_revision_dir.path/name.revision(),target_dir);
}
      
