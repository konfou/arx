/* Read the ++manifest file, making sure that it's integrity is intact

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Checksums.hpp"
#include "sha256.hpp"
#include "tree_root.hpp"
#include "arx_error.hpp"
#include "manifest_version.hpp"
#include "boost/archive/text_iarchive.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void read_manifest(const path &tree_directory, Checksums &checksums)
{
  const path root(tree_root(tree_directory));
  if(lexists(root/"_arx/++manifest"))
    {
      /* Make sure the manifest matches the checksum */
      sha256 manifest_checksum(root/"_arx/++manifest");
      fs::ifstream checksum_file(root/"_arx/++sha256");
      sha256 master_checksum;
      checksum_file >> master_checksum;
      if(master_checksum!=manifest_checksum)
        throw arx_error("INTERNAL ERROR: Checksum mismatch - Either _arx/++manifest or\n_arx/++sha256 is corrupted.");

      /* Read in header information.  First make sure that we are
         reading a compatible manifest format. */
      fs::ifstream manifest(root/"_arx/++manifest");      
      {
        archive::text_iarchive manifest_archive(manifest);
        string temp;
        manifest_archive >> temp;
        if(manifest_version!=temp)
          throw arx_error("Version mismatch for the manifest.  Your version of ArX is either too old\nor too new to read this manifest.\n\tManifest version: "
                          + temp
                          + "\n\tArX version: " + manifest_version);
        /* Get the revision name. */
        manifest_archive >> temp;
        checksums.name=Parsed_Name(temp);
      }
      /* Get all of the elements. */
      while(manifest)
        {
          file_attributes f;
          sha256 checksum;
          f.input(manifest,checksum);
          if(manifest)
            checksums.push_back(make_pair(f,checksum));
        }
    }
}
