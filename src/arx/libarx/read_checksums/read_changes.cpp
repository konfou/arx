/* Read the ++changes file

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "Checksums.hpp"
#include "sha256.hpp"
#include "tree_root.hpp"
#include "arx_error.hpp"
#include "remove_path_from_checksums.hpp"
#include "boost/archive/text_iarchive.hpp"
#include <utility>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void read_changes(const path &tree_directory, Checksums &checksums)
{
  const path arx_root(tree_root(tree_directory)/"_arx");
  if(lexists(arx_root/"++changes"))
    {
      fs::ifstream changes(arx_root/"++changes");
      while(changes)
        {
          string action;
          changes >> action;
          if(changes)
            {
              /* Get the tab character following the action */
              changes.get();

              if(action=="set" || action=="unset")
                {
                  string filename, key, value;
                  archive::text_iarchive
                    changes_archive(changes,archive::no_header);
                  changes_archive >> filename >> key >> value;
                  Checksums::iterator
                    i=find_if(checksums.begin(),checksums.end(),
                              bind2nd(std::ptr_fun(checksums_file_path_eq_string),filename));
                  if(i==checksums.end())
                    throw arx_error("INTERNAL ERROR: There is an action to set or unset the property\n\t"
                                    + key
                                    + "\non a path that is not in the manifest\n\t"
                                    + filename);
                  if(action=="set")
                    {
                      i->first.properties[key]=value;
                    }
                  else
                    {
                      map<string,string>::iterator
                        j=i->first.properties.find(key);
                      if(j==i->first.properties.end())
                        throw arx_error("INTERNAL ERROR: Attempting to unset a property that was never set.\n\t"
                                        + filename + "\n\t"
                                        + key);
                      i->first.properties.erase(j);
                    }
                }
              else
                {
                  sha256 checksum;
                  file_attributes f;
                  f.input(changes,checksum);
                  if(action=="add")
                    {
                      checksums.push_back(make_pair(f,checksum));
                    }
                  else if(action=="delete")
                    {
                      remove_path_from_checksums(checksums,f);
                    }
                  else if(action=="move")
                    {
                      string temp;
                      archive::text_iarchive changes_archive(changes,
                                                             archive::no_header);
                      changes_archive >> temp;
                      string destination(temp);
                      
                      Checksums::iterator j(checksums.end());
                      for(Checksums::iterator i=checksums.begin();
                          i!=checksums.end(); ++i)
                        {
                          if(i->first.inventory_id==f.inventory_id)
                            {
                              j=i;
                              break;
                            }
                        }
                      
                      /* Sanity checks */
                      if(j==checksums.end())
                        throw arx_error("INTERNAL ERROR: Your _arx/++changes file seems to be corrupted.\nIt contains a rename for a path whose inventory id is not in\nthe manifest.\n\toriginal path: " + f.file_path.string()
                                        + "\n\tdestination path: " + destination
                                        + "\n\tinventory id: " + f.inventory_id);
                      if(j->first.file_path.string()!=f.file_path.string())
                        throw arx_error("INTERNAL ERROR: Your _arx/++changes file seems to be corrupted.\nIt contains a rename for a path with a particular inventory id,\nbut the inventory id and path do not match in the manifest.\npath in manifest: "
                                        + j->first.file_path.string()
                                        + "\n\toriginal path in ++changes: "
                                        + f.file_path.string()
                                        + "\n\tdestination path in ++changes: "
                                        + destination
                                        + "\n\tinventory id: " + f.inventory_id);
                      
                      /* Actually renames the element in the list */
                      j->first.file_path=path(destination);
                    }
                  else
                    {
                      throw arx_error("INTERNAL ERROR: Your _arx/++changes file seems to be corrupted.\nI don't know how to interpret this action\n\t" + action);
                    }
                }
            }
        }
    }
}
