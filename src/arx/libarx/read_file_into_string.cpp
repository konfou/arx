/* A simple function to read a complete file into a string

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include <list>
#include <string>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "arx_error.hpp"
#include <fstream>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

string read_file_into_string(istream &infile)
{
  string file_string;
  const unsigned int buffer_size(1<<16);
  char c[buffer_size];

  /* I use read() here rather than readsome() because readsome() has
     problems on RH 9 */

  while(infile)
    {
      infile.read(c,buffer_size);
      file_string+=string(c,infile.gcount());
    }
  return file_string;
}

string read_file_into_string(const path &location)
{
  fs::ifstream infile(location);
  return read_file_into_string(infile);
}
