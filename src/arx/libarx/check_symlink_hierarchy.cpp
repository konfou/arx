/* Check whether a particular path is being accessed through symlinks.
   This is a security check to make sure that a patch doesn't try to
   change things through symlinks.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void check_symlink_hierarchy(const path &root, const path &tail)
{
  const path branch(tail.branch_path());
  if(!branch.empty() && branch.string()!=root.string())
    {
      if(symbolic_link_exists(branch))
        {
          throw arx_error("INTERNAL ERROR: This path is a symlink.  Symlinks are not allowed here\n\t" + branch.native_file_string());
        }
      check_symlink_hierarchy(root,branch);
    }
}
