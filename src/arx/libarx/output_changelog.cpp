/* Print a nicely formatted ChangeLog from the patch logs stored in a
   project tree.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */


#include "arx_error.hpp"
#include <list>
#include "tree_root.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "boost/regex.hpp"
#include "Parsed_Name.hpp"
#include "Patch_Log.hpp"
#include "patch_level_cmp.hpp"
#include <iostream>
#include <cstdlib>
#include <map>
#include "list_patch_logs.hpp"
#include "list_archive_revisions.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

namespace {
  void print_if_not_empty(map<string,list<string> > &header_lists,
                          const string &field,
                          ostream &out, const string exclude="")
  {
    bool first(true);
    if(header_lists.find(field)!=header_lists.end())
      {
        for(list<string>::iterator i=header_lists[field].begin();
            i!=header_lists[field].end(); ++i)
          {
            if(*i!=exclude)
              {
                if(first)
                  {
                    out << "    " << field << ":\n";
                    first=false;
                  }
                out << "      " << *i << "\n";
              }
          }
        if(!first)
          out << "\n";
      }
  }
  
  void print_renames(map<string,list<pair<string,string> > > &rename_lists,
                     const string &field, ostream &out)
  {
    if(rename_lists.find(field)!=rename_lists.end())
      {
        out << "    " << field << ":\n";
        for(list<pair<string,string> >::iterator
              i=rename_lists[field].begin(); i!=rename_lists[field].end(); ++i)
          {
            out << "      "
                << i->first << "\n"
                << "      -> "
                << i->second << "\n";
          }
        out << "\n";
      }
  }
}

void output_changelog(const unsigned int &revision, Patch_Log &log,
                      ostream &out)
{
  Parsed_Name p(log.headers["Archive"] + "/" + log.headers["Revision"]);
  out << log.headers["Standard-date"] << " "
      << log.headers["Creator"] << "\t"
      << revision << "\n\n    "
      << "Summary:\n      " << log.headers["Summary"] << "\n    "
      << "Revision:\n      " << log.headers["Revision"]
      << "\n\n";
  
  /* Print out the headers. */
  
  print_if_not_empty(log.header_lists,"New-files",out,
                     "_arx/patch-log"
                     + p.branch_path() + "/"
                     + patch_level(revision));
  print_if_not_empty(log.header_lists,"Removed-files",out);
  print_if_not_empty(log.header_lists,"Modified-files",out);
  
  print_renames(log.rename_lists,"Renamed-files",out);
  
  print_if_not_empty(log.header_lists,"New-directories",out);
  print_if_not_empty(log.header_lists,"Removed-directories",out);
  print_if_not_empty(log.header_lists,"Modified-directories",out);
  
  print_renames(log.rename_lists,"Renamed-directories",out);
  
  print_if_not_empty(log.header_lists,"New-patches",out,
                     p.complete_branch()
                     + patch_level(revision));
  
  regex re("\\n");
  list<string> body_items;
  regex_split(back_inserter(body_items),log.body,re);
  
  for(list<string>::iterator j=body_items.begin();
      j!=body_items.end(); ++j)
    {
      out << "    " << *j << "\n";
    }  
  out << "\n\n";
}

void output_changelog(const path &tree_directory, const Parsed_Name &p,
                      ostream &out)
{
  /* Find where the logs are. */
      
  Revision_List revisions(list_patch_logs(tree_directory,p));
  revisions.reverse();
      
  for(Revision_List::iterator i=revisions.begin(); i!=revisions.end(); ++i)
    {
      Patch_Log log(tree_directory,Parsed_Name(p).set_revision(*i));
      output_changelog(*i,log,out);
    }
}

/* This version just reads from the archive */

void output_changelog(const Parsed_Name &p, ostream &out)
{
  Revision_List revisions(list_archive_revisions(p));
  revisions.reverse();
      
  for(Revision_List::iterator i=revisions.begin(); i!=revisions.end(); ++i)
    {
      Patch_Log log(Parsed_Name(p).set_revision(*i));
      output_changelog(*i,log,out);
    }
}
