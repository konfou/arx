/* Constructors, input, and output operators for sha256.  This will
   probably fail badly if given a directory or non-existent file.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "sha256.hpp"
#include "sha2.h"
#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/filesystem/exception.hpp"
#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void sha256::hash_string(const std::string &s)
{
  sha256_ctx context;
  sha256_begin(&context);
  sha256_hash((const unsigned char*)s.c_str(),s.size(),&context);
  sha256_end(&internal_hash[0],&context);
}

namespace {
  const int hash_size(32);
}

sha256::sha256(const path &p): internal_hash(hash_size)
{
  if(!lexists(p) || symbolic_link_exists(p) || is_directory(p))
    throw arx_error("INTERNAL ERROR: Can't hash this path.  It either doesn't exist\nor is a symlink or directory\n\t" + p.native_file_string());
  fs::ifstream stream1(p,ios::in);
  const int blocksize(4096);
  unsigned char buf[blocksize];
  
  sha256_ctx context;
  sha256_begin(&context);
  while(stream1.good())
    {
      stream1.read((char *)buf, blocksize);
      sha256_hash(buf,stream1.gcount(),&context);
    }
  sha256_end(&internal_hash[0],&context);
}


sha256::sha256(const std::string &s): internal_hash(hash_size)
{
  hash_string(s);
}

sha256::sha256(const char *c): internal_hash(hash_size)
{
  hash_string(std::string(c));
}

void sha256::serialize(std::ostream &s) const
{
  s.write(reinterpret_cast<const char *>(&internal_hash[0]),hash_size);
}

void sha256::deserialize(std::istream &s)
{
  internal_hash.resize(hash_size);
  s.read(reinterpret_cast<char *>(&internal_hash[0]),hash_size);
}

std::ostream& operator<<(std::ostream &s, const sha256 &sh)
{
  /* Save the internal state, output as hex, and then reset the state */
  ios::fmtflags old_flags=s.setf(ios::hex);
  s << hex;
  for(unsigned int i=0;i<sh.internal_hash.size();++i)
    {
      s.fill('0');
      s.width(2);
      s << (int)(sh.internal_hash[i]);
    }
  s.flags(old_flags);
  return s;
}

istream & operator>>(istream &s, sha256 &sh)
{
  /* Input as hex */
  string ss;
  if(sh.internal_hash.empty())
    sh.internal_hash.resize(hash_size);
  for(int i=0;i<hash_size;++i)
    {
      s >> setw(2) >> ss;
      sh.internal_hash[i]=strtol(ss.c_str(),NULL,16);
    }
  return s;
}

