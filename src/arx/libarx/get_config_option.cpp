/* Get a configuration option from a file in the .arx
   directory of the home directory.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include <list>
#include <map>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "arx_error.hpp"
#include "get_option_from_file.hpp"
#include <cstdlib>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

string get_config_option(const string &filename)
{
  /* A static map so that we don't have to reopen a file every time we
     need the option. */
  static map<string,string> config_map;

  string config_option=config_map[filename];
  if(config_option.empty())
    {
      char * home_env=getenv("HOME");
      if(home_env==NULL)
        {
          throw arx_error("Can't get the HOME environment variable.");
        }
      path home(home_env);
      path config_path(home / ".arx" / filename);

      config_option=get_option_from_file(config_path);
      config_map[filename]=config_option;
    }
  return config_option;
}
