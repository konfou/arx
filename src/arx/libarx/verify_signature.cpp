/* Verify that a revision is properly signed.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "Temp_Directory.hpp"
#include "gpg_verify.hpp"
#include "Current_Path.hpp"
#include "remote_recursive_browse.hpp"
#include "recursive_browse.hpp"
#include "Revision_List.hpp"
#include "tempdir.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

class recursive_verify_signature: public remote_recursive_browse
{
public:
  recursive_verify_signature(const Parsed_Name &Name):
    remote_recursive_browse(Name) {}
  void revision_hook(const Parsed_Name &branch, const Revision_List &rev_list,
                     const gvfs::uri &branch_location)
  {
    for(Revision_List::const_iterator i=rev_list.begin(); i!=rev_list.end();
        ++i)
      {
        if(limit_name.revision().empty() || limit_name.patch_number()==*i)
          {
            Temp_Directory sign(tempdir(),",,verify");
            Current_Path current(sign.path);

            gvfs::uri location(branch_location/patch_level(*i));

            Parsed_Name name(Parsed_Name(branch).set_revision(*i));
            /* Verify the patch */
            if(gvfs::exists(location/(name.revision()
                                      + ".patches.tar.gz.sig")))
              {
                gvfs::copy(location/(name.revision() + ".patches.tar.gz"),
                           name.revision()+ ".patches.tar.gz");
                
                gvfs::copy(location/(name.revision()
                                     + ".patches.tar.gz.sig"),
                           name.revision()+ ".patches.tar.gz.sig");
                gpg_verify(name.revision()+ ".patches.tar.gz.sig",name);
              }
            else 
              throw arx_error("This patch is not signed\n\t"
                              + name.complete_revision());
            
            /* Verify the revision */
            if(gvfs::exists(location/"sha256.sig"))
              {
                gvfs::copy(location/"sha256",gvfs::uri("sha256"));
                gvfs::copy(location/"sha256.sig",gvfs::uri("sha256.sig"));
                gpg_verify("sha256.sig",name);
              }
            else
              throw arx_error("This revision is not signed\n\t"
                              + name.complete_revision());
          }
      }
  }
};
  

void verify_signature(const Parsed_Name &name)
{
  recursive_verify_signature remote(name);
  recursive_browse(name.archive_location()
                   /name.branch_path_no_archive(), name, remote);
}
