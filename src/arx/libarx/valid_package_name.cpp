/* Makes sure that a particular name has all of the required
   components.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>
#include "package_level.hpp"
#include "Parsed_Name.hpp"
#include "arx_error.hpp"

using namespace std;

void valid_package_name(const Parsed_Name &name,
                        const package_level &level=Archive)
{
  if(level==Branch && name.branch().empty())
    {
      throw arx_error("branch required: " + name.full_name());
    }
  else if(level==Revision && name.revision().empty())
    {
      throw arx_error("revision required: " + name.full_name());
    }
}
