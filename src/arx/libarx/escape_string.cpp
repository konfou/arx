/* Escapes all of the characters in a string, so that it can be passed
   to shells and regexes without troubles.

   Copyright (C) 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include <string>

using namespace std;

string escape_string(const string &raw)
{
  string result(raw);
  static const string escaped_characters(".|*?+(){}[]^$\\");
  string::size_type idx(0);
  
  idx=result.find_first_of(escaped_characters,idx);
  while(idx!=string::npos)
    {
      result.replace(idx,1,string("\\") + result[idx]);
      ++++idx;
      if(idx>=result.size())
        idx=string::npos;
      idx=result.find_first_of(escaped_characters,idx);
    }
  return result;
}
