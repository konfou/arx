/* Return a list of all of the cached revisions for a branch in an
   archive.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "patch_level_cmp.hpp"
#include <string>
#include <list>
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "patch_number.hpp"
#include "Component_Regexes.hpp"
#include "is_continuation.hpp"

using namespace std;

Revision_List list_archive_cached_revisions(const Parsed_Name &name)
{
  if(name.branch().empty())
    throw arx_error("This argument needs to specify a branch: "
                    + name.full_name());

  Revision_List cached_revisions(name);
  
  gvfs::Init();
  
  gvfs::uri location(name.archive_location()
                     / name.branch_path_no_archive()
                     / ",cache");
  
  list<string> revision_list;
  
  Component_Regexes reg;
  
  if(gvfs::exists(location))
    {
      gvfs::fill_directory_list(revision_list,location);
      for(list<string>::iterator i=revision_list.begin();
          i!=revision_list.end(); ++i)
        {
          if(i->size() > 8+name.branch().size()
             && i->substr(i->size()-7)==".tar.gz"
             && (name.revision().empty()
                 || name.revision()==i->substr(0,name.revision().size())))
            {
              cached_revisions
                .push_back(patch_number
                           (i->substr
                            (name.branch().size(),
                             i->size()-7-name.branch().size())));
            }
        }
    }
  
  /* Add the initial patch from the void if it exists */
  if((name.revision().empty() || name.patch_number()==0)
     && gvfs::exists(name.archive_location()
                     / name.branch_path_no_archive() / ",0")
     && !is_continuation(Parsed_Name(name).set_revision(0))
     && find(cached_revisions.begin(),cached_revisions.end(),
             static_cast<unsigned int>(0))==cached_revisions.end())
    {
      cached_revisions.push_back(0);
    }

  cached_revisions.sort();
  return cached_revisions;
}
