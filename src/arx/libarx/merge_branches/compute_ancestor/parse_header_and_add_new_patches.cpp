/* Parse a log header stored in a map and add any new patches.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include "Revision_List.hpp"
#include "boost/tokenizer.hpp"
#include "Graph_typedefs.hpp"
#include "add_merges_for_revision.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void parse_header_and_add_new_patches(map<string,list<string> > &header_lists,
                                      const Parsed_Name &input_revision,
                                      const Vertex &v,
                                      const path &tree_path, Graph &history,
                                      property_map<Graph, vertex_name_t>::type
                                      &patch_name,
                                      map<Parsed_Name,Vertex> &name_map)
{
  if(header_lists.find("New-patches")!=header_lists.end())
    {
      for(list<string>::iterator j=header_lists["New-patches"].begin();
          j!=header_lists["New-patches"].end(); ++j)
        {
          Parsed_Name p(*j);
          if(p!=input_revision)
            {
              if(name_map.find(p)==name_map.end())
                {
                  Vertex u=add_vertex(history);
                  patch_name[u]=p;
                  name_map[p]=u;
                  add_merges_for_revision(p,u,tree_path,history,patch_name,
                                          name_map);
                  add_edge(v,u,history);
                }
              else
                {
                  add_edge(v,name_map[p],history);
                }
            }
        }
    }
}

