/* Make sure that two merge points are actually connected, and that
   they both have a complete patch log for the merged branch.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include "valid_package_name.hpp"
#include "valid_package_name.hpp"
#include <map>
#include "Patch_Log.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

bool check_complete_merge(const Parsed_Name &merge, const path &tree_path)
{
  bool complete_merge(true);
  valid_package_name(merge,Revision);

  Parsed_Name current(merge);

  bool done(false);
  while(!done)
    {
      /* If we can't find the patch log for the current revision, then
         we're done. */
      Patch_Log log(tree_path,current);

      if(log.empty())
        {
          complete_merge=false;
          done=true;
        }
      else
        {
          if(log.headers.find("Continuation-of")!=log.headers.end()
             || current.patch_number()==0)
            {
              complete_merge=true;
              done=true;
            }
          else
            {
              --current;
            }
        }
    }
  return complete_merge;
}
