/* Add any local changes to the project tree that haven't been
   committed yet.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include "Revision_List.hpp"
#include "Temp_Directory.hpp"
#include "find_or_make_pristine.hpp"
#include "find_new_and_removed_patches.hpp"
#include "tree_root.hpp"
#include "make_patch.hpp"
#include <list>
#include "Graph_typedefs.hpp"
#include "parse_header_and_add_new_patches.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void add_local_changes(const Parsed_Name &input_revision,
                       const Vertex &v_input,
                       const path &tree_path, Graph &history,
                       property_map<Graph, vertex_name_t>::type
                       &patch_name,
                       map<Parsed_Name,Vertex> &name_map)
{
  Temp_Directory temp_dir(tree_path,",,add_local_changes");

  Temp_Directory pristine_temp_dir(tree_path,",,pristine",false);
  if(make_patch(find_or_make_pristine(tree_path,pristine_temp_dir.path,
                                      input_revision),
                tree_root(tree_path),temp_dir.path/"patch",Path_List()))
  {
    map<string,list<string> > header_lists;
    find_new_and_removed_patches(temp_dir.path/"patch",header_lists,true);
    parse_header_and_add_new_patches(header_lists,input_revision,v_input,
                                     tree_path,history,patch_name,name_map);
  }
}
