/* Find out what was merged for a particular revision and add it (and
   all revisions connected to it) to the list of merged revisions.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_ADD_MERGES_FOR_REVISION_HPP
#define ARX_ADD_MERGES_FOR_REVISION_HPP

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include "Graph_typedefs.hpp"

extern void add_merges_for_revision
(const Parsed_Name &input_revision,
 const Vertex &v_input,
 const boost::filesystem::path &tree_path, Graph &history,
 boost::property_map<Graph, boost::vertex_name_t>::type &patch_name,
 std::map<Parsed_Name,Vertex> &name_map);

#endif
