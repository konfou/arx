#ifndef ARX_GRAPH_TYPEDEFS_HPP
#define ARX_GRAPH_TYPEDEFS_HPP

#include "boost/graph/graph_traits.hpp"
#include "boost/graph/adjacency_list.hpp"
#include <string>
#include "Parsed_Name.hpp"

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
                              boost::property<boost::vertex_name_t, Parsed_Name>,
                              boost::property<boost::edge_name_t, std::string > > Graph;

typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;

#endif
