/* Find out the least common ancestor between two trees using a breadth
   first search.

  Copyright (C) 2004, 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include "valid_package_name.hpp"
#include "Command_Info.hpp"
#include <iostream>
#include "boost/graph/transitive_closure.hpp"
#include <string>
#include "Graph_typedefs.hpp"
#include "add_merges_for_revision.hpp"
#include <limits.h>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern bool check_complete_merge(const Parsed_Name &merge,
                                 const path &tree_path);

extern void add_local_changes(const Parsed_Name &input_revision,
                              const Vertex &v_input,
                              const path &tree_path, Graph &history,
                              property_map<Graph, vertex_name_t>::type
                              &patch_name,
                              map<Parsed_Name,Vertex> &name_map);

list<Parsed_Name> compute_lcas(const Parsed_Name &main,
                               const path &main_path,
                               const Parsed_Name &sibling,
                               const path &sibling_path)
{
  if(Command_Info::verbosity>=report)
    {
      cout << "Computing ancestors:\n\t"
           << main.complete_revision() << "\n\t"
           << sibling.complete_revision() << "\n\t"
           << main_path.native_file_string() << "\n\t"
           << sibling_path.native_file_string()
           << endl;
    }

  valid_package_name(main,Revision);
  valid_package_name(sibling,Revision);

  /* Check if the revisions are the same, in which case the ancestor
     is trivial. */
  if(main==sibling)
    {
      list<Parsed_Name> trivial;
      trivial.push_back(main);
      return trivial;
    }

  /* Compute the entire history graph */

  Graph history;

  map<Parsed_Name, Vertex> name_map;

  Vertex main_vertex, sibling_vertex;

  property_map<Graph, vertex_name_t>::type patch_name
    = get(vertex_name, history);

  /* add the main and sibling vertices */
  main_vertex=add_vertex(history);
  patch_name[main_vertex]=main;
  name_map[main]=main_vertex;

  sibling_vertex=add_vertex(history);
  {
    /* Define a patch number that hasn't been used yet.  This is
       needed for the changes to the tree that haven't been committed
       yet. */
    Parsed_Name temp_name(Parsed_Name(sibling).set_revision(UINT_MAX));
    patch_name[sibling_vertex]=temp_name;
    name_map[temp_name]=sibling_vertex;
  }

  /* Add any local changes in sibling to the history */

  add_local_changes(sibling,sibling_vertex,sibling_path,history,
                    patch_name,name_map);

  {
    /* We reuse the sibling name.  This is ok, because the name is
       mostly important just so that we don't add a vertex twice. */
    Vertex new_sibling_vertex=add_vertex(history);
    patch_name[new_sibling_vertex]=sibling;
    name_map[sibling]=new_sibling_vertex;
    add_edge(sibling_vertex,new_sibling_vertex,history);
    
    /* Add all the rest of the graph.  Note that we add them rather
       indiscriminately.  Later, we will check to make sure that we have
       a real merge. */
    add_merges_for_revision(main,main_vertex,main_path,history,patch_name,
                            name_map);
    add_merges_for_revision(sibling,new_sibling_vertex,sibling_path,history,
                            patch_name,name_map);
  }

  /* Compute the transitive closure of the graph, which then makes it
     trivial to find all of the common ancestors. */
  Graph trans_closure;

  transitive_closure(history,trans_closure);
  
  /* Add all of the common ancestors to a complete ancestor list.
     Remember to include the original revisions themselves.  */

  graph_traits<Graph>::adjacency_iterator main_i, main_end, sibling_begin,
    sibling_end, ai, aend;

  list<Vertex> ancestor_list;
  for(tie(main_i,main_end)=adjacent_vertices(main_vertex,trans_closure);
      main_i!=main_end; ++main_i)
    {
      tie(sibling_begin,sibling_end)=adjacent_vertices(sibling_vertex,
                                                       trans_closure);
      if(find(sibling_begin,sibling_end,*main_i)!=sibling_end
         || *main_i==sibling_vertex)
        ancestor_list.push_back(*main_i);
    }

  tie(sibling_begin,sibling_end)=adjacent_vertices(sibling_vertex,
                                                   trans_closure);
  if(find(sibling_begin,sibling_end,main_vertex)!=sibling_end)
    ancestor_list.push_back(main_vertex);

  /* Remove ancestors that have descendants in the common ancestor
     list, leaving only least common ancestors. */

  list<Vertex> least_common_ancestor;

  for(list<Vertex>::iterator i=ancestor_list.begin(); i!=ancestor_list.end();
      ++i)
    {
      bool no_ancestor(true);
      for(list<Vertex>::iterator j=ancestor_list.begin();
          j!=ancestor_list.end() && no_ancestor;++j)
        {
          if(j!=i)
            {
              tie(ai,aend)=adjacent_vertices(*j,trans_closure);
              if(find(ai,aend,*i)!=aend)
                no_ancestor=false;
            }
        }
      if(no_ancestor)
        least_common_ancestor.push_back(*i);
    }

  /* Finally, remove any ancestors that weren't completely merged into
     either tree.  This can happen if you just cherry-pick some
     patches. */

  list<Parsed_Name> culled_ancestors;
  for(list<Vertex>::iterator i=least_common_ancestor.begin();
      i!=least_common_ancestor.end(); ++i)
    {
      Parsed_Name p(patch_name[*i]);
      if(check_complete_merge(p,main_path)
         && check_complete_merge(p,sibling_path))
        culled_ancestors.push_back(p);
    }

  if(Command_Info::verbosity>=report)
    {
      cout << "The following are the least common ancestors";
      for(list<Parsed_Name>::iterator i=culled_ancestors.begin();
          i!=culled_ancestors.end(); ++i)
        cout << "\n\t" << i->complete_revision();
      cout << endl;
    }

  return culled_ancestors;
}
