/* Find out what was merged for a particular revision and add it (and
   all revisions connected to it) to the list of merged revisions.

  Copyright (C) 2004, 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include "Command_Info.hpp"
#include "Revision_List.hpp"
#include "Patch_Log.hpp"
#include "Graph_typedefs.hpp"
#include "parse_header_and_add_new_patches.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void add_merges_for_revision(const Parsed_Name &input_revision,
                             const Vertex &v_input,
                             const path &tree_path, Graph &history,
                             property_map<Graph, vertex_name_t>::type
                             &patch_name,
                             map<Parsed_Name,Vertex> &name_map)
{
  Parsed_Name current(input_revision);
  Vertex v(v_input);

  bool done(false);
  while(!done)
    {
      Patch_Log log(tree_path,current);
      /* If we can't find the patch log for the current revision, then
         we're done. */
      if(log.empty())
        {
          done=true;
        }
      else
        {
          parse_header_and_add_new_patches(log.header_lists,input_revision,
                                           v_input,tree_path,history,
                                           patch_name,name_map);

          if(log.headers.find("Continuation-of")!=log.headers.end())
            {
              const Parsed_Name continuation(log.headers["Continuation-of"]);
              if(name_map.find(continuation)==name_map.end())
                {
                  Vertex u=add_vertex(history);
                  patch_name[u]=continuation;
                  name_map[continuation]=u;
                  add_merges_for_revision(continuation,u,tree_path,history,
                                          patch_name,name_map);
                  add_edge(v,u,history);
                }
              else
                {
                  add_edge(v,name_map[continuation],history);
                }
              done=true;
            }
          else
            {
              if(current.patch_number()==0)
                {
                  /* We have come back to the beginning of time, where the
                     stuff was originally created. */
                  done=true;
                }
              else
                {
                  Vertex v_old(v);
                  --current;
                  if(name_map.find(current)==name_map.end())
                    {
                      v=add_vertex(history);
                      patch_name[v]=current;
                      name_map[current]=v;
                      add_edge(v_old,v,history);
                    }
                  else
                    {
                      done=true;
                      add_edge(v_old,name_map[current],history);
                    }
                }
            }
        }
    }
}

