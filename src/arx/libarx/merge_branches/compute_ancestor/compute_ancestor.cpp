/* Compute the unique least common ancestor, if it exists.

  Copyright (C) 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "valid_package_name.hpp"
#include "boost/filesystem/operations.hpp"
#include <list>

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern list<Parsed_Name> compute_lcas(const Parsed_Name &main,
                                      const path &main_path,
                                      const Parsed_Name &sibling,
                                      const path &sibling_path);

void compute_ancestor(Parsed_Name &ancestor, const Parsed_Name &sibling,
                      const path &sibling_path, const Parsed_Name &tree,
                      const path &tree_path)
{
  if(ancestor.empty())
    {
      list<Parsed_Name> ancestors(compute_lcas(sibling,sibling_path,
                                               tree,tree_path));
      if(ancestors.empty())
        throw arx_error("Could not find a common ancestor between the two revisions:\n\t" + tree.complete_revision() + "\n\t" + sibling.complete_revision());
      
      if(ancestors.size()>1)
        {
          string message("There is more than one valid least common ancestor for the tree\nand merging branch.  This may mean that you have criss-cross merges.\nPlease see the manual for more information.");
          for(list<Parsed_Name>::iterator i=ancestors.begin();
              i!=ancestors.end(); ++i)
            message+="\n\t"+i->complete_name();
          throw arx_error(message);
         }
      ancestor=*ancestors.begin();
      
      if(Command_Info::verbosity>=report)
        cout << "Using as the common ancestor: "
             << ancestor.complete_revision()
             << endl;
    }
  else
    {
      valid_package_name(ancestor,Revision);
    }
}
