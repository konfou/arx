/* Perform a merge of a branch with its sibling.  Returns whether the
   merge produced conflicts.

  Copyright (C) 2001, 2002, 2003 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry
                           and the Regents of the University of California
  Copyright (C) 2004, 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/convenience.hpp"
#include <list>
#include "patch_level.hpp"
#include "get_revision.hpp"
#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "Temp_Directory.hpp"
#include "make_patch.hpp"
#include "do_patch.hpp"
#include "valid_package_name.hpp"
#include <iostream>
#include "tree_branch.hpp"
#include "Merge_Algo.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

extern Conflicts three_way_merge(const Parsed_Name &tree,
                                 const path &tree_path,
                            const Parsed_Name &sibling,
                            const path &sibling_path,
                            const Parsed_Name &ancestor,
                            const path &ancestor_path,
                            const path &merge_dir);

extern void compute_ancestor(Parsed_Name &ancestor, const Parsed_Name &sibling,
                             const path &sibling_path, const Parsed_Name &tree,
                             const path &tree_path);

Conflicts merge_branches(const Parsed_Name &tree, const Parsed_Name &sibling,
                         Parsed_Name &ancestor, const path &tree_path,
                         const Merge_Algo &merge_algo,
                         const bool &delete_removed, const bool &update)
{
  Conflicts conflicts(tree_path);

  valid_package_name(tree,Revision);
  valid_package_name(sibling,Revision);

  Temp_Directory merge_dir(tree_path,",,merge_branches");

  path sibling_path(merge_dir.path/"sibling");

  /* If tree and sibling are the same, then we don't have to do anything. */
  if(tree==sibling)
    {
      if(Command_Info::verbosity>=default_output)
        cout << "No merge done.  Sibling is the same as the project tree\n\t"
             << sibling.complete_revision() << endl;
      return conflicts;
    }

  get_revision(sibling,sibling_path,tree_path);

  compute_ancestor(ancestor,sibling,sibling_path,tree,tree_path);
  const path ancestor_path(merge_dir.path/"ancestor");
  get_revision(ancestor,merge_dir.path/"ancestor",tree_path);
  if(merge_algo==big_patch)
    {
      make_patch(merge_dir.path/"ancestor",sibling_path,
                 merge_dir.path/"patch",Path_List());
      conflicts=do_patch(merge_dir.path/"patch",tree_path,false,
                         delete_removed,true,false);
    }
  else
    {
      conflicts=three_way_merge(tree,tree_path,sibling,sibling_path,
                                ancestor,ancestor_path,merge_dir.path);
    }

  /* Update the tree cache if necessary */
  if(update)
    {
      path tree_cache(tree_path/ "_arx/++cache"/sibling.revision_path());
      if(!lexists(tree_cache))
        {
          create_directories(tree_cache.branch_path());
          rename(sibling_path,tree_cache);
        }
      remove_all(tree_path/ "_arx/++cache"/tree.revision_path());
      tree_branch(tree_path,sibling);
    }
  return conflicts;
}

