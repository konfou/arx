/* Perform a three-way merge of a tree with a sibling tree and
   ancestor tree.

  Copyright (C) 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "boost/filesystem/operations.hpp"
#include <list>
#include "get_revision.hpp"
#include "Temp_Path.hpp"
#include "make_patch.hpp"
#include "do_patch.hpp"
#include "Path_List.hpp"
#include "read_checksums.hpp"
#include "check_symlink_hierarchy.hpp"
#include "Spawn.hpp"
#include "config.h"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

Conflicts three_way_merge(const Parsed_Name &tree, const path &tree_path,
                          const Parsed_Name &sibling, const path &sibling_path,
                          const Parsed_Name &ancestor,
                          const path &ancestor_path,
                          const path &merge_dir)
{
  /* First, find out what has changed between the ancestor and the
     sibling, and the ancestor and the tree. */

  Checksums sibling_orig_only, sibling_mod_only, sibling_patched,
    tree_checksums;
  list<pair<pair<file_attributes,sha256>,
    pair<file_attributes,sha256> > > sibling_in_both;
  

  make_patch(ancestor_path,sibling_path,merge_dir/"sibling_patch",
             Path_List(), sibling_orig_only, sibling_mod_only, sibling_patched,
             sibling_in_both);

  /* Apply the sibling patch, but don't apply content changes to
     files. */
  Conflicts conflicts=do_patch(merge_dir/"sibling_patch",tree_path,
                               false,true,false,false);

  /* Now do a three way merge on paths that differ in the sibling.  */

  read_checksums(tree_path,tree_checksums);

  for(Checksums::iterator i=sibling_patched.begin(); i!=sibling_patched.end();
      ++i)
    {
      /* Find the corresponding path in the tree, if it exists. */
      Checksums::iterator
        j(find_if(tree_checksums.begin(),tree_checksums.end(),
                  bind2nd(ptr_fun(checksum_inventory_id_eq),*i)));

      if(j!=tree_checksums.end())
        {
          path sibling_file(i->first.file_path),
            tree_file(j->first.file_path), ancestor_file;

          /* Figure out whether the file moved in sibling. */

          list<pair<pair<file_attributes,sha256>,
            pair<file_attributes,sha256> > >::iterator
            j(sibling_in_both.begin());

          for(;j!=sibling_in_both.end();++j)
            {
              if(j->first.first.inventory_id==i->first.inventory_id)
                {
                  ancestor_file=j->first.first.file_path;
                  break;
                }
            }
          if(j==sibling_in_both.end())
            throw arx_error("INTERNAL ERROR: The patch from ancestor to sibling patched a path that\ndoes not exist in both.\n\t"
                            + tree.complete_revision() + "\n\t"
                            + ancestor.complete_revision() + "\n\t"
                            + sibling.complete_revision() + "\n\t"
                            + (sibling_path/sibling_file).native_file_string());

          /* Make sure we aren't applying patches through
             symlinks */
          
          check_symlink_hierarchy(tree_path,tree_path/tree_file);
          
          /* Run diff3 */

          if(!symbolic_link_exists(tree_path/tree_file)
             && !is_directory(tree_path/tree_file))
            {
              path temp(Temp_Path(tree_path,",,merge"));
              fs::rename(tree_path/tree_file,temp);

              Spawn s;
              s << ARXDIFF3;
              s << "-E" << "--merge"
                << "-L" << "TREE" << "-L" << "ANCESTOR" << "-L" << "SIBLING" 
                << temp.native_file_string()
                << (ancestor_path/ancestor_file).native_file_string()
                << (sibling_path/sibling_file).native_file_string();

              s.output=tree_path/tree_file;
              s.error="/dev/null";

              int patch_stat;
              if(!s.execute(patch_stat,true))
                throw arx_error("Can't execute diff3");

              if(patch_stat!=0)
                {
                  path tree_temp(Temp_Path(tree_path,
                                           tree_file.string() + ".tree"));
                  fs::rename(temp,tree_temp);
                  path ancestor_temp(Temp_Path(tree_path,tree_file.string()
                                               + ".ancestor"));
                  fs::rename(ancestor_path/ancestor_file,
                             ancestor_temp);
                  path sibling_temp(Temp_Path(tree_path,tree_file.string()
                                              + ".sibling"));
                  fs::rename(sibling_path/sibling_file,sibling_temp);

                  conflicts.merge.push_back
                    (make_pair(make_pair(tree_file.string(),
                                         tree_temp.string()),
                               make_pair(ancestor_temp.string(),
                                         sibling_temp.string())));

                  path merge_path(path(getenv("HOME"))/".arx/merge3");
                  if(fs::exists(merge_path))
                    {
                      Spawn s;
                      s << merge_path.native_file_string()
                        << (tree_path/tree_temp).native_file_string()
                        << (tree_path/ancestor_temp).native_file_string()
                        << (tree_path/sibling_temp).native_file_string()
                        << (tree_path/tree_file).native_file_string();

                      if(!s.execute(false))
                        throw arx_error("Error when executing merge3 script\n\t"
                                        + fs::system_complete(merge_path)
                                        .native_file_string());
                    }
                }
              else
                {
                  fs::remove(temp);
                }
            }
        }
    }
  conflicts.save_to_conflicts_file();
  return conflicts;
}

