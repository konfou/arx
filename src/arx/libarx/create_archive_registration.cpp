/* Creates a registration for an archive.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include "boost/filesystem/convenience.hpp"
#include "arx_error.hpp"
#include "get_option_from_file.hpp"
#include "set_option_in_file.hpp"
#include "gvfs.hpp"
#include "read_archive_locations.hpp"
#include "write_archive_locations.hpp"
#include "Temp_Directory.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

void create_archive_registration(const string &archive,
                                 const gvfs::uri &location)
{
  path home(getenv("HOME"));
  path location_path(home / ".arx/archives");
  if(!lexists(location_path))
    {
      try
        {
          create_directories(location_path);
        }
      catch (fs::filesystem_error &fs_error)
        {
          throw arx_error("Could not create the location directory: "
                          + location_path.native_file_string());
        }
    }
  else if(!is_directory(location_path))
    {
      throw arx_error("INTERNAL ERROR: \n" + location_path.native_file_string()
                      + "\nis not a directory");
    }

  path config_path(location_path / archive);
  list<string> locations;
  if(lexists(config_path))
    locations=read_archive_locations(config_path/"uri");

  if(find(locations.begin(),locations.end(),location.string())
     ==locations.end())
    {
      locations.push_back(location.string());

      Temp_Directory new_reg(location_path,",,new-registration");
      write_archive_locations(new_reg.path/"uri",locations);
      if(locations.size()==1)
        {
          if(gvfs::exists(location/",meta-info/public_keys"))
            gvfs::copy(location/",meta-info/public_keys",
                       (new_reg.path/"public_keys").native_file_string());
          create_directories(location_path);
          fs::rename(new_reg.path,config_path);
        }
      else if(0!=::rename((new_reg.path/"uri").native_file_string().c_str(),
                          (config_path/"uri").native_file_string().c_str()))
        throw arx_error("Could not add the new location to the list of locations for this archive.\nThe rename failed.\n\t"
                        + archive + "\n\t"
                        + location.string() + "\n\t"
                        + new_reg.path.native_file_string() + "\n\t"
                        + config_path.native_file_string());
    }
}
