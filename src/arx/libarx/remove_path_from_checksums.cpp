/* Remove an element from a Checksums if it has the same inventory id.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Checksums.hpp"
#include "arx_error.hpp"

void remove_path_from_checksums(Checksums &checksums, const file_attributes &f)
{
  Checksums::iterator j(checksums.end());
  for(Checksums::iterator i=checksums.begin();
      i!=checksums.end(); ++i)
    {
      if(i->first.inventory_id==f.inventory_id)
        {
          j=i;
          break;
        }
    }
  
  /* Sanity checks */
  if(j==checksums.end())
    throw arx_error("INTERNAL ERROR: Your _arx/++changes file seems to be corrupted.  It contains\na delete for a path whose inventory id is not in the manifest.\n\tpath: "
                    + f.file_path.string()
                    + "\n\tinventory id: "
                    + f.inventory_id);
  if(j->first.file_path.string()!=f.file_path.string())
    throw arx_error("INTERNAL ERROR: Your _arx/++changes file seems to be corrupted.  It contains\na delete for a path with a particular inventory id, but the inventory id\nand path do not match in the manifest.\npath: "
                    + f.file_path.string()
                    + "\n\tinventory id: "
                    + f.inventory_id);
  
  /* Actually erase the element from the list */
  checksums.erase(j);
}
