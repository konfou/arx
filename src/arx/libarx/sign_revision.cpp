/* Cryptographically sign a patch and/or a revision

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#include "Parsed_Name.hpp"
#include "arx_error.hpp"
#include "gvfs.hpp"
#include "Temp_Directory.hpp"
#include "Current_Path.hpp"
#include "gpg_sign.hpp"
#include "remote_recursive_browse.hpp"
#include "recursive_browse.hpp"
#include "ensure_key_in_archive.hpp"
#include "tempdir.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

class recursive_sign: public remote_recursive_browse
{
  const string gpg_key;
public:
  recursive_sign(const Parsed_Name &Name, const string &Gpg_key):
    remote_recursive_browse(Name), gpg_key(Gpg_key) {}

  /* We only have to redefine the revision hook */
  void revision_hook(const Parsed_Name &branch, const Revision_List &rev_list,
                     const gvfs::uri &branch_location)
  {
    for(Revision_List::const_iterator i=rev_list.begin(); i!=rev_list.end();
        ++i)
      {
        if(limit_name.revision().empty() || limit_name.patch_number()==*i)
          {
            ensure_key_in_archive(branch,gpg_key);
            Temp_Directory sign(tempdir(),",,sign");
            Current_Path current(sign.path);
            
            gvfs::uri location(branch_location/patch_level(*i));
            const Parsed_Name rev(Parsed_Name(branch).set_revision(*i));

            /* Sign the patch if it exists (tags do not have them. */

            if(gvfs::exists(location/(rev.revision() + ".patches.tar.gz")))
              {
                gvfs::copy(location/(rev.revision() + ".patches.tar.gz"),
                           rev.revision()+ ".patches.tar.gz");
                
                gpg_sign(rev.revision()+ ".patches.tar.gz",gpg_key);
                
                gvfs::copy(rev.revision()+ ".patches.tar.gz.sig",
                           location/(rev.revision() + ".patches.tar.gz.sig"));
              }

            /* Sign the revision */
            gvfs::copy(location/"sha256",gvfs::uri("sha256"));
            gpg_sign("sha256",gpg_key);
            gvfs::copy(gvfs::uri("sha256.sig"),location/"sha256.sig");
          }
      }
  }
};


void sign_revision(const Parsed_Name &name, const string &gpg_key)
{
  recursive_sign remote(name,gpg_key);
  recursive_browse(name.archive_location()
                   /name.branch_path_no_archive(), name, remote);
}
