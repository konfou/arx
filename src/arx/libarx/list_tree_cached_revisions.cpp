/* Return a list of all of the cached revisions for a branch in a
   project tree.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "patch_level_cmp.hpp"
#include "get_config_option.hpp"
#include <string>
#include <list>
#include "arx_error.hpp"
#include "patch_number.hpp"
#include "tree_root.hpp"
#include "boost/filesystem/operations.hpp"
#include "recursive_browse.hpp"
#include "push_revision_on_list.hpp"

using namespace std;
using namespace boost;
namespace fs=boost::filesystem;
using fs::path;

list<Revision_List> list_tree_cached_revisions(const path &tree_directory,
                                               const Parsed_Name &name)
{
  list<Revision_List> result;

  path pristines(tree_root(tree_directory)/"_arx/++cache");

  /* I check here that everything matches along the way. */

  if(lexists(pristines))
    for(fs::directory_iterator archives(pristines);
        archives!=fs::directory_iterator(); ++archives)
      if(name.archive().empty() || name.archive()==archives->leaf())
        {
          push_revision_on_list push(result,name);
          recursive_browse(*archives,Parsed_Name(archives->leaf() + "/"),push);
        }
  /* Sort all of the revisions */
  for(list<Revision_List>::iterator i=result.begin(); i!=result.end(); ++i)
    i->sort();

  return result;
}
