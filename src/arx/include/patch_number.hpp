/* Convert a string with a number into a number.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_PATCH_NUMBER_HPP
#define ARX_PATCH_NUMBER_HPP

#include <string>
#include <cstdlib>
#include "arx_error.hpp"
#include "is_revision.hpp"

inline unsigned int patch_number(const std::string &s)
{
  if(!is_revision(s))
    {
      throw arx_error("INTERNAL ERROR: Bad patch level passed to patch number\n\t"
                    + s );
    }
  return atoi(s.substr(1).c_str());
}

#endif
