/* Do the actual work of committing the changes to a tree to the archive

  Copyright (C) 2001, 2002, 2003 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_COMMIT_REVISION_HPP
#define ARX_COMMIT_REVISION_HPP

#include "boost/filesystem/path.hpp"
#include "Parsed_Name.hpp"
#include <string>
#include "Path_List.hpp"
#include <map>
#include "tree_branch.hpp"

void commit_revision(const boost::filesystem::path &tree_directory,
                     Parsed_Name name, const Path_List &path_list,
                     std::map<std::string,std::string> &log_headers,
                     const std::string &log_body,
                     const bool &unchanged_ok, const bool &out_of_date_ok,
                     const bool &partial_commit, const std::string &gpg_key);

inline void commit_revision(const boost::filesystem::path &tree_directory,
                            const std::string &summary_message)
{
  std::map<std::string,std::string> log_headers;
  log_headers["Summary"]=summary_message;
  commit_revision(tree_directory,tree_branch(tree_directory),
                  Path_List(), log_headers,std::string(),false,false,false,
                  std::string());
}

#endif
