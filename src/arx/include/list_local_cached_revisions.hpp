/* Return a list of all of the cached revisions for a branch in local
   caches.  It starts looking one directory up, then looks for _arx
   directories in that directory and everything one level down.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_LIST_LOCAL_CACHED_REVISIONS_HPP
#define ARX_LIST_LOCAL_CACHED_REVISIONS_HPP

#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include "boost/filesystem/operations.hpp"

Revision_List list_local_cached_revisions
(const Parsed_Name &name, const boost::filesystem::path &cache_dir);

#endif
