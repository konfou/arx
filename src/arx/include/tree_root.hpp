/* Get the directory for the root of the project tree.  It throws an
   exception if you are not in a project tree.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_TREE_ROOT_HPP
#define ARX_TREE_ROOT_HPP

#include "boost/filesystem/path.hpp"

extern boost::filesystem::path tree_root(const boost::filesystem::path
                                         &current_directory);

#endif
