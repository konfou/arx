/* A simple structure to hold all of the information about the command
   being executed (command, verbosity).

   Copyright 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */


#ifndef ARX_COMMAND_INFO_HPP
#define ARX_COMMAND_INFO_HPP

#include "command.hpp"
#include "Verbosity.hpp"
class Command_Info
{
public:
  const command cmd;
  static Verbosity verbosity;

  Command_Info(const command &Cmd): cmd(Cmd) {}

  /* We don't want anyone to use the default constructor */
private:
  Command_Info();
};

#endif

