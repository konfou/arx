/* A class for printing out an outline of an archive, useful with
   recursive_browse.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_REMOTE_BROWSE_HPP
#define ARX_REMOTE_BROWSE_HPP

#include "remote_recursive_browse.hpp"
#include "Parsed_Name.hpp"
#include "gvfs.hpp"
#include <iostream>

class remote_browse: public remote_recursive_browse
{
  const bool readmes, full;
  const unsigned int level;
public:
  remote_browse(const Parsed_Name &Name, const bool &Readmes,
                const unsigned int &Level, const bool Full):
    remote_recursive_browse(Name), readmes(Readmes), full(Full),
    level(Level) {}
  bool limit(const Parsed_Name &name)
  {
    return (name.branch_depth()-limit_name.branch_depth() <= level)
      && name.is_subbranch(limit_name);
  }
  void branch_hook(const Parsed_Name &Name, const gvfs::uri &location)
  {
    std::string spacer;
    for(unsigned int i=0; i<Name.branch_depth(); ++i)
      spacer+="  ";
    if(full)
      std::cout << spacer << Name.complete_branch() << std::endl;
    else
      std::cout << spacer << Name.leaf() << std::endl;

    if(readmes && gvfs::exists(location/ ",README"))
      {
        std::cout << spacer << ",README" << std::endl;
        std::cout << read_file_into_string(location/ ",README") << std::endl;
      }
      
  }
  void revision_hook(const Parsed_Name &branch, const Revision_List &rev_list,
                     const gvfs::uri &location)
  {
    /* We add spaces because we want it to be one level deeper than
       the branch */
    std::string spacer("  ");
    for(unsigned int i=0; i<branch.branch_depth(); ++i)
      spacer+="  ";
    unsigned int last(0);
    bool sequence(false);
    for(Revision_List::const_iterator i=rev_list.begin();
        i!=rev_list.end(); ++i)
      {
        if(!sequence)
          {
            std::cout << spacer << *i;
            last=*i;
            sequence=true;
          }
        else if(last+1==*i)
          {
            last=*i;
          }
        else
          {
            std::cout << " .. "
                      << std::endl
                      << spacer
                      << *i;
          }
      }
    if(sequence)
      {
        std::cout << " .. "
                  << last
                  << std::endl;
      }
  }
};

#endif
