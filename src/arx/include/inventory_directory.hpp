/* Inventories an individual directory.  If no directory is given, it
   uses the current directory.  Most of the heavy lifting occurs in
   inventory_path.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_INVENTORY_DIRECTORY_HPP
#define ARX_INVENTORY_DIRECTORY_HPP

#include "Inventory_Flags.hpp"
#include "boost/filesystem/operations.hpp"
#include "Inventory.hpp"
#include "Checksums.hpp"
#include <string>

void inventory_directory(const boost::filesystem::path &directory,
                         Inventory &result,
                         const Inventory_Flags &flags,
                         Checksums &checksums,
                         std::string prefix="");
#endif
