/* A simple superclass to browse remote archives.  This is meant to
   define the common functions for looking at an archive.  You still
   have to define a branch or revision hook.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_REMOTE_RECURSIVE_BROWSE_HPP
#define ARX_REMOTE_RECURSIVE_BROWSE_HPP

#include "gvfs.hpp"
#include "Revision_List.hpp"

class remote_recursive_browse
{
public:
  Parsed_Name limit_name;
  remote_recursive_browse(const Parsed_Name &Name): limit_name(Name)
  {
    gvfs::Init();
  }
  bool limit(const Parsed_Name &name)
  {
    return name.is_subbranch(limit_name);
  }
  void branch_hook(const Parsed_Name &Name, const gvfs::uri &location) {}

  void revision_hook(const Parsed_Name &branch, const Revision_List &rev_list,
    const gvfs::uri &location) {}

  static bool exists(const gvfs::uri &p)
  {
    return gvfs::exists(p);
  }
  static bool is_directory(const gvfs::uri &p)
  {
    return gvfs::is_directory(p);
  }
  static void fill_directory_list(std::list<std::string> &branch_list,
                                  const gvfs::uri &location)
  {
    gvfs::fill_directory_list(branch_list,location);
  }
};

#endif
