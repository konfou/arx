/* Check if a particular path is local.  That is, whether it tries to
   go up the tree using ../, or it is an absolute path.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_CHECK_IF_LOCAL_PATH_HPP
#define ARX_CHECK_IF_LOCAL_PATH_HPP

#include "boost/regex.hpp"
#include <string>
#include "arx_error.hpp"

inline void check_if_local_path(const std::string &s)
{
  if(s.empty())
    {
      throw arx_error("Empty path.");
    }
  else if(s[0]=='/'
          || boost::regex_match(s,boost::regex("(.*/|^)\\.\\.($|/.*)")))
    {
      throw arx_error("Bad path.  Path is not local?\n\t" + s);
    }
}

#endif
