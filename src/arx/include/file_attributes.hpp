/* An class to hold the characteristics of a file: path, inventory
   id, and whether it is a link.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_FILE_ATTRIBUTES_HPP
#define ARX_FILE_ATTRIBUTES_HPP

#include <string>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/exception.hpp"
#include <bitset>
#include <iostream>
#include "sha256.hpp"
#include "arx_error.hpp"
#include "inventory_types.hpp"
#include <map>

class file_attributes
{
public:
  boost::filesystem::path file_path;
  bool link;
  std::string inventory_id;
  std::map<std::string,std::string> properties;

  file_attributes(): link(false) {};
 
  file_attributes(const boost::filesystem::path &File_Path):
    file_path(File_Path),
    link(boost::filesystem::symbolic_link_exists(File_Path)) {}
 
  file_attributes(const std::string &File_Path):
    file_path(File_Path),
    link(boost::filesystem::symbolic_link_exists(File_Path)) {}
  
  file_attributes(const boost::filesystem::path &File_Path,
                  const std::string &id):
    file_path(File_Path),
    link(boost::filesystem::symbolic_link_exists(File_Path)),
    inventory_id(id) {}

  bool operator<(const file_attributes &attribute) const
  {
    return file_path.native_file_string()
      <attribute.file_path.native_file_string();
  }
  bool operator==(const file_attributes &attribute) const
  {
    return file_path.native_file_string()
      ==attribute.file_path.native_file_string()
      && link==attribute.link
      && inventory_id==attribute.inventory_id
      && properties==attribute.properties;
  }
  bool operator!=(const file_attributes &attribute) const
  {
    return !operator==(attribute);
  }

  void output(std::ostream &os, const bool &is_directory) const;
  void output(std::ostream &os, const sha256 &checksum) const;
  void input(std::istream &is, sha256 &checksum);

  bool is_control() const
  {
    return ::is_control(file_path);
  }

  void set_property(const std::string &key, const std::string &value)
  {
    if(value.empty())
      {
        std::map<std::string,std::string>::iterator i(properties.find(key));
        if(i!=properties.end())
          {
            properties.erase(i);
          }
      }
    else
      {
        properties[key]=value;
      }
  }
};

inline bool first_inventory_id_eq_string
(const std::pair<file_attributes, file_attributes> a, const std::string b)
{
  return a.first.inventory_id==b;
}

inline bool first_file_path_eq_string
(const std::pair<file_attributes, file_attributes> a, const std::string b)
{
  return a.first.file_path.string()==b;
}

inline bool second_file_path_eq_string
(const std::pair<file_attributes, file_attributes> a, const std::string b)
{
  return a.second.file_path.string()==b;
}

inline bool file_path_eq_string(const file_attributes a,
                                const std::string b)
{
  return a.file_path.string()==b;
}

inline bool inventory_id_cmp(const file_attributes &a,
                             const file_attributes &b)
{
  return a.inventory_id<b.inventory_id;
}

inline bool inventory_id_eq(const file_attributes &a,
                            const file_attributes &b)
{
  return a.inventory_id==b.inventory_id;
}

inline bool inventory_id_eq_string(const file_attributes a,
                                   const std::string b)
{
  return a.inventory_id==b;
}

#endif
