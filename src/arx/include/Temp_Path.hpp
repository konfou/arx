/* A simple function to create a temporary name that doesn't already exist.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_TEMP_PATH_HPP
#define ARX_TEMP_PATH_HPP

#include "boost/filesystem/operations.hpp"
#include <sstream>

inline boost::filesystem::path Temp_Path(const boost::filesystem::path &parent,
                                         const std::string &s)
{
  int counter(0);

  std::stringstream temp_stream;
  temp_stream << s;
  while(exists(parent/temp_stream.str()))
    {
      ++counter;
      temp_stream.str("");
      temp_stream << s << "." << counter;
    }
  return boost::filesystem::path(parent/temp_stream.str());
}

#endif
