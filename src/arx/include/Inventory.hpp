/* Typedefs to make it easier to declare Inventory's and their
   iterators.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_INVENTORY_HPP
#define ARX_INVENTORY_HPP

#include <list>
#include "inventory_types.hpp"
#include "file_attributes.hpp"


class Inventory 
{
  std::list<file_attributes> inv[num_inventory_types][2];

public:
  std::list<file_attributes> & operator()(const int a, const int b)
  {
    return inv[a][b];
  }

  const std::list<file_attributes> & operator()(const int a, const int b) const
  {
    return inv[a][b];
  }

  typedef std::list<file_attributes>::iterator iterator;
  typedef std::list<file_attributes>::const_iterator const_iterator;
};

#endif
