/* Compare two patch levels numerically, not lexically.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_PATCH_LEVEL_CMP_HPP
#define ARX_PATCH_LEVEL_CMP_HPP

#include <string>
#include <cstdlib>
#include "boost/filesystem/path.hpp"

inline bool patch_level_cmp(const std::string a, const std::string b)
{
  unsigned int i=0;
  while(i<a.size())
    {
      if(a[i]=='-')
        {
          ++i;
          break;
        }
      ++i;
    }
  unsigned int j=0;
  while(j<b.size())
    {
      if(b[j]=='-')
        {
          ++j;
          break;
        }
      ++j;
    }

  return std::atoi(a.substr(i).c_str()) < std::atoi(b.substr(j).c_str());
}

inline bool patch_level_cmp_paths(boost::filesystem::path a,
                           boost::filesystem::path b)
{
  return patch_level_cmp(a.leaf(),b.leaf());
}

#endif
