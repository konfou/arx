/* Fill a path list with arguments from the command line or a file.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_FILL_PATH_LIST_HPP
#define ARX_FILL_PATH_LIST_HPP

#include <list>
#include <string>
#include "boost/filesystem/operations.hpp"
#include "Path_List.hpp"

extern bool fill_path_list(const boost::filesystem::path &project_root,
                           const std::string &paths_file,
                           std::list<std::string> &argument_list,
                           Path_List &path_list);

#endif
