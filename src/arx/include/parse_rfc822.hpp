/* Parses a rfc822 style file, returning the headers in a map and the
   body in a string.

   Copyright 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_PARSE_RFC822_HPP
#define ARX_PARSE_RFC822_HPP

#include "boost/filesystem/path.hpp"
#include <map>
#include <string>
#include "Parsed_Name.hpp"

void parse_rfc822(const boost::filesystem::path &log_name,
                  std::map<std::string,std::string> &headers,
                  std::string &body, const std::string endline="\n");

#endif
