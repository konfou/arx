/* A simple typedef for a list of files and their checksums

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_CHECKSUMS_HPP
#define ARX_CHECKSUMS_HPP

#include <utility>
#include <list>
#include "sha256.hpp"
#include "file_attributes.hpp"
#include "Parsed_Name.hpp"

class Checksums : public std::list<std::pair<file_attributes,sha256> >
{
public:
  Parsed_Name name;
};

inline bool checksum_inventory_id_cmp
(const std::pair<file_attributes,sha256> &a,
 const std::pair<file_attributes,sha256> &b)
{
  return inventory_id_cmp(a.first,b.first);
}

inline bool checksum_inventory_id_eq
(const std::pair<file_attributes,sha256> a,
 const std::pair<file_attributes,sha256> b)
{
  return inventory_id_eq(a.first,b.first);
}

inline bool checksums_first_file_path_eq_string
(const std::pair<std::pair<file_attributes,sha256>,
 std::pair<file_attributes,sha256> > a,
 const std::string b)
{
  return file_path_eq_string(a.first.first,b);
}

inline bool checksums_second_file_path_eq_string
(const std::pair<std::pair<file_attributes,sha256>,
 std::pair<file_attributes,sha256> > a,
 const std::string b)
{
  return file_path_eq_string(a.second.first,b);
}

inline bool checksums_file_path_eq_string
(const std::pair<file_attributes,sha256> a,
 const std::string b)
{
  return file_path_eq_string(a.first,b);
}

inline bool checksums_file_path_cmp
(const std::pair<file_attributes,sha256> a,
 const std::pair<file_attributes,sha256> b)
{
  return a.first.file_path.string() < b.first.file_path.string();
}


#endif
