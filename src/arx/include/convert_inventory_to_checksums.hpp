/* A simple utility to convert an inventory to a list of checksums.
   For files, it creates a dummy checksum, for directories an empty
   one.  We can use a dummy checksum because it is never actually
   used.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_CONVERT_INVENTORY_TO_CHECKSUMS_HPP
#define ARX_CONVERT_INVENTORY_TO_CHECKSUMS_HPP

#include "Checksums.hpp"
#include "Inventory.hpp"

extern void convert_inventory_to_checksums(const Inventory &inv,
                                           Checksums &checksums);
                                    
#endif
