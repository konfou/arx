/* A simple class to encapsulate all of the archive parsing.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_PARSED_NAME_HPP
#define ARX_PARSED_NAME_HPP

#include <string>
#include "patch_number.hpp"
#include "arx_error.hpp"
#include "patch_level.hpp"
#include <sstream>
#include "gvfs.hpp"

class Parsed_Name
{
  std::string Full_Name, Archive, Short_Revision;
  mutable gvfs::uri Archive_Location;
  void parse_package_name(bool require_archive=false);
  std::string compose_components(const char &separator) const
  {
    if(components.empty())
      return "";

    std::string result;
    std::list<std::string>::const_iterator i=components.begin();
    result=*i;
    ++i;
    for(; i!=components.end(); ++i)
      result+=separator + *i;
    return result;
  }

public:
  std::list<std::string> components;

  Parsed_Name(const std::string &full_name, bool require_archive=false)
    : Full_Name(full_name)
  {
    if(!Full_Name.empty())
      parse_package_name(require_archive);
  }
  Parsed_Name() {}

  bool empty() const
  {
    return Archive.empty() && components.empty();
  }

  unsigned int branch_depth() const
  {
    return components.size();
  }

  std::string full_name() const
  {
    return Full_Name;
  }
  std::string archive() const
  {
    return Archive;
  }

  gvfs::uri archive_location() const;

  std::string branch() const
  {
    return compose_components('.');
  }

  std::string revision() const
  {
    if(Short_Revision.empty())
      return Short_Revision;
    return branch() + "," + Short_Revision;
  }
  std::string short_revision() const
  {
    return Short_Revision;
  }

  std::string branch_path_no_archive() const
  {
    return compose_components('/');
  }

  std::string branch_path() const
  {
    if(empty())
      return std::string();
    return Archive + "/" + branch_path_no_archive();
  }

  std::string short_revision_path() const
  {
    return "," + Short_Revision;
  }

  std::string revision_path_no_archive() const
  {
    return branch_path_no_archive() + "/" + short_revision_path();
  }

  std::string revision_path() const
  {
    return branch_path() + "/" + short_revision_path();
  }

  std::string leaf() const
  {
    if(components.empty())
      return Archive;
    else
      return *(components.rbegin());
  }

  void append_branch(const std::string &s)
  {
    components.push_back(s);
  }

  Parsed_Name set_revision(const unsigned int &r)
  {
    Short_Revision=patch_level(r).substr(1);
    return *this;
  }

  Parsed_Name set_revision()
  {
    Short_Revision="";
    return *this;
  }

  bool operator<(const Parsed_Name p) const
  {
    if(complete_branch()!=p.complete_branch())
      {
        if(Short_Revision.empty() || p.Short_Revision.empty())
          throw arx_error("INTERNAL ERROR: These two names must both specify revisions:\n\t"
                          + Full_Name + "\n\t" + p.Full_Name);
        
        return complete_revision()<p.complete_revision();
      }
    return patch_number() < p.patch_number();
  }

  unsigned int patch_number() const
  {
    return ::patch_number(short_revision_path());
  }

  std::string complete_branch() const
  {
    if(empty())
      return std::string();
    return Archive + "/" + branch();
  }

  std::string complete_revision() const
  {
    if(empty())
      return std::string();
    return Archive + "/" + revision();
  }

  std::string complete_name() const
  {
    if(empty())
      return std::string();
    if(revision().empty())
      {
        return complete_branch();
      }
    else
      {
        return complete_revision();
      }
  }

  Parsed_Name parent_branch() const
  {
    Parsed_Name parent(*this);
    if(!parent.components.empty())
      parent.components.pop_back();
    return parent;
  }

  static bool Branch_Equal(const Parsed_Name p1, const Parsed_Name p2)
  {
    return p1.complete_branch()==p2.complete_branch();
  }

  static bool Branch_less(const Parsed_Name p1, const Parsed_Name p2)
  {
    return p1.complete_branch()<p2.complete_branch();
  }

  Parsed_Name operator--()
  {
    if(Short_Revision.empty())
      throw arx_error("INTERNAL ERROR: Trying to decrement a name without a revision" + Full_Name);
    if(patch_number()==0)
      throw arx_error("INTERNAL ERROR: Trying to decrement a revision below zero\n\t" + Full_Name);

    std::stringstream p;
    p << patch_number()-1;
    Short_Revision=p.str();
    Full_Name=Archive + "/" + revision();
    return *this;
  }

  bool operator==(const Parsed_Name p) const
  {
    return (Archive==p.Archive) && (branch()==p.branch())
      && (Short_Revision==p.Short_Revision);
  }
  bool operator!=(const Parsed_Name p) const
  {
    return !operator==(p);
  }

  bool is_subbranch(const Parsed_Name &p) const;
};

#endif
