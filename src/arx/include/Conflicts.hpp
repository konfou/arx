/* A simple class to hold all of the types of conflicts

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_CONFLICTS_HPP
#define ARX_CONFLICTS_HPP

#include <utility>
#include <list>
#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"
#include "Moved_Path.hpp"

class Conflicts
{
  boost::filesystem::path tree_directory;
  Conflicts();
  static void resolve_patch(const boost::filesystem::path &root,
                            std::pair<std::string,std::pair<std::string,
                            std::string> > &p)
  {
    remove(root/p.second.first);
    remove(root/p.second.second);
  }
  static void resolve_xdelta(const boost::filesystem::path &root,
                             std::pair<std::string,std::string> &p)
  {
    remove(root/p.second);
  }
  static void resolve_merge(const boost::filesystem::path &root,
                            std::pair<std::pair<std::string,std::string>,
                            std::pair<std::string,std::string> > &p)
  {
    remove(root/p.first.second);
    remove(root/p.second.first);
    remove(root/p.second.second);
  }


public:
  std::list<std::pair<std::string,std::string> > move_target;
  std::list<std::pair<std::string,
                      std::pair<std::string,
                                std::string> > > move_parent;
  std::list<std::pair<std::string,
                      std::pair<std::string,
                                std::string> > > rename;
  std::list<std::pair<std::string,std::string> > deleted_parent;
  std::list<std::pair<std::string,std::string> > no_parent;
  std::list<std::pair<std::string,
                      std::pair<std::string,
                                std::string> > > patch;
  std::list<std::pair<std::string,std::string> > xdelta;
  std::list<std::pair<std::pair<std::string,std::string>,
                      std::string> > missing_moves;
  std::list<std::string> missing_patches;
  std::list<std::pair<std::pair<std::string,std::string>,std::string> > add;
  std::list<std::pair<std::pair<std::string,std::string>,
                      std::pair<std::string,std::string> > > merge;
  std::list<std::pair<std::pair<std::string,std::string>,
                      std::pair<std::string,std::string> > > directory_loop;

  Conflicts(const boost::filesystem::path &Tree_directory,
            bool load_from_tree=false);

  bool empty()
  {
    return move_target.empty() && move_parent.empty() && rename.empty()
      && deleted_parent.empty() && no_parent.empty() && patch.empty()
      && missing_moves.empty() && missing_patches.empty() && add.empty()
      && merge.empty() && directory_loop.empty();
  }
  void save_to_conflicts_file(const bool append=true);
  template <class Archive> void serialize(Archive &ar,
                                          const unsigned int version)
  {
    ar & move_target;
    ar & move_parent;
    ar & rename;
    ar & deleted_parent;
    ar & no_parent;
    ar & patch;
    ar & xdelta;
    ar & missing_moves;
    ar & missing_patches;
    ar & add;
    ar & merge;
    ar & directory_loop;
  }
  static bool all_resolved(const boost::filesystem::path &Tree_directory);
  static void resolve(const boost::filesystem::path &Tree_directory,
                      const boost::filesystem::path &conflict);
  static void resolve_all(const boost::filesystem::path &Tree_directory);

  friend std::ostream & operator<<(std::ostream &s,
                                   const Conflicts &conflicts);
};


#endif
