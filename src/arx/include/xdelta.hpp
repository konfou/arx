#ifndef ARX_XDELTA_HPP
#define ARX_XDELTA_HPP

// copyright (C) 2002, 2003 graydon hoare <graydon@pobox.com>
// all rights reserved.
// licensed to the public under the terms of the GNU GPL (>= 2)
// see the file COPYING for details

// Modified for use in ArX
// Copyright (C) 2004 Walter Landry

#include <string>

void 
compute_xdelta(std::string const & a,
               std::string const & b,
               std::string & delta);

void
apply_xdelta(std::string const & a,
             std::string const & delta,
             std::string & b);


#endif // ARX_XDELTA_HPP
