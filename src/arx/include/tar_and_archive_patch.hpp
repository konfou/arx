/* Tar's up and stores in the archive a patch.  The patch must be in
   the subdirectory temp_dir/revision.patches, the log is in
   temp_dir/log, and a continuation file is in temp_dir/CONTINUATION.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_TAR_AND_ARCHIVE_PATCH_HPP
#define ARX_TAR_AND_ARCHIVE_PATCH_HPP

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"

void tar_and_archive_patch(const boost::filesystem::path &temp_dir,
                           const Parsed_Name &previous,
                           const Parsed_Name &name,
                           const bool &continuation,
                           const boost::filesystem::path &root,
                           const std::string &gpg_key,
                           const bool log_only=false);

#endif
