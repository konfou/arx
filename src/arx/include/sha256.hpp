/* A simple class to hold checksums of files and strings.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_SHA256_HPP
#define ARX_SHA256_HPP

#include <vector>
#include <iostream>
#include "boost/filesystem/path.hpp"
#include <string>
#include <sstream>

class sha256
{
  std::vector<unsigned char> internal_hash;
  void hash_string(const std::string &s);
public:
  /* To hash files */
  sha256(const boost::filesystem::path &p);
  /* To hash strings */
  sha256(const std::string &s);
  sha256(const char *c);

  sha256() {}

  /* Convenience operators */
  sha256 & operator=(const sha256 &s)
  {
    internal_hash=s.internal_hash;
    return *this;
  }

  bool operator==(const sha256 &s) const
  {
    return internal_hash==s.internal_hash;
  }

  bool operator!=(const sha256 &s) const
  {
    return !operator==(s);
  }

  bool operator<(const sha256 &s) const
  {
    return internal_hash<s.internal_hash;
  }

  bool operator>(const sha256 &s) const
  {
    return internal_hash>s.internal_hash;
  }

  bool operator<=(const sha256 &s) const
  {
    return internal_hash<=s.internal_hash;
  }

  bool operator>=(const sha256 &s) const
  {
    return internal_hash>=s.internal_hash;
  }

  bool empty() const
  {
    return internal_hash.empty();
  }

  std::string string() const
  {
    std::stringstream s;
    operator<<(s,*this);
    return s.str();
  }

  void serialize(std::ostream &s) const;
  void deserialize(std::istream &s);

  friend std::ostream & operator<<(std::ostream &s, const sha256 &sh);
  friend std::istream & operator>>(std::istream &s, sha256 &sh);
};

    
#endif
