/* Find the latest revision that is a continuation in the history of a
   particular revision.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_COMPUTE_LATEST_CONTINUATION_HPP
#define ARX_COMPUTE_LATEST_CONTINUATION_HPP

#include <string>
#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"

/* A version that looks in the project tree patch logs first if they
   exist, otherwise it bails to looking at the archive. */

std::string compute_latest_continuation(const boost::filesystem::path &root,
                                        const Parsed_Name &previous);

/* A version that just looks in the archive. */
      
std::string compute_latest_continuation(const Parsed_Name &previous);

#endif
