/* Rewrite the ++changes file to delete an inventory id.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_UNSAFE_DELETE_INVENTORY_ID_HPP
#define ARX_UNSAFE_DELETE_INVENTORY_ID_HPP

#include "boost/filesystem/operations.hpp"
#include "file_attributes.hpp"

extern void unsafe_delete_inventory_id(const boost::filesystem::path &root,
                                       const file_attributes &f,
                                       const bool &is_directory);

#endif
