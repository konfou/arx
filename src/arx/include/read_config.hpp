/* Read and parse a configuration file

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_READ_CONFIG_HPP
#define ARX_READ_CONFIG_HPP

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"
#include <list>
#include <utility>

extern std::list<std::pair<boost::filesystem::path,Parsed_Name> >
read_config(const boost::filesystem::path &config_path);

#endif
