/* A simple structure to hold the various static characteristics of a
   command (name, help message, function pointer).

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_COMMAND_HPP
#define ARX_COMMAND_HPP

#include <string>
#include <list>

class command
{
public:
  std::string name, brief_help, usage, long_help, command_group;
  int (*action)(std::list<std::string> &, const command &);
  bool visible_by_default;

  command(const std::string &Name, const std::string &Brief_Help,
          const std::string &Usage, const std::string &Long_Help,
          int (*Action)(std::list<std::string> &, const command &),
          const std::string &Command_group, const bool &visible):
    name(Name), brief_help(Brief_Help), usage(Usage), long_help(Long_Help),
    command_group(Command_group), action(Action), visible_by_default(visible)
  {}

  command() {}

  int eval(std::list<std::string> &argument_list)
  {
    return action(argument_list,*this);
  }

  bool operator==(const std::string &s) const
  {
    return name==s;
  }

};

/* Define equality between commands and string so that we can use
   find() */

inline bool operator==(const std::string &s, const command &c)
{
  return c.name==s;
}

#endif
