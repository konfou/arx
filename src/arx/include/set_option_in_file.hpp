/* Set an option in a file, overwriting whatever is there.  So if the
   input string is empty, it unsets the option.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_SET_OPTION_IN_FILE_HPP
#define ARX_SET_OPTION_IN_FILE_HPP

#include <string>
#include "boost/filesystem/path.hpp"

extern void set_option_in_file(const boost::filesystem::path
                               &config_path,  const std::string &input);

#endif
