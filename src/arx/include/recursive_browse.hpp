/* A templated function to recursively go through a directory
   hierarchy, either local or remote, and perform actions for each
   branch and revision.

   The location should be the place in the archive to start.  For
   example, if you are browsing foo/bar.baz, then the location should
   be something like foo/bar/baz.

   The functor provides branch and revision hooks, and some functions
   (is_directory, fill_directory_list, limit).

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_RECURSIVE_BROWSE_HPP
#define ARX_RECURSIVE_BROWSE_HPP

#include "Component_Regexes.hpp"
#include "Revision_List.hpp"

template<class T, class U>
void recursive_browse(const U &location, const Parsed_Name &name, T &functor)
{
  Component_Regexes components;

  if(!T::exists(location))
    {
      std::string error_string="This branch does not exist in this archive\n\t"
        + name.complete_branch();
      std::string full_name=name.full_name();
      if(find(full_name.begin(),full_name.end(),':')!=full_name.end()
         || find(full_name.begin(),full_name.end(),'/')==full_name.end())
        error_string+="\nDid you forget to add a slash \"/\" to the end?";
      throw arx_error(error_string);
    }

  if(functor.limit(name) && T::is_directory(location))
    {
      functor.branch_hook(name,location);
      std::list<std::string> branch_list;
      T::fill_directory_list(branch_list,location);
      Revision_List rev_list(name);
      for(std::list<std::string>::iterator branches=branch_list.begin();
          branches!=branch_list.end(); ++branches)
        {
          if(regex_match(*branches,components.revision))
            {
              rev_list.push_back(patch_number(*branches));
            }
        }
      if(!rev_list.empty())
        {
          rev_list.sort();
          functor.revision_hook(name,rev_list,location);
        }
      for(std::list<std::string>::iterator branches=branch_list.begin();
          branches!=branch_list.end(); ++branches)
        {
          if((*branches)[0]!=',' && (*branches)!=".listing")
            {
              Parsed_Name new_name(name.complete_branch());
              new_name.append_branch(*branches);
              recursive_browse(location/(*branches),new_name,functor);
            }
        }
    }
}

#endif
