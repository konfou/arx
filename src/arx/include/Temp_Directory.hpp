/* A simple class to automatically clean up a temporary directory.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_TEMP_DIRECTORY_HPP
#define ARX_TEMP_DIRECTORY_HPP

#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "Temp_Path.hpp"

class Temp_Directory
{
public:
  const boost::filesystem::path path;

  Temp_Directory(const boost::filesystem::path &parent, const std::string &s,
                 bool make_dir=true): path(Temp_Path(parent,s))
  {
    /* We throw here instead of deleting the path, since it seems
       dangerous to delete whatever is passed to this function. */
    if(make_dir)
      {
        if(lexists(path))
          throw arx_error("This temp directory already exists: "
                          + path.native_file_string());
        boost::filesystem::create_directory(path);
      }
  }    

  ~Temp_Directory()
  {
    boost::filesystem::remove_all(path);
  }
};

#endif
