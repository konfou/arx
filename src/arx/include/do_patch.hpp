/* Apply a patch to a source tree.  There are two versions, one for
   exact patching and one for inexact patching.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  Copyright (C) 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_DO_PATCH_HPP
#define ARX_DO_PATCH_HPP

#include "boost/filesystem/operations.hpp"
#include "Conflicts.hpp"

extern Conflicts do_patch(const boost::filesystem::path &patch,
                          const boost::filesystem::path &source,
                          const bool &reverse,
                          const bool &delete_removed,
                          bool apply_content_patches=true,
                          bool record_missing=true);

extern void do_patch(const boost::filesystem::path &patch,
                     const boost::filesystem::path &source,
                     const bool &reverse);

#endif
