/* Do the actual work of retrieving a patch set from an archive

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                            of the University of California
  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_GET_REVISION_PATCH_HPP
#define ARX_GET_REVISION_PATCH_HPP

#include "Parsed_Name.hpp"
#include "boost/filesystem/path.hpp"

void get_revision_patch(const Parsed_Name &name,
                        const boost::filesystem::path &target,
                        const bool verify=true);

#endif
