/* Regular expressions for the various components of a fully qualified
   revision (archive, branch, revision)

   Copyright (C) 2001, 2002 Tom Lord
   Copyright 2002, 2003 Walter Landry and the Regents of the
   University of California.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_COMPONENT_REGEXES_HPP
#define ARX_COMPONENT_REGEXES_HPP

#include "boost/regex.hpp"

class Component_Regexes
{
public:
  boost::regex archive, component, revision;

  Component_Regexes():
    archive("[^:/,]+"),
    component("[^:/\\.,]+"),
    revision(",[[:digit:]]+") {}
};

#endif
