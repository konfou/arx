/* Parse options that are common to all commands (--help and
   verbosity).  If an option is parsed, then it returns true.
   Otherwise, it returns false.

   Copyright 2003 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_PARSE_COMMON_OPTIONS_HPP
#define ARX_PARSE_COMMON_OPTIONS_HPP

#include <string>
#include <list>
#include "Command_Info.hpp"

extern bool parse_common_options(std::list<std::string> &argument_list,
                                 Command_Info &info);

#endif
