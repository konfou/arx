/* A simple typedef for a list of paths for make_patch, including a
   bool for whether we are doing a straight diff in a no-edit tree.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_PATH_LIST_HPP
#define ARX_PATH_LIST_HPP

#include <list>
#include "boost/filesystem/path.hpp"

class Path_List : public std::list<boost::filesystem::path>
{
public:
  bool no_edit_diff;
  Path_List(): no_edit_diff(false) {}
};

#endif
