/* List all of the branchs in a project tree.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_LIST_TREE_BRANCHES_HPP
#define ARX_LIST_TREE_BRANCHES_HPP

#include <list>
#include "Parsed_Name.hpp"
#include "boost/filesystem/operations.hpp"

std::list<Parsed_Name> list_tree_branches
(const boost::filesystem::path &tree_directory,
 Parsed_Name limit=Parsed_Name());

#endif
