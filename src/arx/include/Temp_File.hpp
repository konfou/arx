/* A simple class to automatically clean up a temporary file.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_TEMP_FILE_HPP
#define ARX_TEMP_FILE_HPP

#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "Temp_Path.hpp"

class Temp_File
{
public:
  const boost::filesystem::path path;

  Temp_File(const boost::filesystem::path &parent,const std::string &s):
    path(Temp_Path(parent,s)) {}
  Temp_File(const boost::filesystem::path &parent,const char *s):
    path(Temp_Path(parent,s)) {}

  /* A separate contructor to allow you to specify which file will be
     deleted. */
  Temp_File(const boost::filesystem::path &p): path(p){}
  ~Temp_File()
  {
    boost::filesystem::remove(path);
  }
};

#endif
