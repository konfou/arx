/* Get a particular revision, starting with a full-tree revision
   cached somewhere (in a project tree or archive) and then patching.
   If hard_link is true, then it attempts to hard link the result.
   It returns the full name and hash of the revision that was fetched.

   Copyright 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_GET_REVISION_HPP
#define ARX_GET_REVISION_HPP

#include "Parsed_Name.hpp"
#include "Command_Info.hpp"
#include "boost/filesystem/path.hpp"
#include "sha256.hpp"

std::pair<Parsed_Name,sha256> get_revision
(const Parsed_Name &target,
 const boost::filesystem::path &target_dir,
 const boost::filesystem::path &cache_dir,
 const bool hard_link=false);

std::pair<Parsed_Name,sha256> get_revision
(const Parsed_Name &target,
 const boost::filesystem::path &target_dir,
 const boost::filesystem::path &cache_dir,
 const bool &no_edit, const bool &pristine,
 const bool &link_tree);

#endif
