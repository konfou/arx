/* Get the name of the gpg executable.  It splits it up if there are
   any spaces, so that you can add extra arguments (e.g. --no-tty or
   --batch).

   Copyright (C) 2004, 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_GET_GPG_NAME_HPP
#define ARX_GET_GPG_NAME_HPP

#include "get_config_option.hpp"
#include "boost/regex.hpp"

namespace {
  std::list<std::string> get_gpg_name()
  {
    std::list<std::string> result;

    std::string gpg=get_config_option("gpg");
    if(!gpg.empty())
      {
        regex_split(back_inserter(result),gpg,boost::regex(" "));
        return result;
      }
#ifdef ARXGPG
    result.push_back(ARXGPG);
#endif
    return result;
  }
};
#endif
