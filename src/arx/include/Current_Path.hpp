/* Change the current directory, saving the old one.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_CURRENT_PATH_HPP
#define ARX_CURRENT_PATH_HPP

#include "boost/filesystem/operations.hpp"


class Current_Path
{
  const boost::filesystem::path old_path, new_path;
public:
  Current_Path(const boost::filesystem::path &New_Path):
    old_path(boost::filesystem::current_path()), new_path(New_Path)
  {
    boost::filesystem::current_path(new_path);
  }
  ~Current_Path()
  {
    boost::filesystem::current_path(old_path);
  }
};

#endif
