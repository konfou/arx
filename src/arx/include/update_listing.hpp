/* Recursively updates the .listing file for a branch and any cached
   revisions.

   Copyright 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_UPDATE_LISTING_HPP
#define ARX_UPDATE_LISTING_HPP

#include "Parsed_Name.hpp"
#include "remote_recursive_browse.hpp"
#include "recursive_browse.hpp"
#include "Temp_File.hpp"
#include "boost/filesystem/fstream.hpp"

class recursive_update: public remote_recursive_browse
{
public:
  recursive_update(const Parsed_Name &Name): remote_recursive_browse(Name) {}

  void make_new_listing(const gvfs::uri &location)
  {
    std::list<std::string> branch_list;
    fill_directory_list(branch_list,location);
    Temp_File listing(",,listing");
    {
      boost::filesystem::ofstream listing_file(listing.path);
      for(std::list<std::string>::iterator i=branch_list.begin();
          i!=branch_list.end(); ++i)
        listing_file << *i << std::endl;
    }
    gvfs::copy(listing.path.native_file_string(),location/".listing");
  }

  /* We only have to redefine the branch hook */
  void branch_hook(const Parsed_Name &Name, const gvfs::uri &location)
  {
    make_new_listing(location);
    /* Make sure to get the list of cached revisions as well. */
    if(gvfs::exists(location/",cache"))
      make_new_listing(location/",cache");
  }
};

inline void update_listing(const Parsed_Name &name)
{
  recursive_update update(name);
  recursive_browse(name.archive_location()/name.branch_path_no_archive(),
                   name,update);
}

#endif
