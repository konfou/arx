/* An enumeration of all of the various inventory types and some
   related constants.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_INVENTORY_TYPES_HPP
#define ARX_INVENTORY_TYPES_HPP

#include <string>
#include "boost/filesystem/path.hpp"

enum inventory_types {source, ignore, control, unrecognized, nested_tree,
                      num_inventory_types };

enum {arx_dir, arx_file};

inline bool is_control(const std::string &s)
{
  return s.substr(0,4)=="_arx";
}

inline bool is_control(const boost::filesystem::path &p)
{
  return is_control(p.string());
}

#endif
