/* A small C++ wrapper for the g_spawn_* functions from glib.  It
   returns -1 on error, otherwise whatever the return value is.

   This shows how awful C code can get when written by someone too
   used to C++.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_SPAWN_HPP
#define ARX_SPAWN_HPP

#include <list>
#include <string>
#include "boost/filesystem/path.hpp"
#include <glib.h>

extern char **environ;

class Spawn
{
  friend Spawn& operator<<(Spawn &s, const std::string &arg);

  std::list<std::string> commands;
  std::list<std::string> environment;
public:
  boost::filesystem::path output, error;
  GSpawnFlags flags;

  Spawn(): flags(G_SPAWN_SEARCH_PATH)
  {
    for(int i=0;environ[i]!=NULL;++i)
      environment.push_back(environ[i]);
  }

  /* Do the actual spawning. */
  bool execute(int &return_exit_status, bool sync) const;

  bool execute(bool sync)
  {
    int return_exit_status;
    return execute(return_exit_status,sync);
  }

  Spawn& add_env(const std::string &label, const std::string &value)
  {
    environment.push_back(label+"="+value);
    return *this;
  }

};

inline Spawn& operator<<(Spawn &s, const std::string &arg)
{
  s.commands.push_back(arg);
  return s;
}

#endif
