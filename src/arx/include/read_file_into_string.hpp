/* A simple function to read a complete file into a string

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_READ_FILE_INTO_STRING_HPP
#define ARX_READ_FILE_INTO_STRING_HPP

#include <string>
#include "boost/filesystem/operations.hpp"

std::string read_file_into_string(std::istream &infile);
std::string read_file_into_string(const boost::filesystem::path &location);

#endif
