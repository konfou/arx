/* A class to hold all of the flags that might be set when
   inventorying a directory.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_INVENTORY_FLAGS_HPP
#define ARX_INVENTORY_FLAGS_HPP

#include "Inventory_Regexes.hpp"
#include "inventory_types.hpp"
#include "Command_Info.hpp"

class Inventory_Flags
{
public:
  bool control, nested, include_subdir,
    source, ignore, unrecognized, trees,
    directories, files, kind, ids, disambiguate;

  Inventory_Regexes regexes;

  Inventory_Flags(): control(false), nested(false),
                     include_subdir(false),
                     source(false), ignore(false),
                     unrecognized(false),
                     trees(false),
                     directories(false), files(false),
                     kind(false), ids(false),
                     disambiguate(true) {};
  Inventory_Flags(const boost::filesystem::path &dir): 
    control(false), nested(false),
    include_subdir(false),
    source(false), ignore(false),
    unrecognized(false),
    trees(false),
    directories(false), files(false),
    kind(false), ids(false),
    disambiguate(true),
    regexes(dir){};
};

#endif
