/* Return the latest revision present in the archive

  Copyright (C) 2004, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_LATEST_ARCHIVE_REVISION_HPP
#define ARX_LATEST_ARCHIVE_REVISION_HPP

#include "Parsed_Name.hpp"

extern Parsed_Name latest_archive_revision(const Parsed_Name &name);

#endif
