/* A class to hold all of the regexes for deciding what type a file is
   when doing an inventory.

   Copyright (C) 2003, 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_INVENTORY_REGEXES_HPP
#define ARX_INVENTORY_REGEXES_HPP

#include "boost/regex.hpp"
#include "tree_root.hpp"
#include <string>
#include "boost/filesystem/operations.hpp"
#include "arx_error.hpp"
#include "get_option_from_file.hpp"

class Inventory_Regexes
{
  boost::regex ignore, global_ignore;
public:
  Inventory_Regexes(): ignore("^$"), global_ignore("^,,.*$") {}

  Inventory_Regexes(const boost::filesystem::path &dir):
    ignore("^$"), global_ignore("^,,.*$")
  {
    set(dir);
  }

  void set(const boost::filesystem::path &dir)
  {
    /* Added a space before "_arx/ignore" to placate swig. */
    const boost::filesystem::path ignore_path(tree_root(dir)/ "_arx/ignore");
    if(lexists(ignore_path))
      {
        ignore=boost::regex(get_option_from_file(ignore_path));
      }
  }

  Inventory_Regexes & operator=(const Inventory_Regexes &i)
  {
    ignore=i.ignore;
    return *this;
  }

  bool match(const std::string &s) const
  {
    return regex_match(s,global_ignore) || regex_match(s,ignore);
  }
};

#endif
