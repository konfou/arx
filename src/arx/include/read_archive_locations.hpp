/* Read in the archive locations from a file.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_READ_ARCHIVE_LOCATIONS_HPP
#define ARX_READ_ARCHIVE_LOCATIONS_HPP

#include "boost/filesystem/fstream.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/serialization/list.hpp"
#include "Parsed_Name.hpp"

inline std::list<std::string> read_archive_locations
(const boost::filesystem::path &p)
{
  std::list<std::string> locations;
  boost::filesystem::ifstream is(p);
  boost::archive::text_iarchive ia(is);
  ia >> locations;
  return locations;
}

inline std::list<std::string> read_archive_locations
(const Parsed_Name &name)
{
  boost::filesystem::path home(getenv("HOME"));
  boost::filesystem::path config_path(home / ".arx/archives" / name.archive()
                                      / "uri");
  if(!lexists(config_path))
    throw arx_error("This archive is not registered:\n\t" + name.archive());
  return read_archive_locations(config_path);
}

#endif
