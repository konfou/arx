/* Check whether a particular path is being accessed through symlinks.
   This is a security check to make sure that a patch doesn't try to
   change things through symlinks.

  Copyright (C) 2003 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_CHECK_SYMLINK_HIERARCHY_HPP
#define ARX_CHECK_SYMLINK_HIERARCHY_HPP

#include "boost/filesystem/operations.hpp"

extern void check_symlink_hierarchy(const boost::filesystem::path &root,
                                    const boost::filesystem::path &tail);

#endif
