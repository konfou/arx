/* Add a cached revision to a tree.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_ADD_TREE_CACHE_HPP
#define ARX_ADD_TREE_CACHE_HPP

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"

extern void add_tree_cache(const boost::filesystem::path &tree_directory,
                           const boost::filesystem::path &cache_dir,
                           const Parsed_Name &input_revision);

#endif
