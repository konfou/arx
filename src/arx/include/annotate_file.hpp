/* Annotate a file.  It takes in a filename and outputs two lists.
   The first list has which patch modified that line, and the second
   list has the lines of the file itself.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_ANNOTATE_FILE_HPP
#define ARX_ANNOTATE_FILE_HPP

#include "boost/filesystem/operations.hpp"
#include <vector>
#include <utility>
#include <string>

std::pair<std::vector<std::string>, std::vector<std::string> > annotate_file
(const boost::filesystem::path &file);

#endif
