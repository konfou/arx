/* A class to lock a branch for commiting.  It expects a full branch
   in the constructor.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_BRANCH_LOCK_HPP
#define ARX_BRANCH_LOCK_HPP

#include "gvfs.hpp"
#include "Parsed_Name.hpp"
#include "arx_error.hpp"
#include "get_config_option.hpp"
#include "valid_package_name.hpp"

class Branch_Lock
{
  gvfs::uri base_uri;
public:
  Branch_Lock(const Parsed_Name &full_branch)
  {
    valid_package_name(full_branch,Branch);

    base_uri=full_branch.archive_location()
      / full_branch.branch_path_no_archive();
    
    gvfs::uri uid(base_uri/ ",,uid");

    try
      {
        gvfs::make_directory(base_uri / "++revision_lock");
        gvfs::write_archive(uid,get_config_option("id"));
      }
    catch (const gvfs::exception &ex)
      {
        if(ex.error_code==GNOME_VFS_ERROR_FILE_EXISTS)
          {
            try
              {
                throw arx_error("No commits allowed.  The lock on\n\t"
                                + full_branch.complete_branch()
                                + "\nis held by "
                                + gvfs::read_archive(uid));
              }
            catch (const gvfs::exception &ex2)
              {}

            throw arx_error("No commits allowed.  This revision is locked, but I don't know who\nholds the lock.\n\t" + full_branch.complete_branch());
          }
      }
  }

  ~Branch_Lock()
  {
    gvfs::remove((base_uri / ",,uid").string());
    gvfs::remove_all(base_uri / "++revision_lock");
  }

  gvfs::uri uri()
  {
    return base_uri / "++revision_lock";
  }
};

#endif
