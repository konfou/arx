/* Make sure that a particular branch exists in the archive.  It
   calls hooks if anything is created.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_ENSURE_BRANCH_EXISTS_IN_ARCIHVE_HPP
#define ARX_ENSURE_BRANCH_EXISTS_IN_ARCIHVE_HPP

#include "Parsed_Name.hpp"

extern void ensure_branch_exists_in_archive(const Parsed_Name &p);

#endif
