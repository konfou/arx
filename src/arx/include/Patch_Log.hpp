/* A simple class to hold the contents of a patch log.

   Copyright 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_PATCH_LOG_HPP
#define ARX_PATCH_LOG_HPP

#include <list>
#include <utility>
#include <string>
#include <map>
#include "Parsed_Name.hpp"

class Patch_Log
{
  void read_patch_log(std::istream &s);
public:
  std::map<std::string,std::string> headers;
  std::map<std::string,std::list<std::string> > header_lists;
  std::map<std::string,std::list<std::pair<std::string,std::string> > >
  rename_lists;
  std::string body;

  bool empty()
  {
    return headers.empty() && header_lists.empty()
      && rename_lists.empty() && body.empty();
  }
  
  /* Constructors */
  Patch_Log() {}
  Patch_Log(const boost::filesystem::path &log_name);
  Patch_Log(const std::string &log_string);
  Patch_Log(const boost::filesystem::path &tree_directory,
            const Parsed_Name &name);
  Patch_Log(const Parsed_Name &name);

  /* Take all of the elements in the header_lists and rename_lists and
     stuff it into the headers. */
  void compress_lists()
  {
    for(std::map<std::string,std::list<std::string> >::iterator
          i=header_lists.begin(); i!=header_lists.end(); ++i)
      {
        for(std::list<std::string>::iterator j=i->second.begin();
            j!=i->second.end(); ++j)
          {
            headers[i->first]+="\n\t" + *j;
          }
      }
    header_lists.clear();

    for(std::map<std::string,std::list<std::pair<std::string,std::string> > >::iterator
          i=rename_lists.begin(); i!=rename_lists.end(); ++i)
      {
        for(std::list<std::pair<std::string,std::string> >::iterator
              j=i->second.begin(); j!=i->second.end(); ++j)
          {
            headers[i->first]+="\n\t" + j->first + " -> " + j->second;
          }
      }
    rename_lists.clear();
  }

  /* Can't use this serialization because it gives the wrong results
     because of bugs in the serialization library. */
//   template<class Archive>
//   void serialize(Archive & ar, const unsigned int version)
//   {
//     ar & headers;
//     ar & header_lists;
//     ar & rename_lists;
//     ar & body;
//   }
 };

#endif
