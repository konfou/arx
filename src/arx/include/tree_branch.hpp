/* Get or set the default branch for the current project tree.

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                                  University of California
   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_TREE_BRANCH_HPP
#define ARX_TREE_BRANCH_HPP

#include "boost/filesystem/path.hpp"
#include "get_option_from_file.hpp"
#include "tree_root.hpp"
#include "Parsed_Name.hpp"
#include "set_option_in_file.hpp"
#include "valid_package_name.hpp"

inline Parsed_Name tree_branch(const boost::filesystem::path &tree_directory)
{
  return Parsed_Name(get_option_from_file(tree_root(tree_directory)
                                          / "_arx/++default-branch"));
}

inline void tree_branch(const boost::filesystem::path &tree_directory,
                        const Parsed_Name &name)
{
  valid_package_name(name,Branch);
  set_option_in_file(tree_root(tree_directory)
                     / "_arx/++default-branch",name.complete_branch());
}

#endif
