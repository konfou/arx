/* A simple class to hold information about a patch and print its
   contents

   Copyright 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_PATCH_INFO_HPP
#define ARX_PATCH_INFO_HPP

#include <list>
#include <utility>
#include <string>
#include <iostream>
#include "boost/filesystem/path.hpp"

class Patch_Info
{
public:
  std::list<std::pair<std::string,std::string> > regular_patched, moved[2];
  std::list<std::string> patched[2], orig_only[2], mod_only[2],
    symlink_patched, binary_patched, metadata_patched;
  const bool diffs, include_control;
  bool html;
  std::string title,link_root;

  Patch_Info(const bool &Diffs, const bool &Html,
             const std::string &Title, const std::string &Link_root,
             bool Include_control=false):
    diffs(Diffs), include_control(Include_control), html(Html),
    title(Title), link_root(Link_root) {}

  Patch_Info(const boost::filesystem::path &patch_dir,
             const bool &Diffs, const bool &Html,
             const std::string &Title, const std::string &Link_root,
             bool Include_control=false):
    diffs(Diffs), include_control(Include_control), html(Html),
    title(Title), link_root(Link_root)
  {
    append(patch_dir);
  }
  void append(const boost::filesystem::path &patch_dir,
              const std::string prefix="");
  friend std::ostream & operator<<(std::ostream &os, const Patch_Info &pi);
};

#endif
