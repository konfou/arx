/* Read in the archive locations from a file.

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_READ_ARCHIVE_LOCATIONS_HPP
#define ARX_READ_ARCHIVE_LOCATIONS_HPP

#include "boost/filesystem/fstream.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/serialization/list.hpp"
#include "Parsed_Name.hpp"

inline bool registered_archive(const Parsed_Name &name)
{
  boost::filesystem::path home(getenv("HOME"));
  boost::filesystem::path config_path(home / ".arx/archives" / name.archive()
                                      / "uri");
  return lexists(config_path);
}

#endif
