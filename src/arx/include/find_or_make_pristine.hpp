/* Find where a pristine tree is for a given tree.
   
  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents of the
                           University of California
  Minor bug fix 2003 Chris Gray
  Copyright (C) 2005 Walter Landry

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_FIND_PRISTINE_HPP
#define ARX_FIND_PRISTINE_HPP

#include "boost/filesystem/operations.hpp"
#include "Parsed_Name.hpp"

boost::filesystem::path find_or_make_pristine
(const boost::filesystem::path &tree, const boost::filesystem::path &temp_path,
 Parsed_Name P=Parsed_Name());

#endif
