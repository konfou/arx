/* A class to represent files that are being deleted or renamed.
   Deleted files are renamed to "".

  Copyright (C) 2003, 2005 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_MOVED_PATH_HPP
#define ARX_MOVED_PATH_HPP

#include <list>
#include <string>
#include "boost/filesystem/path.hpp"
#include "Inventory.hpp"

class Moved_Path
{
public:
  boost::filesystem::path initial, destination, patch_initial,
    patch_destination, local_path;
  std::string inventory_id, local_parent_id, leaf;
  std::list<std::string> destination_parent_ids;
  bool is_directory, deleted, directory_loop;
  Inventory::iterator inventory_iterator;
  Moved_Path(const std::string &Initial, const std::string &Destination,
             const std::string &Inventory_id, const bool &Is_directory) :
    initial(Initial), destination(Destination), inventory_id(Inventory_id),
    is_directory(Is_directory), deleted(false), directory_loop(false) {}
private:
  Moved_Path();
};

inline bool Moved_Path_inventory_id_cmp(const Moved_Path &m1,
                                         const Moved_Path &m2)
{
  return m1.inventory_id < m2.inventory_id;
}

inline bool Moved_Path_inventory_id_eq_string(const Moved_Path m1,
                                              const std::string m2)
{
  return m1.inventory_id == m2;
}

inline bool Moved_Path_initial_eq_string(const Moved_Path m1,
                                         const std::string s)
{
  return m1.initial.string() == s;
}

inline bool Moved_Path_initial_cmp(const Moved_Path m1,
                                   const Moved_Path m2)
{
  return m1.initial.string() < m2.initial.string();
}

inline bool Moved_Path_destination_cmp(const Moved_Path &m1,
                                       const Moved_Path &m2)
{
  return m1.destination.string() < m2.destination.string();
}

#endif
