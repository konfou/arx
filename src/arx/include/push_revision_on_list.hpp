/* A class for use with recursive_browse that pushes a list of all
   revisions onto a list.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_PUSH_REVISION_ON_LIST_HPP
#define ARX_PUSH_REVISION_ON_LIST_HPP

#include "Revision_List.hpp"
#include "Parsed_Name.hpp"
#include <string>
#include <list>
#include "boost/filesystem/operations.hpp"

class push_revision_on_list
{
  std::list<Revision_List> &result;
  const Parsed_Name &limit_name;
public:
  push_revision_on_list(std::list<Revision_List> &Result,
                        const Parsed_Name &Name):
    result(Result), limit_name(Name) {}
  bool limit(const Parsed_Name &name)
  {
    return name.is_subbranch(limit_name) || limit_name.is_subbranch(name);
  }
  void branch_hook(const Parsed_Name &Name,
                   const boost::filesystem::path &location) {}
  void revision_hook(const Parsed_Name &branch, const Revision_List &rev_list,
                     const boost::filesystem::path &location)
  {
    if(rev_list.name.is_subbranch(limit_name))
      result.push_back(rev_list);
  }

  static bool exists(const boost::filesystem::path &p)
  {
    return boost::filesystem::exists(p);
  }
  static bool is_directory(const boost::filesystem::path &p)
  {
    return boost::filesystem::is_directory(p);
  }
  static void fill_directory_list(std::list<std::string> &branch_list,
                                  const boost::filesystem::path &location)
  {
    for(boost::filesystem::directory_iterator i(location);
        i!=boost::filesystem::directory_iterator(); ++i)
      branch_list.push_back(i->leaf());
  }
};

#endif
