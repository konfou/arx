/* A simple class to hold a list of revision numbers plus the archive,
   and branch associated with it.

   Copyright 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_REVISION_LIST_HPP
#define ARX_REVISION_LIST_HPP

#include <list>
#include "Parsed_Name.hpp"

class Revision_List : public std::list<unsigned int>
{
public:
  Parsed_Name name;

  Revision_List(const Parsed_Name &Name)
    : name(Name) {}
  
  Revision_List() {};

  typedef std::list<unsigned int>::iterator iterator;
  typedef std::list<unsigned int>::const_iterator const_iterator;  
};

#endif
