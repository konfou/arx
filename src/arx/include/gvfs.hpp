/* A collection of C++ wrappers around gnome-vfs functions.  This is
   used instead of gnome-vfsmm because gnome-vfsmm requires gtk.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_GVFS_HPP
#define ARX_GVFS_HPP

#include <libgnomevfs/gnome-vfs.h>
#include <string>
#include <iostream>
#include "boost/filesystem/operations.hpp"
#include <list>
#include "boost/archive/text_iarchive.hpp"
#include "boost/archive/text_oarchive.hpp"
#include <sstream>
#include <stdexcept>
#include "Command_Info.hpp"

#include <unistd.h>

/* Wrap everything into a namespace */

namespace gvfs
{
  /* A simple class to redirect stderr and then put it back again,
     because libxml2 inside gnome-vfs2 prints out spurious warnings
     about the 'DAV:' namespace. */

  class Capture_Error
  {
    int stderr_fd;
  public:
    Capture_Error(): stderr_fd(0)
    {
      if(Command_Info::verbosity<=report)
        {
          stderr_fd=dup(2);
          freopen("/dev/null","w",stderr);
        }
    }
    ~Capture_Error()
    {
      if(Command_Info::verbosity<=report)
        {
          fflush(stderr);
          dup2(stderr_fd,2);
          close(stderr_fd);
        }
    }
  };

  /* A simple wrapper around string that defines some convenience
     operations. */
  class uri
  {
  private:
    std::string location;
    void canonicalize()
    {
      /* If there is no scheme (e.g. file:, sftp:), then make it
         canonical and add file:// */
      
      if(!location.empty()
         && find(location.begin(),location.end(),':')==location.end())
        {
          location="file://"+(boost::filesystem::system_complete(location)).string();
        }
    }
  public:
    uri(const std::string &Location): location(Location)
    {
      canonicalize();
    }
    uri(const char *Location): location(Location)
    {
      canonicalize();
    }
    uri(): location("") {}
    std::string string() const
    {
      return location;
    }
    bool empty() const
    {
      return location.empty();
    }
    std::string escaped_string() const
    {
#ifdef ARX_GNOMEVFS_MUST_ESCAPE_HTTP
      if(location.substr(0,4)=="http")
        {
          /* Escape the string for http.  We can't use it for
             everything, because other servers don't do the
             conversion.  We only do this for older versions of
             gnome-vfs-2.0, because newer version do the escaping for
             you, and break if you escape it yourself.*/

          char *escaped(gnome_vfs_escape_host_and_path_string
                        (location.c_str()));
          std::string result(escaped);
          g_free(escaped);
          return result;
          
//           return std::string(gnome_vfs_escape_host_and_path_string
//                              (location.c_str()));
        }
#endif
      return location;
    }
    uri operator /(const std::string &tail) const
    {
      return uri(location + "/" + tail);
    }
    ~uri() throw () {}
  };

  /* A simple exception class to hold what went wrong. */

  class exception: public std::runtime_error
  {
  public:
    GnomeVFSResult error_code;
    uri uri1, uri2;
    exception(const std::string &What, const GnomeVFSResult &Error_Code,
              const uri Uri1, const uri Uri2=gvfs::uri("")):
      std::runtime_error(What), error_code(Error_Code), uri1(Uri1),
      uri2(Uri2){}
    exception(const std::string &What, const GnomeVFSResult &Error_Code,
              const std::string Uri1="", const std::string Uri2=""):
      std::runtime_error(What), error_code(Error_Code), uri1(Uri1),
      uri2(Uri2) {}
  };


  void fill_directory_list(std::list<std::string> &dir_list,
                           const uri &location);

  /* Initialize gnome-vfs */
  inline void Init()
  {
    if(gnome_vfs_init()!=TRUE)
      throw exception("Gnome VFS Init",GNOME_VFS_ERROR_GENERIC);
  }

  /* Some helper functions (exists, is_directory, make_directory) */

  inline bool exists(const uri &Uri)
  {
    Capture_Error capture;
    bool result;
    GnomeVFSURI *temp_uri=gnome_vfs_uri_new(Uri.escaped_string().c_str());

    /* I'm not sure whether this is the equivalent of stat or lstat. */

    result=(gnome_vfs_uri_exists(temp_uri)==TRUE);
    gnome_vfs_uri_unref(temp_uri);
    return result;
  }

  inline bool is_directory(const uri &Uri)
  {
    Capture_Error capture;

    if(Uri.string().substr(0,5)=="http:" && exists(Uri/ ".listing"))
      return true;

    GnomeVFSFileInfo *file_info=
      (GnomeVFSFileInfo*)(g_malloc(sizeof(GnomeVFSFileInfo)));

    GnomeVFSResult error_code=
      gnome_vfs_get_file_info(Uri.escaped_string().c_str(),file_info,
                              GNOME_VFS_FILE_INFO_DEFAULT);

    if(error_code!=GNOME_VFS_OK)
      throw exception("Get File Info",error_code,Uri);

    bool result=(file_info->type & GNOME_VFS_FILE_TYPE_DIRECTORY);
    g_free(file_info);
    return result;
  }

  inline bool is_link(const uri &Uri)
  {
    Capture_Error capture;
    GnomeVFSFileInfo *file_info=
      (GnomeVFSFileInfo*)(g_malloc(sizeof(GnomeVFSFileInfo)));
    GnomeVFSResult error_code=
      gnome_vfs_get_file_info(Uri.escaped_string().c_str(),file_info,
                              GNOME_VFS_FILE_INFO_DEFAULT);
    if(error_code!=GNOME_VFS_OK)
      throw exception("Get File Info",error_code,Uri);

    bool result=file_info->type==GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK;
    g_free(file_info);
    return result;

//     return GNOME_VFS_FILE_INFO_SYMLINK(&file_info);
  }

  inline void make_directory(const uri &Uri,
                             const unsigned int permissions=0777)
  {
    Capture_Error capture;
    GnomeVFSResult error_code=
      gnome_vfs_make_directory(Uri.escaped_string().c_str(),permissions);
    if(error_code!=GNOME_VFS_OK)
      throw exception("Make Directory",error_code,Uri);
  }

  void copy(const uri &source, const uri &target);

  inline void rename(const uri &source, const uri &target)
  {
    Capture_Error capture;
    GnomeVFSResult error_code=
      gnome_vfs_move(source.escaped_string().c_str(),
                     target.escaped_string().c_str(),TRUE);
    if(error_code!=GNOME_VFS_OK)
      throw exception("Renaming a path",error_code,source,target);
  }
    
  inline void remove(const uri &target)
  {
    if(gvfs::exists(target))
      {
        Capture_Error capture;
        GnomeVFSResult error_code;
        if(!gvfs::is_link(target) && gvfs::is_directory(target))
          error_code=
            gnome_vfs_remove_directory(target.escaped_string().c_str());
        else
          error_code=gnome_vfs_unlink(target.escaped_string().c_str());
        if(error_code!=GNOME_VFS_OK)
          throw exception("Deleting a path",error_code,target);
      }
  }

  inline void remove_all(const gvfs::uri &target)
  {
    if(gvfs::exists(target))
      {
        Capture_Error capture;
        if(!gvfs::is_link(target) && gvfs::is_directory(target))
          {
            std::list<std::string> dir_list;
            gvfs::fill_directory_list(dir_list,target);
            for(std::list<std::string>::iterator i=dir_list.begin();
                i!=dir_list.end(); ++i)
              if(*i!="." && *i!="..")
                gvfs::remove_all(target/(*i));
          }
        gvfs::remove(target);
      }
  }

  /* A simple file handle class for writing files. */

  class out_file
  {
    uri file_name;
    GnomeVFSHandle *handle;
    
  public:
    out_file(const uri &Uri): file_name(Uri)
    {
      Capture_Error capture;
      GnomeVFSResult error_code=
        gnome_vfs_create(&handle,file_name.escaped_string().c_str(),
                         GNOME_VFS_OPEN_WRITE,FALSE,0777);
      if(error_code!=GNOME_VFS_OK)
        throw exception("Create a File",error_code,file_name);
    }

    void write(const std::string &s)
    {
      Capture_Error capture;
      GnomeVFSFileSize bytes_written;
      GnomeVFSResult error_code=
        gnome_vfs_write(handle,s.c_str(),
                        static_cast<GnomeVFSFileSize>(s.size()),
                        &bytes_written);
      if(error_code!=GNOME_VFS_OK)
        throw exception("Writing a file",error_code,file_name);
    }

    ~out_file()
    {
      Capture_Error capture;
      if(gnome_vfs_close(handle)!=GNOME_VFS_OK)
        std::cerr << "WARNING: Can't close the output file "
                  << file_name.escaped_string() << std::endl;
    }
  };

  /* A simple file handle class for reading files. */

  class in_file
  {
    uri file_name;
    GnomeVFSHandle *handle;
    bool open;

  public:
    in_file(const uri &Uri): file_name(Uri), open(true)
    {
      Capture_Error capture;
      GnomeVFSResult error_code=
        gnome_vfs_open(&handle,file_name.escaped_string().c_str(),
                       GNOME_VFS_OPEN_READ);
      if(error_code!=GNOME_VFS_OK)
        throw exception("Open a File for Reading",error_code,file_name);
    }
    
    std::string read_line()
    {
      std::string read;
      if(open)
        {
          Capture_Error capture;
          char c('a');
          GnomeVFSFileSize bytes_read;
          
          while(c!='\n')
            {
              GnomeVFSResult result=gnome_vfs_read(handle,&c,1,&bytes_read);

              switch(result)
                {
                case GNOME_VFS_OK:
                  if(c!='\n' && c!='\r')
                    read+=c;
                  break;
                case GNOME_VFS_ERROR_EOF:
                  c='\n';
                  open=false;
                  break;
                default:
                  throw exception("Reading a File",result,file_name);
                  break;
                }
            }
        }
      return read;
    }

    bool operator()() const
    {
      return open;
    }

    ~in_file()
    {
      Capture_Error capture;
      if(gnome_vfs_close(handle)!=GNOME_VFS_OK)
        std::cerr << "WARNING: Can't close the input file "
                  << file_name.escaped_string() << std::endl;
    }
  };

  std::string read_file_into_string(const uri &location);

  inline void write_archive(const uri &location, const std::string &s)
  {
    std::stringstream temp;
    boost::archive::text_oarchive a(temp);
    a << s;
    out_file out(location);
    out.write(temp.str());
  }
  inline std::string read_archive(const uri &location)
  {
    std::string s;
    std::stringstream temp(read_file_into_string(location));
    boost::archive::text_iarchive a(temp);
    a >> s;
    return s;
  }
}
  
#endif
