/* Compare the properties of original and modified versions of a path,
   and write the original and modified properties in the supplied
   paths.
   
  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_WRITE_PROPERTY_DIFFS_HPP
#define ARX_WRITE_PROPERTY_DIFFS_HPP

#include "boost/filesystem/operations.hpp"
#include <map>
#include <string>

void write_property_diffs(const boost::filesystem::path &orig_filename,
                          const boost::filesystem::path &mod_filename,
                          const std::map<std::string,std::string> &orig,
                          const std::map<std::string,std::string> &mod,
                          const boost::filesystem::path &orig_path,
                          const boost::filesystem::path &mod_path);

#endif
