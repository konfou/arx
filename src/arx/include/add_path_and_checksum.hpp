/* Add a specified inventory id to a file.

   Copyright (C) 2004 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_ADD_PATH_AND_CHECKSUMS_HPP
#define ARX_ADD_PATH_AND_CHECKSUMS_HPP

#include "boost/filesystem/operations.hpp"
#include <string>
#include "sha256.hpp"
#include "file_attributes.hpp"

extern void add_path_and_checksum(const boost::filesystem::path &dir,
                                  const file_attributes &f,
                                  const sha256 &checksum);
#endif
