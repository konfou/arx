/* Move an inventory id from one place to another, marking it in
   the ,,changes file.  It doesn't check that the file is in the
   original manifest, so it is a little unsafe.

  Copyright (C) 2004 Walter Landry
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_UNSAFE_MOVE_INVENTORY_ID_HPP
#define ARX_UNSAFE_MOVE_INVENTORY_ID_HPP

#include "boost/filesystem/operations.hpp"

void unsafe_move_inventory_id(const boost::filesystem::path &root,
                              const boost::filesystem::path &source,
                              const boost::filesystem::path &destination,
                              const std::string &inventory_id);

void unsafe_move_inventory_id(const boost::filesystem::path &root,
                              const boost::filesystem::path &source,
                              const boost::filesystem::path &destination,
                              const std::string &inventory_id,
                              const bool is_dir);

#endif
