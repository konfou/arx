/* Find all of the new and removed patch logs in the tree.  If there
   are any removed patches, then flag it as an error unless
   --out-of-date-ok is true.

  Copyright (C) 2004 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_FIND_NEW_AND_REMOVED_PATCHES_HPP
#define ARX_FIND_NEW_AND_REMOVED_PATCHES_HPP


#include "arx_error.hpp"
#include "boost/filesystem/operations.hpp"
#include <map>
#include <string>

extern void find_new_and_removed_patches
(const boost::filesystem::path &patch_dir,
 std::map<std::string,std::list<std::string> > &header_lists,
 const bool &out_of_date_ok);

#endif
