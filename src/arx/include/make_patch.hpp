/* Create a patch between two source trees.

  Copyright (C) 2001, 2002 Tom Lord
  Copyright (C) 2002, 2003 Walter Landry and the Regents
                           of the University of California
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_MAKE_PATCH_HPP
#define ARX_MAKE_PATCH_HPP

#include "Path_List.hpp"
#include "Checksums.hpp"

extern bool make_patch(const boost::filesystem::path &original,
                       const boost::filesystem::path &modified,
                       const boost::filesystem::path &destination,
                       const Path_List &path_list);

extern bool make_patch(const boost::filesystem::path &original,
                       const boost::filesystem::path &modified,
                       const boost::filesystem::path &destination,
                       const Path_List &path_list,
                       Checksums &orig_only, Checksums &mod_only,
                       Checksums &patched,
                       std::list<std::pair<std::pair<file_attributes,sha256>,
                       std::pair<file_attributes,sha256> > > &in_both);

#endif
