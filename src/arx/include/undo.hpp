/* Temporarily undo changes in working directory in order to do
   another checkin on the clean revision first

   Copyright (C) 2001, 2002 Tom Lord
   Copyright (C) 2002 Alexander Deruwe
   Copyright (C) 2003 Walter Landry and the Regents
                      of the University of California
   Copyright (C) 2005 Walter Landry
   
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA */

#ifndef ARX_UNDO_HPP
#define ARX_UNDO_HPP

#include "boost/filesystem/operations.hpp"
#include "Path_List.hpp"

void undo(const boost::filesystem::path &tree_directory,
          const boost::filesystem::path &undo_dir,
          const Path_List path_list=Path_List());

#endif
