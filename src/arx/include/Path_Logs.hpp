/* A simple class that records when a patch modifies a path

   Copyright (C) 2005 Walter Landry

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA */

#ifndef ARX_FILE_LOGS_HPP
#define ARX_FILE_LOGS_HPP

#include "Parsed_Name.hpp"
#include <set>

class Path_Logs
{
  std::set<Parsed_Name> &names;
public:
  Path_Logs(std::set<Parsed_Name> &Names): names(Names) {}
  
  void add(const Parsed_Name &name)
  {
    names.insert(name);
  }
  void add(const std::string &name) {}
  void patch(const std::string &current_name, const Parsed_Name &name)
  {
    names.insert(name);
  }
  void patch(const std::string &current_name, const std::string &name,
             const boost::filesystem::path &patch_dir) {}
  void rename(const Parsed_Name &name)
  {
    names.insert(name);
  }
  void rename(const std::string &name) {}
  void finish() {}
};

#endif
