#!/bin/sh

# This script requires you to have already set up a default archive
# and id.  It will create a category called hello in your default
# archive.  It will be removed at the end, but if something breaks,
# you can manually clean up with
#
#   rm -rf hello* foo*
#   arx archives -d foo

# Copyright 2002, 2003 Walter Landry and the Regents
#                          of the University of California
# Copyright (C) 2004, 2005 Walter Landry

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

arx make-archive --key "" foo foo
arx make-archive --mirror foo foo-mirror

mkdir hello
cd hello
echo "echo Hello, World" > Hello
arx init foo/hello.main.1.0
arx ignore "^\\+.*$"
arx inventory -U -F | grep -v "Unrecognized"
arx property --set "I am the very model
of a modern major general" foo Hello
arx property --set false "arx:other-read" Hello
arx property --set true "arx:other-read" Hello
arx property --unset "arx:other-read" Hello
arx commit -s "initial"

arx ignore --delete
echo "echo Hello World" > Hello
mkdir foo
cd foo
touch bar
cd ..
arx add -R foo
touch foobar
arx add foobar
arx tree-lint --strict
arx undo
arx redo
arx file-undo Hello
mv ,,Hello--undo-0 Hello
arx file-diff Hello > /dev/null
ln -s link_target1 link1
echo "foo" > link2
arx add link1 link2
arx commit -s "Removed a comma" --author "Foo Bar <foo@bar>" --date "Yesterday"

# Now check for paths with funky names

touch "foo
bar"
touch "foo bar"
touch "foo\tbar"
touch "foo

bar

baz"
arx add -q foo*
arx property --set false "arx:other-read" Hello
arx commit -s "funky names"

# Testing integrity checks
echo "echo Hello World.  Goodbye" > _arx/++cache/foo/hello/main/1/0/,2/Hello
rm -f _arx/++cache/foo/hello/main/1/0/,2/link1
ln -s bad_target1 _arx/++cache/foo/hello/main/1/0/,2/link1
rm -f _arx/++cache/foo/hello/main/1/0/,2/link2
ln -s bad_target2 _arx/++cache/foo/hello/main/1/0/,2/link2


arx diff
echo "echo Hello World.  Goodbye" > Hello
if test -z "`arx diff 2>&1 > /dev/null`"; then
    echo "First diff did not fail"
    exit 1
fi
rm -rf ,,diff*

echo "echo Hello World" > _arx/++cache/foo/hello/main/1/0/,2/Hello
rm -f link1
ln -s bad_target1 link1
if test -z "`arx diff 2>&1 > /dev/null`"; then
    echo "Second diff did not fail"
    exit 2
fi
rm -rf ,,diff*
rm -f link1
rm -f _arx/++cache/foo/hello/main/1/0/,2/link1
ln -s link_target1 _arx/++cache/foo/hello/main/1/0/,2/link1
ln -s link_target1 link1
rm -f link2
ln -s bad_target2 link2
if test -z "`arx diff 2>&1 > /dev/null`"; then
    echo "Third diff did not fail"
    exit 3
fi
rm -rf ,,diff*

rm -f _arx/++cache/foo/hello/main/1/0/,2/link2
echo "foo" > _arx/++cache/foo/hello/main/1/0/,2/link2
arx diff > /dev/null

echo "aaa" > foobar
arx diff foo
echo "echo Hello World.  Goodbye" > Hello
arx fork foo/hello.main.1.1
arx diff foo
arx commit -s "Added goodbye" Hello

# Now make the german branch

cd ..
arx get --no-edit --link-tree foo/hello.main.1.0 hello.german
arx fork --dir hello.german foo/hello.german.1.0 
cd hello.german
arx commit -s "German version"

arx edit Hello
echo "Guten Tag" > Hello
cat > path_file <<EOF
Hello
EOF
arx undo
arx redo
arx commit -s "Translated" --paths-file path_file

arx fork foo/hello.french.1.0

arx edit Hello
echo "Bonjour" > Hello
arx commit -s "Translated" --paths-file path_file

cd ..

# Remove some logs

cd hello.german
arx tree-cache | grep -v "hello.french.1.0,0"
arx tree-cache --add foo/hello.german.1.0,1
arx tree-cache | grep -v "hello.french.1.0,0" | grep -v hello.german.1.0,1
arx tree-cache --delete foo/hello.french.1.0,0
arx tree-cache | grep -v hello.german.1.0,1

arx history --delete foo/hello.main.1.0
arx history | grep -v hello.german.1.0 | grep -v hello.french.1.0
arx history --delete foo/hello.french.1.0
arx history | grep -v hello.german.1.0
arx history --delete foo/hello.german.1.0
arx history
cd ..

cd hello
arx history --delete foo/hello.main.1.0
arx history | grep -v hello.main.1.1
arx history --delete foo/hello.main.1.1
arx history
cd ..

# Try a tag
arx tag foo/hello.join.1.0 foo/hello.german.1.0,1
arx tag --author "argle bargle" foo/hello.tag.1.0 foo/hello.german.1.0,0 foo/hello.join.1.0 join
arx tag --date "tomorrow" foo/hello.tag.1.0 foo/hello.german.1.0,0 foo/hello.tag.1.0,0 tag foo/hello.join.1.0 join
arx tag --author "foo bargle" --date "yesteryear" foo/hello.tag.1.0 foo/hello.german.1.0,0 foo/hello.tag.1.0,1 tag foo/hello.join.1.0 join

arx get foo/hello.tag.1.0 hello.tag1

arx tag foo/hello.tag.1.0 foo/hello.german.1.0,0
arx tag foo/hello.tag.1.0 foo/hello.german.1.0,0
arx tag foo/hello.tag.1.0 foo/hello.german.1.0,0

rm -rf hello.tag1

arx get foo/hello.join.1.0 hello.tag1
arx get foo/hello.tag.1.0 hello.tag2

arx export foo/hello.main.1.0
arx export --tarball foo/hello.main.1.0,0 hello-tarball
arx export --changelog foo/hello.german.1.0 hello-changelog

arx mirror -q foo/ file:///home/boo/arx/tests/foo file:///home/boo/arx/tests/foo-mirror

arx browse foo/ > /dev/null

# Try out export

arx export foo/hello.tag.1.0
arx export foo/hello.tag.1.0 hello.export
arx export foo/hello.german.1.0,0
mkdir hello.export_dir
arx export foo/hello.main.1.0,1 different hello.export_dir
arx export --tarball foo/hello.main.1.0,1 different hello.export_dir

# Finally, try some funky names for revisions

mkdir hello-funky
cd hello-funky
echo "echo Hello, World" > Hello
arx init -q "foo/hello.ma

in.1.0"
arx ignore "^\\+.*$"
arx inventory -U -F | grep -v "Unrecognized"
arx commit -s "initial"

arx ignore --delete
echo "echo Hello World" > Hello
mkdir foo
arx add foo
cd foo
touch bar
arx add bar
cd ..
touch foobar
arx add foobar
arx tree-lint --strict
arx undo
arx redo
arx file-undo Hello
mv ,,Hello--undo-0 Hello
arx file-diff Hello > /dev/null
ln -s link_target1 link1
echo "foo" > link2
arx add link1 link2
arx commit -s "Removed a comma"

cd ..

arx get "foo/hello.ma

in.1.0"

# Check for whether reparenting works

arx get foo/hello.main.1.0 hello-reparent
cd hello-reparent
touch foo/reparent
arx add foo/reparent
arx property --set true arx:user-read foo
arx commit -s "reparent"
cd ..
arx get foo/hello.main.1.0,2 hello-reparent2
cd hello-reparent2
arx mv foo reparent_parent
arx replay
if ! test -f reparent_parent/reparent; then
  echo "Reparenting does not work"
  exit 1
fi

cd ..
# More complicated reparenting, without the hints
arx get foo/hello.main.1.0 hello-reparent3
cd hello-reparent3
touch foo/rp3
mkdir foo/rp4
mkdir foo/rp4/rp5
arx add foo/rp3 foo/rp4 foo/rp4/rp5
arx commit -s "reparent"
cd ..
arx get foo/hello.main.1.0,3 hello-reparent4
cd hello-reparent4
arx mv foo reparent-parent
arx replay

if ! test -f reparent-parent/rp3; then
  echo "Reparenting a file with hinting does not work"
  exit 1
fi
if ! test -d reparent-parent/rp4; then
  echo "Reparenting a directory does not work"
  exit 1
fi
if ! test -d reparent-parent/rp4/rp5; then
  echo "Reparenting a subdirectory does not work"
  exit 1
fi

cd ..


# Check for whether just renaming, rather than moving, a file works

arx get foo/hello.main.1.0 hello-rename
cd hello-rename
arx fork foo/hello.rename
arx mv foo/rp3 foo/rename
arx mv foo/rp4 foo/rename-dir
arx commit -s "rename"
cd ..
arx get foo/hello.main.1.0 hello-rename2
cd hello-rename2
arx mv foo/rp3 foo/rp4/
mkdir rename-parent
arx add rename-parent
arx mv foo/rp4 rename-parent
arx replay foo/hello.rename
if ! test -d rename-parent/rename-dir; then
  echo "Renaming without moving a directory does not work"
  exit 1
fi

cd ..

# Clean up

arx get foo/hello.main.1.0,2 hello-main
arx delete-revision foo/hello.main.1.0,2

arx get foo/hello.main.1.0,1 hello.forked
arx fork --dir hello.forked foo/hello.main.1.0
arx tag foo/hello.main.1.0 foo/hello.main.1.0,1

rm -rf hello*

arx delete-branch --force foo/hello
arx delete-branch --force file:///home/boo/arx/tests/foo-mirror/hello
arx archives -d foo file:///home/boo/arx/tests/foo-mirror
arx archives -d foo
arx archives -a file:///home/boo/arx/tests/foo
arx archives -d foo
rm -rf foo/ foo-mirror/
