#!/bin/sh

# This script requires you to have already set up a default archive
# and id.  It will create a category called hello in your default
# archive.  It will be removed at the end, but if something breaks
# along the way, you can manually clean up with
#
#   rm -rf hello* foo
#   arx archives -d foo
#
# Copyright (C) 2002, 2003 Walter Landry and the Regents
#                          of the University of California
# Copyright (C) 2004, 2005 Walter Landry
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 dated June, 1991.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

arx make-archive --key "" foo foo

mkdir hello
cd hello
echo "echo Hello, World" > Hello
arx init foo/hello.main.1.0
arx commit -s initial

# Make the german branch

arx get --no-edit --link-tree foo/hello.main.1.0 ../hello.german
arx fork --dir ../hello.german foo/hello.german.1.0
cd ../hello.german
arx commit -s "Initial branch from main"
arx diff --revision foo/hello.main.1.0,0 Hello

# Merge the original hello directory with hello.german

arx merge --dir ../hello foo/hello.german.1.0
cd ../hello
arx commit -s "Initial merge from german" --branch foo/hello.main.1.0

# Modify and commit the main branch

cd ../hello
echo "echo Hello World.  Goodbye" > Hello
arx mv Hello HHello
echo "Goodbye" > bye
arx add bye
arx commit -s "Added goodbye"

# Exercise replay

arx get foo/hello.main.1.0,1 ../hello.germain
arx replay --dir ../hello.germain foo/hello.main.1.0

arx get --no-edit --link-tree foo/hello.main.1.0,0 ../hello.foo
arx replay --dir ../hello.foo --exact foo/hello.main.1.0,2
arx replay --dir ../hello.foo --exact foo/hello.german.1.0,0
rm -rf ../hello.foo

# Check for whether readding a file will not cause conflicts

arx get --no-edit --link-tree foo/hello.german.1.0 ../hello.foo
arx replay --dir ../hello.foo --exact foo/hello.main.1.0,1
rm -rf ../hello.foo

# Use patch lists

cat > patch_file <<EOF
foo/hello.main.1.0,1
foo/hello.main.1.0,2
foo/hello.german.1.0,0
EOF

arx get --no-edit --link-tree foo/hello.main.1.0,0 ../hello.foo
arx replay -q --dir ../hello.foo --list patch_file
rm -rf ../hello.foo

arx get --no-edit --link-tree foo/hello.main.1.0,0 ../hello.foo
arx replay --dir ../hello.foo foo/hello.main.1.0
rm -rf ../hello.foo

# Update the german branch from the main branch

cp -al ../hello.german ../hello.foo
arx merge --dir ../hello.foo foo/hello.main.1.0 
arx merge --dir ../hello.german foo/hello.main.1.0

diff -r ../hello.foo ../hello.german

cd ../hello.german
arx commit -s "update from main"

# Modify the german branch

arx edit HHello
echo "Guten Tag" > HHello
arx commit -s "tranlated Hello"

# Update the main branch from the german branch

arx get foo/hello.german.1.0 ../hello.automerge
arx merge --dir ../hello.automerge foo/hello.main.1.0 
cp -al ../hello ../hello.merge
arx merge --dir ../hello.merge foo/hello.german.1.0
arx merge --dir ../hello foo/hello.german.1.0

cd ..

# A simple update
arx get foo/hello.main.1.0,0 hello.update
arx merge -u --dir hello.update
if test -z `arx tree-cache --dir hello.update | grep hello.main.1.0,2`; then
  echo "merge -u did not update the cache"
  exit 1
fi

# A switch
arx get foo/hello.main.1.0,0 hello.switch
arx merge -u --dir hello.switch foo/hello.german.1.0
if test -z `arx tree-cache --dir hello.switch | grep hello.german.1.0,2`; then
  echo "merge -u did not update the cache during a switch"
  exit 1
fi

# Delete a revision
arx delete-revision foo/hello.main.1.0,2
arx get --no-edit --link-tree foo/hello.main.1.0,1 hello_del
arx fork --dir hello_del foo/hello.main.1.0
cd hello_del
arx commit -s "skipped"

cd ..

rm -rf hello*
arx delete-branch --force foo/hello
arx archives -d foo
rm -rf foo/
