#!/bin/sh

# Make sure that arx-move handles all of the cases correctly.

# First check things if they are not in a project tree

# Copyright 2003 Walter Landry

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

mkdir hello
cd hello
touch bar baz bax
mkdir ddd
arx mv bar gag
arx mv ba* ddd
arx mv gag ddd/
arx mv ddd/* .
mkdir eee
arx mv gag bar
arx mv ba* eee
arx mv eee/* ddd/
cd ddd
arx mv * ..
arx mv ../ba* .
arx mv * ../eee
arx mv ../eee/bar gag
cd ..
arx mv ddd/gag bar
arx mv eee/* .

# Then check things if they are in a project tree, including moving
# between trees

arx init --no-add foo/hello.main.1_0
arx add ba*

mkdir orig
touch orig/foo orig/bar
arx add orig/foo
arx mv orig mod
arx rm -R -f mod


cd ddd
arx init --no-add foo/hello.ddd.1_0
touch bah
arx add bah

cd ../eee
arx init --no-add foo/hello.eee.1_0
touch baj
arx add baj

cd ..
arx mv bar gag
arx tree-lint
arx mv ba* ddd
arx tree-lint
arx mv gag ddd/
arx tree-lint
arx mv ddd/[a-z]* .
arx tree-lint
arx mv gag bar
arx tree-lint
arx mv ba* eee
arx tree-lint
arx mv eee/[a-z]* ddd/
arx tree-lint
cd ddd
arx tree-lint
arx mv [a-z]* ..
arx tree-lint
arx mv ../ba* .
arx tree-lint
arx mv [a-z]* ../eee
arx tree-lint
arx mv ../eee/bar gag
arx tree-lint
cd ..
arx tree-lint
arx mv ddd/gag bar
arx tree-lint
arx mv eee/[a-z]* .
arx tree-lint

# Cleanup

cd ..
rm -rf hello
